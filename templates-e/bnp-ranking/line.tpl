<div class="row result d-flex justify-content-center {c2r-class}">
	<div class="col-sm-10 col-sm-offset-1">
		<div class="row">
			<div class="col-sm-2">
				<h3>{c2r-i}º</h3>
			</div>
			<div class="col-sm-6 sm-taleft">
				<h3>{c2r-name}</h3>
			</div>
			<div class="col-sm-3 sm-tacenter">
				<h3>{c2r-points}</h3>
			</div>
		</div>
	</div>
</div>
