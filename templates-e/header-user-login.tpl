<header>
	<div class="container-fluid">
		<div class="sm-spacer60"></div>
		<div class="row">
			<div class="col-sm-3 link sm-tacenter">
				<h4>brandnplay.com</h4>
			</div>
			<!-- CLIENT LOGO -->
			<div class="col-sm-4 col-sm-offset-1 client-logo sm-tacenter">
				<img class="sm-block150" src="{c2r-path}/site-assets/images/logo.svg" alt="{c2r-sitename}">
			</div>
			<!-- CLIENT LOGO -->
			<div class="col-sm-3 link sm-taright" style="font-size:2em;">
				<a id="logout" href="#" title="logout"><i class="fa fa-power-off" style="color:#FFF;" aria-hidden="true"></i> <span class="hidden-sm hidden-md hidden-lg">Logout</span></a>
			</div>
		</div>
		<div class="sm-spacer30"></div>
	</div>
</header>
