<div class="overlays-list">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="row">
					<div class="col">
						<img src="{c2r-path}/uploads/overlay_1.png" alt="" class="item">
					</div>
					<div class="col">
						<img src="{c2r-path}/uploads/overlay_2.png" alt="" class="item">
					</div>
					<div class="col">
						<img src="{c2r-path}/uploads/overlay_3.png" alt="" class="item">
					</div>
					<div class="col-12 text-center">
						<div class="spacer sm-30"></div>
						<button class="btn bnp-btn btn-primary overlay-choose">
							<img src="{c2r-path}/uploads/btn-choose.png" alt="">
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
