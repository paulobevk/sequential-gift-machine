<div class="overlay video-system">
	<video id="video" autoplay loop style="display: none;">
		<source src="{c2r-path}/uploads/out.mp4" type='video/mp4; codecs="avc1.42E01E"' />
	</video>
	<canvas width="720" height="1440" id="buffer"></canvas>
	<canvas width="720" height="720" id="output"></canvas>
</div>
