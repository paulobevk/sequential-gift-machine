<div class="col-sm-4">
	<div class="sm-spacer30"></div>
	<!-- MDL -->
	<div class="mdl mdl-printer">
		<div class="mdl-header">
			<div class="icon">
				<i class="fa fa-users fa-5x" aria-hidden="true"></i>
			</div>
			<div class="sm-spacer15"></div>
			<h5 class="sm-tacenter"><strong>User</strong></h5>
		</div>
		<div class="sm-spacer15"></div>
		<!-- MDL BODY -->
		<div class="row sm-tacenter">
			<div class="col-sm-12">
				<div class="tip">
					<div class="sm-spacer15"></div>
					<p>Completed users</p>
					<h3>{c2r-completed}</h3>
					<p>of {c2r-total}</p>
					<div class="sm-spacer15"></div>
				</div>
			</div>
		</div>
		<!-- MDL BODY -->
		<!--
		<div class="sm-spacer15"></div>
		<div class="mdl-footer row">
			<div class="col-sm-12 sm-tacenter">
				<a href="#" class="btn-block modal-action" data-api="printer_reset_paper" data-toggle="modal" data-target="#modal">
					<div class="sm-spacer15"></div>
					Reset Paper Values!
					<div class="sm-spacer15"></div>
				</a>
			</div>
		</div>
		-->
	</div>
	<!-- MDL -->
</div>
