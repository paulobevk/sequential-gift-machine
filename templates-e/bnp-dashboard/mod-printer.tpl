<div class="col-sm-4">
	<div class="sm-spacer30"></div>
	<!-- MDL -->
	<div class="mdl mdl-printer">
		<div class="mdl-header">
			<div class="icon">
				<i class="fa fa-print fa-5x" aria-hidden="true"></i>
			</div>
			<div class="sm-spacer15"></div>
			<h5 class="sm-tacenter"><strong>Printer</strong></h5>
		</div>
		<div class="sm-spacer15"></div>
		<!-- MDL BODY -->
		<div class="row sm-tacenter">
			<div class="col-sm-6">
				<div class="tip">
					<div class="sm-spacer15"></div>
					<p>Printed</p>
					<h3>{c2r-printer-length-printed}</h3>
					<p>of {c2r-printer-length} m</p>
					<div class="sm-spacer15"></div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="tip">
					<div class="sm-spacer15"></div>
					<p>Printed</p>
					<h3>{c2r-printer-units-printed}</h3>
					<p>of {c2r-printer-units} tickets</p>
					<div class="sm-spacer15"></div>
				</div>
			</div>
		</div>
		<!-- MDL BODY -->
		<div class="sm-spacer15"></div>
		<div class="mdl-footer row">
			<div class="col-sm-12 sm-tacenter">
				<a href="#" class="btn-block modal-action" data-api="printer_reset_paper" data-toggle="modal" data-target="#modal">
					<div class="sm-spacer15"></div>
					Reset Paper Values!
					<div class="sm-spacer15"></div>
				</a>
			</div>
		</div>
	</div>
	<!-- MDL -->
</div>
