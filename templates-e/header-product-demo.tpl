<header class="bnp">
	<div class="container-fluid">
		<div class="sm-spacer60"></div>
		<div class="row">
			<div class="col-sm-3 link sm-tacenter">
				<h4>brandnplay.com</h4>
			</div>
			<!-- CLIENT LOGO -->
			<div class="col-sm-4 col-sm-offset-1 app-logo sm-tacenter">
				<img src="{c2r-path}/site-assets/images/demo/logo-{c2r-a}.svg" alt="{c2r-sitename}">
			</div>
			<!-- CLIENT LOGO -->
			<div class="col-sm-2 col-sm-offset-2 sm-tacenter">
				<a href="{c2r-path}/{c2r-lg}/home-demo/">
					<img src="{c2r-path}/site-assets/images/demo/button-home.png">
				</a>
			</div>
		</div>
		<div class="sm-spacer30"></div>
	</div>
</header>
