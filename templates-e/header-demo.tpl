<header class="bnp">
	<div class="container-fluid">
		<div class="sm-spacer60"></div>
		<div class="row">
			<div class="col-sm-3 link sm-tacenter">
				<h4>brandnplay.com</h4>
			</div>
			<!-- CLIENT LOGO -->
			<div class="col-sm-4 col-sm-offset-1 client-logo sm-tacenter">
				<img src="{c2r-path}/site-assets/images/logo.svg" alt="{c2r-sitename}">
			</div>
			<!-- CLIENT LOGO -->
		</div>
		<div class="sm-spacer30"></div>
	</div>
</header>
