<header class="bnp">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="bgHeaderGame" style="background-image: url({c2r-header-bg})">
					<div class="clock-area">
						<div class="counter"></div>
						<div class="clock-bg" style="background-image:url({c2r-header-clock})"></div>
						<div class="clockBarMain">
							<div class="clockBar"></div>
						</div>
						<div class="time sm-tacenter">TEMPO</div>
					</div>
					<div class="points-area">
						<div class="points sm-tacenter">0</div>
						<div class="textPoints">PONTUAÇÃO</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
