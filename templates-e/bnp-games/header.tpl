<header class="bnp">
	<div class="container-fluid">
		<div class="row bgHeaderGame">
			<div class="sm-spacer60"></div>
			<!-- AVATAR -->
			<div class="col-sm-2 col-sm-offset-1 avatar sm-tacenter">
				<div class="sm-spacer60"></div>
				<div class="sm-spacer15"></div>
				<div class="avatar-container">
					<div class="img" style="background-image: url({c2r-path}/site-assets/images/avatar-default.png)"></div>
				</div>
				<!-- USERNAME -->
				<div class="username"></div>
				<!-- USERNAME -->
			</div>
			<!-- AVATAR -->
			<!-- TIMER -->
			<div class="col-sm-6 sm-tacenter">
				<div class="sm-spacer60"></div>
				<div class="counter"></div>
				<img src="{c2r-path}/site-assets/images/bnp-header/clock.png" alt="{c2r-sitename}" class="clock" >
				<div class="clockBarMain">
					<div class="clockBar"></div>
				</div>
				<div>TEMPO</div>
			</div>
			<!-- TIMER -->
			<!-- POINTS -->
			<div class="col-sm-2 sm-tacenter">
				<div class="sm-spacer60"></div>
				<div class="points">0</div>
				<div class="textPoints">PONTUAÇÃO</div>
			</div>
		</div>
		<div class="sm-spacer60"></div>
	</div>
</header>
