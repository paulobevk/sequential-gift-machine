<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
<div class="row d-flex justify-content-center">
	<div class="col-md-10 tacenter">
		<div class="bgAnswers {c2r-answer-type}">
			<div class="spacer xs-15 sm-15 md-60 lg-120 textAnswers" data-status="{c2r-status}">{c2r-title}</div>
		</div>
	</div>
</div>
<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
