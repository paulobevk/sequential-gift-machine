<div class="carousel-item {c2r-class}">
	<div class="display">
		<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
		<div class="row d-flex justify-content-center">
			<div class="col-sm-8 col-md-8 tacenter">
				<div class="bgNumberQuestion block xs-15 sm-15 md-120 lg-150 sm-tacenter">
					<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
					<h2>{c2r-index}/{c2r-total-question}</h2>
					<div>PERGUNTAS</div>
					<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
					<div class="spacer xs-15 sm-15 md-15 lg-15"></div>
				</div>
			</div>
		</div>
		<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
		<div class="row d-flex justify-content-center">
			<div class="col-md-8">
				<h1 class="tacenter ttupper">{c2r-title}</h1>
			</div>
		</div>
		<div class="spacer xs-15 sm-15 md-15 lg-90"></div>
		{c2r-answer}
	</div>
</div>
