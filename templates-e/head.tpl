<title>{c2r-sitename}</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no , shrink-to-fit=no">

<!-- begin favicon -->
<link rel="shortcut icon" href="{c2r-path-bo}/site-assets/favicon/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="57x57" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="{c2r-path-bo}/site-assets/favicon/apple-touch-icon-180x180.png" />
<!-- end favicon -->

<meta property="og:site_name" content="{c2r-sitename}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{c2r-path}" />
<meta property="og:image" content="{c2r-og-image}" />
<meta property="og:title" content="{c2r-sitename}" />
<meta property="og:description" content="{c2r-og-description}" />

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<meta name="keywords" content="{c2r-keywords}" />
<meta name="description" content="{c2r-description}" />
<meta name="robots" content="index" />
<meta name="author" content="One:Shift at one-shift.com" />
<meta name="author-code" content="Developer" />

<!-- begin jquery -->
<script src="{c2r-libs}/jquery-3.3.1/jquery-3.3.1.min.js" charset="utf-8"></script>
<!-- end jquery -->

<!-- begin bootstrap -->
<script src="{c2r-libs}/popper-1.14.3/popper.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-libs}/bootstrap-4.1.3/css/bootstrap.min.css">
<script src="{c2r-libs}/bootstrap-4.1.3/js/bootstrap.min.js" charset="utf-8"></script>
<!-- end bootstrap -->

<!-- begin fontawesome -->
<link rel="stylesheet" href="{c2r-libs}/fontawesome-5.3.0/css/all.css">
<!-- end fontawesome -->

<link rel="stylesheet" href="{c2r-path}/site-assets/css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="{c2r-path}/site-assets/css/spacers.min.css" type="text/css" media="screen" />

<script type="text/javascript" src="{c2r-path}/site-assets/js/animatescroll.min.js"></script>
<script type="text/javascript" src="{c2r-path}/site-assets/js/cookies.js"></script>
<script type="text/javascript" src="{c2r-path}/site-assets/js/script.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/camanjs/3.3.0/caman.full.min.js"></script>

<script>
	var path = "{c2r-path}";
	var lg = "{c2r-lg}";
</script>

{c2r-analytics}
