<header class="bnp">
	<div class="container-fluid">
		<div class="spacer xs-15 sm-15 md-30 lg-60"></div>
		<div class="row">
			<!-- CLIENT LOGO -->
			<div class="col-sm-4 offset-sm-4 client-logo tacenter">
				<img class="{c2r-client-logo-display}" src="{c2r-client-logo}" alt="{c2r-sitename}">
			</div>
			<!-- EOF CLIENT LOGO -->
		</div>
		<div class="spacer xs-15 sm-15 md-30 lg-60"></div>
	</div>
</header>
