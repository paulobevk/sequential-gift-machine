-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 23-Ago-2019 às 17:29
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bnp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_bike_settings`
--

CREATE TABLE `os_bnp_bike_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_bike_settings`
--

INSERT INTO `os_bnp_bike_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'app-logo', '{c2r-path}/site-assets/images/bnp-bike/app-logo.png ', '2019-03-28 23:39:22', '2019-03-28 23:41:02'),
(2, 'video', '/uploads/30-9/metro.mp4', '2019-08-23 12:47:46', '2019-08-23 12:52:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_config`
--

CREATE TABLE `os_bnp_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_config`
--

INSERT INTO `os_bnp_config` (`id`, `name`, `value`, `sync`, `date`, `date_update`) VALUES
(1, 'project-name', '30-5', 1, '2018-09-13 15:26:10', '2018-09-13 17:04:32'),
(2, 'app-type', 'gift', 1, '2018-09-13 15:26:10', '2019-08-23 15:27:53'),
(3, 'client-logo', 'https://design.firefox.com/product-identity/firefox-focus/firefox-logo-focus.png', 1, '2018-09-13 15:26:10', '2019-02-27 16:00:00'),
(4, 'app-bg', '/uploads/30-9/bg.jpg', 1, '2018-09-13 15:26:10', '2019-08-21 16:03:27'),
(6, 'run', 'bnp-gift-h', 1, '2018-09-13 17:30:46', '2019-08-23 15:30:43'),
(7, 'run-timeout', '3000', 1, '2018-09-13 17:32:44', '2018-09-18 15:37:23'),
(8, 'email-service', '1', 1, '2018-09-13 17:37:59', '2018-09-13 17:38:03'),
(9, 'header-bg', '', 0, '2018-11-20 10:34:10', '2018-11-20 10:34:10'),
(10, 'header-clock', '', 0, '2018-11-20 10:37:55', '2018-11-20 10:37:55'),
(11, 'db-version', '1.0.0', 0, '2019-03-11 18:06:33', '2019-03-11 18:06:33'),
(12, 'time', '120', 0, '2019-08-21 16:05:25', '2019-08-21 16:05:25'),
(13, 'target', '', 0, '2019-08-21 16:05:51', '2019-08-21 16:18:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_gift_settings`
--

CREATE TABLE `os_bnp_gift_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_gift_settings`
--

INSERT INTO `os_bnp_gift_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '.horizontal #machine_1, .horizontal #machine_2, .horizontal #machine_3 {\r\n	border-color: #002b5c;\r\n}\r\n\r\n\r\n.panel-1 h1, .panel-2 h1, .panel-3 h1 {\r\n	color: #002b5c;\r\n}\r\n', '2018-03-07 18:02:47', '2019-03-04 16:52:12'),
(2, 'app-logo', '/site-assets/images/bnp-gift/app-logo.png ', '2018-03-07 18:35:42', '2019-03-04 17:09:50'),
(3, 'prize-init', 'Puxa a alavanca com força!', '2019-03-04 16:25:12', '2019-08-23 15:12:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_gif_settings`
--

CREATE TABLE `os_bnp_gif_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_gif_settings`
--

INSERT INTO `os_bnp_gif_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'overlays', '{c2r-path}/uploads/overlays/ov-gif-1.png, {c2r-path}/uploads/overlays/ov-gif-2.png, {c2r-path}/uploads/overlays/ov-gif-3.png, {c2r-path}/uploads/overlays/ov-gif-4.png', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'snap-width', '720', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'snap-height', '540', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'snap-limit', '5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'snap-interval', '250', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'gif-quality', '50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'app-logo', '{c2r-path}/uploads/app-logo.svg', '2018-02-02 11:20:18', '2018-02-02 11:20:18'),
(9, 'sight', '', '2018-02-02 11:20:18', '2018-02-02 11:20:18'),
(10, 'app-bg', '{c2r-path}/site-assets/images/bnp-gif/bg.png', '2018-02-02 11:21:22', '2018-02-02 11:21:22'),
(11, 'client-logo', '', '2018-02-02 11:21:22', '2018-02-02 11:21:22'),
(12, 'image-width', '1440', '2018-02-02 11:21:49', '2018-02-02 11:21:49'),
(13, 'image-height', '1080', '2018-02-02 11:21:49', '2018-02-02 11:21:49'),
(14, 'button-bg', '{c2r-path}/site-assets/images/bnp-photo/2_Botao_Foto_BG.png', '2018-02-02 11:23:17', '2018-02-02 11:23:17'),
(15, 'step1-button', '{c2r-path}/site-assets/images/bnp-photo/3_Icon.png', '2018-02-02 11:23:17', '2018-02-02 11:23:17'),
(16, 'step2-clock', '{c2r-path}/site-assets/images/bnp-photo/2_clock_icon.png', '2018-02-02 11:26:30', '2018-02-02 11:26:30'),
(17, 'step1-intro', 'Toca no botão<br>e tira a tua foto!', '2018-02-02 11:26:30', '2018-02-02 11:26:30'),
(18, 'step1-button-text', 'Tirar Foto', '2018-02-02 11:27:03', '2018-02-02 11:27:03'),
(19, 'step2-intro', 'Posiciona-te...<br>Aguarda...', '2018-02-02 11:27:03', '2018-02-02 11:27:03'),
(20, 'step3-intro', 'Pronto...<br>Avança, ou repete a foto', '2018-02-02 11:27:32', '2018-02-02 11:27:32'),
(21, 'step3-text-repeat', 'Repetir', '2018-02-02 11:27:32', '2018-02-02 11:27:32'),
(22, 'step3-text-accept', 'Avançar', '2018-02-02 11:28:09', '2018-02-02 11:28:09'),
(23, 'step3-button-repeat', '{c2r-path}/site-assets/images/bnp-photo/3_repetir_icon.png', '2018-02-02 11:28:09', '2018-02-02 11:28:09'),
(24, 'step3-button-accept', '{c2r-path}/site-assets/images/bnp-photo/arrow-next.svg', '2018-02-02 11:28:49', '2018-02-02 11:28:49'),
(25, 'step3-5-intro', 'A processar...', '2018-02-02 11:28:49', '2018-02-02 11:28:49'),
(26, 'step4-check', '{c2r-path}/site-assets/images/bnp-photo/1_visto.png', '2018-02-02 11:30:02', '2018-02-02 11:30:02'),
(27, 'step4-intro', 'Escolhe um efeito <br> para o teu GIF', '2018-02-02 11:30:02', '2018-02-02 11:30:02'),
(28, 'step4-text-accept', 'Avançar', '2018-02-02 11:30:29', '2018-02-02 11:30:29'),
(29, 'step4-button', '{c2r-path}/site-assets/images/bnp-photo/arrow-next.svg', '2018-02-02 11:30:29', '2018-02-02 11:30:29'),
(30, 'target', '/pt/bnp-thanks/', '2018-02-02 12:11:51', '2019-08-22 14:36:41'),
(31, 'app-avatar', '{c2r-path}/site-assets/images/avatar-default.png', '2018-02-02 12:14:47', '2018-02-02 12:14:47'),
(32, 'client-logo', '{c2r-path}/site-assets/images/logo.svg', '2018-02-02 12:14:47', '2018-02-02 12:14:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_memory_settings`
--

CREATE TABLE `os_bnp_memory_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_memory_settings`
--

INSERT INTO `os_bnp_memory_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '/pt/bnp-photo/', '2019-03-05 17:51:18', '2019-03-05 17:51:18'),
(2, 'app-bg', '/uploads/14-3/memory_bg.jpg', '2019-03-07 15:04:49', '2019-03-07 15:05:44'),
(3, 'step1-button', '/uploads/14-3/memory_play_btn.png', '2019-03-07 15:09:28', '2019-03-07 15:12:59'),
(4, 'css', '.memory header {\r\n	color: #000;\r\n}\r\n\r\nheader.bnp .clockBar {\r\n	background-color: #000;\r\n}\r\n\r\nheader.bnp .clockBarMain {\r\n	width: calc(100% - 175px);\r\n}\r\n\r\n.memory .points {\r\n	font-style: normal;\r\n}\r\n\r\n.memory .countDown {\r\n	background-color: transparent;\r\n}\r\n\r\n.memory .countDown div {\r\n	font-style: normal;\r\n	color: #000;\r\n	-webkit-text-stroke: 7px #000;\r\n}\r\n\r\n.memory .step-1 h1 {\r\n	color: #000\r\n}\r\n\r\n.memory .flex-center {\r\n	font-style: normal;\r\n	color: #000;\r\n	-webkit-text-stroke: 7px #000;\r\n	background-color: transparent;\r\n}\r\n\r\n.app-logo {opacity: 0 !important;}', '2019-03-07 15:37:25', '2019-03-07 17:23:35'),
(5, 'card-back', '/uploads/14-3/memory_backcard.png', '2019-03-07 15:40:27', '2019-03-07 15:40:27'),
(6, 'cards', '[\"uploads/14-3/cards/card1.png\", \"uploads/14-3/cards/card2.png\", \"uploads/14-3/cards/card3.png\", \"uploads/14-3/cards/card4.png\", \"uploads/14-3/cards/card5.png\", \"uploads/14-3/cards/card6.png\", \"uploads/14-3/cards/card7.png\", \"uploads/14-3/cards/card8.png\"] ', '2019-03-07 15:40:27', '2019-03-08 12:56:12'),
(7, 'header-bg', '/uploads/14-3/memory_header_bg.png', '2019-03-07 15:50:28', '2019-03-07 15:50:28'),
(8, 'size', '4x4', '2019-03-07 16:01:55', '2019-03-07 16:01:55'),
(9, 'time', '60', '2019-03-07 16:01:55', '2019-03-07 17:16:23'),
(10, 'time-bar', '/uploads/14-3/memory_clock.png', '2019-03-07 16:01:55', '2019-03-07 17:05:09'),
(11, 'app-logo', '/site-assets/images/bnp-memory/app-logo.png', '2019-03-07 16:03:01', '2019-03-08 14:49:45'),
(12, 'header-clock', '/uploads/14-3/memory_clock.png', '2019-03-07 17:04:22', '2019-03-07 17:04:22'),
(13, 'time-convertion', '3', '2019-03-08 14:48:44', '2019-03-08 14:48:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_app`
--

CREATE TABLE `os_bnp_photo_app` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_photo_app`
--

INSERT INTO `os_bnp_photo_app` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(2, 'target', '{c2r-path}/{c2r-lg}/bnp-memory/', '2017-07-21 00:00:00', '2019-01-18 16:40:06'),
(5, 'filters', 'sunrise, hazyDays, love, orangePeel', '2017-07-21 00:00:00', '2017-07-21 00:00:00'),
(8, 'overlays', '/bnp/uploads/overlays/ov-gif-1.png, /bnp/uploads/overlays/ov-gif-2.png, /bnp/uploads/overlays/ov-gif-3.png', '2017-07-21 00:00:00', '2018-05-17 18:28:06'),
(10, 'layoutSight', 'layout2.png', '2017-07-21 00:00:00', '2017-07-21 00:00:00'),
(13, 'css', '', '2017-09-13 00:00:00', '2017-09-13 00:00:00'),
(15, 'app-logo', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'sight', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'app-bg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'app-avatar', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'client-logo', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'image-width', '1440', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'image-height', '1080', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'button-bg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'step1-button', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'step2-clock', '', '2018-01-31 17:45:21', '2018-01-31 17:45:21'),
(25, 'step1-intro', 'Toca no botão<br>e tira a tua foto!', '2018-01-31 17:52:54', '2018-01-31 17:52:54'),
(26, 'step1-button-text', 'Tirar Foto', '2018-01-31 17:52:54', '2018-01-31 17:52:54'),
(27, 'step2-intro', 'Posiciona-te...<br>Aguarda...', '2018-01-31 18:15:33', '2018-01-31 18:15:33'),
(28, 'step3-intro', 'Pronto...<br>Avança, ou repete a foto', '2018-02-01 12:14:31', '2018-02-01 12:14:31'),
(29, 'step3-text-repeat', 'Repetir', '2018-02-01 12:14:31', '2018-02-01 12:14:31'),
(30, 'step3-text-accept', 'Avançar', '2018-02-01 12:16:41', '2018-02-01 12:16:41'),
(31, 'step3-button-repeat', '', '2018-02-01 12:16:41', '2018-02-01 12:16:41'),
(32, 'step3-button-accept', '', '2018-02-01 12:17:27', '2018-02-01 12:17:27'),
(33, 'step3-5-intro', 'A processar..', '2018-02-01 12:34:01', '2018-02-01 12:34:01'),
(34, 'step4-check', '', '2018-02-01 12:34:01', '2018-02-01 12:34:01'),
(35, 'step4-intro', 'Escolhe um efeito<br>para a tua foto', '2018-02-01 12:50:42', '2018-02-01 12:50:42'),
(36, 'step4-text-accept', 'Avançar', '2018-02-01 12:52:13', '2018-02-01 12:52:13'),
(37, 'step4-button', '', '2018-02-01 12:52:13', '2018-02-01 12:52:13'),
(38, 'format', '1:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'mode', 'photo', '2018-12-05 15:53:37', '2018-12-05 15:53:37'),
(40, 'gif_quality', '0.75', '2018-12-05 15:53:37', '2018-12-05 15:53:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_modes`
--

CREATE TABLE `os_bnp_photo_modes` (
  `id` int(11) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delay` text NOT NULL,
  `render_delay` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_photo_modes`
--

INSERT INTO `os_bnp_photo_modes` (`id`, `mode`, `quantity`, `delay`, `render_delay`, `date`, `date_update`) VALUES
(1, 'photo', 1, '1', '1', '2018-10-10 11:40:24', '2018-10-10 11:40:24'),
(2, 'timelapse', 120, '1000/30', '1000/120', '2018-10-10 11:40:24', '2018-10-17 16:59:40'),
(3, 'slowmotion', 120, '1000/40', '1000/12', '2018-10-10 11:40:24', '2018-10-11 16:51:52'),
(4, 'gif', 5, '9000/3', '1000/2', '2018-10-10 11:40:24', '2018-10-10 18:11:22'),
(5, 'boomerang', 60, '1000/30', '1000/90', '2018-10-10 11:40:24', '2018-10-11 14:30:19'),
(6, 'cinemagraph', 120, '1000/30', '1000/30', '2018-10-10 11:40:24', '2018-10-17 16:18:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_settings`
--

CREATE TABLE `os_bnp_photo_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_photo_settings`
--

INSERT INTO `os_bnp_photo_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(2, 'target', '/pt/bnp-bike/', '2017-07-21 00:00:00', '2019-08-21 18:23:27'),
(5, 'filters', 'sunrise, hazyDays, love, orangePeel', '2017-07-21 00:00:00', '2017-07-21 00:00:00'),
(8, 'overlays', '/uploads/30-9/overlay_4.png', '2017-07-21 00:00:00', '2019-08-21 18:23:24'),
(10, 'layoutSight', 'layout2.png', '2017-07-21 00:00:00', '2017-07-21 00:00:00'),
(13, 'css', '', '2017-09-13 00:00:00', '2019-04-11 14:59:04'),
(15, 'app-logo', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'sight', '', '0000-00-00 00:00:00', '2019-04-11 14:59:19'),
(17, 'app-bg', '/uploads/30-9/bg.jpg', '0000-00-00 00:00:00', '2019-08-21 16:52:39'),
(18, 'app-avatar', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'client-logo', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'image-width', '1440', '0000-00-00 00:00:00', '2019-03-05 18:26:14'),
(21, 'image-height', '1080', '0000-00-00 00:00:00', '2019-03-05 18:26:14'),
(22, 'button-bg', '', '0000-00-00 00:00:00', '2019-04-11 14:59:25'),
(23, 'step1-button', '', '0000-00-00 00:00:00', '2019-04-11 14:59:51'),
(24, 'step2-clock', '', '2018-01-31 17:45:21', '2019-04-11 14:59:49'),
(25, 'step1-intro', 'Toca no botão<br>e tira a tua foto!', '2018-01-31 17:52:54', '2018-01-31 17:52:54'),
(26, 'step1-button-text', 'Tirar Foto', '2018-01-31 17:52:54', '2018-01-31 17:52:54'),
(27, 'step2-intro', 'Posiciona-te...<br>Aguarda...', '2018-01-31 18:15:33', '2018-01-31 18:15:33'),
(28, 'step3-intro', 'Pronto...<br>Avança, ou repete a foto', '2018-02-01 12:14:31', '2018-02-01 12:14:31'),
(29, 'step3-text-repeat', 'Repetir', '2018-02-01 12:14:31', '2018-02-01 12:14:31'),
(30, 'step3-text-accept', 'Avançar', '2018-02-01 12:16:41', '2018-02-01 12:16:41'),
(31, 'step3-button-repeat', '', '2018-02-01 12:16:41', '2019-04-11 14:59:32'),
(32, 'step3-button-accept', '', '2018-02-01 12:17:27', '2019-04-11 14:59:37'),
(33, 'step3-5-intro', 'A processar..', '2018-02-01 12:34:01', '2018-02-01 12:34:01'),
(34, 'step4-check', '', '2018-02-01 12:34:01', '2018-02-01 12:34:01'),
(35, 'step4-intro', 'Escolhe um efeito<br>para a tua foto', '2018-02-01 12:50:42', '2018-02-01 12:50:42'),
(36, 'step4-text-accept', 'Avançar', '2018-02-01 12:52:13', '2018-02-01 12:52:13'),
(37, 'step4-button', '', '2018-02-01 12:52:13', '2019-04-11 15:00:47'),
(38, 'format', '1:1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'mode', 'photo', '2018-12-05 15:53:37', '2018-12-05 15:53:37'),
(40, 'gif_quality', '0.75', '2018-12-05 15:53:37', '2018-12-05 15:53:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_printer_settings`
--

CREATE TABLE `os_bnp_printer_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_printer_settings`
--

INSERT INTO `os_bnp_printer_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'photo_status', '0', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(2, 'template', '<page format=\"80x150\" orientation=\"P\" backcolor=\"#FFFFFF\" style=\"font-weight:bold; font: arial; text-align: center; padding: 0; margin: 0; text-align:center;\">\r\n	<table width=\"90%\" align=\"center\" >\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center;\">\r\n				<img src=\"{c2r-logo}\" width=\"85%\">\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center; font-size: 16px;\">\r\n				Visita a Ilha de Natal no piso zero às Sextas, Sábados ou Domingos (das 11h às 21h) e troca este talão por um brinde de natal!\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center; font-size: 12px;\">\r\n				Oferta válida para crianças até aos 14 anos e limitada ao stock existente.\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</page>', '2017-11-28 18:16:57', '2017-11-28 18:16:57'),
(3, 'logo', 'http://192.168.2.61/brand-n-play/u-files/printed-logo.png', '2017-11-28 18:18:13', '2017-11-28 18:18:13'),
(4, 'width', '80', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(5, 'height', '150', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(6, 'orientation', 'P', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(7, 'paper_length', '50000', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(8, 'paper_length_per_print', '96', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(9, 'paper_length_warning', '1', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(10, 'paper_length_email', 'carloss@evoke.pt', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(11, 'paper_length_printed', '5517', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(12, 'paper_length_limit', '45600', '2017-11-29 17:52:48', '2017-11-29 17:52:48'),
(13, 'paper_length_warning_message', '<!doctype html>\n<html>\n	<head>\n		<meta name=\"viewport\" content=\"width=device-width\">\n		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n		<title>Brand n Play</title>\n		<style>\n			/* GLOBAL */\n			* {\n				font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n				font-size: 100%;\n				line-height: 1.6em;\n				margin: 0;\n				padding: 0;\n			}\n\n			img {\n				max-width: 600px;\n				width: 100%;\n			}\n\n			body {\n				-webkit-font-smoothing: antialiased;\n				height: 100%;\n				-webkit-text-size-adjust: none;\n				width: 100% !important;\n			}\n\n\n			/* ELEMENTS */\n			a {\n				color: #348eda;\n			}\n\n			.btn-primary {\n				Margin-bottom: 10px;\n				width: auto !important;\n			}\n\n			.btn-primary td {\n				background-color: #348eda;\n				border-radius: 25px;\n				font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n				font-size: 14px;\n				text-align: center;\n				vertical-align: top;\n			}\n\n			.btn-primary td a {\n				background-color: #348eda;\n				border: solid 1px #348eda;\n				border-radius: 25px;\n				border-width: 10px 20px;\n				display: inline-block;\n				color: #ffffff;\n				cursor: pointer;\n				font-weight: bold;\n				line-height: 2;\n				text-decoration: none;\n			}\n\n			.last {\n				margin-bottom: 0;\n			}\n\n			.first {\n				margin-top: 0;\n			}\n\n			.padding {\n				padding: 10px 0;\n			}\n\n\n			/* BODY */\n			table.body-wrap {\n				padding: 20px;\n				width: 100%;\n			}\n\n			table.body-wrap .container {\n				border: 1px solid #f0f0f0;\n			}\n\n\n			/* FOOTER */\n			table.footer-wrap {\n				clear: both !important;\n				width: 100%;\n			}\n\n			.footer-wrap .container p {\n				color: #666666;\n				font-size: 12px;\n\n			}\n\n			table.footer-wrap a {\n				color: #999999;\n			}\n\n\n			/* TYPOGRAPHY */\n			h1, h2, h3 {\n				color: #111111;\n				font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n				font-weight: 200;\n				line-height: 1.2em;\n				margin: 40px 0 10px;\n			}\n\n			h1 {\n				font-size: 36px;\n			}\n\n			h2 {\n				font-size: 28px;\n			}\n\n			h3 {\n				font-size: 22px;\n			}\n\n			p, ul, ol {\n				font-size: 14px;\n				font-weight: normal;\n				margin-bottom: 10px;\n			}\n\n			ul li, ol li {\n				margin-left: 5px;\n				list-style-position: inside;\n			}\n\n			/* RESPONSIVENESS */\n\n			/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n			.container {\n				clear: both !important;\n				display: block !important;\n				Margin: 0 auto !important;\n				max-width: 600px !important;\n			}\n\n			/* Set the padding on the td rather than the div for Outlook compatibility */\n			.body-wrap .container {\n				padding: 20px;\n			}\n\n			/* This should also be a block element, so that it will fill 100% of the .container */\n			.content {\n				display: block;\n				margin: 0 auto;\n				max-width: 600px;\n			}\n\n			/* Let\'s make sure tables in the content area are 100% wide */\n			.content table {\n				width: 100%;\n			}\n		</style>\n	</head>\n	<body bgcolor=\"#f6f6f6\">\n		<!-- body -->\n		<table class=\"body-wrap\" bgcolor=\"#f6f6f6\">\n			<tr>\n				<td></td>\n				<td class=\"container\" bgcolor=\"#FFFFFF\">\n					<!-- content -->\n					<div class=\"content\">\n						<table>\n							<tr>\n								<td>\n									<p>A impressora encontra-se com pouco papel!</p>\n								</td>\n							</tr>\n						</table>\n					</div>\n					<!-- /content -->\n				</td>\n				<td></td>\n			</tr>\n		</table>\n		<!-- /body -->\n\n		<!-- footer -->\n		<table class=\"footer-wrap\">\n			<tr>\n				<td></td>\n				<td class=\"container\">\n					<!-- content -->\n					<div class=\"content\">\n						<table>\n							<tr>\n								<td align=\"center\">\n									<p>This message was generated by the system.</p>\n								</td>\n							</tr>\n						</table>\n					</div>\n					<!-- /content -->\n				</td>\n				<td></td>\n			</tr>\n		</table>\n		<!-- /footer -->\n	</body>\n</html>\n', '2017-11-29 18:05:21', '2017-11-29 18:05:21'),
(14, 'paper_length_warning_subject', 'Impressora com pouco papel!', '2017-11-29 18:06:34', '2017-11-29 18:06:34'),
(15, 'photo_ratio', '1.48', '2018-01-31 10:35:48', '2018-01-31 10:35:48'),
(16, 'folder_photo_sp', 'pdf2print', '2018-01-31 10:41:04', '2018-01-31 10:41:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_answers`
--

CREATE TABLE `os_bnp_quiz_answers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `id_ass` int(11) NOT NULL,
  `code` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_quiz_answers`
--

INSERT INTO `os_bnp_quiz_answers` (`id`, `title`, `id_ass`, `code`, `status`, `date`, `date_update`) VALUES
(1, 'Roxo', 1, '', 1, '2018-01-19 15:05:04', '2018-09-18 16:09:05'),
(18, 'Algo2', 1, '', 0, '2018-07-04 19:53:45', '2018-07-04 20:05:56'),
(5, 'resp true', 2, '', 1, '2018-01-23 12:24:59', '2018-01-23 12:24:59'),
(6, 'resp A', 2, '', 0, '2018-01-23 12:24:59', '2018-09-18 16:53:06'),
(7, 'resp B', 2, '', 0, '2018-01-23 12:25:16', '2018-09-18 16:54:03'),
(24, 'Definitivamente o Alex!', 4, '', 1, '2018-01-23 12:25:16', '2018-09-18 17:39:20'),
(23, 'Ou então o Alex!', 4, '', 1, '2018-01-23 12:25:16', '2018-09-18 17:39:18'),
(22, 'Só pode ser o Alex!', 4, '', 1, '2018-01-23 12:25:16', '2018-09-18 17:39:14'),
(25, 'Its alex!', 4, '', 1, '2018-01-23 12:25:16', '2018-09-18 17:39:22'),
(464, 'Têm o som de U', 138, ' ', 1, '2019-08-23 11:19:24', '2019-08-23 11:19:24'),
(465, 'Tem um som qualquer', 138, ' ', 0, '2019-08-23 11:19:51', '2019-08-23 11:19:51'),
(466, 'tem o som da Sasha', 138, ' ', 0, '2019-08-23 11:20:08', '2019-08-23 11:20:08'),
(467, 'Tem um #somostodos', 138, ' ', 0, '2019-08-23 11:20:33', '2019-08-23 11:20:33'),
(468, 'Nopeeeee', 139, ' ', 0, '2019-08-23 11:21:17', '2019-08-23 11:21:17'),
(469, 'Não, afinal era a Sasha', 139, ' ', 0, '2019-08-23 11:21:43', '2019-08-23 11:21:43'),
(470, 'YEPPPPP', 139, ' ', 1, '2019-08-23 11:21:59', '2019-08-23 11:21:59'),
(471, '#somostodos', 139, ' ', 0, '2019-08-23 11:22:18', '2019-08-23 11:22:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_question`
--

CREATE TABLE `os_bnp_quiz_question` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_quiz_question`
--

INSERT INTO `os_bnp_quiz_question` (`id`, `title`, `code`, `status`, `date`, `date_update`) VALUES
(138, 'U que som têm?', ' ', 1, '2019-08-23 10:54:55', '2019-08-23 10:54:55'),
(139, 'O teu irmão chegou lá baixo?', ' ', 1, '2019-08-23 11:20:54', '2019-08-23 11:20:54');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_settings`
--

CREATE TABLE `os_bnp_quiz_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_quiz_settings`
--

INSERT INTO `os_bnp_quiz_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '', '2017-09-13 00:00:00', '2018-07-04 16:54:02'),
(2, 'target', '/pt/bnp-ranking/', '2017-09-13 00:00:00', '2019-08-23 11:18:28'),
(3, 'app-logo', '/site-assets/images/bnp-quiz/app-logo.png', '2017-09-13 00:00:00', '2019-02-20 10:50:00'),
(4, 'number_questions', '2', '2018-01-19 15:29:09', '2019-08-23 11:18:28'),
(5, 'number_wrong_answers', '3', '2018-01-19 15:32:59', '2018-01-19 15:32:59'),
(7, 'number_right_answers', '1', '2018-07-05 13:04:38', '2018-07-05 13:04:38'),
(8, 'number_max_answers', '4', '2018-07-05 13:11:52', '2018-07-05 13:11:52'),
(9, 'startgame-btn', '', '2018-12-11 11:14:08', '2018-12-11 11:14:08'),
(10, 'touch-start', 'Toque para jogar!', '2018-12-11 11:14:22', '2018-12-11 11:14:22'),
(11, 'start-btn', '/uploads/30-9/begin.jpg', '2019-08-23 11:18:28', '2019-08-23 11:18:28'),
(12, 'points-per-question', '2', '2019-08-23 11:18:28', '2019-08-23 11:18:28'),
(13, 'time-conversion', '0', '2019-08-23 11:18:28', '2019-08-23 11:18:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking`
--

CREATE TABLE `os_bnp_ranking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_ranking`
--

INSERT INTO `os_bnp_ranking` (`id`, `user_id`, `points`, `status`, `sync`, `date`) VALUES
(1, 49, 157, 1, 1, '2019-03-01 16:54:42'),
(2, 50, 93, 1, 1, '2019-03-05 18:54:42'),
(3, 51, 94, 1, 1, '2019-03-05 19:42:06'),
(4, 52, 69, 1, 1, '2019-03-05 19:51:30'),
(5, 52, 65, 1, 1, '2019-03-05 19:52:14'),
(6, 53, 88, 1, 1, '2019-03-05 20:12:41'),
(7, 54, 83, 1, 1, '2019-03-05 20:19:14'),
(8, 55, 77, 1, 1, '2019-03-05 20:21:04'),
(9, 56, 0, 1, 1, '2019-03-06 10:16:24'),
(10, 57, 69, 1, 1, '2019-03-06 10:56:13'),
(11, 58, 89, 1, 1, '2019-03-06 11:38:03'),
(12, 59, 65, 1, 1, '2019-03-06 11:42:02'),
(13, 60, 0, 1, 1, '2019-03-07 18:10:07'),
(14, 60, 0, 1, 1, '2019-03-07 18:14:09'),
(15, 60, 0, 1, 1, '2019-03-07 18:17:38'),
(16, 60, 0, 1, 1, '2019-03-07 18:19:18'),
(17, 60, 0, 1, 1, '2019-03-07 18:21:36'),
(18, 61, 0, 1, 1, '2019-03-07 18:21:51'),
(19, 62, 41, 1, 1, '2019-03-08 13:31:16'),
(20, 63, 25, 1, 1, '2019-03-08 13:45:31'),
(21, 66, 139, 1, 1, '2019-03-08 16:04:34'),
(22, 68, 121, 1, 1, '2019-03-08 16:13:16'),
(23, 69, 195, 1, 1, '2019-04-11 16:37:00'),
(24, 71, 310, 1, 1, '2019-04-11 16:47:54'),
(25, 72, 470, 1, 1, '2019-04-11 16:52:31'),
(26, 73, 520, 1, 1, '2019-04-11 16:55:18'),
(27, 74, 540, 1, 1, '2019-04-11 17:26:19'),
(28, 75, 60, 1, 1, '2019-08-22 12:01:10'),
(29, 79, 60, 1, 1, '2019-08-22 12:04:04'),
(30, 84, 390, 1, 1, '2019-08-23 11:43:13'),
(31, 85, 47, 1, 1, '2019-08-23 12:27:14'),
(32, 85, 60, 1, 1, '2019-08-23 12:27:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking_schedule`
--

CREATE TABLE `os_bnp_ranking_schedule` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_ranking_schedule`
--

INSERT INTO `os_bnp_ranking_schedule` (`id`, `label`, `date_start`, `date_end`, `date`, `date_update`) VALUES
(6, 'teste 1', '2018-11-21 00:00:00', '2019-12-22 00:00:00', '2017-07-26 12:12:00', '2018-09-18 18:24:57'),
(17, 'Dev Demo 1', '2019-03-01 00:00:00', '2019-03-31 23:59:59', '2019-03-01 10:45:43', '2019-03-01 10:45:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking_settings`
--

CREATE TABLE `os_bnp_ranking_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_ranking_settings`
--

INSERT INTO `os_bnp_ranking_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '/pt/thanks/', '0000-00-00 00:00:00', '2019-08-22 17:33:25'),
(2, 'client-logo', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'app-bg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'timeout', '10', '2018-11-20 16:06:31', '2019-04-11 15:34:00'),
(5, 'position-text', 'A tua posição!', '2018-11-21 14:23:18', '2018-11-21 14:23:18'),
(6, 'css', '', '2018-12-11 11:14:44', '2018-12-11 11:14:44'),
(7, 'redirect', 'true', '2018-12-11 11:24:01', '2018-12-11 11:24:01'),
(8, 'accept-check', '1', '2019-01-18 17:33:23', '2019-01-18 17:33:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_screensaver_settings`
--

CREATE TABLE `os_bnp_screensaver_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_screensaver_settings`
--

INSERT INTO `os_bnp_screensaver_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '/pt/bnp-signup/', '2017-12-06 15:33:55', '2019-08-22 14:43:10'),
(2, 'css', '', '2017-12-06 15:34:00', '2017-12-06 15:34:00'),
(3, 'status', '0', '2017-12-06 15:34:05', '2019-08-23 16:03:56'),
(4, 'time', '120', '2017-12-06 15:34:11', '2019-08-21 16:22:00'),
(5, 'video', '[[\"{c2r-path}/uploads/screensaver/default.mp4\"],[\"{c2r-path}/uploads/screensaver/secondary.mp4\"]]', '2018-09-13 16:11:58', '2019-04-11 14:49:18'),
(6, 'width', '1080', '2018-09-13 16:12:58', '2018-09-13 16:12:58'),
(7, 'height', '1920', '2018-09-13 16:12:58', '2018-09-13 16:12:58'),
(8, 'ntarget', '/pt/bnp-signup/', '2019-08-21 16:22:00', '2019-08-21 16:22:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_item`
--

CREATE TABLE `os_bnp_stock_item` (
  `id` int(11) NOT NULL,
  `schedule_id` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_stock_item`
--

INSERT INTO `os_bnp_stock_item` (`id`, `schedule_id`, `name`, `image`, `quantity`, `message`, `sort`, `date`, `date_update`) VALUES
(31, 18, '01', '/uploads/prize/item-1.png', 100, 'Parabéns!<br>Ganhou 15% de desconto num Curso Executivo! ', 1, '2019-03-04 12:49:24', '2019-04-11 15:32:02'),
(32, 18, '02', '/uploads/prize/item-2.png', 100, 'Parabéns!<br>Ganhou o Curso Fundamentos de Marketing!', 2, '2019-03-04 12:49:24', '2019-04-11 15:31:59'),
(33, 18, '03', '/uploads/prize/item-3.png', 100, 'Parabéns!<br>Ganhou um bloco e uma caneta!', 3, '2019-03-04 12:49:24', '2019-04-11 15:31:56'),
(34, 18, '04', '/uploads/prize/item-4.png', 100, 'Parabéns!<br>Ganhou um powerbank!', 4, '2019-03-04 12:49:24', '2019-04-11 15:31:50'),
(253, 36, '01', '/uploads/30-9/bg.jpg', 100, 'Ui, ganhaste uma coisa boa! # Somostodos', 1, '2019-08-22 17:00:01', '2019-08-22 17:36:32'),
(254, 37, '01', '/uploads/30-5/car.jpg', 3, 'Espetáculooooo', 1, '2019-08-23 15:19:03', '2019-08-23 15:19:03'),
(255, 37, '02', '/uploads/30-5/bag.jpg', 7, 'Impecável', 2, '2019-08-23 15:19:38', '2019-08-23 15:19:38'),
(256, 37, '03', '/uploads/30-5/dog.jpg', 3, 'Vais fazer limpezas!', 3, '2019-08-23 15:20:15', '2019-08-23 15:20:15'),
(257, 37, '04', '/uploads/30-5/bottle.jpg', 5, 'É até cair!!', 4, '2019-08-23 15:20:50', '2019-08-23 15:20:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_log`
--

CREATE TABLE `os_bnp_stock_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_stock_log`
--

INSERT INTO `os_bnp_stock_log` (`id`, `user_id`, `item_id`, `sync`, `date`) VALUES
(1, 0, 33, 1, '2019-04-11 16:26:54'),
(2, 0, 34, 1, '2019-04-11 16:31:40'),
(3, 0, 32, 1, '2019-04-11 16:32:04'),
(4, 0, 34, 1, '2019-04-11 16:32:45'),
(5, 0, 34, 1, '2019-04-11 16:33:01'),
(6, 1, 33, 1, '2019-04-11 16:37:07'),
(7, 1, 34, 1, '2019-04-11 16:48:01'),
(8, 1, 31, 1, '2019-04-11 16:52:39'),
(9, 1, 31, 1, '2019-04-11 16:55:25'),
(10, 0, 33, 1, '2019-04-11 17:08:54'),
(11, 1, 31, 1, '2019-04-11 17:26:27'),
(12, 0, 253, 1, '2019-08-22 18:30:28'),
(13, 0, 253, 1, '2019-08-22 18:37:05'),
(14, 1, 254, 1, '2019-08-23 17:13:55'),
(15, 1, 254, 1, '2019-08-23 17:15:22'),
(16, 1, 254, 1, '2019-08-23 17:15:38'),
(17, 1, 255, 1, '2019-08-23 17:15:59'),
(18, 1, 255, 1, '2019-08-23 17:16:23'),
(19, 1, 255, 1, '2019-08-23 17:17:05'),
(20, 1, 255, 1, '2019-08-23 17:17:24'),
(21, 1, 255, 1, '2019-08-23 17:17:59'),
(22, 1, 255, 1, '2019-08-23 17:18:14'),
(23, 1, 255, 1, '2019-08-23 17:19:01'),
(24, 1, 256, 1, '2019-08-23 17:19:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_schedule`
--

CREATE TABLE `os_bnp_stock_schedule` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `premium` tinyint(1) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_stock_schedule`
--

INSERT INTO `os_bnp_stock_schedule` (`id`, `label`, `date_start`, `date_end`, `premium`, `date`, `date_update`) VALUES
(37, 'Gift Dummy', '2019-08-23 15:00:00', '2019-08-23 23:00:00', 0, '2019-08-23 15:14:01', '2019-08-23 16:11:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_settings`
--

CREATE TABLE `os_bnp_stock_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_stock_settings`
--

INSERT INTO `os_bnp_stock_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '/pt/bnp-gift-h/', '0000-00-00 00:00:00', '2019-08-23 15:32:46'),
(2, 'css', '.horizontal #machine_1, .horizontal #machine_2, .horizontal #machine_3 {\r\n	border-color: #002b5c;\r\n}\r\n\r\na {}', '0000-00-00 00:00:00', '2019-03-04 16:20:30'),
(3, 'prize-btn', '', '2018-12-11 11:15:50', '2018-12-11 11:15:50'),
(4, 'prize-touch', 'Toque no ícone<br>e descubra o seu brinde!', '2018-12-11 11:16:08', '2018-12-11 11:16:08'),
(5, 'accept-check', '1', '2019-01-18 17:33:09', '2019-01-18 17:33:09'),
(6, 'app-bg', '/uploads/bg.jpg', '2019-08-23 16:02:33', '2019-08-23 16:02:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_tapgame_settings`
--

CREATE TABLE `os_bnp_tapgame_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_tapgame_settings`
--

INSERT INTO `os_bnp_tapgame_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'time', '60', '2017-10-11 00:00:00', '2017-10-11 00:00:00'),
(2, 'columns', '4', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(3, 'buff', '2', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(4, 'debuff', '10', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(5, 'active', '5', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(6, 'game-area-start', '300', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(7, 'game-area-end', '1500', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(8, 'game-area-active', '350', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(9, 'time-increase', '10', '2017-10-11 00:00:00', '2017-10-11 00:00:00'),
(10, 'target', '/bnp-thanks/', '2017-10-11 00:00:00', '2019-08-23 10:44:01'),
(11, 'css', '', '2017-10-11 00:00:00', '2017-10-11 00:00:00'),
(12, 'buff-chance', '5', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(13, 'debuff-chance', '10', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(15, 'game-speed', '11', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(16, 'app-logo', '{c2r-path}/site-assets/images/bnp-tapgame/app-logo.png', '2018-01-18 18:07:27', '2018-01-18 18:07:27'),
(17, 'objects', '{\"object1\":{\"src\":\"/uploads/30-9/bag.jpg\",\"score\":10,\"chance\":30},\"object2\":{\"src\":\"/uploads/30-9/bottle.jpg\",\"score\":10,\"chance\":30},\"object3\":{\"src\":\"/uploads/30-9/bag.jpg\",\"score\":10,\"chance\":30}}', '2018-01-18 18:07:27', '2019-08-23 10:24:51'),
(18, 'logo-chance', '70', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(19, 'max-speed', '5', '2017-10-11 12:14:04', '2017-10-11 12:14:04'),
(20, 'app-bg', '', '2018-11-20 10:32:15', '2018-11-20 10:32:15'),
(21, 'start-btn', '/uploads/30-9/begin.png', '2018-11-21 10:57:35', '2019-08-23 10:24:51'),
(22, 'play-text', 'Toca para jogar!', '2018-11-21 14:22:39', '2018-11-21 14:22:39'),
(23, 'objects_double', '{\"object1\":{\"front\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_1.png\",\"back\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_1.png\",\"score\":5,\"chance\":10},\"object2\":{\"front\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_2.png\",\"back\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_2.png\",\"score\":10,\"chance\":10},\"object3\":{\"front\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_3.png\",\"back\":\"{c2r-path}/site-assets/images/bnp-tapgame/active_motion_3.png\",\"score\":15,\"chance\":10}}', '2019-02-11 14:25:44', '2019-02-11 14:25:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_thanks_settings`
--

CREATE TABLE `os_bnp_thanks_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_thanks_settings`
--

INSERT INTO `os_bnp_thanks_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '', '0000-00-00 00:00:00', '2019-04-11 15:34:21'),
(2, 'target', '{c2r-path}/pt/bnp-screensaver/', '0000-00-00 00:00:00', '2019-04-11 14:52:50'),
(3, 'timeout', '6', '0000-00-00 00:00:00', '2019-03-08 12:47:57'),
(4, 'app-bg', '', '2018-02-06 17:46:13', '2019-04-11 15:34:30'),
(5, 'client-logo', '', '2018-02-06 17:46:13', '2018-02-06 17:46:13'),
(6, 'thanks-image', '', '2018-02-06 17:47:02', '2019-04-11 15:34:32'),
(7, 'thanks-text', 'OBRIGADO <br> POR PARTICIPARES!', '2018-02-06 17:47:02', '2019-08-22 12:50:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_thermo_printer_settings`
--

CREATE TABLE `os_bnp_thermo_printer_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_thermo_printer_settings`
--

INSERT INTO `os_bnp_thermo_printer_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'status', '1', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(2, 'template', '<page format=\"80x150\" orientation=\"P\" backcolor=\"#FFFFFF\" style=\"font-weight:bold; font: arial; text-align: center; padding: 0; margin: 0; text-align:center;\">\r\n	<table width=\"90%\" align=\"center\" >\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center;\">\r\n				<img src=\"{c2r-logo}\" width=\"85%\">\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center; font-size: 16px;\">\r\n				Visita a Ilha de Natal no piso zero às Sextas, Sábados ou Domingos (das 11h às 21h) e troca este talão por um brinde de natal!\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"width: 100%; text-align: center; font-size: 12px;\">\r\n				Oferta válida para crianças até aos 14 anos e limitada ao stock existente.\r\n			</td>\r\n		</tr>\r\n	</table>\r\n</page>', '2017-11-28 18:16:57', '2017-11-28 18:16:57'),
(3, 'logo', 'http://192.168.2.61/brand-n-play/u-files/printed-logo.png', '2017-11-28 18:18:13', '2017-11-28 18:18:13'),
(4, 'width', '80', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(5, 'height', '150', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(6, 'orientation', 'P', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(7, 'paper_length', '50000', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(8, 'paper_length_per_print', '96', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(9, 'paper_length_warning', '1', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(10, 'paper_length_email', 'carloss@evoke.pt', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(11, 'paper_length_printed', '5421', '2017-11-28 18:16:36', '2017-11-28 18:16:36'),
(12, 'paper_length_limit', '45600', '2017-11-29 17:52:48', '2017-11-29 17:52:48'),
(13, 'paper_length_warning_message', '<!doctype html>\n<html>\n	<head>\n		<meta name=\"viewport\" content=\"width=device-width\">\n		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n		<title>Brand n Play</title>\n		<style>\n			/* GLOBAL */\n			* {\n				font-family: \"Helvetica Neue\", \"Helvetica\", Helvetica, Arial, sans-serif;\n				font-size: 100%;\n				line-height: 1.6em;\n				margin: 0;\n				padding: 0;\n			}\n\n			img {\n				max-width: 600px;\n				width: 100%;\n			}\n\n			body {\n				-webkit-font-smoothing: antialiased;\n				height: 100%;\n				-webkit-text-size-adjust: none;\n				width: 100% !important;\n			}\n\n\n			/* ELEMENTS */\n			a {\n				color: #348eda;\n			}\n\n			.btn-primary {\n				Margin-bottom: 10px;\n				width: auto !important;\n			}\n\n			.btn-primary td {\n				background-color: #348eda;\n				border-radius: 25px;\n				font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n				font-size: 14px;\n				text-align: center;\n				vertical-align: top;\n			}\n\n			.btn-primary td a {\n				background-color: #348eda;\n				border: solid 1px #348eda;\n				border-radius: 25px;\n				border-width: 10px 20px;\n				display: inline-block;\n				color: #ffffff;\n				cursor: pointer;\n				font-weight: bold;\n				line-height: 2;\n				text-decoration: none;\n			}\n\n			.last {\n				margin-bottom: 0;\n			}\n\n			.first {\n				margin-top: 0;\n			}\n\n			.padding {\n				padding: 10px 0;\n			}\n\n\n			/* BODY */\n			table.body-wrap {\n				padding: 20px;\n				width: 100%;\n			}\n\n			table.body-wrap .container {\n				border: 1px solid #f0f0f0;\n			}\n\n\n			/* FOOTER */\n			table.footer-wrap {\n				clear: both !important;\n				width: 100%;\n			}\n\n			.footer-wrap .container p {\n				color: #666666;\n				font-size: 12px;\n\n			}\n\n			table.footer-wrap a {\n				color: #999999;\n			}\n\n\n			/* TYPOGRAPHY */\n			h1, h2, h3 {\n				color: #111111;\n				font-family: \"Helvetica Neue\", Helvetica, Arial, \"Lucida Grande\", sans-serif;\n				font-weight: 200;\n				line-height: 1.2em;\n				margin: 40px 0 10px;\n			}\n\n			h1 {\n				font-size: 36px;\n			}\n\n			h2 {\n				font-size: 28px;\n			}\n\n			h3 {\n				font-size: 22px;\n			}\n\n			p, ul, ol {\n				font-size: 14px;\n				font-weight: normal;\n				margin-bottom: 10px;\n			}\n\n			ul li, ol li {\n				margin-left: 5px;\n				list-style-position: inside;\n			}\n\n			/* RESPONSIVENESS */\n\n			/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */\n			.container {\n				clear: both !important;\n				display: block !important;\n				Margin: 0 auto !important;\n				max-width: 600px !important;\n			}\n\n			/* Set the padding on the td rather than the div for Outlook compatibility */\n			.body-wrap .container {\n				padding: 20px;\n			}\n\n			/* This should also be a block element, so that it will fill 100% of the .container */\n			.content {\n				display: block;\n				margin: 0 auto;\n				max-width: 600px;\n			}\n\n			/* Let\'s make sure tables in the content area are 100% wide */\n			.content table {\n				width: 100%;\n			}\n		</style>\n	</head>\n	<body bgcolor=\"#f6f6f6\">\n		<!-- body -->\n		<table class=\"body-wrap\" bgcolor=\"#f6f6f6\">\n			<tr>\n				<td></td>\n				<td class=\"container\" bgcolor=\"#FFFFFF\">\n					<!-- content -->\n					<div class=\"content\">\n						<table>\n							<tr>\n								<td>\n									<p>A impressora encontra-se com pouco papel!</p>\n								</td>\n							</tr>\n						</table>\n					</div>\n					<!-- /content -->\n				</td>\n				<td></td>\n			</tr>\n		</table>\n		<!-- /body -->\n\n		<!-- footer -->\n		<table class=\"footer-wrap\">\n			<tr>\n				<td></td>\n				<td class=\"container\">\n					<!-- content -->\n					<div class=\"content\">\n						<table>\n							<tr>\n								<td align=\"center\">\n									<p>This message was generated by the system.</p>\n								</td>\n							</tr>\n						</table>\n					</div>\n					<!-- /content -->\n				</td>\n				<td></td>\n			</tr>\n		</table>\n		<!-- /footer -->\n	</body>\n</html>\n', '2017-11-29 18:05:21', '2017-11-29 18:05:21'),
(14, 'paper_length_warning_subject', 'Impressora com pouco papel!', '2017-11-29 18:06:34', '2017-11-29 18:06:34'),
(15, 'photo_ratio', '1.48', '2018-01-31 10:35:48', '2018-01-31 10:35:48'),
(16, 'folder_photo_sp', 'pdf2print', '2018-01-31 10:41:04', '2018-01-31 10:41:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users`
--

CREATE TABLE `os_bnp_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `comercial` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `processed` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_users`
--

INSERT INTO `os_bnp_users` (`id`, `name`, `email`, `code`, `comercial`, `status`, `sync`, `processed`, `date`, `date_update`) VALUES
(1, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-09-14 16:29:06', '2018-09-14 16:29:06'),
(2, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 0, 1, 1, '2018-09-14 16:29:06', '2018-09-14 16:29:06'),
(3, 'badjoras', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 0, 1, 1, '2018-09-14 16:29:06', '2018-09-14 16:29:06'),
(4, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 0, 1, 1, '2018-09-19 13:00:43', '2018-09-19 13:00:43'),
(5, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-09-20 13:58:20', '2018-09-20 13:58:20'),
(6, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-09-20 14:02:51', '2018-09-20 14:02:51'),
(7, 'Badjoras', 'rafaeld@evoke.pt', '{\"name\":\"Badjoras\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-09-20 16:15:40', '2018-09-20 16:15:40'),
(8, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-20 16:28:50', '2018-11-20 16:28:50'),
(9, 'rafael', 'rafaeld@evoke.pt', '{\"name\":\"rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-20 17:09:19', '2018-11-20 17:09:19'),
(10, 'bea', 'beatrizn@evoke.pt', '{\"name\":\"bea\",\"email\":\"beatrizn@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-20 19:01:10', '2018-11-20 19:01:10'),
(11, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-20 19:20:55', '2018-11-20 19:20:55'),
(12, 'Rafael', 'rafaeld@evoke.pt', '{\"name\":\"Rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-21 12:24:26', '2018-11-21 12:24:26'),
(13, 'rafael', 'rafaeld@evoke.pt', '{\"name\":\"rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-21 12:42:28', '2018-11-21 12:42:28'),
(14, 'rd9', 'rafaeld@evoke.pt', '{\"name\":\"rd9\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-21 12:51:59', '2018-11-21 12:51:59'),
(15, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-11-21 14:00:57', '2018-11-21 14:00:57'),
(16, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-12-06 15:26:14', '2018-12-06 15:26:14'),
(17, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2018-12-07 12:58:23', '2018-12-07 12:58:23'),
(18, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-03 15:28:24', '2019-01-03 15:28:24'),
(19, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:01:59', '2019-01-04 12:01:59'),
(20, 'rafael', 'rafaeld@evoke.pt', '{\"name\":\"rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:03:57', '2019-01-04 12:03:57'),
(21, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:08:13', '2019-01-04 12:08:13'),
(22, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:23:55', '2019-01-04 12:23:55'),
(23, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:27:42', '2019-01-04 12:27:42'),
(24, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:31:53', '2019-01-04 12:31:53'),
(25, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-04 12:47:30', '2019-01-04 12:47:30'),
(26, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-07 12:49:48', '2019-01-07 12:49:48'),
(27, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-07 12:54:42', '2019-01-07 12:54:42'),
(28, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-07 12:55:35', '2019-01-07 12:55:35'),
(29, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-07 13:04:48', '2019-01-07 13:04:48'),
(30, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 1, '2019-01-07 13:13:10', '2019-01-07 13:13:10'),
(31, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-01-18 15:43:54', '2019-01-18 15:43:54'),
(32, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-01-18 17:40:43', '2019-01-18 17:40:43'),
(33, 'rafa', 'rafaeld@evoke.pt', '{\"name\":\"rafa\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-01-18 17:41:28', '2019-01-18 17:41:28'),
(34, 'rafael', 'rafaeld@evoke.pt', '{\"name\":\"rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-01-18 17:46:09', '2019-01-18 17:46:09'),
(35, 'rafael', 'rafaeld@evoke.pt', '{\"name\":\"rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-01-18 17:48:20', '2019-01-18 17:48:20'),
(36, 'rafa d', 'rafaeld@evoke.pt', '{\"name\":\"rafa d\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-01-18 17:54:35', '2019-01-18 17:54:35'),
(37, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-01-18 18:40:42', '2019-01-18 18:40:42'),
(38, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-01-18 18:44:40', '2019-01-18 18:44:40'),
(39, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-02-27 18:08:47', '2019-02-27 18:08:47'),
(40, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-02-28 12:56:45', '2019-02-28 12:56:45'),
(41, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-02-28 15:56:04', '2019-02-28 15:56:04'),
(42, 'llll', 'lllllljjjjjj@mmm.pu', '{\"name\":\"llll\",\"email\":\"lllllljjjjjj@mmm.pu\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-02-28 18:10:01', '2019-02-28 18:10:01'),
(43, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 0, 0, 0, '2019-02-28 19:10:50', '2019-02-28 19:10:50'),
(44, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-02-28 19:12:10', '2019-02-28 19:12:10'),
(45, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 0, 0, 0, '2019-02-28 19:13:17', '2019-02-28 19:13:17'),
(46, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-02-28 19:14:42', '2019-02-28 19:14:42'),
(47, 'carlos', 'carloss@evokd.tl', '{\"name\":\"carlos\",\"email\":\"carloss@evokd.tl\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-02-28 19:17:18', '2019-02-28 19:17:18'),
(48, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '2019-03-01 13:30:11', '2019-03-01 13:30:11'),
(49, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-01 16:54:28', '2019-03-01 16:54:28'),
(50, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 18:54:28', '2019-03-05 18:54:28'),
(51, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 19:41:52', '2019-03-05 19:41:52'),
(52, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 19:42:53', '2019-03-05 19:42:53'),
(53, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 20:12:21', '2019-03-05 20:12:21'),
(54, 'carlos', 'carloss@evok.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evok.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 20:18:49', '2019-03-05 20:18:49'),
(55, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-05 20:20:34', '2019-03-05 20:20:34'),
(56, 'carlos', 'carloss@evoke.pr', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pr\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-06 10:15:13', '2019-03-06 10:15:13'),
(57, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-06 10:55:36', '2019-03-06 10:55:36'),
(58, 'RAFA', 'rafaeld@evoke.pt', '{\"name\":\"RAFA\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-06 11:37:45', '2019-03-06 11:37:45'),
(59, 'tiagos', 'tiagos@evoke.pt', '{\"name\":\"tiagos\",\"email\":\"tiagos@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-06 11:41:19', '2019-03-06 11:41:19'),
(60, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-07 15:57:54', '2019-03-07 15:57:54'),
(61, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-03-07 18:20:39', '2019-03-07 18:20:39'),
(62, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-08 13:30:07', '2019-03-08 13:30:07'),
(63, 'carlos', 'carloss@evokd.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evokd.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-03-08 13:44:22', '2019-03-08 13:44:22'),
(64, 'Rafaeu', 'rafaeld@evoke.pt', '{\"name\":\"Rafaeu\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"0\"}', 0, 0, 0, 0, '2019-03-08 13:59:39', '2019-03-08 13:59:39'),
(65, 'Raffaeu', 'rafaeld@evoke.pt', '{\"name\":\"Raffaeu\",\"email\":\"rafAELD@evoke.pt\",\"accept\":\"0\"}', 0, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 'Rafael', 'rafaeld@evoke.pt', '{\"name\":\"Rafael\",\"email\":\"rafaeld@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-04-11 15:35:38', '2019-08-21 15:56:03'),
(70, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-04-11 15:38:59', '2019-04-11 15:38:59'),
(71, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-04-11 15:44:07', '2019-08-21 15:57:01'),
(72, 'Joao silva', 'joao_8d@hotmail.com', '{\"name\":\"Joao silva\",\"email\":\"joao_8d@hotmail.com\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-04-11 15:50:54', '2019-08-21 15:58:02'),
(73, 'Paulo', 'paulob@evoke.pt', '{\"name\":\"Paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-04-11 15:53:48', '2019-08-21 15:59:02'),
(74, 'yfu', 'gjhdff@gmail.com', '{\"name\":\"yfu\",\"email\":\"gjhdff@gmail.com\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-04-11 16:23:51', '2019-08-21 16:00:02'),
(75, 'carlos', 'carloss@evoke.pt', '{\"name\":\"carlos\",\"email\":\"carloss@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-08-21 17:42:41', '2019-08-22 11:02:02'),
(76, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-21 17:43:58', '2019-08-21 17:43:58'),
(77, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-21 18:16:55', '2019-08-21 18:16:55'),
(78, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-21 18:21:01', '2019-08-21 18:21:01'),
(79, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 1, 1, 0, '2019-08-22 11:01:12', '2019-08-22 11:07:01'),
(80, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-22 14:39:18', '2019-08-22 14:39:18'),
(81, 'pauop', 'paulob@evoke.pt', '{\"name\":\"pauop\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-22 14:44:33', '2019-08-22 14:44:33'),
(82, 'psdes', 'paulob@evoke.pt', '{\"name\":\"psdes\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-22 16:53:47', '2019-08-22 16:53:47'),
(83, 'poggj', 'pauob@evoke.pt', '{\"name\":\"poggj\",\"email\":\"pauob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-22 17:07:42', '2019-08-22 17:07:42'),
(84, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-23 10:42:05', '2019-08-23 10:42:05'),
(85, 'paulo', 'paulob@evoke.pt', '{\"name\":\"paulo\",\"email\":\"paulob@evoke.pt\",\"accept\":\"1\"}', 1, 0, 0, 0, '2019-08-23 11:23:44', '2019-08-23 11:23:44');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users_fields`
--

CREATE TABLE `os_bnp_users_fields` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `error_message` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `sort` int(11) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_users_fields`
--

INSERT INTO `os_bnp_users_fields` (`id`, `label`, `name`, `value`, `type`, `error_message`, `required`, `sort`, `status`, `date`, `date_update`) VALUES
(1, 'Nome', 'name', '', 'text', 'O nome precisa de ter pelo menos 3 letras!', 1, 0, 1, '2017-07-20 00:00:00', '2018-09-19 11:57:29'),
(2, 'Email', 'email', '', 'email', 'O email indicado é inválido!', 1, 1, 1, '2017-07-20 00:00:00', '2018-11-20 11:36:18'),
(3, 'Mais um teste! A', 'email', '', 'email', 'O email indicado é inválido!', 1, 1, 1, '2017-07-20 00:00:00', '2018-09-19 12:00:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users_settings`
--

CREATE TABLE `os_bnp_users_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_users_settings`
--

INSERT INTO `os_bnp_users_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '', '2017-09-13 00:00:00', '2019-03-21 09:20:00'),
(2, 'target', '/pt/bnp-bike/', '2017-09-13 00:00:00', '2019-08-23 12:44:21'),
(6, 'app-bg', '/uploads/30-9/bg.jpg', '2018-01-24 16:35:54', '2019-08-09 12:37:20'),
(7, 'client-logo', '', '2018-01-24 16:39:18', '2018-01-24 16:40:52'),
(8, 'app-avatar', '', '2018-01-24 16:46:26', '2018-01-24 19:15:25'),
(9, 'app-terms-bg', '', '2018-01-24 16:53:12', '2019-04-11 14:58:39'),
(10, 'app-terms-check', '', '2018-01-24 16:53:49', '2019-03-21 09:20:14'),
(11, 'terms-text', 'Aceito que os dados sejam utilizados em ações promocionais da marca.', '2018-12-11 11:13:17', '2019-03-05 17:50:35'),
(12, 'terms-full-text', '<p>A utilização desta app não implica o fornecimento de dados pessoais. No entanto, caso não queira disponibilizar os dados, ficará impossibilitado de receber a fotografia por email</p> ', '2019-03-04 17:57:39', '2019-08-09 12:37:20'),
(13, 'keyboard-theme', 'light', '2019-03-07 12:50:58', '2019-03-21 09:20:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_files`
--

CREATE TABLE `os_files` (
  `id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `id_ass` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `sort` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_files`
--

INSERT INTO `os_files` (`id`, `file`, `type`, `module`, `id_ass`, `description`, `code`, `sort`, `user_id`, `date`, `date_update`) VALUES
(2, '2018-09-03-16-11-46-DocumentoTransporte.pdf', 'doc', '', 0, 'img', '', 0, 0, '2018-09-03 16:11:46', '0000-00-00 00:00:00'),
(3, '2018-09-03-16-21-39-DocumentoTransporte.pdf', 'doc', '', 0, '', '', 0, 0, '2018-09-03 16:21:39', '0000-00-00 00:00:00'),
(4, '2018_09_13_18_16_11.jpg', 'img', 'bnp-users', 1, '', '', 0, 1, '2018-09-03 16:21:39', '2018-09-17 16:09:49'),
(5, '2018_09_13_18_16_12.jpg', 'img', 'bnp-users', 1, '', '', 0, 1, '2018-09-03 16:21:39', '2018-09-17 16:09:51'),
(12, 'photo-gif/21/21.mp4', 'file', 'bnp-users', 21, '', '', 0, 21, '2019-01-04 12:19:54', '2019-01-04 12:19:54'),
(11, 'photo-gif/21/21.mp4', 'file', 'bnp-users', 21, '', '', 0, 21, '2019-01-04 12:15:35', '2019-01-04 12:15:35'),
(10, 'photo-gif/17/17.mp4', 'file', 'bnp-users', 17, '', '', 0, 17, '2018-12-07 13:13:57', '2018-12-07 13:13:57'),
(13, 'photo-gif/22/22.mp4', 'file', 'bnp-users', 22, '', '', 0, 22, '2019-01-04 12:25:48', '2019-01-04 12:25:48'),
(14, 'photo-gif/23/23.mp4', 'file', 'bnp-users', 23, '', '', 0, 23, '2019-01-04 12:29:16', '2019-01-04 12:29:16'),
(15, 'photo-gif/24/24.mp4', 'file', 'bnp-users', 24, '', '', 0, 24, '2019-01-04 12:46:43', '2019-01-04 12:46:43'),
(16, 'photo-gif/25/25.mp4', 'file', 'bnp-users', 25, '', '', 0, 25, '2019-01-04 13:28:12', '2019-01-04 13:28:12'),
(17, 'photo-gif/26/26.mp4', 'file', 'bnp-users', 26, '', '', 0, 26, '2019-01-07 12:56:41', '2019-01-07 12:56:41'),
(18, 'photo-gif/28/28.mp4', 'file', 'bnp-users', 28, '', '', 0, 28, '2019-01-07 12:57:10', '2019-01-07 12:57:10'),
(19, 'photo-gif/27/27.mp4', 'file', 'bnp-users', 27, '', '', 0, 27, '2019-01-07 12:58:21', '2019-01-07 12:58:21'),
(20, 'photo-gif/28/28.mp4', 'file', 'bnp-users', 28, '', '', 0, 28, '2019-01-07 12:59:06', '2019-01-07 12:59:06'),
(21, 'photo-gif/27/27.mp4', 'file', 'bnp-users', 27, '', '', 0, 27, '2019-01-07 13:03:44', '2019-01-07 13:03:44'),
(22, 'photo-gif/29/29.mp4', 'file', 'bnp-users', 29, '', '', 0, 29, '2019-01-07 13:11:08', '2019-01-07 13:11:08'),
(23, 'photo-gif/30/30.mp4', 'file', 'bnp-users', 30, '', '', 0, 30, '2019-01-07 13:14:02', '2019-01-07 13:14:02'),
(24, '55.jpg', 'img', 'bnp-users', 0, '', '', 0, 55, '2019-03-05 19:21:22', '2019-03-05 19:21:22'),
(25, '56.jpg', 'img', 'bnp-users', 0, '', '', 0, 56, '2019-03-06 09:19:04', '2019-03-06 09:19:04'),
(26, '57.jpg', 'img', 'bnp-users', 0, '', '', 0, 57, '2019-03-06 09:56:32', '2019-03-06 09:56:32'),
(27, '58.jpg', 'img', 'bnp-users', 0, '', '', 0, 58, '2019-03-06 10:38:54', '2019-03-06 10:38:54'),
(28, '59.jpg', 'img', 'bnp-users', 0, '', '', 0, 59, '2019-03-06 10:42:47', '2019-03-06 10:42:47'),
(29, '62.jpg', 'img', 'bnp-users', 0, '', '', 0, 62, '2019-03-08 12:31:52', '2019-03-08 12:31:52'),
(30, '63.jpg', 'img', 'bnp-users', 0, '', '', 0, 63, '2019-03-08 12:47:19', '2019-03-08 12:47:19'),
(31, '2019_03_08_15_37_32.jpg', 'img', 'bnp-users', 0, '', '', 0, 0, '2019-03-08 14:37:32', '2019-03-08 14:37:32'),
(32, '65.jpg', 'img', 'bnp-users', 0, '', '', 0, 65, '2019-03-08 14:54:28', '2019-03-08 14:54:28'),
(33, '66.jpg', 'img', 'bnp-users', 0, '', '', 0, 66, '2019-03-08 15:05:26', '2019-03-08 15:05:26'),
(34, '68.jpg', 'img', 'bnp-users', 0, '', '', 0, 68, '2019-03-08 15:13:38', '2019-03-08 15:13:38'),
(35, '69.jpg', 'img', 'bnp-users', 0, '', '', 0, 69, '2019-04-11 15:35:51', '2019-04-11 15:35:51'),
(36, '71.jpg', 'img', 'bnp-users', 0, '', '', 0, 71, '2019-04-11 15:46:15', '2019-04-11 15:46:15'),
(37, '72.jpg', 'img', 'bnp-users', 0, '', '', 0, 72, '2019-04-11 15:51:12', '2019-04-11 15:51:12'),
(38, '73.jpg', 'img', 'bnp-users', 0, '', '', 0, 73, '2019-04-11 15:54:04', '2019-04-11 15:54:04'),
(39, '74.jpg', 'img', 'bnp-users', 0, '', '', 0, 74, '2019-04-11 16:24:42', '2019-04-11 16:24:42'),
(40, '78.jpg', 'img', 'bnp-users', 0, '', '', 0, 78, '2019-08-21 18:25:05', '2019-08-21 18:25:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_history`
--

CREATE TABLE `os_history` (
  `id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_history`
--

INSERT INTO `os_history` (`id`, `module`, `user_id`, `description`, `date`) VALUES
(1, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2018-09-03 15:56:52'),
(2, 'sys-login', 1, '{\"ip\":\"192.168.2.136\"}', '2018-09-03 15:57:12'),
(3, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2018-09-04 11:30:12'),
(4, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2018-09-04 11:32:26'),
(5, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2018-09-04 11:32:58'),
(6, 'sys-login', 1, '{\"ip\":\"192.168.2.136\"}', '2018-09-04 11:36:03'),
(7, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2018-09-04 14:05:22'),
(8, 'sys-login', 1, '{\"ip\":\"127.0.0.1\"}', '2018-09-13 12:39:43'),
(9, 'sys-login', 1, '{\"ip\":\"127.0.0.1\"}', '2018-09-13 16:34:40'),
(10, 'sys-login', 1, '{\"ip\":\"127.0.0.1\"}', '2018-09-14 13:49:53'),
(11, 'sys-login', 1, '{\"ip\":\"127.0.0.1\"}', '2018-09-20 19:41:36'),
(12, 'sys-login', 1, '{\"ip\":\"192.168.2.136\"}', '2018-12-21 18:02:55'),
(13, 'sys-login', 1, '{\"ip\":\"192.168.2.101\"}', '2019-01-30 13:45:50'),
(14, 'sys-login', 1, '{\"ip\":\"192.168.1.79\"}', '2019-03-05 16:37:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_modules`
--

CREATE TABLE `os_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_modules`
--

INSERT INTO `os_modules` (`id`, `name`, `folder`, `code`, `sort`) VALUES
(1, 'Home', 'mod-5-home', '{\r\n	\"fa-icon\": \"fa-home\",\r\n	\"img\": \"\",\r\n	\"sub-items\": {},\r\n\"sidebar\": true,\r\n\"dropdown\": false\r\n}', 0),
(2, 'Account', 'mod-6-account', '{\r\n	\"fa-icon\": \"fa-user-cog\",\r\n	\"img\": \"\",\r\n	\"sub-items\": {},\r\n\"sidebar\": false,\r\n\"dropdown\": true\r\n}', 1),
(3, 'Users', 'mod-9-users', '{\r\n	\"fa-icon\": \"fa-users\",\r\n	\"img\": \"\",\r\n	\"sub-items\": {\r\n		\"List\": {\r\n			\"url\": \"\"\r\n		},\r\n		\"Add user\": {\r\n			\"url\": \"add\"\r\n		},\r\n\"Logs\": {\r\n			\"url\": \"logs\"\r\n		}\r\n	},\r\n\"sidebar\": true,\r\n\"dropdown\": false\r\n}', 2),
(25, 'BNP Config', 'mod-0-bnpconfig', '{   \"fa-icon\": \"fa-cogs\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 6),
(6, 'Files', 'mod-4-files', '{\r\n	\"fa-icon\": \"fa-file\",\r\n	\"img\": \"\",\r\n	\"sub-items\": {},\r\n\"sidebar\": true,\r\n\"dropdown\": false\r\n}', 5),
(8, 'BNP Photo', 'mod-0-bnpphoto', '{   \"fa-icon\": \"fa-image\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 15),
(10, 'BNP Ranking', 'mod-0-bnpranking', '{   \"fa-icon\": \"fa-gamepad\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 10),
(13, 'BNP Stock', 'mod-0-bnpstock', '{   \"fa-icon\": \"fa-gamepad\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 10),
(14, 'BNP Users', 'mod-0-bnpusers', '{   \"fa-icon\": \"fa-gamepad\",   \"img\": \"\",     \"sub-items\": {\r\n    \"Users\": {\r\n      \"url\": \"users\",\r\n\"icon\": \"fa-users\"\r\n    },\r\n    \"Fields\": {\r\n      \"url\": \"\",\r\n\"icon\": \"fa-list-ul\"\r\n    },\r\n\"Settings\": {\r\n\"url\": \"settings\",\r\n\"icon\": \"fa-cog\"\r\n}\r\n  }, \"sidebar\": true,\r\n\"dropdown\": false }', 10),
(22, 'BNP GIF', 'mod-0-bnpgif', '{   \"fa-icon\": \"fa-image\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 15),
(23, 'BNP Thanks', 'mod-0-bnpthanks', '{   \"fa-icon\": \"fa-gift\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 16),
(24, 'BNP Quiz', 'mod-0-bnpquiz', '{   \"fa-icon\": \"fa-question-circle\",   \"img\": \"\",   \"sub-items\": {}, \"sidebar\": true,\r\n\"dropdown\": false }', 15);

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_trash`
--

CREATE TABLE `os_trash` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_trash`
--

INSERT INTO `os_trash` (`id`, `module`, `code`, `user_id`, `date`) VALUES
(1, 'mod-4-files', '{\"id\":\"1\",\"file\":\"2018-09-03-16-11-06-DocumentoTransporte.pdf\",\"type\":\"doc\",\"module\":\"\",\"id_ass\":\"0\",\"description\":\"\",\"code\":\"\",\"sort\":\"0\",\"user_id\":\"0\",\"date\":\"2018-09-03 16:11:06\",\"date_update\":\"0000-00-00 00:00:00\"}', 1, '2018-09-03 16:11:40'),
(2, 'mod-0-bnpconfig', '{\"id\":\"5\",\"name\":\"client-logo\",\"value\":\"{c2r-path}/site-assets/images/logo.svg\",\"date\":\"2018-02-06 17:46:13\",\"date_update\":\"2018-02-06 17:46:13\"}', 1, '2018-09-13 17:06:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_users`
--

CREATE TABLE `os_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rank` enum('owner','manager','member') DEFAULT 'member',
  `email` varchar(255) DEFAULT NULL,
  `code` text DEFAULT NULL,
  `custom_css` text NOT NULL,
  `user_key` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_users`
--

INSERT INTO `os_users` (`id`, `username`, `password`, `rank`, `email`, `code`, `custom_css`, `user_key`, `status`, `date`, `date_update`) VALUES
(1, 'EVOKE IT', '99e02aa483d94d94dbbbe73ffe87a8eda90d883c', 'owner', 'geral@evoke.pt', '', '', '14a9afbdae2fd07f32be0132632d6065', 1, '2017-01-01 09:00:00', '2018-09-04 11:32:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_users_fields`
--

CREATE TABLE `os_users_fields` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL,
  `type` text NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `os_bnp_bike_settings`
--
ALTER TABLE `os_bnp_bike_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_config`
--
ALTER TABLE `os_bnp_config`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_gift_settings`
--
ALTER TABLE `os_bnp_gift_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_gif_settings`
--
ALTER TABLE `os_bnp_gif_settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_memory_settings`
--
ALTER TABLE `os_bnp_memory_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_app`
--
ALTER TABLE `os_bnp_photo_app`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_modes`
--
ALTER TABLE `os_bnp_photo_modes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_settings`
--
ALTER TABLE `os_bnp_photo_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_printer_settings`
--
ALTER TABLE `os_bnp_printer_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_answers`
--
ALTER TABLE `os_bnp_quiz_answers`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_question`
--
ALTER TABLE `os_bnp_quiz_question`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_settings`
--
ALTER TABLE `os_bnp_quiz_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking`
--
ALTER TABLE `os_bnp_ranking`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking_schedule`
--
ALTER TABLE `os_bnp_ranking_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking_settings`
--
ALTER TABLE `os_bnp_ranking_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_screensaver_settings`
--
ALTER TABLE `os_bnp_screensaver_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_item`
--
ALTER TABLE `os_bnp_stock_item`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_log`
--
ALTER TABLE `os_bnp_stock_log`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_bnp_stock_schedule`
--
ALTER TABLE `os_bnp_stock_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_settings`
--
ALTER TABLE `os_bnp_stock_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_tapgame_settings`
--
ALTER TABLE `os_bnp_tapgame_settings`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_bnp_thanks_settings`
--
ALTER TABLE `os_bnp_thanks_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_thermo_printer_settings`
--
ALTER TABLE `os_bnp_thermo_printer_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_users`
--
ALTER TABLE `os_bnp_users`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_users_fields`
--
ALTER TABLE `os_bnp_users_fields`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_users_settings`
--
ALTER TABLE `os_bnp_users_settings`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_files`
--
ALTER TABLE `os_files`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_history`
--
ALTER TABLE `os_history`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_modules`
--
ALTER TABLE `os_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `folder` (`folder`);

--
-- Índices para tabela `os_trash`
--
ALTER TABLE `os_trash`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_users`
--
ALTER TABLE `os_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_prefix_users_prefix_products1` (`id`),
  ADD KEY `fk_prefix_users_prefix_articles1` (`id`);

--
-- Índices para tabela `os_users_fields`
--
ALTER TABLE `os_users_fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `os_bnp_bike_settings`
--
ALTER TABLE `os_bnp_bike_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `os_bnp_config`
--
ALTER TABLE `os_bnp_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `os_bnp_gift_settings`
--
ALTER TABLE `os_bnp_gift_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `os_bnp_gif_settings`
--
ALTER TABLE `os_bnp_gif_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de tabela `os_bnp_memory_settings`
--
ALTER TABLE `os_bnp_memory_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_app`
--
ALTER TABLE `os_bnp_photo_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_modes`
--
ALTER TABLE `os_bnp_photo_modes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_settings`
--
ALTER TABLE `os_bnp_photo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de tabela `os_bnp_printer_settings`
--
ALTER TABLE `os_bnp_printer_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_answers`
--
ALTER TABLE `os_bnp_quiz_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=472;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_question`
--
ALTER TABLE `os_bnp_quiz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_settings`
--
ALTER TABLE `os_bnp_quiz_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking`
--
ALTER TABLE `os_bnp_ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking_schedule`
--
ALTER TABLE `os_bnp_ranking_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking_settings`
--
ALTER TABLE `os_bnp_ranking_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `os_bnp_screensaver_settings`
--
ALTER TABLE `os_bnp_screensaver_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_item`
--
ALTER TABLE `os_bnp_stock_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_log`
--
ALTER TABLE `os_bnp_stock_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_schedule`
--
ALTER TABLE `os_bnp_stock_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_settings`
--
ALTER TABLE `os_bnp_stock_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `os_bnp_tapgame_settings`
--
ALTER TABLE `os_bnp_tapgame_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `os_bnp_thanks_settings`
--
ALTER TABLE `os_bnp_thanks_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `os_bnp_thermo_printer_settings`
--
ALTER TABLE `os_bnp_thermo_printer_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de tabela `os_bnp_users`
--
ALTER TABLE `os_bnp_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT de tabela `os_bnp_users_fields`
--
ALTER TABLE `os_bnp_users_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `os_bnp_users_settings`
--
ALTER TABLE `os_bnp_users_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `os_files`
--
ALTER TABLE `os_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de tabela `os_history`
--
ALTER TABLE `os_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `os_modules`
--
ALTER TABLE `os_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de tabela `os_trash`
--
ALTER TABLE `os_trash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `os_users`
--
ALTER TABLE `os_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
