<?php

class _global_ {

	public static $helloWorldString = "Hello World!";

	public function __construct() {}

	public static function start () {
		print self::$helloWorldString;
	}

	public static function folder ($path, $name) {
		global $cfg;

		if (
			file_exists($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/") &&
			is_dir($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/")
		) {
			if (is_writable($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/")) {
				if (!file_exists($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/index.html")) {
					touch($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/index.html");
				}
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			if (mkdir($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/", 0755)) {
				if(is_writable($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/")) {
					touch($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/{$name}/index.html");
					return TRUE;
				} else {
					return FALSE;
				}
			}
		}
	}

	public static function check_folder ($path) {
		global $cfg;

		if (
			file_exists($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/") &&
			is_dir($_SERVER['DOCUMENT_ROOT'].$cfg->system->path."/".$path."/")
		) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// need to be moved for a bnp config::class
	public static function getAppLogo ($appType) {
		global $cfg, $db;

		if (isset($appType) && !empty($appType)) {
			$query = sprintf(
				"SELECT * FROM %s_bnp_%s_settings WHERE name = '%s'",
				$cfg->db->prefix,
				$appType,
				'app-logo'
			);

			$source = $db->query($query);
			if ($source->num_rows > 0) {
				$data = $source->fetch_object();

				return $data->value;
			}
		}

		return false;
	}
}
