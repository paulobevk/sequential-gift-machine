<?php

$page_tpl = bo3::load("bnp-quiz.tpl");
$item_tpl = bo3::loade("bnp-games/bnp-quiz/item.tpl");
$answers_tpl = bo3::loade("bnp-games/bnp-quiz/answer.tpl");

include "pages-e/header-game.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_quiz::getSettings();
$screensaver_settings = bnp_screensaver::return_settings();

$questions = bnp_quiz::getQuestions($settings['number_questions']);

$toReturn = "";
$index = 0;

foreach ($questions as $q => $question) {
	$answers_list = [];

	$answersCorrect = bnp_quiz::getAnswer($question->id);
	$answersWrong = bnp_quiz::getWrongAnswer($question->id, ($settings['number_max_answers'] - 1));

	array_push($answers_list, $answersCorrect);

	foreach ($answersWrong as $an => $answer) {
		array_push($answers_list, $answer);
	}

	shuffle($answers_list);

	$listAnswer = "";

	foreach ($answers_list as $i => $item) {
		if($i >= $settings["number_max_answers"]) {
			break;
		}

		$listAnswer .= bo3::c2r([
			'status' => $item->status,
			'title' => isset($item->title) ? $item->title : '',
			"answer-type" => ($item->status) ? "right" : "wrong"
		], $answers_tpl);
	}

	$toReturn .= bo3::c2r([
		'index' => $q + 1,
		'class' => ($q == 0) ? 'active' : '',
		'answer' => $listAnswer,
		'total-question' => $settings['number_questions'],
		'title' => $question->title
	], $item_tpl);
}

$app_logo = _global_::getAppLogo($configs["app-type"]);

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,
	'header-bg' => (isset($settings['header-bg']) && !empty($settings['header-bg'])) ? $settings['header-bg'] : '{c2r-path}/site-assets/images/bnp-header/bg_new.png',
	'header-clock' => isset($settings['time-bar']) && !empty($settings['time-bar']) ? $settings['time-bar'] : '{c2r-path}/site-assets/images/bnp-header/clock_new.png',

	'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
	'time-convertion' => (isset($settings['time-convertion'])) ? $settings['time-convertion'] : '1',
	'startgame-btn' => (isset($settings['startgame-btn']) && !empty($settings['startgame-btn'])) ? $settings['startgame-btn'] : '{c2r-path}/site-assets/images/bnp-quiz/bg_touch.png',
	'touch-start' => isset($settings['touch-start']) && !empty($settings['touch-start']) ? $settings['touch-start'] : 'Toca para jogar!',

	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',

	'time' => (isset($settings['time'])) ? $settings['time'] : '60',
	'points-per-question' => (isset($settings['points-per-question'])) ? $settings['points-per-question'] : 50,
	'total-question' => $settings['number_questions'],

	'app-logo-hidden' => !isset($app_logo) || empty($app_logo) ? 'hidden' : '',
	'app-logo' => isset($app_logo) && !empty($app_logo) && !is_bool($app_logo) ? $app_logo : $settings['app-logo'],
	'app-bg' => (isset($settings["app-bg"]) && !empty($settings["app-bg"])) ? $settings["app-bg"] : (isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-quiz/bg.png'),
	'question-list' => $toReturn,
	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
