<?php

$page_tpl = bo3::load("bnp-tapgame.tpl");

include "pages-e/header-game.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_tapgame::return_settings();
$screensaver_settings = bnp_screensaver::return_settings();

$chances = [
	"logo" => (isset($settings['logo-chance'])) ? $settings['logo-chance'] : 30,
	'buff' => (isset($settings['buff-chance'])) ? $settings['buff-chance'] : 10,
	'debuff' => (isset($settings['debuff-chance'])) ? $settings['debuff-chance'] : 40
];

$app_logo = _global_::getAppLogo($configs["app-type"]);

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,
	'header-bg' => (isset($settings['header-bg']) && !empty($settings['header-bg'])) ? $settings['header-bg'] : '{c2r-path}/site-assets/images/bnp-header/bg_new.png',
	'header-clock' => isset($settings['time-bar']) && !empty($settings['time-bar']) ? $settings['time-bar'] : '{c2r-path}/site-assets/images/bnp-header/clock_new.png',

	'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',
	'time' => (isset($settings['time'])) ? $settings['time'] : 60,
	'columns' => (isset($settings['columns'])) ? $settings['columns'] : 4,
	'buff' => (isset($settings['buff'])) ? $settings['buff'] : 10,
	'debuff' => (isset($settings['debuff'])) ? $settings['debuff'] : -10,
	'active' => (isset($settings['active'])) ? $settings['active'] : 10,
	'objects' => (isset($settings['objects'])) ? $settings['objects'] : "",
	'game-speed' => (isset($settings['game-speed'])) ? $settings['game-speed']*1000 : 10000,
	'speed' => (isset($settings['game-speed'])) ? $settings['game-speed'] : 5,
	'max-speed' => (isset($settings['max-speed'])) ? $settings['max-speed'] : 10,
	'game-area-start' => (isset($settings['game-area-start'])) ? $settings['game-area-start'] : 300,
	'game-area-end' => (isset($settings['game-area-end'])) ? $settings['game-area-end'] : 1500,
	'game-area-active' => (isset($settings['game-area-active'])) ? $settings['game-area-active'] : 400,
	'time-increase-velocity' => (isset($settings['time-increase'])) ? $settings['time-increase']*1000 : 6000,
	'chances' => json_encode($chances),
	'logo-chance' => (isset($settings['logo-chance'])) ? $settings['logo-chance'] : 30,
	'buff-chance' => (isset($settings['buff-chance'])) ? $settings['buff-chance'] : 10,
	'debuff-chance' => (isset($settings['debuff-chance'])) ? $settings['debuff-chance'] : 30,
	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'd-none' : '',
	'app-logo' => isset($app_logo) && !empty($app_logo) && !is_bool($app_logo) ? $app_logo : $settings['app-logo'],
	'app-bg' => (isset($configs["app-bg"]) && !empty($configs["app-bg"])) ? $configs["app-bg"] : '{c2r-path}/site-assets/images/bnp-tapgame/bg.png',
	'start-btn' => (isset($settings["start-btn"]) && !empty($settings["start-btn"])) ? $settings["start-btn"] : "{c2r-path}/site-assets/images/bnp-memory/bg_touch.png",
	'play-text' => (isset($settings["play-text"]) && !empty($settings["play-text"])) ? $settings["play-text"] : "",
	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
