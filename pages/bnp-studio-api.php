<?php

function saveJPG ($img = false, $path, $name) {
	if (!is_bool($img)) {
		if (_global_::folder("uploads/studio", $path)) {
			$path = "uploads/studio/".$path."/".$name.'.jpg';
			$imgData = substr($img, 1 + strrpos($img, ','));
			file_put_contents($path, base64_decode($imgData));
		}
		//$image = imagecreatefromjpeg($path);
		//$rotate = imagerotate($image, "-90", 0);
		//imagejpeg($rotate, $path);
	}
	return json_encode(['status'=> true]);
}

function savePNG ($img = false, $path, $name) {
	if (!is_bool($img)) {
		if (_global_::folder("uploads/studio", $path)) {
			$path = "uploads/studio/".$path."/".$name.'.png';
			$imgData = substr($img, 1 + strrpos($img, ','));
			file_put_contents($path, base64_decode($imgData));
		}
		//$image = imagecreatefrompng($path);
		//$rotate = imagerotate($image, "-90", 0);
		//imagepng($rotate, $path);
	}
	return json_encode(['status'=> true]);
}

function saveSelectedPhoto ($img = false) {
	if ($img !== false) {
		$filename = ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s'));
		$path = "uploads/{$filename}.jpg";
		$imgData = substr($img, 1 + strrpos($img, ','));
		file_put_contents($path, base64_decode($imgData));
		$img = imagecreatefromjpeg($path);
		imagejpeg($img, $path, 90);

		setcookie("photo", $path, 0, "/");
		setcookie("avatar", saveAvatar($path), 0, "/");

		// RUN IF SERVICE IS ACTIVE
		$printer_settings = bnp_printer::return_settings();
		if ((bool)$printer_settings["photo_status"]) {
			$size = getimagesize($path);
			$size['width'] = $size[0];
			$size['height'] = $size[1];
			$size['new-width'] = $size['height'] * (float)$printer_settings["photo_ratio"];
			$size['new-height'] = $size['height'];
			if ($size['new-width'] <= $size['width'] ) {
				$config = ['x' => ($size['width'] - $size['new-width']) / 2, 'y' => 0, 'width' => $size['new-width'], 'height' => $size['new-height']];
			} else {
				$size['new-width'] = $size['width'];
				$size['new-height'] = $size['width'] / (float)$printer_settings["photo_ratio"];
				$config = ['x' => 0, 'y' => ($size['height'] - $size['new-height']) / 2, 'width' => $size['new-width'], 'height' => $size['new-height']];
			}
			crop_img ('JPG', $path, $config);
			printService($printer_settings, $filename);
		}

		return json_encode(['status' => true]);
	}

	return json_encode(['status' => false]);
}

function printService ($printer_settings, $filename) {
	$html2pdf = new Html2Pdf($printer_settings['orientation'], [(int)$printer_settings['width'],(int)$printer_settings['height']], 'en', true, 'UTF-8', [0,0,0,0]);
	$html2pdf->pdf->SetDisplayMode('real');
	$html2pdf->writeHTML(bo3::c2r( ['filename' => $filename], bo3::loade('api/pdf.tpl')));
	$html2pdf->output("uploads/pdf/{$filename}.pdf", 'F');
	rename("uploads/pdf/{$filename}.pdf", "uploads/pdf/{$printer_settings['folder_photo_sp']}/{$filename}.pdf");
}

function crop_img ($format = 'PNG', $file, $config = ['x' => 0, 'y' => 0, 'width' => 0, 'height' => 0]) {
	$path = explode("/", $file);
	$path = "{$path[0]}/resized/{$path[1]}";
	if ($format == 'PNG') {
		$img = imagecreatefrompng($file);
	} else if ($format == 'JPG') {
		$img = imagecreatefromjpeg($file);
	} else {
		return false;
	}
	$toReturn = imagecrop($img, $config);
	if ($toReturn !== false) {
		if ($format == 'PNG') {
			if (imagepng($toReturn, $path)) {
				return true;
			}
		} else if ($format == 'JPG') {
			if (imagejpeg($toReturn, $path)) {
				return true;
			}
		}
	}

	return false;
}

function makeitrain($id = 0) {

}

switch ($a) {
	case 'saveImg':
		savePNG($_POST["overlay"], $_COOKIE["id"], "overlay");

		foreach ($_POST["pictures"] as $index => $image) {
			$filename = str_pad($index, 4, "0", STR_PAD_LEFT );
			saveJPG($image, $_COOKIE["id"], $filename);
			file_put_contents(
				sprintf("uploads/studio/%s/files.txt", $_COOKIE["id"]),
				sprintf("file '/opt/lampp/htdocs/uploads/studio/%s/%s.jpg'", $_COOKIE["id"], $filename).PHP_EOL
				// sprintf("file 'C:\\xampp\\htdocs\\bnp\\uploads\\studio\\%s\\%s.jpg'", $_COOKIE["id"], $filename).PHP_EOL
			, FILE_APPEND | LOCK_EX);
		}
		$tpl = "";
		break;
	case "saveVid":
		foreach ($_POST["pictures"] as $index => $image) {
			$filename = str_pad($index, 4, "0", STR_PAD_LEFT );
			saveJPG($image, $_COOKIE["id"], $filename);
			file_put_contents(
				sprintf("uploads/studio/%s/files.txt", $_COOKIE["id"]),
				sprintf("file '/opt/lampp/htdocs/uploads/studio/%s/%s.jpg'", $_COOKIE["id"], $filename).PHP_EOL
				// sprintf("file 'C:\\xampp\\htdocs\\bnp\\uploads\\studio\\%s\\%s.jpg'", $_COOKIE["id"], $filename).PHP_EOL
			, FILE_APPEND | LOCK_EX);
		}
		$tpl = "";
		break;
	default:
		$tpl = '{}';
		break;
}
