<?php

$page_tpl = bo3::load("bnp-prize.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_stock::return_settings();
$items = bnp_stock::returnItems();

$screensaver_settings = bnp_screensaver::return_settings();

$schedule = bnp_ranking::returnCurrentSchedule();

if (!is_null($schedule) && !empty($schedule)) {

	if ($settings['accept-check']) {
		if (isset($_COOKIE["accept"]) && $_COOKIE["accept"] != "1") {
			header("Location: {$settings['target']}");
			die();
		}
	}

	$prize = json_decode(bnp_stock::returnRafflePrize());

	if ($prize->status) {
		foreach ($items as $key => $item) {
			if (!isset($items_list)) {
				$items_list = "";
				$item_tpl = bo3::loade('bnp-prize/item.tpl');
			}

			$items_list .= bo3::c2r([
				'id' => $item->id,
				'file' => $item->image,
				'message' => $item->message
			], $item_tpl);
		}

		// CREATE COOKIE FOR THIS PRIZE TO USE IN OTHER MODULES AT FRONT
		setcookie("prize", json_encode($prize->object), 0, "/");

		// DECREASE STOCK & ADD STOCK LOG
		bnp_stock::decreaseStock (
			isset($_COOKIE['id']) ? (int)isset($_COOKIE['id']) : false,
			$prize->object->id
		);
	} else {
		header("Location: {$settings['target']}");
		die();
	}
} else {
	header("Location: {$settings['target']}");
	die();
}

if (isset($configs["app-type"]) && !empty($configs["app-type"])) {
	$query = sprintf(
		"SELECT * FROM %s_bnp_%s_settings WHERE name = '%s'",
		$cfg->db->prefix, $configs["app-type"], 'app-logo'
	);

	$source = $db->query($query);

	$data = $source->fetch_object();

	$app_logo = $data->value;
}

if (isset($configs["app-type"]) && !empty($configs["app-type"])) {
	$query = sprintf(
		"SELECT * FROM %s_bnp_%s_settings WHERE name = '%s'",
		$cfg->db->prefix, $configs["app-type"], 'app-logo'
	);

	$source = $db->query($query);

	$data = $source->fetch_object();

	$app_logo = $data->value;
}

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'items' => isset($items_list) ? $items_list : '',

	'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
	'prize-btn' => (isset($settings['prize-btn']) && !empty($settings['prize-btn'])) ? $settings['prize-btn'] : '{c2r-path}/site-assets/images/bnp-prize/prize-btn.svg',
	'prize-init' => (isset($settings['prize-init']) && !empty($settings['prize-init'])) ? $settings['prize-init'] : 'Toca no icon<br>e descobre o teu brinde!',

	'js-target' => isset($settings['target']) && !empty($settings['target']) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',

	'app-logo-hidden' => !isset($app_logo) || empty($app_logo) ? 'hidden' : '',
	'app-logo' => isset($app_logo) ? $app_logo : '',
	'app-bg' => (isset($configs["app-bg"]) && !empty($configs["app-bg"])) ? $configs["app-bg"] : '{c2r-path}/site-assets/images/bnp-prize/bg.png',
	'client-logo' => isset($configs["client-logo"]) && !empty($configs["client-logo"]) ? $configs["client-logo"] : "{c2r-path}/site-assets/images/logo.svg",
	'client-logo-display' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? 'd-block' : 'd-none',

	'rounds-min' => (isset($winner_id)) ? $winner_id : '10',
	'rounds-max' => (isset($winner_id)) ? $winner_id : '15',
	'winner-id' => (isset($prize->object->id)) ? $prize->object->id : 'false',

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
