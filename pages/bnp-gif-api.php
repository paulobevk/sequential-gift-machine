<?php

function saveGif ($img = false) {
	global $cfg, $db;
	if (!is_bool($img)) {
		$path = 'uploads/render/'. ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s')) .'.gif';
		$imgData = substr($img, 1+strrpos($img, ','));
		file_put_contents($path, base64_decode($imgData));

		if(isset($_COOKIE['id'])) {
			$query = sprintf(
				"INSERT INTO %s_files (file, type, module, user_id) VALUES ('%s', '%s', '%s', %s)",
				$cfg->db->prefix, 'render/'. ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s')) .'.gif', 'gif', 'bnp-users', $_COOKIE['id']
			);

			$db->query($query);
		}

		setcookie("photo", $path, 0, "/");
	}
	return json_encode(['status'=> true]);
}

function savePhoto ($img = false, $id = 0) {
	if (!is_bool($img)) {
		$path = 'uploads/raw/'. ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s')) .'_'.$id.'.jpg';
		$imgData = substr($img, 1+strrpos($img, ','));
		file_put_contents($path, base64_decode($imgData));

		if ($id == 0) {
			// setcookie("photo", $path, 0, "/");
			setcookie("avatar", saveAvatar($path), 0, "/");
		}
	}
	return json_encode(['status'=> true]);
}

function saveAvatar ($img = false) {
	if ($img != false) {
		$file_name = explode('/', $img);
		$file_name = $file_name[count($file_name) - 1];
		$size = getimagesize($img);
		$ratio = $size[0]/$size[1]; // width/height
		if ($ratio > 1) {
			$width = 150;
			$height = 150 / $ratio;
		} else {
			$width = 150 * $ratio;
			$height = 150;
		}
		$src = imagecreatefromstring(file_get_contents($img));
		$dst = imagecreatetruecolor($width, $height);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
		imagedestroy($src);
		imagejpeg($dst, "uploads/avatar/{$file_name}");
		imagedestroy($dst);
		return $file_name;
	}
}

switch ($a) {
	case 'saveGif':
		$tpl = saveGif((isset($_POST['img'])) ? $_POST['img'] : null);
		break;
	case 'savePhoto':
		$tpl = savePhoto((isset($_POST['img'])) ? $_POST['img'] : null, $_POST['id'] );
		break;
	default:
		$tpl = '{}';
		break;
}
