<?php

$page_tpl = bo3::load("user-login.tpl");

include "pages-e/header-user-login.php";
include "pages-e/footer.php";

if (isset($_POST["submit"])) {
	if (isset($_POST["email"]) && isset($_POST["password"]) && !empty($_POST["email"]) && !empty($_POST["password"])) {
		if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
			$user = new user();
			$user->setEmail($_POST["email"]);
			$user->setStatus(true);
			$user_obj = $user->returnOneUserByEmail();
			if ($user_obj !== false) {
				if ($user_obj->password == user::getSecurePassword($_POST["password"]) && $user_obj->rank != "member") {
					if (setcookie($cfg->system->cookie,"{$user_obj->id}.{$user_obj->password}",time() + ($cfg->system->cookie_time * 60),(!empty($cfg->system->path)) ? $cfg->system->path : "/")) {
						header("Location: {$cfg->system->path}/{$lg_s}/user-dashboard/");
					}
				} else {
					$return_message = $lang["user-login"]["invalid-data"];
				}
			} else {
				$return_message = $lang["user-login"]["invalid-data"];
			}
		} else {
			$return_message = $lang["user-login"]["invalid-email"];
		}
	}
}

$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,
	'return-message' => isset($return_message) ? $return_message : ""
], $page_tpl);
