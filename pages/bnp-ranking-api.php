<?php

function set () {
	global $cfg, $db;

	$toReturn = [
		'status' => false,
		'message' => '',
		'object' => []
	];

	if (isset($_COOKIE['id']) && isset($_COOKIE['points']) && isset($_COOKIE['time']) && isset($_COOKIE['accept']) && $_COOKIE['accept'] == 1) {
		$query = sprintf(
			"INSERT INTO %s_bnp_ranking (`user_id`, `points`, `status`, `date`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$_COOKIE['id'],
			$_COOKIE['points'],
			1,
			date('Y-m-d H:i:s')
		);

		$toReturn['status'] = $db->query($query);
	}
	return json_encode($toReturn);
}

switch ($a) {
	case 'set':
		$tpl = set();
		break;

	default:
		$tpl = json_encode(
			[
				'status' => false,
				'message' => 'default error',
				'object' => []
			]
		);
		break;
}
