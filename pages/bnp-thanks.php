<?php

$page_tpl = bo3::load("bnp-thanks.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

// SET USER AS FINISHED
if (isset($_COOKIE['id'])) {
	$query = sprintf(
		"UPDATE %s_bnp_users SET status = 1 WHERE id = %s LIMIT %s",
		$cfg->db->prefix, (int)$_COOKIE['id'], 1
	);

	$db->query($query);
}

$configs = bnp_config::return_settings();
$settings = bnp_thanks::return_settings();
$screensaver_settings = bnp_screensaver::return_settings();

if (isset($configs["app-type"]) && !empty($configs["app-type"])) {
	$query = sprintf(
		"SELECT * FROM %s_bnp_%s_settings WHERE name = '%s'",
		$cfg->db->prefix, $configs["app-type"], 'app-logo'
	);

	$source = $db->query($query);

	$data = $source->fetch_object();

	$app_logo = $data->value;
}

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'app-logo-hidden' => !isset($app_logo) || empty($app_logo) ? 'hidden' : '',
	'app-logo' => isset($app_logo) ? $app_logo : '',
	'app-bg' => isset($settings["app-bg"]) && !empty($settings["app-bg"]) ? $settings["app-bg"] : (isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-signup/bg.png'),
	'client-logo' => isset($configs["client-logo"]) && !empty($configs["client-logo"]) ? $configs["client-logo"] : "{c2r-path}/site-assets/images/logo.svg",
	'client-logo-display' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? 'd-block' : 'd-none',

	'thanks-image' => isset($settings['thanks-image']) && !empty($settings['thanks-image']) ? $settings['thanks-image'] : '{c2r-path}/site-assets/images/bnp-thanks/thumbsup.svg',
	'thanks-text' => isset($settings['thanks-text']) && !empty($settings['thanks-text']) ? $settings['thanks-text'] : 'Obrigado<br>por participares!',

	'css' => (isset($settings['css']) && !empty($settings['css'])) ? $settings['css'] : '',

	'js-target' => isset($settings['target']) && !empty($settings['target']) ? $settings['target'] : '',
	'js-timeout' => isset($settings['timeout']) && !empty($settings['timeout']) ? (int)$settings['timeout'] : 5,
	'id' => isset($_COOKIE['id']) ? $_COOKIE['id'] : '',
	'signup' => isset($_COOKIE['signup']) ? $_COOKIE['signup'] : '',

	'js-screensaver-status' => isset($screensaver_settings['status']) && !empty($screensaver_settings['status']) ? $screensaver_settings['status'] : 'false',
	'js-screensaver-time' => isset($screensaver_settings['time']) && !empty($screensaver_settings['time']) ? $screensaver_settings['time'] : 'false'
], $page_tpl);

// REMOVE COOKIES FROM SERVICE
setcookie('id', null, -1, '/');
setcookie('points', null, -1, '/');
setcookie('time', null, -1, '/');
setcookie('avatar', null, -1, '/');
setcookie('photo', null, -1, '/');
setcookie('signup', null, -1, '/');
setcookie('accept', null, -1, '/');
