<?php

$page_tpl = bo3::load("bnp-gift-h.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_gift::return_settings();
$items = bnp_stock::returnItems();

if (is_array($items)) {
	foreach ($items as $key => $item) {
		if (!isset($items_list)) {
			$items_list = "";
			$item_tpl = bo3::loade('bnp-gift/item.tpl');
		}

		$items_list .= bo3::c2r([
			'id' => $item->id,
			'file' => $item->image,
			'message' => $item->message
		], $item_tpl);
	}
}

$prize = json_decode(bnp_stock::returnRafflePrize());

$screensaver_settings = bnp_screensaver::return_settings();

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'items' => isset($items_list) ? $items_list : '',
	'items-list' => json_encode($items),

	'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
	'prize-init' => (isset($settings['prize-init']) && !empty($settings['prize-init'])) ? $settings['prize-init'] : 'Puxa a alavanca e Ganha Prémios!',

	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',

	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'hidden' : '',
	'app-logo' => isset($settings['app-logo']) && !empty($settings['app-logo']) ? $settings['app-logo'] : '',

	'app-bg' => isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-gift/bg.png',
	'client-logo' => isset($settings['client-logo']) && !empty($settings['client-logo']) ? $settings['client-logo'] : '{c2r-path}/site-assets/images/logo.svg',

	'winner-id' => (isset($prize->object->item_id)) ? $prize->object->item_id : 'false',

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? (int)$screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
