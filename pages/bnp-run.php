<?php

$page_tpl = bo3::load("bnp-run.tpl");

$config = bnp_config::return_settings();

if(!isset($config["project-name"]) || empty($config["project-name"])) {
	header("location: {$cfg->system->path}/{$lg_s}/bnp-install/");
}

/* last thing */
$tpl = bo3::c2r( [
	"run" => (isset($config["run"]) && !empty($config["run"])) ? $config["run"] : "bnp-screensaver",
	"run-timeout" => (isset($config["run-timeout"]) && !empty($config["run-timeout"])) ? $config["run-timeout"] : 5000
], $page_tpl );
