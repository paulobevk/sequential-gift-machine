<?php

$page_tpl = bo3::load("bnp-bike-video.tpl");

include "pages-e/header-game.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_bike::return_settings();
$screensaver_settings = bnp_screensaver::return_settings();

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,

	'footer' => $footer,

    'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
	'video' => isset($settings['video']) ? $settings['video'] : '{c2r-path}/uploads/bike/bike.mp4',
	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',

	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'd-none' : '',
	'app-logo' => isset($settings['app-logo']) && !empty($settings['app-logo']) ? $settings['app-logo'] : '',
	'app-bg' => isset($settings["app-bg"]) && !empty($settings["app-bg"]) ? $settings["app-bg"] : (isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-signup/bg.png'),

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "0",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : false
], $page_tpl);
