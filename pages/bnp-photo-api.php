<?php

function savePhoto ($img = false) {
	if (!is_bool($img)) {
		$path = 'uploads/raw/'. ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s')) .'.jpg';
		$imgData = substr($img, 1+strrpos($img, ','));
		file_put_contents($path, base64_decode($imgData));
	}
	return json_encode(['status'=> true]);
}

function saveSelectedPhoto ($img = false) {
	if ($img !== false) {
		$filename = ((isset($_COOKIE['id'])) ? $_COOKIE['id'] : date('Y_m_d_H_i_s'));
		$path = "uploads/{$filename}.jpg";
		$imgData = substr($img, 1 + strrpos($img, ','));
		file_put_contents($path, base64_decode($imgData));
		$img = imagecreatefromjpeg($path);
		imagejpeg($img, $path, 90);

		if (isset($_COOKIE['id']) && (int)$_COOKIE['id'] != 0) {
			insert_file ($_COOKIE['id'], $filename.".jpg");
		}

		setcookie("photo", $path, 0, "/");

		// RUN IF SERVICE IS ACTIVE
		$printer_settings = bnp_printer::return_settings();
		if ((bool)$printer_settings["photo_status"]) {
			$size = getimagesize($path);
			$size['width'] = $size[0];
			$size['height'] = $size[1];
			$size['new-width'] = $size['height'] * (float)$printer_settings["photo_ratio"];
			$size['new-height'] = $size['height'];
			if ($size['new-width'] <= $size['width'] ) {
				$config = ['x' => ($size['width'] - $size['new-width']) / 2, 'y' => 0, 'width' => $size['new-width'], 'height' => $size['new-height']];
			} else {
				$size['new-width'] = $size['width'];
				$size['new-height'] = $size['width'] / (float)$printer_settings["photo_ratio"];
				$config = ['x' => 0, 'y' => ($size['height'] - $size['new-height']) / 2, 'width' => $size['new-width'], 'height' => $size['new-height']];
			}

			crop_img ('JPG', $path, $config);
			printService($printer_settings, $filename);
		}

		return json_encode(['status' => true]);
	}

	return json_encode(['status' => false]);
}

function printService ($printer_settings, $filename) {
	$html2pdf = new Html2Pdf($printer_settings['orientation'], [(int)$printer_settings['width'],(int)$printer_settings['height']], 'en', true, 'UTF-8', [0,0,0,0]);
	$html2pdf->pdf->SetDisplayMode('real');
	$html2pdf->writeHTML(bo3::c2r( ['filename' => $filename], bo3::loade('api/pdf.tpl')));
	$html2pdf->output("uploads/pdf/{$filename}.pdf", 'F');
	rename("uploads/pdf/{$filename}.pdf", "uploads/pdf/{$printer_settings['folder_photo_sp']}/{$filename}.pdf");

	//shell_exec("lpr /opt/lampp/htdocs/photomaton/bnp/pdf/pdf2print/{$filename}.pdf"); // print cmd for linux
}

function crop_img ($format = 'PNG', $file, $config = ['x' => 0, 'y' => 0, 'width' => 0, 'height' => 0]) {
	$path = explode("/", $file);
	$path = "{$path[0]}/resized/{$path[1]}";

	if ($format == 'PNG') {
		$img = imagecreatefrompng($file);
	} else if ($format == 'JPG') {
		$img = imagecreatefromjpeg($file);
	} else {
		return false;
	}
	$toReturn = imagecrop($img, $config);
	if ($toReturn !== false) {
		if ($format == 'PNG') {
			if (imagepng($toReturn, $path)) {
				return true;
			}
		} else if ($format == 'JPG') {
			if (imagejpeg($toReturn, $path)) {
				return true;
			}
		}
	}

	return false;
}

function insert_file ($id, $file) {
	global $cfg, $db;

	return (bool)$db->query(sprintf(
		"INSERT INTO %s_files (file, type, module, id_ass, description, code, sort, user_id) VALUES ('%s', '%s','%s','%s','%s','%s','%s','%s')",
		$cfg->db->prefix, $file, 'img', 'bnp-users', 0, '', '', 0, $id
	));
}

switch ($a) {
	case 'savePhoto':
		$tpl = savePhoto((isset($_POST['img'])) ? $_POST['img'] : null);
		break;
	case 'saveSelectedPhoto':
		$tpl = saveSelectedPhoto((isset($_POST['img'])) ? $_POST['img'] : null);
		break;

	default:
		$tpl = '{}';
		break;
}
