<?php

$page_tpl = bo3::load("bnp-studio.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$settings = bnp_photo::return_settings();
$mode = bnp_photo::return_mode_settings();
$filters = !empty($settings['filters']) ? explode(', ', $settings['filters']) : [];
$overlays = !empty($settings['overlays']) ? explode(', ', $settings['overlays']) : [];

$screensaver_settings = bnp_screensaver::return_settings();

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'd-none' : '',
	'app-logo' => isset($settings['app-logo']) && !empty($settings['app-logo']) ? $settings['app-logo'] : '',
	'app-bg' => isset($settings['app-bg']) && !empty($settings['app-bg']) ? $settings['app-bg'] : '{c2r-path}/site-assets/images/bnp-signup/bg.png',
	'client-logo' => isset($settings['client-logo']) && !empty($settings['client-logo']) ? $settings['client-logo'] : '{c2r-path}/site-assets/images/logo.svg',

	'settings-css' => (isset($settings['css']) && !empty($settings['css'])) ? $settings['css'] : '',

	'sight' => isset($settings['sight']) && !empty($settings['sight']) ? $settings['sight'] : '{c2r-path}/site-assets/images/bnp-photo/sight-default.png',
	'filters' => json_encode($filters),
	'overlays' => json_encode($overlays),
	'overlay-system' => isset($settings["mode"]) && $settings["mode"] == "cinemagraph" ? bo3::loade("bnp-photo-gif/video-overlay.tpl") : bo3::loade("bnp-photo-gif/overlays.tpl"),
	'final-overlay' => isset($settings["mode"]) && $settings["mode"] != "cinemagraph" ? bo3::loade("bnp-photo-gif/final-overlay.tpl") : "",

	'settings-image-width' =>  (isset($settings['image-width'])) ? (int)$settings['image-width'] : (int)1920,
	'settings-image-height' =>  (isset($settings['image-height'])) ? (int)$settings['image-height'] : (int)1080,
	"settings-format" =>(isset($settings['format'])) ? $settings['format'] : "9:16",
	"settings-mode" => (isset($settings['mode'])) ? $settings['mode'] : "photo",
	"settings-gif-quality" => (isset($settings['gif_quality'])) ? $settings['gif_quality'] : 0.75,
	"settings-gif-quantity" => (isset($mode->quantity)) ? $mode->quantity : 120,
	"settings-gif-delay" => (isset($mode->delay)) ? $mode->delay : 1000/60,
	"settings-gif-render-delay" => (isset($mode->render_delay)) ? $mode->render_delay : 1000/30,
	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',

	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',
	'button-bg' => isset($settings['button-bg']) && !empty($settings['button-bg']) ? $settings['button-bg'] : '',

	'step1-intro' => isset($settings['step1-intro']) && !empty($settings['step1-intro']) ? $settings['step1-intro'] : '',
	'step1-button' => isset($settings['step1-button']) && !empty($settings['step1-button']) ? $settings['step1-button'] : '',
	'step1-button-text' => isset($settings['step1-button-text']) && !empty($settings['step1-button-text']) ? $settings['step1-button-text'] : '',

	'step2-clock' => isset($settings['step2-clock']) && !empty($settings['step2-clock']) ? $settings['step2-clock'] : '{c2r-clock}/site-assets/images/bnp-photo/2_clock_icon.png',
	'step2-intro' => isset($settings['step2-intro']) && !empty($settings['step2-intro']) ? $settings['step2-intro'] : '',

	'step3-intro' => isset($settings['step3-intro']) && !empty($settings['step3-intro']) ? $settings['step3-intro'] : '',
	'step3-text-repeat' => isset($settings['step3-text-repeat']) && !empty($settings['step3-text-repeat']) ? $settings['step3-text-repeat'] : '',
	'step3-text-accept' => isset($settings['step3-text-accept']) && !empty($settings['step3-text-accept']) ? $settings['step3-text-accept'] : '',
	'step3-button-repeat' => isset($settings['step3-button-repeat']) && !empty($settings['step3-button-repeat']) ? $settings['step3-button-repeat'] : '{c2r-path}/site-assets/images/bnp-photo/bg_repeat.png',
	'step3-button-accept' => isset($settings['step3-button-accept']) && !empty($settings['step3-button-accept']) ? $settings['step3-button-accept'] : '{c2r-path}/site-assets/images/bnp-photo/bg_next.png',
	'step3-5-intro' => isset($settings['step3-5-intro']) && !empty($settings['step3-5-intro']) ? $settings['step3-5-intro'] : '',

	'step4-check' => isset($settings['step4-check']) && !empty($settings['step4-check']) ? $settings['step4-check'] : '{c2r-path}/site-assets/images/bnp-photo/1_visto.png',
	'step4-button' => isset($settings['step4-button']) && !empty($settings['step4-button']) ? $settings['step4-button'] : '{c2r-path}/site-assets/images/bnp-photo/bg_next.png',
	'step4-intro' => isset($settings['step4-intro']) && !empty($settings['step4-intro']) ? $settings['step4-intro'] : '',
	'step4-text-accept' => isset($settings['step4-text-accept']) && !empty($settings['step4-text-accept']) ? $settings['step4-text-accept'] : '',

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
