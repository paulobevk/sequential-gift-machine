<?php

$page_tpl = bo3::load("bnp-gif.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$settings = bnp_gif::return_settings();

if (isset($settings['overlays'])) {
	$settings['overlays'] = explode(", ", $settings['overlays']);
}

if (isset($settings['overlays']) && is_array($settings['overlays'])) {
	foreach($settings['overlays'] as $i => $item) {
		if(!isset($overlays_list)) {
			$overlays_list = "";
		}

		$overlays_list .= bo3::c2r([
			'step4-check' => isset($settings['step4-check']) && !empty($settings['step4-check']) ? $settings['step4-check'] : '',
		], bo3::loade("bnp-gif/overlay.tpl"));
	}
}

/* last thing */
$tpl = bo3::c2r([
	"header" => $header,
	"footer" => $footer,

	'css' => isset($settings['css']) ? $settings['css'] : '',
	'settings-snap-width' =>  (isset($settings['snap-width'])) ? (int)$settings['snap-width'] : (int)1440,
	'settings-snap-height' =>  (isset($settings['snap-height'])) ? (int)$settings['snap-height'] : (int)1080,
	'settings-snap-limit' =>  (isset($settings['snap-limit'])) ? (int)$settings['snap-limit'] : (int)5,
	'settings-snap-interval' =>  (isset($settings['snap-interval'])) ? (int)$settings['snap-interval'] : (int)250, //250
	'settings-gif-quality' =>  (isset($settings['gif-quality'])) ? (int)$settings['gif-quality'] : (int)70, //70

	'app-bg' => isset($settings['app-bg']) && !empty($settings['app-bg']) ? $settings['app-bg'] : '',
	'client-logo' => isset($configs["client-logo"]) && !empty($configs["client-logo"]) ? $configs["client-logo"] : "{c2r-path}/site-assets/images/logo.svg",
	'client-logo-display' => isset($settings['client-logo']) && !empty($settings['client-logo']) ? "d-block" : 'd-none',

	'button-bg' => isset($settings['button-bg']) && !empty($settings['button-bg']) ? $settings['button-bg'] : '',

	'sight' => isset($settings['sight']) && !empty($settings['sight']) ? $settings['sight'] : '',
	'step1-intro' => isset($settings['step1-intro']) && !empty($settings['step1-intro']) ? $settings['step1-intro'] : '',
	'step1-button-text' => isset($settings['step1-button-text']) && !empty($settings['step1-button-text']) ? $settings['step1-button-text'] : '',

	'step2-clock' => isset($settings['step2-clock']) && !empty($settings['step2-clock']) ? $settings['step2-clock'] : '',
	'step2-clock-display' => isset($settings['step2-clock']) && !empty($settings['step2-clock']) ? 'd-block' : 'd-none',
	'step2-intro' => isset($settings['step2-intro']) && !empty($settings['step2-intro']) ? $settings['step2-intro'] : '',

	'step3-intro' => isset($settings['step3-intro']) && !empty($settings['step3-intro']) ? $settings['step3-intro'] : '',
	'step3-text-repeat' => isset($settings['step3-text-repeat']) && !empty($settings['step3-text-repeat']) ? $settings['step3-text-repeat'] : '',
	'step3-text-accept' => isset($settings['step3-text-accept']) && !empty($settings['step3-text-accept']) ? $settings['step3-text-accept'] : '',
	'step3-button-repeat' => isset($settings['step3-button-repeat']) && !empty($settings['step3-button-repeat']) ? $settings['step3-button-repeat'] : '{c2r-path}/site-assets/images/bnp-photo/bg_repeat.png',
	'step3-button-accept' => isset($settings['step3-button-accept']) && !empty($settings['step3-button-accept']) ? $settings['step3-button-accept'] : '{c2r-path}/site-assets/images/bnp-photo/bg_next.png',

	'step3-5-intro' => isset($settings['step3-5-intro']) && !empty($settings['step3-5-intro']) ? $settings['step3-5-intro'] : '',

	'step4-button' => isset($settings['step4-button']) && !empty($settings['step4-button']) ? $settings['step4-button'] : '',
	'step4-intro' => isset($settings['step4-intro']) && !empty($settings['step4-intro']) ? $settings['step4-intro'] : '',
	'step4-text-accept' => isset($settings['step4-text-accept']) && !empty($settings['step4-text-accept']) ? $settings['step4-text-accept'] : '',

	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'd-none' : '',
	'app-logo' => isset($settings['app-logo']) && !empty($settings['app-logo']) ? $settings['app-logo'] : '',

	'js-target' => isset($settings['target']) && !empty($settings['target']) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',
	'overlays' =>  isset($settings['overlays']) && !empty($settings['overlays']) ? json_encode($settings['overlays']) : 'false',
	'overlays-list' => (isset($overlays_list)) ? $overlays_list : ''
], $page_tpl);
