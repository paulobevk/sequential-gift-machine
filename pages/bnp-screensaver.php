<?php

$page_tpl = bo3::load("bnp-screensaver.tpl");

$settings = bnp_screensaver::return_settings();

if(isset($settings["status"]) && $settings["status"] == TRUE) {
	if(isset($settings["video"]) && !empty($settings["video"])) {
		$videos = json_decode($settings["video"]);
		$n = 0;
		foreach ($videos as $v => $video) {
			if(!isset($videos_list)) {
				$videos_list = "";
			}

			$items_list = "";

			foreach ($video as $i => $item) {
				$items_list .= bo3::c2r([
					"src" => $item,
					"type" => pathinfo($item, PATHINFO_EXTENSION),
				], bo3::loade("bnp-screensaver/video.tpl"));
			}

			$videos_list .= bo3::c2r([
				"active" => ($n == 0) ? "active" : "",
				"videos" => $items_list,
				"width" => (isset($settings["width"])) ? $settings["width"] : "1080",
				"height" => (isset($settings["height"])) ? $settings["height"] : "1920",
			], bo3::loade("bnp-screensaver/videos.tpl"));
			$n ++;
		}
	}

	/* last thing */
	$tpl = bo3::c2r([
		'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',
		"videos-list" => (isset($videos_list)) ? $videos_list : bo3::loade("bnp-screensaver/default.tpl"),
		'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	], $page_tpl );
} else {
	header("Location: {$cfg->system->path}/{$lg_s}/bnp-signup/");
}
