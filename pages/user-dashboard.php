<?php

$page_tpl = bo3::load("user-dashboard.tpl");

include "pages-e/header-user-login.php";
include "pages-e/footer.php";

$modules_list = "";

if ($auth) {
	// PRINTER MODULE
	$printer_settings = bnp_printer::return_settings();

	$modules_list .= bo3::c2r([
		'printer-length' => isset($printer_settings) ? (double)$printer_settings["paper_length"] / 1000 : "",
		'printer-length-printed' => isset($printer_settings) ? (double)$printer_settings["paper_length_printed"] / 1000 : "",
		'printer-units' => isset($printer_settings) ? (int)((double)$printer_settings["paper_length"] / (double)$printer_settings["paper_length_per_print"]) : "",
		'printer-units-printed' => isset($printer_settings) ? (int)((double)$printer_settings["paper_length_printed"] / (double)$printer_settings["paper_length_per_print"]) : "",
	], bo3::loade('bnp-dashboard/mod-printer.tpl'));

	// USERS MODULE
	$user_list = bnp_users::returnUsers();

	foreach ($user_list as $i => $user) {
		if (!isset($user_total)) {
			$user_completed = 0;
			$user_total = 0;
		}
		if ((bool)$user->status) {
			$user_completed++;
		}

		$user_total++;
	}

	$modules_list .= bo3::c2r([
		'completed' => isset($user_completed) ? $user_completed : 0,
		'total' => isset($user_total) ? $user_total : 0
	], bo3::loade('bnp-dashboard/mod-user.tpl'));
} else {
	header("Location: {$cfg->system->path}/{$lg_s}/user-login/");
}

$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'modules-list' => $modules_list
], $page_tpl);
