<?php

function printer_reset_paper () {
	$toReturn = [
		'status' => false,
		'message' => '',
		'object' => []
	];

	$toReturn['status'] = bnp_printer::update_settings([
		'paper_length_printed' => 0
	]);
	
	return json_encode($toReturn);
}

if ($auth) {
	switch ($a) {
		case 'printer_reset_paper':
			$tpl = printer_reset_paper();
			break;
		default:
			$tpl = '{}';
			break;
	}
} else {
	$tpl = json_encode([
		'status' => false,
		'message' => '',
		'object' => []
	]);
}
