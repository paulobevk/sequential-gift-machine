<?php

$page_tpl = bo3::load("bnp-ranking.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$settings = bnp_ranking::return_settings();
$configs = bnp_config::return_settings();

$schedule = bnp_ranking::returnCurrentSchedule();

if (!is_null($schedule) && !empty($schedule)) {
	if ($settings['accept-check']) {
		if (isset($_COOKIE["accept"]) && $_COOKIE["accept"] != "1") {
			header("Location: {$settings['target']}");
			die();
		}
	}

	$top10 = bnp_ranking::returnData(10, $schedule);

	if ($top10 != false) {
		foreach ($top10 as $i => $item) {
			if (!isset($list)) {
				$list = "";
				$line_tpl = bo3::loade('bnp-ranking/line.tpl');
			}

			$list .= bo3::c2r([
				'i' => $i+1,
				//'class' => $item->user_id == (isset($_COOKIE['id']) ? $_COOKIE['id'] : false) ? 'of-top-10' : '',
				'name' => $item->name,
				'points' => $item->points,
				'time' => date('i:s', strtotime($item->date))
			], $line_tpl);
		}
	}

	if (isset($_COOKIE['id'])) {
		$userResult = bnp_ranking::returnResultById($_COOKIE['id'],$schedule);

		if (!empty($userResult)) {
			if (!isset($line_tpl)) {
				$line_tpl = bo3::loade('bnp-ranking/line.tpl');
			}

			$ofTop10 = bo3::c2r([
				'i' => $userResult->position,
				'class' => 'of-top-10',
				'name' => $userResult->name,
				'points' => $userResult->points
			], $line_tpl);
		}
	}
} else {
	header("Location: {$cfg->system->path}/{$lg_s}/bnp-thanks/");
	die();
}

if (isset($configs["app-type"]) && !empty($configs["app-type"])) {
	$query = sprintf(
		"SELECT * FROM %s_bnp_%s_settings WHERE name = '%s'",
		$cfg->db->prefix, $configs["app-type"], 'app-logo'
	);

	$source = $db->query($query);

	$data = $source->fetch_object();

	$app_logo = $data->value;
}

$screensaver_settings = bnp_screensaver::return_settings();

/* last thing */
$tpl = bo3::c2r([
	"header" => $header,
	'footer' => $footer,

	'list' => isset($list) ? $list : '',
	'of-top-10' => isset($ofTop10) && $ofTop10 !== false ? $ofTop10 : '',

	'settings-css' => (isset($settings['css'])) ? $settings['css'] : '',

	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',
	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',
	'redirect' => isset($settings['redirect']) && !empty($settings['redirect']) ? $settings['redirect'] : 'true',
	'user-active' => (isset($userResult) && !empty($userResult)) ? 'd-block' : ' d-none',
	'position-text' => (isset($settings["position-text"]) && !empty($settings["position-text"])) ? $settings["position-text"] : '',

	'app-logo-hidden' => !isset($app_logo) || empty($app_logo) ? 'hidden' : '',
	'app-logo' => isset($app_logo) ? $app_logo : '',
	'app-bg' => (isset($settings["app-bg"]) && !empty($settings["app-bg"])) ? $settings["app-bg"] : (isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-ranking/bg.png'),
	'client-logo' => isset($configs["client-logo"]) && !empty($configs["client-logo"]) ? $configs["client-logo"] : "{c2r-path}/site-assets/images/logo.svg",
	'client-logo-display' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? 'd-block' : 'd-none',
	'js-timeout' => (isset($settings['timeout']) && !empty($settings['timeout'])) ? $settings['timeout'] : 5,
	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
