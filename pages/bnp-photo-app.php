<?php

$page_tpl = bo3::load("bnp-photo-app.tpl");
$o_tpl = bo3::loade("bnp-photo/item.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$settings = bnp_photo_app::return_settings();
$filters = !empty($settings['filters']) ? explode(', ', $settings['filters']) : [];
$overlays = !empty($settings['overlays']) ? explode(', ', $settings['overlays']) : [];

if (count($overlays) > 0) {
	foreach ($overlays as $o => $overlay) {
		if (!isset($overlays_list)) {
			$overlays_list = "";
		}

		$overlays_list .= bo3::c2r([], $o_tpl);
	}
}

$screensaver_settings = bnp_screensaver::return_settings();

/* last thing */
$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'app-logo-hidden' => !isset($settings['app-logo']) || empty($settings['app-logo']) ? 'd-none' : '',
	'app-logo' => isset($settings['app-logo']) && !empty($settings['app-logo']) ? $settings['app-logo'] : '',
	'app-bg' => isset($settings['app-bg']) && !empty($settings['app-bg']) ? $settings['app-bg'] : '{c2r-path}/site-assets/images/bnp-photo/bg.png',
	'client-logo' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? $configs['client-logo'] : '',
	'client-logo-display' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? 'd-block' : 'd-none',

	'css' => (isset($settings['css']) && !empty($settings['css'])) ? $settings['css'] : '',

	'sight' => isset($settings['sight']) && !empty($settings['sight']) ? $settings['sight'] : '{c2r-path}/site-assets/images/bnp-photo/sight-default.png',
	'filters' => json_encode($filters),
	'overlays' => json_encode($overlays),
	"overlay" => (count($overlays) > 0) ? $overlays[0] : "",
	'overlays-list' => isset($overlays_list) && !empty($overlays_list) ? $overlays_list : "",

	'settings-image-width' =>  (isset($settings['image-width'])) ? (int)$settings['image-width'] : (int)1440,
	'settings-image-height' =>  (isset($settings['image-height'])) ? (int)$settings['image-height'] : (int)1080,
	'js-target' => (isset($settings['target']) && !empty($settings['target'])) ? $settings['target'] : '',

	'id' => (isset($_COOKIE['id'])) ? $_COOKIE['id'] : '',
	'signup' => (isset($_COOKIE['signup'])) ? $_COOKIE['signup'] : '',
	'button-bg' => isset($settings['button-bg']) && !empty($settings['button-bg']) ? $settings['button-bg'] : '{c2r-path}/site-assets/images/bnp-photo/bg_take.png',

	'step1-intro' => isset($settings['step1-intro']) && !empty($settings['step1-intro']) ? $settings['step1-intro'] : '',
	'step1-button' => isset($settings['step1-button']) && !empty($settings['step1-button']) ? $settings['step1-button'] : '',
	'step1-button-text' => isset($settings['step1-button-text']) && !empty($settings['step1-button-text']) ? $settings['step1-button-text'] : '',

	'step2-clock' => isset($settings['step2-clock']) && !empty($settings['step2-clock']) ? $settings['step2-clock'] : '{c2r-path}/site-assets/images/bnp-photo/2_clock_icon.png',
	'step2-intro' => isset($settings['step2-intro']) && !empty($settings['step2-intro']) ? $settings['step2-intro'] : '',

	'step3-intro' => isset($settings['step3-intro']) && !empty($settings['step3-intro']) ? $settings['step3-intro'] : '',
	'step3-text-repeat' => isset($settings['step3-text-repeat']) && !empty($settings['step3-text-repeat']) ? $settings['step3-text-repeat'] : '',
	'step3-text-accept' => isset($settings['step3-text-accept']) && !empty($settings['step3-text-accept']) ? $settings['step3-text-accept'] : '',
	'step3-button-repeat' => isset($settings['step3-button-repeat']) && !empty($settings['step3-button-repeat']) ? $settings['step3-button-repeat'] : '{c2r-path}/site-assets/images/bnp-photo/bg_repeat.png',
	'step3-button-accept' => isset($settings['step3-button-accept']) && !empty($settings['step3-button-accept']) ? $settings['step3-button-accept'] : '{c2r-path}/site-assets/images/bnp-photo/bg_next.png',
	'step3-5-intro' => isset($settings['step3-5-intro']) && !empty($settings['step3-5-intro']) ? $settings['step3-5-intro'] : '',

	'step4-check' => isset($settings['step4-check']) && !empty($settings['step4-check']) ? $settings['step4-check'] : '{c2r-path}/site-assets/images/bnp-photo/1_visto.png',
	'step4-button' => isset($settings['step4-button']) && !empty($settings['step4-button']) ? $settings['step4-button'] : '{c2r-path}/site-assets/images/bnp-photo/bg_next.png',
	'step4-intro' => isset($settings['step4-intro']) && !empty($settings['step4-intro']) ? $settings['step4-intro'] : '',
	'step4-text-accept' => isset($settings['step4-text-accept']) && !empty($settings['step4-text-accept']) ? $settings['step4-text-accept'] : '',

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : "false",
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : "false"
], $page_tpl);
