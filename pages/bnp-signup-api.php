<?php

function set ($post) {
	global $cfg, $db;

	$toReturn = [
		'status' => false,
		'message' => '',
		'object' => []
	];
	if (isset($post['name']) && isset($post['email'])) {
		$query = sprintf(
			"INSERT INTO %s_bnp_users (`name`, `email`, `code`, `comercial`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($post['name']),
			$db->real_escape_string(strtolower($post['email'])),
			json_encode($post),
			(int)$post['accept']
		);
		$toReturn['status'] = $db->query($query);

		$toReturn['object']['id'] = $db->insert_id;
	}

	$toReturn['object'] = json_encode($toReturn['object']);

	return json_encode($toReturn);
}

switch ($a) {
	case 'set':
		$tpl = set($_POST);
		break;

	default:
		$tpl = json_encode(
			[
				'status' => false,
				'message' => 'default error',
				'object' => []
			]
		);
		break;
}
