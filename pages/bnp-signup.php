<?php

$page_tpl = bo3::load("bnp-signup.tpl");

include "pages-e/header.php";
include "pages-e/footer.php";

$configs = bnp_config::return_settings();
$settings = bnp_users::return_settings();
$screensaver_settings = bnp_screensaver::return_settings();

$all_fields = bnp_users::returnFields();

$slides = array_chunk($all_fields, 2);

foreach ($slides as $i => $slide) {
	if (!isset($slides_list)) {
		$slides_list = "";
		$indicators_list = "";

		$slide_tpl = bo3::loade("bnp-signup/slide.tpl");
		$indicator_tpl = bo3::loade("bnp-signup/indicator.tpl");
		$field_tpl = bo3::loade("bnp-signup/field.tpl");
	}

	foreach ($slide as $o => $field) {
		if(!isset($fields_list)) {
			$fields_list = "";
		}

		$fields_list .= bo3::c2r([
			'label' => $field->label,
			'name-canonical' => strtolower(bo3::clean($field->name)),
			'name' => $field->name,
			'value' => $field->value,
			'type' => $field->type,
			'error-message' => $field->error_message,
		], $field_tpl);
	}

	$slides_list .= bo3::c2r([
		'list' => isset($fields_list) ? $fields_list : '',
		'active' => empty($slides_list) ? 'active' : ''
	], $slide_tpl);

	$indicators_list .= bo3::c2r([
		'id' => $i + 1,
		'active' => empty($indicators_list) ? 'active' : ''
	], $indicator_tpl);
}

$app_logo = _global_::getAppLogo($configs["app-type"]);

$tpl = bo3::c2r([
	'header' => $header,
	'footer' => $footer,

	'list' => isset($slides_list) ? $slides_list : '',
	'indicator-list' => isset($indicators_list) ? $indicators_list : '',

	'app-logo-hidden' => !isset($app_logo) || empty($app_logo) ? 'hidden' : '',
	'app-logo' => isset($app_logo) ? $app_logo : '',
	'app-terms-bg' => isset($settings['app-terms-bg']) && !empty($settings['app-terms-bg']) ? $settings['app-terms-bg'] : '{c2r-path}/site-assets/images/bnp-signup/5_visto.png',

	'terms-text' => isset($settings['terms-text']) && !empty($settings['terms-text']) ? $settings['terms-text'] : 'Aceito que os meus dados sejam utilizados em ações promocionais da marca',
	'terms-full-text' => isset($settings['terms-full-text']) && !empty($settings['terms-full-text']) ? $settings['terms-full-text'] : '',
	'keyboard-theme' => isset($settings['keyboard-theme']) && !empty($settings['keyboard-theme']) ? $settings['keyboard-theme'] : 'light',

	'app-bg' => isset($settings["app-bg"]) && !empty($settings["app-bg"]) ? $settings["app-bg"] : (isset($configs['app-bg']) && !empty($configs['app-bg']) ? $configs['app-bg'] : '{c2r-path}/site-assets/images/bnp-signup/bg.png'),
	'client-logo' => isset($configs["client-logo"]) && !empty($configs["client-logo"]) ? $configs["client-logo"] : "{c2r-path}/site-assets/images/logo.svg",
	'client-logo-display' => isset($configs['client-logo']) && !empty($configs['client-logo']) ? 'd-block' : 'd-none',

	'css' => (isset($settings['css']) && !empty($settings['css'])) ? $settings['css'] : '',
	'js-target' => isset($settings['target']) && !empty($settings['target']) ? $settings["target"] : "",

	'js-screensaver-status' => (isset($screensaver_settings['status']) && !empty($screensaver_settings['status'])) ? $screensaver_settings['status'] : 'false',
	'js-screensaver-time' => (isset($screensaver_settings['time']) && !empty($screensaver_settings['time'])) ? $screensaver_settings['time'] : 'false'
], $page_tpl);
