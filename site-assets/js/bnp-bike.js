var configs = {
	steps : {
		complete : 610 / 4,
		atual : 0,
		cicles : 0,
		canWalk : true
	},
	size : {
		max : 610
	}
};

$(document).ready(() => {
	// new WOW().init();

	updateCicle ();
	$.when($("body").fadeIn(1000));

	$('body').keyup(function(e) {
		if (e.keyCode == 32) {
			if (configs.steps.canWalk) {
				configs.steps.atual++;
				if (configs.steps.atual >= configs.steps.complete) {
					configs.steps.atual = 0;
					configs.steps.cicles++;
					updateCicle ();
					animateLoader ();

					new WOW().init();
				} else {
					$('.bike .animator .loader').css('height', calcHeight () + 'px');
				}
			}
		}
	});
});

function calcPercentage () {
	return (100 * configs.steps.atual) / configs.steps.complete;
}

function calcHeight () {
	return configs.size.max * (calcPercentage () / 100);
}

function updateCicle () {
	$('.cicle').text('x ' + configs.steps.cicles);
}

function animateLoader () {
	configs.steps.canWalk = false;
	$.when(
		$('.message').fadeOut(), $('.loader').fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn()).done(() => {
			$('.message').fadeIn('slow'),
			configs.steps.canWalk = true;
	});
}
