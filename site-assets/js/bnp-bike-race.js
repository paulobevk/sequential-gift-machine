var configs = {
	time: 60,
	steps: {
		complete: 610 / 4,
		atual: 0,
		cicles: 0,
		canWalk: true
	},
	size: {
		max: 610
	},
	height: 615,
	score_per_tick: 1 / 10,
	points: 0,
	final_points: 0,
	countDown: {
		starts: 5
	},
	playable: false,
	timeLeft: 0
},
gameOver = false,
countDownHandler,
timeHandler,
converterHandler;

$(document).ready(() => {

	$(".counter").html(formatTime(window.configs.time));
	$(".points").html(configs.points);

	$("body").fadeIn(1000);

	$('body').on('click', '#startGame', function () {
		$.when($('#step-1').fadeOut(500)).done(function () {
			$('.countDown').fadeIn(500);
			$('.countDown div').html(configs.countDown.starts);
			countDown(configs.countDown.starts);
		});
	});

	$('body').keyup(function (e) {
		if (e.keyCode == 32) {
			if(window.playable) {
				if (configs.steps.canWalk) {
					configs.steps.atual++;
					if (configs.steps.atual >= configs.steps.complete) {
						configs.steps.atual = 0;
						configs.steps.cicles++;
						animateLoader();
						$.when($('#step-2').fadeOut(500)).done(function() {
							$(".clockBar").stop();
							window.configs.final_points = window.configs.points + (window.time * window.time_convertion);
							time_converter(function () {
								$('.points-append').html(window.configs.final_points);
							});
							endGame();
						});
						new WOW().init();
					} else {
						$('.bike .animator .loader').css('height', calcHeight() + 'px');
						pointsConverter(calcHeight());
					}
				}
			}
		}
	});

});

function countDown(seconds) {
	configs.timeLeft = seconds;

	countDownHandler = setInterval(function() {
		configs.timeLeft -= 1;
		console.log(configs.timeLeft);
		$('.countDown div').html(configs.timeLeft);
		if(configs.timeLeft <= 0) {
			$.when($(".countDown").fadeOut(500)).done(function() {
				$("#step-2").fadeIn(500);
				window.playable = true;
				clearInterval(countDownHandler);
				$(".clockBar").animate({'width': 0}, window.configs.time * 1000, 'linear');
				// count down for time
				timeHandler = setInterval(function () {
					if(!gameOver) {
						if (window.configs.time >= 0) {
							$(".counter").html(formatTime(window.configs.time));
							window.configs.time--;
						} else {
							clearInterval(timeHandler);
							window.configs.final_points = window.configs.points;
							$('.points-append').html(window.configs.final_points);
							$.when($('#step-2').fadeOut(500)).done(function() {
								endGame();
							});
						}
					} else {
						clearInterval(timeHandler);
					}
				}, 1000);
			});
		}
	}, 1000);
}

function pointsConverter(height) {
    window.points = configs.score_per_tick * height;
    updatePoints(Math.floor(points));
}

function updatePoints(points) {
    $(".points").html(points);
}

function calcPercentage() {
    return (100 * configs.steps.atual) / configs.steps.complete;
}

function calcHeight() {
    return configs.size.max * (calcPercentage() / 100);
}

function animateLoader() {
    configs.steps.canWalk = false;
    $.when(
        $('.message').fadeOut()).done(() => {
			$('.loader').fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn().fadeOut().fadeIn()
        });
}

function time_converter(callback) {
    $(".clockBar").animate({ 'width': 0 }, window.configs.time * 1000 / 60, 'linear');
    converterHandler = setInterval(function () {
		if (window.configs.time > 0) {
			window.configs.final_points += (1* window.time_convertion);
			updatePoints(window.configs.final_points);
			$(".counter").html(formatTime(window.configs.time));
			window.configs.time--;
		} else {
			$(".counter").html(formatTime(0));
			clearInterval(converterHandler);
			callback();
			return;
		}
    }, 1000 / 30);
    return true;
}

function endGame() {
	window.gameOver = true;
	window.playable = false;

    $('#step-3').fadeIn(500);

    $.when(
        document.cookie = "points=" + window.configs.final_points + "; path=/",
        document.cookie = "time=" + window.configs.time + "; path=/"
    ).done(function () {
        $.post(
            js_api,
            function (data) {
                data = JSON.parse(data);

                if (data.status) {
                    setTimeout(function () {
                        $.when($('#step-3').fadeOut(500)).done(function () {
                            $('#step-4').fadeIn(500);
                            setTimeout(function () {
                                $.when($("body").fadeOut(500)).done(function () {
                                    window.location = js_target;
                                });
                            }, 3000);
                        });
                    }, 3000);
                } else {
                    setTimeout(function () {
                        $.when($('#step-3').fadeOut(500)).done(function () {
                            console.log("Missing Parameters! But moving on!");
                            $('#step-4').fadeIn(500);
                            setTimeout(function () {
                                $.when($("body").fadeOut(500)).done(function () {
                                    window.location = js_target;
                                });
                            }, 3000);
                        });
                    }, 3000);
                }
            }
        );
    });
}
