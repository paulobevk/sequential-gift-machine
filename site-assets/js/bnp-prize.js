function randomIntFromInterval(min,max) {
	return Math.floor(Math.random()*(max-min+1)+min);
}

$(document).ready(function () {
	$.when($("body").fadeIn(1000)).done(function() {
		$('body').on('click', '.prize #prize-btn', function () {
			if (winner_id != false) {
				$.when($('.step-1').fadeOut(250),$('.step-1-1').fadeOut(250)).done(function () {
					$('.step-2').fadeIn(250);
					$('.step-2-1').fadeIn(250);

					var prize_imgs = $('.prize-img');
					var i = 0, rounds = 0, rounds_limit = randomIntFromInterval(window.rounds_min, window.rounds_max);

					for (var i = 0; i < prize_imgs.length; i++) {
						if ($(prize_imgs[i]).data('id') == window.winner_id) {
							$('.prize .step-2-2 .message').html($(prize_imgs[i]).data('message'));
						}
					}

					var interval = setInterval(function () {
						if (rounds != rounds_limit) {
							$(prize_imgs[i]).fadeToggle(100).fadeToggle(100);
						} else {
							if ($(prize_imgs[i]).data('id') != window.winner_id) {
								$(prize_imgs[i]).fadeToggle(100).fadeToggle(100);
							} else {
								$.when($('.step-2-1').fadeOut(250)).done(function () {
									$('.step-2-2').delay(2000).fadeIn(350);
								});
								$(prize_imgs[i]).fadeIn(350).fadeOut(350).fadeIn(350).fadeOut(350).fadeIn(350).fadeOut(350).fadeIn(350);
								clearInterval(interval);

								setTimeout(function () {
									$.when($("body").fadeOut(500)).done(function() {
										window.location = js_target;
									});
								}, 5000);
							}
						}

						if (i + 1 < prize_imgs.length) {
							i++;
						} else {
							i = 0;
							if (rounds < rounds_limit) {
								rounds++;
							}
						}

					}, 200);
				});
			} else {
				console.log("You are not a winner!");
			}
		});
	});
});
