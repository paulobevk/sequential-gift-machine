$(document).ready(function () {
	$.when($("body").fadeIn(1000)).done(function() {
		if (signup !== '') {
			signup = JSON.parse(signup);
			$('.avatar .username').html(signup.name);
		} else {
			$('.avatar .username').empty();
		}

		setTimeout(function () {
			$.when($("body").fadeOut(500)).done(function() {
				window.location = js_target;
			});
		}, js_timeout * 1000);
	});
});
