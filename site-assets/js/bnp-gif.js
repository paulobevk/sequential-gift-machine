var snaps_imgs = [];
var snaps_taken = 0;

var gifs = [];

function takeSnap(delay = 250) {
	setTimeout(function() {
		var canvas = document.createElement("canvas");
		canvas.width = window.snaps_width;
		canvas.height = window.snaps_height;
		var context = canvas.getContext('2d');
		context.drawImage(video, 0, 0, window.snaps_width, window.snaps_height);

		var i = new Image();
		i.src = canvas.toDataURL();

		window.snaps_imgs.push(i);
		snaps_taken++;

		if (snaps_taken < snaps_limit) {
			takeSnap(delay);
		} else {
			renderGIF(window.snaps_imgs, function(image) {
				image.id = "target-img";
				image.class = "d-none";
				image.width = "1080";

				$('#target-img').replaceWith(image);

				$('#video').css('display', 'none');
				$('#target-img').removeClass('d-none');
			});

			$.when($('.step-2').fadeOut(250)).done(function() {
				$('.step-3').fadeIn(250);
				$('.sight').fadeOut(250);
			});
		}
	}, delay);
}

function renderGIF(list, callback) {
	var gif = new GIF({
		workers: 1,
		quality: window.gif_quality,
		workerScript: path + '/site-assets/js/gif.worker.js',
		width: window.snaps_width,
		height: window.snaps_height
	});

	for (var i = 0; i < list.length; i++) {
		gif.addFrame(list[i],   {
			delay: window.snaps_interval
		});
	}

	gif.on('finished', function(blob) {
		var reader = new window.FileReader();
		reader.readAsDataURL(blob);

		var img = new Image();
		reader.onloadend = function() {
			img.src = reader.result;
			if (list[0].id !== undefined) {
				img.id = list[0].id;
			}

			window.gifs.push(img);
			callback(img);
		};
	});

	setTimeout(function() {
		gif.render();
	}, 500);

}

function addOverlay(overlay, list) {
	var toReturn = [];
	for (c = 0; c < list.length; c++) {
		var tmpCanvas = document.createElement("canvas");

		tmpCanvas.width = window.snaps_width;
		tmpCanvas.height = window.snaps_height;

		var tmpContext = tmpCanvas.getContext('2d');

		tmpContext.drawImage(list[c], 0, 0);
		tmpContext.drawImage(overlay, 0, 0, window.snaps_width, window.snaps_height, 0, 0, window.snaps_width, window.snaps_height);
		// tmpContext.drawImage(overlay, 0, 0, 1440, 1080, 0, 0, window.snaps_width, window.snaps_height);

		var i = new Image();
		i.src = tmpCanvas.toDataURL();
		if (overlay.id !== undefined) {
			i.id = overlay.id;
		}

		toReturn.push(i);
	}

	return toReturn;
}

function createGIF(overlay, first) {
	var gif = new GIF({
		workers: 1,
		quality: 70,
		workerScript: path + '/site-assets/js/gif.worker.js',
		width: window.snaps_width,
		height: window.snaps_height
	});

	// add a image element
	for (var i = 0; i < window.snaps_imgs.length; i++) {
		if (window.snaps_imgs[i][0] == overlay) {
			gif.addFrame(window.snaps_imgs[i],   {
				delay: 300
			});
		}
	}

	gif.on('finished', function(blob) {
		var reader = new window.FileReader();
		reader.readAsDataURL(blob);

		var img = new Image();
		reader.onloadend = function() {
			img.src = reader.result;
			window.gifs.push(img);
		};

		if (overlay == "target-img") {
			$('#video').css('display', 'none');
			$('#target-img').removeClass('d-none');
			reader.readAsDataURL(blob);
			reader.onloadend = function() {
				base64data = reader.result;
				$('#target-img').attr('src', base64data).css({
					"width": "100%"
				});
			};
		} else {
			var idImg = "#" + overlay;
			reader.readAsDataURL(blob);
			reader.onloadend = function() {
				var img = new Image();
				img.src = reader.result;
				img.class = "d-none gifImg";
				img.id = overlay;
				img.width = "100%";

				$(".live-cam").append(img);
			};
		}

		if (first == "first") {
			$.when($('.step-2').fadeOut(250)).done(function() {
				$('.step-3').fadeIn(250);
				$('.sight').fadeOut(250);
			});
		}
	});

	gif.render();
}

function recursive_render(index) {
	var overlay = new Image();
	overlay.src = window.overlays[index];
	overlay.id = index;

	overlay.onload = function() {
		var list_w_overlay;

		$.when(list_w_overlay = addOverlay(overlay, window.snaps_imgs)).done(function() {
			renderGIF(list_w_overlay, function(image) {
				if (image.id !== undefined) {

					$(image).addClass('d-none');
					$($('.step-4 .filter .img-container')[image.id]).css('background-image', 'url(' + image.src + ')');
					$($('.step-4 .filter .img-container')[image.id]).find('img.d-none').replaceWith(image);
					$($('.step-4 .filter .img-container > img')[image.id]).replaceWith(image);

					$('.step-3-5 .heading-progress span').html((index + 1) * 25);
				}

				if ((index + 1) < window.overlays.length) {
					recursive_render(index + 1);
				} else {
					$.when($('.step-3-5').fadeOut(250)).done(function() {
						$('.step-4').fadeIn(250);
						$('.step-4 .filter:first').click();
					});
				}
			});
		});
	}
}

$(document).ready(function() {
	if (signup !== '') {
		signup = JSON.parse(signup);
		$('.avatar .username').html(signup.name);
	} else {
		$('.avatar .username').empty();
	}


	/* Elements for taking the snapshot */
	var canvas = document.getElementById('canvas');
	var context = canvas.getContext('2d');
	var video = document.getElementById('video');

	/* Get access to the camera! */
	if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		/* Not adding `{ audio: true }` since we only want video now */
		navigator.mediaDevices.getUserMedia({
			video: true,
			video: {
				width: {
					ideal: 1440
				},
				height: {
					ideal: 1080
				}
			}
		}).then(function(stream) {
			video.srcObject = stream;
			video.play();
		});
	} /* Legacy code below: getUserMedia */
	else if (navigator.getUserMedia) { /* Standard */
		navigator.getUserMedia({
			video: true
		}, function(stream) {
			video.src = stream;
			video.play();
		}, errBack);
	} else if (navigator.webkitGetUserMedia) { /* WebKit-prefixed */
		navigator.webkitGetUserMedia({
			video: true
		}, function(stream) {
			video.src = window.webkitURL.createObjectURL(stream);
			video.play();
		}, errBack);
	} else if (navigator.mozGetUserMedia) { /* Mozilla-prefixed */
		navigator.mozGetUserMedia({
			video: true
		}, function(stream) {
			video.src = window.URL.createObjectURL(stream);
			video.play();
		}, errBack);
	}

	$.when($("body").fadeIn(1000)).done(function() {
		$('body').on(
			'click',
			'.step-1 #camera-shot-btn',
			function() {
				$.when($('.step-1').fadeOut(250)).done(function() {
					$('.step-2').fadeIn(250);
					$('.sight').html(3);
					setTimeout(function() {
						$('.sight').html(2);
					}, 1000);
					setTimeout(function() {
						$('.sight').html(1);
					}, 2000);
					setTimeout(function() {
						$('.sight').html('');
					}, 3000);
					setTimeout(function() {
						$('.sight').html('');
						takeSnap(snaps_interval);
					}, 3150);
				});
			}
		);

		$('body').on(
			'click',
			'.step-3 #repeat-btn',
			function() {
				$.when($('.step-3').fadeOut(250)).done(function() {
					window.snaps_imgs = [];
					window.snaps_taken = 0;
					$('.step-1').fadeIn(250);
					$('.sight').fadeIn(250);
					$('#video').css('display', 'initial');
					$('#target-img').addClass('d-none');
				});
			}
		);

		$('body').on(
			'click',
			'.step-3 #next-btn',
			function() {
				$.when($('.step-3').fadeOut(250)).done(function() {
					$('.step-3-5').fadeIn(250)
					recursive_render(0);

					for (var i = 0; i < window.snaps_imgs.length; i++) {
						var img = window.snaps_imgs[i].src;
						$.post(
							path + '/' + lg + '/bnp-gif-api/0/savePhoto', {
								'img': img,
								'id': i
							},
							function(data) {
								data = JSON.parse(data);

								if (data.status) {
									recursive_render(0);
								}
							}
						);
					}
				});
			}
		);

		$('body').on(
			'click',
			'.step-4 .filter:not(.active)',
			function() {
				$('.step-4 .filter.active').removeClass('active');
				$(this).addClass('active');

				var obj = $(this).find('img.d-none');

				$('#target-img').attr('src', obj[0].src);
			}
		);

		$('body').on(
			'click',
			'.step-4 #next-btn',
			function() {
				$.when($('.step-4').fadeOut(250)).done(function() {
					var obj = $('.step-4 .filter.active').find('img.d-none');

					img = obj[0].src;
					$.post(path + '/' + lg + '/bnp-gif-api/0/saveGif', {
							'img': img
						},

						function(data) {
							console.log(data);
							data = JSON.parse(data);

							if (data.status) {
								$.when($("body").fadeOut(500)).done(function() {
									window.location = js_target;
								});
							}
						}
					);
				});
			}
		);

		// BUTTON ANIMATION
		setInterval(function() {
			$('.camera-shot-btn').effect("shake", {
				distance: 10,
				times: 2
			}, 1000);
		}, 3000);
	});
});
