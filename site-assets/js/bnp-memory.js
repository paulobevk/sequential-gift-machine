// array das cartas, que devera vir da BD
var noCardsFliped = 0;
var idCard = "";
var points = 0;
var gameOver = false;

var config = {
	points : {
		win : 5,
		lose : 0
	},
	player : {
		points : 0
	}
}

$(document).ready(() => {
	var signup = getCookie('signup');

	if (signup !== null) {
		signup = JSON.parse(signup);
		$('.avatar-area .username').html(signup.name);
	} else {
		$('.avatar-area .username').empty();
	}

	$(".counter").html(formatTime(window.time));
	$('.points').html(window.config.player.points);

	var gameTable = window.size.split("x");
	// var newArray = duplicateArray(window.cardsList);

	createGameTable(gameTable[0], gameTable[1], window.cardsList);

	$.when($("body").fadeIn(1000)).done(() => {
		$('body').on('click', '.step-1 #startGame', function () {
			$.when($('.step-1').fadeOut(250)).done(function () {
				$('.step-2').fadeIn(250);
				$('.countDown').fadeIn(250);
				$(".cards").flip();
				$('.gameTable').css("margin-top", ((1480 - $('.gameTable').height())/2)+"px");
				$('.countDown div').html(3);
				setTimeout(function () {$('.countDown div').html(2);}, 1000);
				setTimeout(function () {$('.countDown div').html(1);}, 2000);
				setTimeout(function () {
					$(".cards").flip(true);
					$('.countDown div').html('');
					$('.countDown').fadeOut(250);
					// animation for timebar
					$(".clockBar").animate({'width': 0}, window.time * 1000, 'linear');
					// count down for time
					var intervalFunction = setInterval(function () {
						if(!gameOver) {
							if (window.time >= 0) {
								$(".counter").html(formatTime(window.time));
								window.time--;
							} else {
								clearInterval(intervalFunction);
								window.final_points = window.config.player.points;
								$('.points-append').html(window.final_points);
								$.when($('.step-2').fadeOut(500)).done(function() {
									end_game();
								});
							}
						} else {
							clearInterval(intervalFunction);
						}
					}, 1000);
				}, 3000);
			});
		});

		$('body').on('click', '.cards:not(.notClicked,.wait)', function () {
			$(this).flip(false);
			window.noCardsFliped++;
			$(this).addClass("notClicked shake");
			if (window.noCardsFliped == "2") {
				window.noCardsFliped = 0;
				// ACERTOU NA COMBINAÇÃO
				if(window.idCard == this.id) {
					$('.' + this.id).removeClass("cards shake");
					updatePoints(window.config.points.win);
					if ($('.cards').length == 0) {
						gameOver = true;
						$.when($('.step-2').fadeOut(500)).done(function() {
							$(".clockBar").stop();
							window.final_points = window.config.player.points + (window.time * window.time_convertion);
							time_converter(function () {
								$('.points-append').html(window.final_points);
							});
							end_game();
						});
					}
				} else {
					updatePoints(config.points.lose * -1);
					$('.shake').effect("shake", { distance: 5, times: 2 }, 700 );
					$('.cards').addClass("wait");
						setTimeout(function () {
							$(".cards").flip(true);
							$('.cards').removeClass("notClicked shake wait");
						}, 700);
					window.idCard = "";
					$(this).removeClass("clicked");
				}
			} else {
				window.idCard = this.id;
			}
		});

		$('body').on('click', '.notClicked', function () {
			$(this).flip(false);
		});

		$('body').on('click', '.wait:not(.notClicked)', function () {
			$(this).flip(true);
		});
	});
});

function end_game() {
	$('.end-game').fadeIn(500);
	$.when(
		document.cookie = "points=" + window.final_points + "; path=/",
		document.cookie = "time=" + window.time + "; path=/"
	).done(function() {
		$.post(
			js_api,
			function (data) {
				data = JSON.parse(data);

				if (data.status) {
					setTimeout(function() {
						$.when($('.end-game').fadeOut(500)).done(function() {
							$('.final-points').fadeIn(500);
							setTimeout(function() {
								$.when($("body").fadeOut(500)).done(function() {
									window.location = js_target;
								});
							}, 3000);
						});
					}, 3000);
				} else {
					setTimeout(function() {
						$.when($('.end-game').fadeOut(500)).done(function() {
							console.log("Missing Parameters! But moving on!");
							$('.final-points').fadeIn(500);
							setTimeout(function() {
								$.when($("body").fadeOut(500)).done(function() {
									window.location = js_target;
								});
							}, 3000);
						});
					}, 3000);
				}
			}
		);
	});
}

function createGameTable (width, height, cards_list) {
	var list = [];

	cards_list.forEach((o, i) => {
		for (var c = 0; c < 2; c++) {
			list.push({
				"id" : "card_" + i,
				"img" : cards_list[i]
			});
		}
	});

	var classwidth = "col-sm-" + 12 / width;

	cards_list = shuffle(list);

	$(cards_list).each((i,o) => {
		$(".gameTable").append(
			'<div class="'+classwidth+'"><div class="cards ' + cards_list[i].id+'" id="' + cards_list[i].id + '"><div class="front"><img src="' + path + '/' + cards_list[i].img + '"></div><div class="back"><img src="'+cardBack+'"></div></div><div class="sm-spacer15"></div></div>'
		);
	});
}

function time_converter (callback) {
	$(".clockBar").animate({'width' : 0}, window.time * 1000/60, 'linear');
	var time_handler = setInterval(function() {
		if (window.time > 0) {
			updatePoints(1 * window.time_convertion);
			$(".counter").html(formatTime(window.time));
			window.time--;
		} else {
			$(".counter").html(formatTime(0));
			clearInterval(time_handler);
			callback();
			return;
		}
	}, 1000/30);
	return true;
}

function updatePoints (points) {
	window.config.player.points += points;
	if (window.config.player.points < 0) {
		window.config.player.points = 0;
	}
	$('.points').html(window.config.player.points);
}
