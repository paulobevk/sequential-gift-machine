var points = 0; // points counter
var column = 1; // column counter
var point;
var element;
var items_pool = [];
var turn_pool = [];
//const travel_speed = 3120/time_to_finish;
var time_fraction = (time / (game_speed - 1)),
old_time_fraction = time_fraction,
num_items = 10,
time_to_add = (game_speed / num_items),
time_to_turn = (game_speed / 6),
object_speed = game_speed * 1000,
last_item = "";

$(document).ready(function () {

	$(".counter").html(formatTime(window.time));
	$('.points').html(window.points);
	create_game_table();

	var handler;
	var handler_add;
	var handler_turn;
	var handler_unturn;
	var handler_remove;

	$.when($("body").fadeIn(1000)).done(function() {
		$('body').on(
			'click',
			'.step-1 #startGame',
			function () {
				$.when($('.step-1').fadeOut(0)).done(function () {
					$('.step-2').fadeIn(250);
					$('.countDown').html(3);
					setTimeout(function () {$('.countDown').html(2);}, 1000);
					setTimeout(function () {$('.countDown').html(1);}, 2000);
					setTimeout(function () {
						$('.countDown').html('');
						// timer function
						var intervalFunction = setInterval(function () {
							timer();
						}, 1000);

						// increase velocity every x ms [defined in DB]
						var speed_update = function() {
							if (window.game_speed > window.max_speed) {
								window.game_speed--;
								window.object_speed = window.game_speed * 1000;
								time_to_add = window.game_speed / window.num_items;
								update_speed();
							}

							clearInterval(handler);
							clearInterval(handler_add);
							/*clearInterval(handler_turn);
							clearInterval(handler_unturn);*/
							clearInterval(handler_remove);

							handler = setInterval(speed_update, time_fraction * 1000);
							handler_add = setInterval(add_update, time_to_add * 1000);
							/*handler_turn = setInterval(turn_objects, 100 * window.game_speed);
							handler_unturn = setInterval(unturn_objects, 100 * window.game_speed);*/
							handler_remove = setInterval(remove_object, 1000 * window.game_speed);

							if (window.time_fraction !== window.old_time_fraction) {
								window.old_time_fraction = window.time_fraction;
							}
						}

						var add_update = function() {
							column = getRandomIntInclusive(1, columns);
							while (last_item == column) {
								column = getRandomIntInclusive(1, columns);
							}
							last_item = column;
							add_object(column);
						}

						var remove_object = function() {
							var objects = $('.fallingItem');
							if(objects.length > 1) {
								$.each(objects, function(i, obj) {
									var object_pos = parseInt($(obj).css("top"));
									if(object_pos >= 2100) {
										$(obj).remove();
									}
								});
							}
						}

						var turn_objects = function () {
							if(items_pool.length > 1) {
								$.each(items_pool, function(i, obj){
									var object_pos = parseInt($(obj).css("top"));
									if(object_pos >= gameAreaStart && object_pos <= gameAreaEnd) {
										items_pool.slice(i, 1);
										turn_pool.push(obj);
									}
								});
							}

							if(turn_pool.length > 1) {
								$.each(turn_pool, function(i,obj) {
									var visibility = $(obj).attr("data-visi");
									if(visibility == "false") {
										$(obj).attr("data-visi", true);
										var data_element = $(obj).attr("data-element");
										switch (data_element) {
											case "logo":
											get_random_object(list);
											$(obj).attr("src", path+point.src);
											$(obj).attr("data-point", point.score);
											break;
											case "debuff":
											$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/active_debuff.png');
											break;
											case "buff":
											$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/active_buff.png');
											break;
										}
									}
								});
							}
						}

						var unturn_objects = function() {
							if (turn_pool.length > 1) {
								$.each(turn_pool, function(i ,obj) {
									var object_pos = parseInt($(obj).css("top"));
									if(object_pos >= gameAreaEnd) {
										$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/default_motion.png');
										turn_pool.slice(i, 1);
									}
								});
							}
						}

						handler = setInterval(speed_update, time_fraction * 1000);
						handler_add = setInterval(add_update, time_to_add * 1000);
					}, 3000);

					/*setTimeout(function(){
						handler_turn = setInterval(turn_objects, 100 * game_speed);
					}, 3000 + ((time_fraction * 1000) * 0.3));

					setTimeout(function(){
						handler_unturn = setInterval(unturn_objects, 100 * game_speed);
					}, 3000 + ((time_fraction * 1000) * 0.7));*/

					setTimeout(function(){
						handler_remove = setInterval(remove_object, 1000 * game_speed);
					}, 3000 + (time_fraction * 1000));
				});
			}
		);
	});
});

function get_random_object (items = []) {
	var count = Object.keys(items);
	if (count.length > 0) {
		var base = 0;
		var top = 0;
		var number = Math.random();
		$.each(items, function(i, obj) {
			base = top;
			top = (obj.chance / 100) + top;
			if (number >= base && number <= top) {
				point = obj;
				return false;
			}
		});
	}
	return false;
}

function remove_object() {
	var objects = $('.fallingItem');
	if(objects.length > 1) {
		$.each(objects, function(i, obj) {
			var object_pos = parseInt($(obj).css("top"));
			if(object_pos >= 2100) {
				$(obj).remove();
			}
		});
	}
}

function update_speed () {
	$(".fallingItem").each(function(i, obj) {
		$(obj).stop();
		$(obj).animate({top: 3120}, object_speed, "linear");
	});
}

function get_object_from_pool () {
	var count = 0;
	if(items_pool.length > 3) {
		$.each(items_pool, function(i, obj){
			if(count < 3) {
				var object_pos = parseInt($(obj).css("top"));
				if(object_pos >= gameAreaStart && object_pos <= gameAreaEnd) {
					items_pool.slice(i, 1);
					turn_pool.push(obj);
					count ++;
				}
			}
			return false;
		});
	}
}

/*
function turn_objects () {
	if(items_pool.length > 1) {
		$.each(items_pool, function(i, obj){
			var object_pos = parseInt($(obj).css("top"));
			if(object_pos >= gameAreaStart && object_pos <= gameAreaEnd) {
				items_pool.slice(i, 1);
				turn_pool.push(obj);
			}
		});
	}

	if(turn_pool.length > 1) {
		$.each(turn_pool, function(i,obj) {
			var visibility = $(obj).attr("data-visi");
			if(visibility == "false") {
				$(obj).attr("data-visi", true);
				var data_element = $(obj).attr("data-element");
				switch (data_element) {
					case "logo":
					get_random_object(list);
					$(obj).attr("src", path+point.src);
					$(obj).attr("data-point", point.score);
					break;
					case "debuff":
					$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/active_debuff.png');
					break;
					case "buff":
					$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/active_buff.png');
					break;
				}
			}
		});
	}
}

function unturn_objects () {
	if (turn_pool.length > 1) {
		$.each(turn_pool, function(i ,obj) {
			var object_pos = parseInt($(obj).css("top"));
			if(object_pos >= gameAreaEnd) {
				$(obj).attr("src", path+'/site-assets/images/bnp-tapgame/default_motion.png');
				$(obj).attr("data-element", "none");
				turn_pool.slice(i, 1);
			}
		});
	}
} */

function falling_objects () {
	// check this information every 10ms
	var fall = setInterval(function() {
		for (var i = 0; i < $(".fallingItem").length; i++) {
			var object = $($(".fallingItem")[i]);
			var object_pos = parseInt(object.css("top"));
			var object_top = parseInt(object.attr("data-top"));
			var data_element = object.attr("data-element");
			var increase_speed = (velocity * speed) / 100;

			object.css({
				"top": "+=" + increase_speed + "px"
			});

			// frist verify if object is in the visbile area
			/*if (object_pos >= 0 && object_pos <= 1720) {
				object.attr("data-visi", true);
				// verify if the object is in the area of element display
				if (
					(object_pos >= (object_top - gameAreaActive)) &&
					(object_pos <= object_top)
				) {
					// determinate what object it will be based on the attribute data-element
					switch (data_element) {
						case "logo":
							get_random_object(list);
							object.attr("src", path+point.src);
							object.attr("data-point", point.score);
							break;
						case "debuff":
							object.attr("src", path+'/site-assets/images/bnp-tapgame/active_debuff.png');
							break;
						case "buff":
							object.attr("src", path+'/site-assets/images/bnp-tapgame/active_buff.png');
							break;
					}
				} else {
					object.attr("src", path+'/site-assets/images/bnp-tapgame/default_motion.png');
				}
			} */
			if (object_pos >= 2100) {
				object.remove();
			}
		}
	}, 10);
}

function verify_for_new_object (column) {
	var numElements = $(".gamecolumn"+column).children().length;
	if (numElements > 0) {
		// lastitem from top
		var lastItem = $(".gamecolumn"+column).children()[numElements-1];
		if ( $(lastItem).position().top > 0) {
			add_object(column);
		}
	} else {
		add_object (column);
	}

}

function get_element_type(chances = []) {
	var count = Object.keys(chances);
	if (count.length > 0) {
		var base = 0;
		var top = 0;
		var num = Math.random();

		$.each(chances, function(type, chance) {
			base = top;
			top = (chance / 100) + top;
			if (num >= base && num <= top) {
				element = type;
				return false;
			} else {
				element = "none";
			}
		});
	}
	return false;
}

function add_object (column) {
	var ran = getRandomIntInclusive(gameAreaStart+gameAreaActive, gameAreaEnd);
	var start_point = ran / 3;

 	get_element_type(chances); // get the element type based on chances
	var front_src,
	back_src,
	data_point = 0;

	switch (element) {
		case "logo":
			get_random_object(list);
			front_src = point.front;
			back_src = point.back;
			data_point = point.score;
			break;
		case "buff":
			front_src = path+'/site-assets/images/bnp-tapgame/active_buff.png';
			break;
		default:

	}

	// create the object
	var img = new Image();
	img.src  = front_src;
	img.className = "fallingItem";
	img.style = "top: -600px;";
	$(img).attr("data-visi", true); // define visibility [not being used]
	$(img).attr("data-top", ran); // define a position to calculate the area of display
	$(img).attr("data-element", element); // define what element the object will be
	$(img).attr("data-point", data_point);

	$(img).appendTo('.gamecolumn' + column); // insert the object in one of the columns of the game
	$(img).animate({top: 3120}, window.object_speed, "linear");
	items_pool.push(img);

	$(img).on("touchstart", function(e) {
		var data_element = $(this).attr("data-element");
		e.preventDefault();
		e.stopPropagation();

		// game actions
		switch (data_element) {
			case "logo":
			$(this).attr("src", back_src);
			update_points(parseInt($(this).attr('data-point')));
			break;
			case "debuff":
			window.time -= window.debuff;
			$(this).remove();
			break;
			case "buff":
			window.time += window.buff;
			window.maxTime =window.maxTime + window.buff;
			$(this).remove();
			break;
		}

	});
}

function update_points (points) {
	window.points += points;

	if (window.points < 0) {
		window.points = 0;
	}

	$('.points').html(window.points);
}
function timer() {
	if (window.time > 0) {
		$(".counter").html(formatTime(window.time));
		var percentTime = (window.time*100)/maxTime;
		$(".clockBar").css('width',percentTime+'%');
		window.time--;
	} else {

		$(".fallingItem").remove();

		clearInterval(window.intervalFunction);
		clearInterval(window.handler);
		clearInterval(window.handler_add);
		clearInterval(window.handler_turn);
		clearInterval(window.handler_unturn);
		clearInterval(window.handler_remove);

		end_game();
	}
}

function end_game() {
	$.when(
		document.cookie = "points=" + window.points + "; path=/",
		document.cookie = "time=" + window.time + "; path=/"
	).done(function() {
		$.post(
			js_api,
			function (data) {
				data = JSON.parse(data);

				if (data.status) {
					$.when($("body").fadeOut(500)).done(function() {
						window.location = js_target;
					});
				} else {
					console.log("Missing Parameters! But moving on!");
					setTimeout(function() {
						$.when($("body").fadeOut(500)).done(function() {
							window.location = js_target;
						});
					}, 2000)
				}
			}
		);
	});
}

function create_game_table () {
	for (var i = 1; i <= columns; i++) {
		$(".gameTable").append('<div class="gamecolumn'+i+' col-sm-'+12/columns+'"></div>');
	}
}
