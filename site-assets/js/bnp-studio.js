var video, canvas, context,
config = {
	mode : "photo", // photo, gif, slowmotion, hyperlapse, boomerang
	video : { format: "16:9", width: 1280, height: 720 },
	gif : {
		quality: 0.75, /* 1.0, 0.5, 0.1 */
		quantity: 120,
		delay: 1000 / 60,
		render_delay: 1000/60
	},
	overlay : false,
	pictures : []
};

function Initialize() {
	$(".photo .counter h3").html("/" + config.gif.quantity);

	video = document.getElementById("videoElement");

	canvas = document.getElementById("canvasElement");
	canvas.width = config.video.height;
	canvas.height = config.video.width;

	context = canvas.getContext("2d");

	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	/* || navigator.msGetUserMedia || navigator.oGetUserMedia; */

	if (navigator.getUserMedia) {
		navigator.getUserMedia(
			{
				video: {
					frameRate: {
						ideal: 24,
						max: 30
					},
					/* facingMode: { exact: "user" }, */
					/* facingMode: { exact: "environment" }, */
					facingMode:{ideal: "environment"},
					width: config.video.width,
					height: config.video.height
				},
				audio: false
			},
			VideoCapture,
			Error
		);
		requestAnimationFrame(IdleFunc);
	} else {
		alert("Error: your browser is not supported.");
	}
}

function VideoCapture(stream) {
	video.srcObject = stream;
}

function Error(e) {
	alert("Error: " + e);
}

function IdleFunc() {
	requestAnimationFrame(IdleFunc);

	if (video.readyState === video.HAVE_ENOUGH_DATA) {
		/* context.drawImage(video, 0, 0, canvas.width, canvas.height); */
	}
}

function VideoStop () {
	video.srcObject = null;
}

function TakePicture () {
	context.save();
	context.translate(canvas.width/2,canvas.height/2);
	context.rotate(-90*Math.PI/180);
	context.drawImage(video, -canvas.height/2, -canvas.width/2, canvas.height, canvas.width);
	context.restore();

	config.pictures[config.pictures.length] = canvas.toDataURL('image/jpeg', config.gif.quality);
}

function TakeSequentialPicture (skip = 0) {
	var i = 0, interval = setInterval(function () {
		$(".photo .counter h1").html(i+1);
		if (i < config.gif.quantity) {
			TakePicture();
			i++;
		} else {
			VideoStop ();
			$(".photo .processing").css("opacity", 1);
			clearInterval(interval);
			GenerateGifEffect (skip);
		}
	}, config.gif.delay);
}

function GenerateGifEffect (skip = 0) {
	var img;

	for (var i = 0; i < config.pictures.length; i += 1 + skip) {
		img = new Image();
		img.src = config.pictures[i];
		$(".work-area").append(img);

		if (i >= config.pictures.length - (1 + skip)) {
			$(".video").hide();
			if(config.mode === "cinemagraph") {
				videoEffect = document.getElementById('video'),
				videoEffect.currentTime = 0;
			}

			$(".step-2").css("display", "flex").hide().fadeIn();
		}
	}

	$(".work-area img").hide();

	i = 0;
	setInterval(function () {
		if (i == 0) {
			$($(".work-area img:not(.overlay)")[$(".work-area img:not(.overlay)").length-1]).hide();
			$($(".work-area img:not(.overlay)")[0]).show();
		} else {
			$($(".work-area img:not(.overlay)")[i-1]).hide();
			$($(".work-area img:not(.overlay)")[i]).show();
		}

		if (i == $(".work-area img:not(.overlay)").length-1) {
			i = 0;
			if(config.mode === "cinemagraph") {
				var video = document.getElementById('video');
				video.currentTime = 0;
			}
		} else {
			i++;
		}

	}, config.gif.render_delay);
}

function action () {
	switch (config.mode) {
		case "photo":
			run_photo();
			break;
		case "gif":
			run_gif ();
			break;
		case "slowmotion":
			run_slowmotion();
			break;
		case "timelapse":
			run_timelapse();
			break;
		case "cinemagraph":
			run_cinemagraph();
			break;
		case "boomerang":
			run_boomerang();
			break;
		default:
			console.log("default");
	}
}

function run_photo () {
	$(".text h1").html("3");
	setTimeout(function () {
		$(".text h1").html("2");
		setTimeout(function () {
			$(".text h1").html("1");
			setTimeout(function () {
				$(".text h1").html("Let's Go!");
				$(".flash").fadeIn(50);
				setTimeout(function () {
					$(".text h1").html("");
					$.when(TakePicture()).done(function () {
						$(".flash").fadeOut(50);
						VideoStop ();
						img = new Image();
						img.src = config.pictures[0];
						$(".work-area").append(img);

						$(".step-2").css("display", "flex").hide().fadeIn();
					});
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}

function run_gif () {
	var d1 = $.Deferred();
	let i = 0;

	var interval = setInterval(function () {
		console.log(i); i++;

		$(".text h1").html("3");
		setTimeout(function () {
			$(".text h1").html("2");
			setTimeout(function () {
				$(".text h1").html("1");
				setTimeout(function () {
					$(".text h1").html("Let's Go!");
					setTimeout(function () {
						$(".text h1").html("");
						$.when($(".flash").fadeIn(250)).done(function() {
							$.when(TakePicture()).done(function() {
								$(".flash").fadeOut(250);
							});
						});
					}, 1000);
				}, 1000);
			}, 1000);
		}, 1000);

		if (i == config.gif.quantity) { clearInterval(interval); GenerateGifEffect (); }
	}, 4150);
}

function run_slowmotion () {
	$(".text h1").html("3");
	setTimeout(function () {
		$(".text h1").html("2");
		setTimeout(function () {
			$(".text h1").html("1");
			setTimeout(function () {
				$(".text h1").html("Let's Go!");
				setTimeout(function () {
					$(".text h1").html("");
					TakeSequentialPicture ();
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}

function run_cinemagraph () {
	var videoEffect = document.getElementById('video');
	videoEffect.currentTime = 0;
	videoEffect.pause();
	$(".text h1").html("3");
	setTimeout(function () {
		$(".text h1").html("2");
		setTimeout(function () {
			$(".text h1").html("1");
			setTimeout(function () {
				$(".text h1").html("Let's Go!");
				setTimeout(function () {
					$(".text h1").html("");

					videoEffect.play();
					TakeSequentialPicture ();
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}

function run_timelapse () {
	$(".text h1").html("3");
	setTimeout(function () {
		$(".text h1").html("2");
		setTimeout(function () {
			$(".text h1").html("1");
			setTimeout(function () {
				$(".text h1").html("Let's Go!");
				setTimeout(function () {
					$(".text h1").html("");
					TakeSequentialPicture (1);
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);
}

function run_boomerang () {
	$(".text h1").html("3");
	setTimeout(function () {
		$(".text h1").html("2");
		setTimeout(function () {
			$(".text h1").html("1");
			setTimeout(function () {
				$(".text h1").html("Let's Go!");
				setTimeout(function () {
					$(".text h1").html("");
					var o = 0;
					var interval = setInterval(function () {
						$(".photo .counter h1").html(i+1);

						if (o < config.gif.quantity) {
							TakePicture();
							o++;
						} else {
							VideoStop ();
							$(".photo .processing").css("opacity", 1);
							clearInterval(interval);

							var img, dir = "down";

							for (let i = 0; i < config.pictures.length; i++) {
								img = new Image();
								img.src = config.pictures[i];
								$(".work-area").append(img);

								if (i == config.pictures.length) {
									$(".video").hide();
								}
							}

							$(".work-area img").hide();

							var i = 0;
							setInterval(function () {
								if (dir == "down") {
									$($(".work-area img")[i-1]).hide();
									$($(".work-area img")[i]).show();

									if (i == $(".work-area img").length-1) {
										dir = "up";
									} else {
										i++;
									}
								} else {
									$($(".work-area img")[i+1]).hide();
									$($(".work-area img")[i]).show();

									if (i == 0) {
										dir = "down";
									} else {
										i--;
									}
								}
							}, config.gif.render_delay);

							$(".step-2").css("display", "flex").hide().fadeIn();
						}
					}, config.gif.delay);
				}, 1000);
			}, 1000);
		}, 1000);
	}, 1000);

}

function repeat () {}

function calcCrop () {
	let format = window.config.video.format.split(":");

	return {
		width : $("body").width(),
		height : $("body").width() / format[0] * format[1]
	};
}

function setCrop (format) {
	let height = (window.innerHeight - format.height) / 2;
	$(".layer.top").height(height);
	$(".layer.bottom").height(height);

	$(".overlay").css("top", height + "px").height(window.innerHeight - (2 * height));
}

function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};

	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

function sendImg () {
	if (config.mode === "cinemagraph")  {
		return $.post( path + "/pt/bnp-studio-api/0/saveVid", { pictures : config.pictures }, function(data) {
			$("body").fadeOut();
		});
	} else {
		return $.post( path + "/pt/bnp-studio-api/0/saveImg", { overlay : config.overlay, pictures : config.pictures }, function(data) {
			$("body").fadeOut();
		});
	}
}

function cinemagraph_effect() {
	var outputCanvas = document.getElementById('output'),
			output = outputCanvas.getContext('2d'),
			bufferCanvas = document.getElementById('buffer'),
			buffer = bufferCanvas.getContext('2d'),
			//buffer2Canvas = document.getElementById('buffer2'),
			//buffer2 = buffer2Canvas.getContext('2d'),
			video = document.getElementById('video'),
			//video2 = document.getElementById('video2'),
			width = outputCanvas.width,
			height = outputCanvas.height,
			interval;

		function processFrame() {
			buffer.drawImage(video, 0, 0);
			//buffer2.drawImage(video2, 0, 0);

			// this can be done without alphaData, except in Firefox which doesn't like it when image is bigger than the canvas
			var	image = buffer.getImageData(0, 0, width, height),
				imageData = image.data,
				alpha = buffer.getImageData(0, height, width, height),
				alphaData = alpha.data;

			for (var i = 3, len = imageData.length; i < len; i = i + 4) {
				imageData[i] = alphaData[i-1];
			}

			output.putImageData(image, 0, 0, 0, 0, width, height);
		}

		video.addEventListener('play', function() {
			clearInterval(interval);
			interval = setInterval(processFrame, 30)
		}, false);

	$(".controls").fadeIn(1000);
}

$(document).ready(function () {
	// SELECT OVERLAY
	$("body").on("click", ".overlays-list .item", function () {
		let src = $(this).attr("src");

		$(".overlay img").attr("src", src)
		toDataURL(src, function (data) { config.overlay = data; });
	});

	// FINISH CHOOSING
	$("body").on("click", ".overlay-choose", function () {
		$(".overlays-list").fadeOut();

		$(".text").fadeIn();
		$(".controls").fadeIn();
	});

	$("body").on("click", ".camera-shot-btn", function () {
		$(".step-1").fadeOut();
		action ();
	});

	// REPEAT
	$("body").on("click", "#repeat-btn",function () {
		$.when($(".step-2").fadeOut()).done(function () {
			$(".work-area").empty();
			$(".text").css("display", "flex").hide().fadeIn();
			config.pictures = [];
			if(config.mode === "cinemagraph") {
				videoEffect = document.getElementById('video'),
				videoEffect.currentTime = 0;
			}
			Initialize();
			$(".step-1").fadeIn();
		});
	});

	// NEXT
	$("body").on("click", "#next-btn",function () {
		$.when($(".step-2").fadeOut()).done(function () {
			$.when(sendImg()).done(function () {
				window.location = window.js_target;
			});
		});
	});

	$("body").fadeIn();
	Initialize();

	if(config.mode === "cinemagraph") {
		cinemagraph_effect();
	}

	$("body").fadeIn(1000);
	setCrop (calcCrop ());
	$($(".overlays-list .item")[0]).click();
});
