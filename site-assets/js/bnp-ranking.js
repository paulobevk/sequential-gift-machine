$(document).ready(function() {
	$("body").fadeIn(1000);

	if(redirect) {
		setTimeout(function () {
			$.when($("body").fadeOut(500)).done(function() {
				window.location = js_target;
			});
		}, js_timeout * 1000);
	}
});
