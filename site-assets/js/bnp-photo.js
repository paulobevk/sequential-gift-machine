var video, canvas, context,
config = {
	mode : "photo", // photo, gif, slowmotion, hyperlapse, boomerang
	video : { format: "16:9", width: 1440, height: 1080 },
	gif : {
		quality: 0.75, /* 1.0, 0.5, 0.1 */
		quantity: 120,
		delay: 1000 / 60,
		render_delay: 1000/60
	},
	overlay : false,
	pictures : []
};

var imageBase;

function Initialize() {
	video = document.getElementById("video");
	canvas = document.getElementById("canvas");
	context = canvas.getContext("2d");

	navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

	if (navigator.getUserMedia) {
		navigator.getUserMedia(
			{
				video: {
					frameRate : {
						ideal : 24,
						max : 30
					},
					/* facingMode: { exact: "user" }, */
					/* facingMode: { exact: "environment" }, */
					facingMode : {ideal: "environment"},
					width : config.video.width,
					height : config.video.height
				},
				audio: false
			},
			VideoCapture,
			Error
		);
		requestAnimationFrame(IdleFunc);
	} else {
		alert("Error: your browser is not supported.");
	}
}

function VideoCapture(stream) {
	video.srcObject = stream;
}

function Error(e) {
	alert("Error: " + e);
}

function IdleFunc() {
	requestAnimationFrame(IdleFunc);

	if (video.readyState === video.HAVE_ENOUGH_DATA) {
		/* context.drawImage(video, 0, 0, canvas.width, canvas.height); */
	}
}

function VideoStop () {
	video.srcObject = null;
}

function recursive_render(index) {
	var overlay = new Image();
	overlay.src = window.overlays[index];
	overlay.id = index;

	overlay.onload = function() {
		var list_w_overlay;

		$.when(list_w_overlay = addOverlay(overlay)).done(function() {
			$('body').append(list_w_overlay);
			$('.step-3-5 .heading-progress span').html((index + 1) * 25);
			renderPhoto(list_w_overlay, index, function(image, index) {
				if (image[0].id !== undefined) {
					$(image).addClass('d-none');
					$($('.step-4 .filter .img-container')[image[0].id]).css('background-image', 'url(' + image[0].src + ')');
					$($('.step-4 .filter .img-container')[image[0].id]).find('img.d-none').replaceWith(image);
					$($('.step-4 .filter .img-container > img')[image[0].id]).replaceWith(image);
					$('.step-3-5 .heading-progress span').html((index + 1) * 25);
				}
				if ((index + 1) < window.overlays.length) {
					recursive_render(index + 1);
				} else {
					setTimeout(function() {
						$.when($('.step-3-5').fadeOut(250)).done(function() {
							$('.step-4').fadeIn(250);
							var obj = $('.step-4 .filter:first').find('img.d-none');

							$('#target-img').attr('src', obj[0].src);
						});
					}, 500);
				}
			});
		});
	}
}

function addOverlay(overlay) {

	var toReturn = [];
	var tmpCanvas = document.createElement("canvas");

	tmpCanvas.width = window.image_width;
	tmpCanvas.height = window.image_height;

	var tmpContext = tmpCanvas.getContext('2d');
	tmpContext.drawImage(window.imageBase, 0, 0);
	tmpContext.drawImage(overlay, 0, 0, window.image_width, window.image_height, 0, 0, window.image_width, window.image_height);
	var i = new Image();
	i.src = tmpCanvas.toDataURL('image/jpeg', 1.0);
	if (overlay.id !== undefined) {
		i.id = overlay.id;
	}
	toReturn.push(i);

	return toReturn;
}

function renderPhoto(list, index, callback) {
	callback(list, index);
}

$(document).ready(() => {
	var signup = getCookie('signup');

	if (signup !== null) {
		signup = JSON.parse(signup);
		$('.avatar .username').html(signup.name);
	} else {
		$('.avatar .username').empty();
	}

	Initialize();

	$.when($("body").fadeIn(1000)).done(function() {
		$('body').on('click', '.step-1 #camera-shot-btn', function() {
			$('.step-1 #camera-shot-btn').attr('disabled', true);
			$.when($('.step-1').fadeOut(250)).done(function() {
				$('.step-2').fadeIn(250);

				$('.sight').html(3);
				setTimeout(function() {
					$('.sight').html(2);
				}, 1000);
				setTimeout(function() {
					$('.sight').html(1);
				}, 2000);
				setTimeout(function() {
					$('.sight').html('');
				}, 3000);
				setTimeout(function() {
					$('.sight').html('');
					// context.filter = "sepia(100%)";
					context.drawImage(video, 0, 0, 1440, 1080); // capture the webcam img and put him in a canvas
					$('#target-img').attr('src', canvas.toDataURL('image/jpeg', 1.0)); // get PNG base64
					$('#video').css('display', 'none');
					$('#target-img').removeClass('d-none');
					window.imageBase = $('#target-img')[0];

					$.when($('.step-2').fadeOut(250)).done(function() {
						$('.step-3').fadeIn(250);
						$('.sight').fadeOut(250);
					});
				}, 3150);
			});
		});

		$('body').on('click', '.step-3 #repeat-btn', function() {
			$('.step-1 #camera-shot-btn').attr('disabled', false);
			$.when($('.step-3').fadeOut(250)).done(function() {
				$('.step-1').fadeIn(250)
				$('.sight').fadeIn(250);
				$('#video').css('display', 'initial');
				$('#target-img').addClass('d-none');
			});
		});

		$('body').on('click', '.step-3 #next-btn', function() {
			$('.step-3 #next-btn').attr('disabled', true);
			$.when($('.step-3').fadeOut(250)).done(function() {
				$('.step-3-5').fadeIn(250);

				var img = $('#target-img').attr('src');

				$.post(
					path + '/' + lg + '/bnp-photo-api/0/savePhoto', {
						'img': img
					},
					function(data) {
						data = JSON.parse(data);

						if (data.status) {
							recursive_render(0);
						}
					}
				);

			});
		});

		$('body').on('click', '.step-4 .filter', function() {
			$('.step-4 .filter.choosen').removeClass('choosen');
			$(this).addClass('choosen');

			var obj = $(this).find('img.d-none');

			$('#target-img').attr('src', obj[0].src);
		});

		$('body').on('click', '.step-4 #confirm-btn', function() {
			if ($('.step-4 .filter').hasClass('choosen')) {
				$('.step-4 #confirm-btn').attr('disabled', true);
				$.when($('.step-4').fadeOut(250)).done(function() {
					var img = $('#target-img').attr('src');

					$.post(
						path + '/' + lg + '/bnp-photo-api/0/saveSelectedPhoto', {
							'img': img
						},
						function(data) {
							data = JSON.parse(data);

							if (data.status) {
								$.when($("body").fadeOut(500)).done(function() {
									window.location = js_target;
								});
							}
						}
					);
				});
			}
		});

		// BUTTON ANIMATION
		setInterval(() => {
			$('.camera-shot-btn').effect("shake", { distance: 10, times: 2 }, 1000);
		}, 3000);
	});
});
