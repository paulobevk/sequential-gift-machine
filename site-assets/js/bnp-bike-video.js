var configs = {
	ciclesPerFrame : 30
};

$(document).ready(() => {
	$.when($("body").fadeIn(1000));

	var video = document.getElementById('bike-video');

	video.addEventListener('ended', resetVideo, false);

	window.addEventListener('keypress', function (evt) {
		if (video.paused) { //or you can force it to pause here
			if (evt.keyCode === 32) { //space
				video.currentTime = Math.min(video.duration, video.currentTime + (1 / configs.ciclesPerFrame));
			}
		}
	});
});

function resetVideo(e) {
	video.currentTime = 0;
}
