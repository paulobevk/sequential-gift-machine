var i = 0;

function theLoop () {
	var list = $('.screen .img');

	setTimeout(function () {
		$('.screen .img.active').removeClass('active');
		$(list[i]).addClass('active');

		i++;
		if (i <= list.length) {
			if (i == list.length) {
				i = 0;
			}

			theLoop();
		}
	}, 3000);
}

$(document).ready(function () {
	theLoop();
});
