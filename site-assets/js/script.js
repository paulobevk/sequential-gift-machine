( function ($) { $.fn.serializeObject = function () { var o = {}; var a = this.serializeArray(); $.each( a, function () { if (o[this.name]) { if (!o[this.name].push) { o[this.name] = [o[this.name]]; } o[this.name].push(this.value || ''); } else { o[this.name] = this.value || ''; } }); return o; }; } )(jQuery);

function formatTime(nbSeconds, hasHours) {
	var time = [], s = 1;
	var calc = nbSeconds;
	if (hasHours) {
		s = 3600;
		calc = calc / s;
		time.push(format(Math.floor(calc)));//hour
	}
	calc = ((calc - (time[time.length-1] || 0)) * s) / 60;
	time.push(format(Math.floor(calc)));//minute
	calc = (calc - (time[time.length-1])) * 60;
	time.push(format(Math.round(calc)));//second

	function format(n) {//it makes "0X"/"00"/"XX"
		return (("" + n) / 10).toFixed(1).replace(".", "");
	}

	return time.join(":");
};

function shuffle(a) {
	for (let i = a.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[a[i], a[j]] = [a[j], a[i]];
	}
	return a;
}

function getRandomIntInclusive(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isMobile() {
	if (
		navigator.userAgent.match(/Android/i)
		|| navigator.userAgent.match(/webOS/i)
		|| navigator.userAgent.match(/iPhone/i)
		|| navigator.userAgent.match(/iPad/i)
		|| navigator.userAgent.match(/iPod/i)
		|| navigator.userAgent.match(/BlackBerry/i)
	) {
		return true;
	}

	return false;
}

$(document).ready(() => {});
