var screensaver_timeout;

$(document).ready(function () {
	$(document).on("mousemove keydown click tap swipe touchend touchstart touchmove", function() {
		if (js_screensaver_status == true) {
			clearTimeout(window.screensaver_timeout);

			screensaver_timeout = setTimeout(function() {
				$.when($('body').css('background','none').fadeOut(500)).done(function () {
					window.location = path + "/" + lg + "/bnp-screensaver/";
				});
			}, js_screensaver_time * 1000); // 1 minutos
		}
	}).click();
});
