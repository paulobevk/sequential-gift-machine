$(document).ready(function () {

	$('body').fadeIn(1000);
	$(".carousel-item.active .screensaver-video")[0].play();

	$('body').on('click', '.screensaver .hover', function () {
		$.when($('body').fadeOut()).done(function () {
			window.location = js_target;
		});
	});

	var videos = document.querySelectorAll(".carousel-item .screensaver-video");

	videos.forEach(function(e) {
        e.addEventListener('ended', myHandler, false);
    });

	function myHandler(e) {
		$.when($("#videos-slide").carousel("next")).done(function() {
			setTimeout(function() {
				$(".carousel-item.active .screensaver-video")[0].play();
			}, 1000);
		});
    }
});
