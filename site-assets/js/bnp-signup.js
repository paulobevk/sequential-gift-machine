function validate_email(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	return re.test(email);
}

function allFielded () {
	var toReturn = true;

	$('.carousel-item.active .form-control').each((i, obj) => {
		if (!field_verify(obj)) { toReturn = false; }
	});

	return toReturn;
}

function field_verify (obj) {
	var type = $(obj).attr('type');
	var val = $(obj).val();

	switch(type) {
		case 'text':
			if (val.length >= 3) {
				return true;
			}
			break;
		case 'email':
			if (validate_email($(obj).val())) {
				return true;
			}
			break;
	}

	return false;
}

var config = {
	terms_check : false
};

$(document).ready(() => {
	$.when(cKeyboard()).done(() => {
		$.when($("body").fadeIn(1000)).done(() => {
			// ACCEPT BUTTON CONTROLLER
			$('body').on('touchstart', '.accept-btn', (e) => {
				if ($('#accept-checkbox:checked').length > 0) {
					$($(e.currentTarget)).animate({opacity: .25}, 250);
					$('#accept-checkbox:checked').click();
					window.config.terms_check = false;
				} else {
					$($(e.currentTarget)).animate({opacity: 1}, 250);
					$('#accept-checkbox').click();
					window.config.terms_check = true;
				}
			});

			$('body').on('touchstart', '#termsModal .go_ahead', () => {
				window.config.terms_check = true;
				$('#termsModal').modal('hide');
				$('.cK.cKFunction.cKey-return').trigger('touchstart');
			})

			// SET FOCUSROW
			$('body').on('touchstart', '.overInput', (e) => {
				cKeyboard_config.input_target = '#input_' + e.target.id;
				$.when($('.focusRow').removeClass('focusRow')).done(() => {
					$(e.currentTarget).parent('.row').addClass('focusRow')
				});
			});

			$('body').on('touchstart', 'input.form-control', (e) => {
				cKeyboard_config.input_target = '#' + e.currentTarget.id;
				$('.focusRow').removeClass('focusRow');
				$(e.currentTarget).parent('div').parent('.row').addClass('focusRow');
			});

			// PREVENT INFINTE LOOP
			$('body').on('slide', '#signup-slider', (e) => {
				$(e.currentTarget).children('.carousel-control').show();
				if ($('.carousel-inner .carousel-item:first').hasClass('active')) {
					$(e.currentTarget).children('.left.carousel-control').hide();
				} else if($('.carousel-inner .item:last').hasClass('active')) {
					$(e.currentTarget).children('.right.carousel-control').hide();
				}
			});

			$('body').on('touchstart', '#keyboard', () => {
				if ($('.signup .focusRow input').val().length <= 26) {
					// LOW IN CHARACTERS
					if (!isMobile()) {
						$('.signup .focusRow input').css({'font-size': '3rem', 'line-height': '3rem'});
					} else {
						$('.signup .focusRow input').css({'font-size': '2rem', 'line-height': '2rem'});
					}
				} else if ($('.signup .focusRow input').val().length > 27 && $('.signup .focusRow input').val().length <= 120) {
					// MEDIUM IN CHARACTERS
					if (!isMobile()) {
						$('.signup .focusRow input').css({'font-size': '2.375rem', 'line-height': '2.375rem'});
					} else {
						$('.signup .focusRow input').css({'font-size': '1.5rem', 'line-height': '1.5rem'});
					}
				} else {
					// HIGH IN CHARACTERS
					$('.signup .focusRow input').css({'font-size': '1rem', 'line-height': '1rem'});
				}
			});

			// RETURN FUNCTION
			$('body').on('touchstart', '.cK.cKFunction.cKey-return', () => {
				if (!allFielded()) {
					var fielded = true;

					$('.carousel-item.active .form-control').each((i, obj) => {
						if (!field_verify(obj)) {

							var row = $(obj).parent('div').parent('div.row');
							row.addClass('invalid');
							setTimeout(() => { row.removeClass('invalid'); }, 2500);

							// this panel has empty fields
							fielded = false;
							// set empty field as new target
							$(obj).trigger('touchstart');

							return false;
						}
					});

					if (fielded) {
						cKeyboard_config.input_target = null;
						$($('#signup-slider a.right')[0]).click();
						setTimeout(() => { $($('.carousel-item.active input')[0]).click(); }, 800);
					}
				} else {
					// VERIFY IF TERMS CHECKBOX IS CHECKED
					// if ($('#accept-checkbox:checked').length == 0 && window.config.terms_check == false) {
					if (!window.config.terms_check) {
						$('#termsModal').modal();
					} else {
						// PREPARE ALL DATA TO SUBMIT
						$('#keyboard').fadeOut();
						var toSubmit = {};

						$('.carousel-item.active .form-control').each((i, obj) => {
							toSubmit[$(obj).attr('name')] = $(obj).val();
						});

						if ($('#accept-checkbox:checked').length) {
							toSubmit['accept'] = 1;
						} else {
							toSubmit['accept'] = 0;
						}

						$.post( js_api, toSubmit, (data) => {
							data = JSON.parse(data);

							if (data.status) {
								data.object = JSON.parse(data.object);

								toSubmit['id'] = data.object.id;
							} else {
								console.log('Fields configuration error!');
								toSubmit = false;
							}
						}).done(() => {
							if (toSubmit !== false) {
								$.when(
									document.cookie = "signup=" + JSON.stringify(toSubmit) + "; path=/",
									document.cookie = "id=" + toSubmit.id + "; path=/",
									document.cookie = "accept=" + toSubmit.accept + "; path=/"
								).done(() => {
									$.when($("body").fadeOut(500)).done(() => { window.location = js_target; });
								});
							}
						});
					}
				}
			});

			$($('.carousel-item.active .overInput')[0]).trigger('touchstart')
		});
	});
});
