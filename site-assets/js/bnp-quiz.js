var points = 0, current_Q = 0;
var intervalFunction;
var heights = [];
var tallest;
var final_points;

function carouselNormalization() {
	function normalizeHeights() {
		$('.quiz .carousel-inner .carousel-item').each(function(i, obj) {
			window.heights.push($(obj).outerHeight());
		});

		window.tallest = Math.max.apply(null, window.heights);

		$('.quiz .carousel-inner .carousel-item').each(function(i, obj) {
		$(obj).css('min-height', tallest + 'px');
		});
	}
	normalizeHeights();
}

function updatePoints (points) {
	window.points += points;
	if (window.points < 0) {
		window.points = 0;
	}
	$('.points').html(window.points);
}

function end_game() {
	$('.end-game').fadeIn(500);
	$.when(
		document.cookie = "points=" + window.final_points + "; path=/",
		document.cookie = "time=" + window.time + "; path=/"
	).done(function() {
		$.post(
			js_api,
			function (data) {
				data = JSON.parse(data);

				if (data.status) {
					setTimeout(function() {
						$.when($("body").fadeOut(500)).done(function() {
							window.location = js_target;
						});
					}, 3000);
				} else {
					console.log("Missing Parameters! But moving on!");
					setTimeout(function() {
						$.when($("body").fadeOut(500)).done(function() {
							window.location = js_target;
						});
					}, 3000);
				}
			}
		);
	});
}

function time_converter (callback) {
	$(".clockBar").animate({'width' : 0}, window.time * 1000/60, 'linear');
	var time_handler = setInterval(function() {
		if (window.time > 0) {
			updatePoints(1 * window.time_convertion);
			$(".counter").html(formatTime(window.time));
			window.time--;
		} else {
			$(".counter").html(formatTime(0));
			clearInterval(time_handler);
			callback();
			return;
		}
	}, 1000/30);
	return true;
}

$(document).ready(function () {
	$(".counter").html(formatTime(window.time));
	$('.points').html(window.points);

	$.when($("body").fadeIn(1000)).done(function() {
		$('body').on( 'touchstart', '.step-1 .start-btn',
			function () {
				$.when($('.step-1').fadeOut(250)).done(function () {
					$('.step-2').fadeIn(250);
					$('.countDown').html(3);
					setTimeout(function () {$('.countDown').html(2);}, 1000);
					setTimeout(function () {$('.countDown').html(1);}, 2000);
					setTimeout(function () {
						$.when($('.step-2').fadeOut(250)).done(function() {
							$.when($('.step-3').fadeIn(250)).done(function() {
								carouselNormalization();
								$(".clockBar").animate({'width' : 0}, window.time * 1000, 'linear');
								// count down for time
								window.intervalFunction = setInterval(function () {
									if (window.time >= 0) {
										$(".counter").html(formatTime(window.time));
										window.time--;
									} else {
										clearInterval(window.intervalFunction);
										$.when($('.step-3').fadeOut(250)).done(function() {
											$('.step-4').fadeIn(250);
											setTimeout(function() {
												$.when($('.step-4').fadeOut(250)).done(function() {
													$('.your-points').html(window.points);
													$('.step-5').fadeIn(250);
													end_game();
												});
											}, 3000);
										});
									}
								}, 1000);
							});
						});
					}, 3000);
				});
			}
		);

		$('body').on( 'touchstart', '.step-3 .textAnswers', function () {
				el = $('.carousel-item.active');
				$($(el).find($('.textAnswers'))).css("opacity", "0.25");
				$($(el).find($('.textAnswers'))).removeClass('answer');
				$($(el).find($('.textAnswers'))).bind('touchstart', false);;
				$(this).css("opacity", "1");
				$($(this).closest(".bgAnswers")).addClass("active");

				if ($(this).data("status")) {
					updatePoints (points_per_question);
				}

				if(!$('#carousel-game-quiz .carousel-item:last-child').hasClass('active')) {
					setTimeout(function() {
						$("#carousel-game-quiz").carousel('next');
					}, 1000);
				} else {
					clearInterval(window.intervalFunction);
					$(".clockBar").stop();
					window.final_points = window.points + (window.time * window.time_convertion);
					setTimeout(function() {
						$.when($('.step-3').fadeOut(250)).done(function() {
							$('.step-4').fadeIn(250);
							time_converter(function () {
								setTimeout(function() {
									$.when($('.step-4').fadeOut(250)).done(function() {
										$('.your-points').html(window.final_points);
										$('.step-5').fadeIn(250);
										end_game();
									});
								}, 1000);
							});
						});
					}, 1000);
				}
			}
		);
	});
});
