// get index from the list, jquery.slotmachine only works with object index
function getIndex () {
	for (var i = 0; i < window.items_list.length; i++) {
		if (window.winner_object.id == window.items_list[i].id) {
			return i;
		}
	}
}

var spins = 50,
total_items,
winner_object,
service_available = false,
interval,
no_stock_interval = 5 * 1000; // only gets true when item id returns

$(document).ready(function() {

	$.when($("body").fadeIn(1000)).done(function() {
		$.get(path + "/" + lg + "/bnp-gift-api/0/getPrize", function (data) {
			data = JSON.parse(data);

			if (data.status) {
				window.winner_object = data.object;
				window.winner_id =	getIndex();
				window.service_available = true; // service gets available
			} else {
				// no stcok, what now?
				$.when($(".panel-1").fadeOut()).done(function () {
					$(".panel-3.no-stock").fadeIn()

					console.log("Sem stock!");

					window.interval = setInterval(function () {
						$.get(path + "/" + lg + "/bnp-gift-api/0/getPrize", function (data) {
							data = JSON.parse(data);

							if (data.status) {
								window.winner_object = data.object;
								window.winner_id = getIndex();
								window.service_available = true; // service gets available

								clearInterval(window.interval);

								$.when($(".panel-3.no-stock").fadeOut()).done(function () {
									$(".panel-1").fadeIn()
									console.log("We have stock!");
								});
								return true;
							}

							console.log("Still no stock!");
							return false;
						});
					}, no_stock_interval);
				});
			}
		});

		// pressing a key, in this case, space is the key.
		$(window).keypress(function (e) {
			if ((e.keyCode === 0 || e.keyCode === 32) && service_available == true) {
				e.preventDefault();
				service_available = false; // after starts gets false, this prevents multiple starts

				console.log(winner_id);

				total_items = $("#planeMachine_1").children("div.slotMachineContainer").children("div").length;

				var machine_1 = $("#machine_1").slotMachine({randomize: function() { return 0; }});
				var machine_2 = $("#machine_2").slotMachine({randomize: function() { return 0; }, direction : 'down'});
				var machine_3 = $("#machine_3").slotMachine({randomize: function() { return 0; }});

				machine_1.setRandomize(winner_id); machine_2.setRandomize(winner_id); machine_3.setRandomize(winner_id);

				// start suffle
				setTimeout(function () {machine_2.shuffle(window.spins, function () {});}, 250);
				setTimeout(function () {machine_3.shuffle(window.spins, function () {});}, 850);
				setTimeout(function () {
					machine_1.shuffle(window.spins, function () {
						// get message
						$(".message h1").html(window.winner_object.message);
						$.when($(".panel-1").fadeOut()).done(function () {
							$(".panel-2.message").fadeIn();
						});

						// after player pull the lever a request to decrease the stock of the prize is sent to the server
						$.get(path + "/" + lg + "/bnp-gift-api/" + window.winner_object.id + "/decreaseStock", function (data) {
							// data = JSON.parse(data);

							// get another item from server
							$.get(path + "/" + lg + "/bnp-gift-api/0/getPrize", function (data) {
								data = JSON.parse(data);
								console.log(data);
								if (data.status) {
									window.winner_object = data.object;
									window.winner_id = getIndex();

									setTimeout(function () {
										$.when($(".panel-2.message").fadeOut()).done(function () {
											$(".panel-1").fadeIn()
											window.service_available = true;
										});
									}, 6000);
								} else {
									// no stock, what now?
									setTimeout(function () {
										$(".panel-2.message").hide();
										$(".panel-3.no-stock").fadeIn();
									}, 6000);

									console.log("Sem stock!");

									window.interval = setInterval(function () {
										$.get(path + "/" + lg + "/bnp-gift-api/0/getPrize", function (data) {
											data = JSON.parse(data);

											if (data.status) {
												window.winner_object = data.object;
												window.winner_id = getIndex();
												window.service_available = true; // service gets available

												clearInterval(window.interval);

												$.when($(".panel-3.no-stock").fadeOut()).done(function () {
													$(".panel-1").fadeIn()
													console.log("We have stock!");
												});
												return true;
											}

											console.log("Still no stock!");
											return false;
										});
									}, no_stock_interval);
								}
							});
						});
					});
				}, 550);
			}
		});
	});
}); 	