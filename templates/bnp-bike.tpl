<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-bike.js" charset="utf-8"></script>

		<link rel="stylesheet" href="{c2r-path}/site-assets/libs/animate.css/animate.css">
		<script src="{c2r-path}/site-assets/libs/wow.js-1.3.0/wow.min.js" charset="utf-8"></script>
	</head>
	<body>
		<main class="bike" style="background-image:url({c2r-app-bg})">
			<!-- { c2r-header } -->
			<div class="container-fluid">
				<div class="row">
					<div class="col animator" style="background-image: url('{c2r-path}/uploads/battery.png')">
						<div class="loader" style="background-image: url('{c2r-path}/uploads/animator-loader.svg')"></div>
					</div>
				</div>
				<div class="spacer sm-90"></div>
				<div class="row">
					<div class="col text-center message">
						<h1>Venha carregar<br>as baterias</h1>
					</div>
				</div>
				<div class="battery-footer wow fadeInLeft" data-wow-duration="250ms" style="background-image: url('{c2r-path}/uploads/battery-footer.png')"></div>
				<div class="cicle"></div>
				<div class="app-logo {c2r-app-logo-hidden}">
					<img src="{c2r-app-logo}" alt="">
				</div>
			</div>
			{c2r-footer}
		</main>
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',

			time = '{c2r-time}',
			size = '{c2r-size}',
			cardsList = {c2r-cards-list},
			cardBack = '{c2r-card-back}',
			time_convertion = {c2r-time-convertion},

			signup = '{c2r-signup}',

			js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
