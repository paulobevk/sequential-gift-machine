<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-screensaver.css">
		<script src="{c2r-path}/site-assets/js/bnp-screensaver.js" charset="utf-8"></script>
	</head>
	<body>
		<div class="screensaver tacenter">
			<div id="videos-slide" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
				<div class="carousel-inner">
					{c2r-videos-list}
				</div>
			</div>
			<div class="hover"></div>
		</div>
		<script type="text/javascript">
			var js_target = '{c2r-js-target}';
		</script>
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
