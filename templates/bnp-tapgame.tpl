<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-tapgame.css">
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery.flip.min.js"></script>
	</head>
	<body>
		<main class="tapgame" style="background-image: url({c2r-app-bg})">
			{c2r-header}
			<div class="container-fluid sm-tacenter">
				<!-- STEP 1 -->
				<div class="row step-1">
					<div class="col-sm-12">
						<div class="spacer all-150"></div>
						<div class="row d-flex justify-content-center">
							<div class="col-sm-3">
								<img src="{c2r-path}/site-assets/images/bnp-tapgame/active.png">
								<div class="legendHome">Coleta pontos</div>
							</div>
							<div class="col-sm-4">
								<img src="{c2r-path}/site-assets/images/bnp-tapgame/buff.png">
								<div class="legendHome">Ganha tempo</div>
							</div>
							<div class="col-sm-3">
								<img src="{c2r-path}/site-assets/images/bnp-tapgame/debuff.png">
								<div class="legendHome">Evita as bombas</div>
							</div>
						</div>
						<div class="spacer all-150"></div>
						<div class="spacer all-60"></div>
						<div class="boxShake">
							<button type="button" class="camera-shot-btn bnp-btn" id="startGame" style="background-image: url({c2r-start-btn})"></button>
						</div>
						<div class="spacer all-60"></div>
						<h1 class="play-text">{c2r-play-text}</h1>
					</div>
				</div>
				<!-- STEP 2 -->
				<div class="container step-2">
					<div class="row gameTable">
						<div class="countDown">3</div>
					</div>
				</div>

				<div class="end-game step-3-1">
					<div class="flex-center">
						Fim do Jogo
					</div>
				</div>
				<div class="final-points step-3-2">
					<div class="flex-center">
						<div>
							Fizeste
							<div class="points-append">

							</div>
							pontos!
						</div>
					</div>
				</div>
			</div>

			<div class="app-logo {c2r-app-logo-hidden}">
				<img src="{c2r-app-logo}" alt="">
			</div>
			{c2r-footer}
		</main>

		<!-- Configs -->
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',
				js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',
				columns = '{c2r-columns}',
				time = {c2r-time},
				maxTime  = {c2r-time},
				buff  = {c2r-buff},
				debuff  = {c2r-debuff},
				active  = {c2r-active},
				list  = JSON.parse('{c2r-objects}'),
				gameAreaStart  = {c2r-game-area-start},
				gameAreaEnd  = {c2r-game-area-end},
				gameAreaActive  = {c2r-game-area-active},
				timeIncreaseVelocity  = {c2r-time-increase-velocity},
				chances = JSON.parse('{c2r-chances}'),
				buffChance = {c2r-buff-chance} / 100,
				debuffChance = {c2r-debuff-chance} / 100 + buffChance,
				logoChance = {c2r-logo-chance} / 100,
				velocity1 = 19200/{c2r-game-speed},
				game_speed = {c2r-speed},
				max_speed = {c2r-max-speed},
				signup = '{c2r-signup}',

				js_screensaver_status = {c2r-js-screensaver-status},
				js_screensaver_time = {c2r-js-screensaver-time};
		</script>

		<!-- Game JS -->
		<script src="{c2r-path}/site-assets/js/bnp-tapgame.js" charset="utf-8"></script>

		<!-- Styling -->
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
