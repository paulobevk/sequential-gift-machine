<!DOCTYPE html>
<html lang="{c2r-lg}">

<head>
	{c2r-head}
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-studio.css">
	<script src="{c2r-path}/site-assets/js/bnp-studio.js" charset="utf-8"></script>
	<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
	<!-- <script src="{c2r-path}/site-assets/js/filereader.min.js"></script> -->
	<script src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js"></script>
</head>

<body>
	<main class="photo">
		<!-- OVERLAYS -->
		{c2r-overlay-system}
		<!-- OVERLAYS -->

		<!-- CONTROLS -->
		<div class="controls">
			<div class="container-fluid">
				<div class="row step-1">
					<div class="col text-center">
						<button type="button" class="btn camera-shot-btn bnp-btn counter" id="camera-shot-btn">
							<img src="{c2r-path}/uploads/btn-photo.png" alt="">
						</button>
					</div>
				</div>
				<div class="row step-2">
					<div class="col text-center">
						<button type="button" class="btn btn-lg bnp-btn counter" id="repeat-btn">
							<img src="{c2r-path}/uploads/btn-repeat.png" alt="">
						</button>
					</div>
					<div class="col text-center">
						<button type="button" class="btn btn-lg bnp-btn counter" id="next-btn">
							<img src="{c2r-path}/uploads/btn-next.png" alt="">
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- CONTROLS -->
		<div class="text">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<h1></h1>
					</div>
				</div>
			</div>
		</div>
		<div class="flash"></div>
		<!-- OVERLAY -->
		{c2r-final-overlay}
		<!-- OVERLAY -->
		<canvas id="canvasElement"></canvas>
		<video id="videoElement" src="" autoplay poster=""></video>

		<!-- CUT MASK -->
		<div class="layer top"></div>
		<div class="layer bottom"></div>
		<!-- CUT MASK -->

		<div class="work-area"></div>
	</main>
	<!-- Configs -->
	<script type="text/javascript">
		var js_target = '{c2r-js-target}',
			filters = JSON.parse('{c2r-filters}'),
			overlays = JSON.parse('{c2r-overlays}'),
			signup = '{c2r-signup}',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};

			config.mode = "{c2r-settings-mode}";
			config.video.format = "{c2r-settings-format}";
			config.video.width = {c2r-settings-image-width};
			config.video.height = {c2r-settings-image-height};

			config.gif.quality = {c2r-settings-gif-quality};
			config.gif.quantity = {c2r-settings-gif-quantity};
			config.gif.delay = {c2r-settings-gif-delay};
			config.gif.render_delay = {c2r-settings-gif-render-delay};
	</script>

	<!-- Styling -->
	<style media="screen">
		{c2r-settings-css}
	</style>
</body>

</html>
