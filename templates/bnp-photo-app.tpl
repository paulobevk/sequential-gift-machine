<!DOCTYPE html>
<html lang="{c2r-lg}">

<head>
	{c2r-head}
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-photo-app.css">
	<script src="{c2r-path}/site-assets/js/bnp-photo-app.js" charset="utf-8"></script>
	<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
	<!-- <script src="{c2r-path}/site-assets/js/filereader.min.js"></script> -->
	<script src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js"></script>
</head>

<body>
	<main class="photo-app" style="background-image:url({c2r-app-bg});">
		{c2r-header}
		<!-- LIVE CAMERA -->
		<div class="live-cam">
			<div class="overlay">
				<img src="{c2r-overlay}" style="width:540px;">
			</div>
			<div class="sight sm-tacenter" style="background-image: url('{c2r-path}/site-assets/images/bnp-photo/sight-default.png')"></div>
			<video id="video" width="1080" height="810" autoplay></video>
			<canvas id="canvas" class="d-none" width="1440" height="1080"></canvas>
			<img id="target-img" class="d-none" src="" data-camanwidth="1440" data-camanheight="1080">
		</div>
		<!-- EOF LIVE CAMERA -->

		<div class="container-fluid tacenter">
			<div class="spacer all-90"></div>
			<!-- STEP 1 -->
			<div class="row step-1">
				<div class="col-sm-12">
					<h1>{c2r-step1-intro}</h1>
					<div class="spacer all-60"></div>
					<button type="button" class="camera-shot-btn bnp-btn" id="camera-shot-btn" style="background-image:url({c2r-button-bg});">
						<div class="spacer all-90"></div>
						<div class="spacer all-15"></div>
						<span>{c2r-step1-button-text}</span>
					</button>
				</div>
			</div>
			<!-- EOF STEP 1 -->

			<!-- STEP 2 -->
			<div class="row step-2 tacenter">
				<h1>{c2r-step2-intro}</h1>
				<div class="spacer all-60"></div>
				<img src="{c2r-step2-clock}" class="wait-btn">
			</div>
			<!-- EOF STEP 2 -->

			<!-- STEP 3 -->
			<div class="row step-3">
				<h1>{c2r-step3-intro}</h1>
				<div class="spacer all-60"></div>
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-4 taright">
							<div class="spacer all-30"></div>
							<button type="button" class="repeat-btn bnp-btn" id="repeat-btn" style="background-image:url({c2r-step3-button-repeat});">
								<div class="spacer all-30"></div>
								<div class="spacer all-15"></div>
								<span>{c2r-step3-text-repeat}</span>
							</button>
						</div>
						<div class="col-sm-4">
							<button type="button" class="next-btn bnp-btn arrow" id="next-btn" style="background-image:url({c2r-step3-button-accept});">
								<div class="spacer all-90"></div>
								<div class="spacer all-15"></div>
								<span>{c2r-step3-text-accept}</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<!-- EOF STEP 3 -->

			<!-- STEP 3.5 -->
			<div class="row step-3-5">
				<h1>{c2r-step3-5-intro}</h1>
				<h1 class="heading-progress"><span>0</span>%</h1>
			</div>
			<!-- EOF STEP 3.5 -->

			<!-- STEP 4 -->
			<div class="row step-4">
				<div class="container-fluid">
					<div class="row d-flex align-items-center justify-content-center">
						<!-- -->
						{c2r-overlays-list}
						<!-- -->
					</div>
					<div class="spacer all-90"></div>
					<div class="row">
						<div class="col-sm-12">
							<h1>{c2r-step4-intro}</h1>
						</div>
					</div>
					<div class="spacer all-30"></div>
					<div class="row d-flex justify-content-center">
						<div class="col-sm-4">
							<button type="button" class="next-btn bnp-btn arrow" id="confirm-btn" style="background-image:url({c2r-step4-button});">
								<div class="spacer all-90"></div>
								<div class="spacer all-15"></div>
								<span>{c2r-step4-text-accept}</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<!-- EOF STEP 4 -->
		</div>
		<div class="app-logo {c2r-app-logo-hidden}">
			<img src="{c2r-app-logo}" alt="">
		</div>
		{c2r-footer}
	</main>
	<!-- Configs -->
	<script type="text/javascript">
		var js_target = '{c2r-js-target}',
			filters = JSON.parse('{c2r-filters}'),
			image_width = {c2r-settings-image-width},
			image_height = {c2r-settings-image-height},
			overlays = JSON.parse('{c2r-overlays}');
			signup = '{c2r-signup}',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
	</script>

	<!-- Styling -->
	<style media="screen">
		{c2r-settings-css}
	</style>
</body>

</html>
