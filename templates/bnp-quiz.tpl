<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-quiz.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-quiz.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-quiz.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery.flip.min.js"></script>
	</head>
	<body>
		<main class="quiz" style="background-image: url({c2r-app-bg});">
			{c2r-header}
			<div class="container-fluid ">
				<!-- STEP 1 -->
				<div class="row step-1 sm-tacenter">
					<div class="col-sm-12">
						<div class="spacer xs-15 sm-150 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="boxShake">
							<button type="button" class="start-btn bnp-btn" style="background-image: url({c2r-startgame-btn})"></button>
						</div>
						<div class="spacer all-30"></div>
						<h1>{c2r-touch-start}</h1>
					</div>
				</div>

				<div class="step-2">
					<div class="countDown">3</div>
				</div>
				<!-- STEP 3 -->
				<div class="row step-3">
					<div class="col-sm-12">
						<div class="spacer all-30"></div>
						<div id="carousel-game-quiz" class="carousel slide" data-ride="carousel"  data-interval="false">
							<div class="carousel-inner">
								{c2r-question-list}
							</div>
						</div>
					</div>
				</div>

				<div class="step-4">
					<div class="eof-game">
						Fim do Jogo
					</div>
				</div>

				<div class="row step-5">
					<div class="col-sm-12 md-tacenter">
						<div class="spacer xs-15 sm-150 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<h1><span class="your-points"></span> pontos</h1>
					</div>
				</div>
				<div class="app-logo {c2r-app-logo-hidden}">
					<img src="{c2r-app-logo}" alt="">
				</div>
			</div>

			{c2r-footer}
		</main>

		<!-- Configs -->
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',

			time = '{c2r-time}',
			size = '{c2r-size}',

			avatar = '{c2r-avatar}',
			signup = '{c2r-signup}',
			points_per_question = {c2r-points-per-question},
			totalQ = {c2r-total-question},
			time_convertion = {c2r-time-convertion},

			js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>

		<!-- Styling -->
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
