<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike-video.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-bike-video.js" charset="utf-8"></script>

		<link rel="stylesheet" href="{c2r-path}/site-assets/libs/animate.css/animate.css">
		<script src="{c2r-path}/site-assets/libs/wow.js-1.3.0/wow.min.js" charset="utf-8"></script>
	</head>
	<body>
		<main class="bike" style="background-image:url({c2r-app-bg})">
            <div class="vertical-video">
                <video id="bike-video" width="1080" height="1920">
                    <source src="{c2r-path}/uploads/bike/bike.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
			<div class="container-fluid">
				<div class="app-logo {c2r-app-logo-hidden}">
					<img src="{c2r-app-logo}" alt="">
				</div>
			</div>
		</main>
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',

			signup = '{c2r-signup}',

			js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
