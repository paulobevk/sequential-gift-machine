<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
	</head>

	<body class="b-active">
		<div class="bnp-install">
			<div id="bnp-installer" class="container {c2r-installer-active}">
				<div class="spacer all-120"></div>
				<div class="row justify-content-center">
					<div class="col-8 tacenter">
						<form id="bnp-installer-form" class="" action="{c2r-path}/{c2r-lg}/bnp-install/" method="post">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="project-id">PROJECT ID</label>
										<input type="number" class="form-control" id="project-id" name="project-id" aria-describedby="emailHelp" placeholder="Put the PROJECT ID here." required>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="product-id">PRODUCT ID</label>
										<input type="number" class="form-control" id="product-id" name="product-id" aria-describedby="emailHelp" placeholder="Put the PRODUCT ID here." required>
									</div>
								</div>
							</div>
							<div class="spacer all-30"></div>
							<button type="submit" name="submit" id="submit" class="btn btn-primary">SEND</button>
						</form>
					</div>
				</div>
				<div class="spacer all-120"></div>
			</div>
			<div id="bnp-installer-feedback" class="container {c2r-feedback-active}">
				<div class="spacer all-30"></div>
				<div class="row justify-content-center">
					<div class="col-8 tacenter">
						<h1>Installation completed!</h1>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var machine_json = {c2r-machine-json};
		</script>
		<script src="{c2r-js}/bnp-install.js" charset="utf-8"></script>
	</body>
</html>
