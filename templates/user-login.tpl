<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/user-login.css">
		<script src="{c2r-path}/site-assets/js/user-login.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
	</head>
	<body>
		{c2r-header}
		<div class="user-login container">
			<form action="" method="post" name="Login_Form" class="form-signin">       
				<h3 class="form-signin-heading">Welcome Back! Please Sign In</h3>
				<hr class="colorgraph"><br>
				<input type="text" class="form-control" name="email" placeholder="Email" required="" autofocus="" />
				<div class="sm-spacer15"></div>
				<input type="password" class="form-control" name="password" placeholder="Password" required=""/>     		  
				<div class="sm-spacer15"></div>
				<button class="btn btn-lg btn-primary btn-block"  name="submit" value="Login" type="Submit">Login</button>
				{c2r-return-message}
			</form>			
			<div class="sm-spacer30"></div>
		</div>
		{c2r-footer}
	</body>
</html>
