<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-thanks.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-thanks.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-thanks.js" charset="utf-8"></script>
	</head>
	<body>
		<main class="thanks tacenter" style="background-image:url({c2r-app-bg});">
			{c2r-header}
			<div class="spacer xs-15 sm-15 md-15 lg-150"></div>
			<div class="spacer xs-15 sm-15 md-15 lg-150"></div>
			<div class="spacer xs-15 sm-15 md-90 lg-90"></div>
			<img class="thanksImg" src="{c2r-thanks-image}">
			<div class="spacer xs-15 sm-15 md-60 lg-150"></div>
			<h1>{c2r-thanks-text}</h1>
			<div class="app-logo {c2r-app-logo-hidden}">
				<img src="{c2r-app-logo}" alt="">
			</div>
			{c2r-footer}
		</main>

		<!-- Configs -->
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',
			js_timeout = {c2r-js-timeout},
			id = '{c2r-id}',
			avatar = '{c2r-avatar}',
			signup = '{c2r-signup}',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>

		<!-- Styling -->
		<style media="screen">
			{c2r-css}
		</style>

	</body>
</html>
