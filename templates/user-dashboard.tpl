<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/user-login.css">
		<script src="{c2r-path}/site-assets/js/user-dashboard.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
	</head>
	<body>
		{c2r-header}
		<div class="user-dashboard">
			<div class="sm-spacer60"></div>
			<div class="container">
				<div class="row">
					{c2r-modules-list}
				</div>
			</div>
			<!-- Modal -->
			<div id="modal" class="modal fade" role="dialog">
				<div class="modal-dialog modal-sm">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-body sm-tacenter">
							<p>Confirm your operation?</p>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-sm-6 sm-taleft">
									<button type="button" class="btn btn-default btn-block btn-yes" data-dismiss="modal">YES</button>
								</div>
								<div class="col-sm-6">
									<button type="button" class="btn btn-default btn-block" data-dismiss="modal">NO</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{c2r-footer}
	</body>
</html>
