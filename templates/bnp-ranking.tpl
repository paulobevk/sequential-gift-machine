<!DOCTYPE html>
<html lang="{c2r-lg}">

<head>
	{c2r-head}
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-ranking.css">
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-ranking.mbl.css">
	<script src="{c2r-path}/site-assets/js/bnp-ranking.js" charset="utf-8"></script>
	<script src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js"></script>
</head>

<body>
	<main class="ranking tacenter" style="background-image: url({c2r-app-bg});">
		{c2r-header}
		<div class="spacer xs-15 sm-15 md-15 lg-60"></div>
		<div class="container-fluid">
			<div class="row d-flex justify-content-center">
				<div class="col-sm-10">
					<div class="row th">
						<div class="col-sm-2 tacenter">
							<h4>#</h4>
						</div>
						<div class="col-sm-6 taleft">
							<h4>Jogador</h4>
						</div>
						<div class="col-sm-3 tacenter">
							<h4>Pontuação</h4>
						</div>
					</div>
				</div>
			</div>
			{c2r-list}
			<div class="spacer xs-15 sm-15 md-15 lg-60"></div>
		</div>
		<div class="container-fluid {c2r-user-active}">
			<div class="row d-flex justify-content-center">
				<div class="col-sm-8">
					<h1 class="position-text">{c2r-position-text}</h1>
				</div>
			</div>
			<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
			{c2r-of-top-10}
		</div>
		<div class="app-logo {c2r-app-logo-hidden}">
			<img src="{c2r-app-logo}" alt="">
		</div>
		{c2r-footer}
	</main>
	<script type="text/javascript">
		var js_target = '{c2r-js-target}',

			id = '{c2r-id}',
			signup = '{c2r-signup}',
			redirect = {c2r-redirect},
			js_timeout = {c2r-js-timeout},

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
	</script>
	<style media="screen">
		{c2r-settings-css}
	</style>
</body>

</html>
