<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<style>
			html,
			body {
				background: #081421;
				color: #d3d7de;
				font-family: "Courier new";
				font-size: 35px;
				line-height: 1.5em;
				cursor: default;
			}

			a {
				color: #fff;
			}

			.code-area {
				position: absolute;
				width: 515px;
				margin: auto;
				top: 50%;
				left: 50%;
				-webkit-transform: translate(-50%, -50%);
				transform: translate(-50%, -50%);
			}

			.code-area > span {
				display: block;
			}

			.big {
				font-size: 150px;
			}

			@media screen and (max-width: 320px) {
				.code-area {
					font-size: 5vw;
					min-width: auto;
					width: 95%;
					margin: auto;
					padding: 5px;
					padding-left: 10px;
					line-height: 6.5vw;
				}
			}
		</style>
	</head>
	<body>
		<!--
		<div class="home">
			<div class="container">
				<div class="spacer all-150"></div>
				<div class="tacenter">
					<h1>Oops,</h1>
					<div class="spacer all-30"></div>
					<h2 class="big">404!</h2>
					<div class="spacer all-30"></div>
					<div class="text">
						<p>Parece que a página procurada não existe ou algum erro ocorreu.</p>
						<p>Clique <a href="{c2r-path}/{c2r-lg}/">aqui</a> para voltar</p>
					</div>
				</div>
			</div>
			Marianna Almeida - contato@agyrafa.com
		</div>
		-->
		<div class="code-area">
			<span style="color: #777;font-style:italic;">// 404 page not found.</span>
			<span>
				<span style="color:#d65562;">
					if
				</span>
				(<span style="color:#4ca8ef;">!</span>
				<span style="font-style: italic;color:#bdbdbd;">found</span>)
			{
			</span>
			<span>
			<span style="padding-left: 15px;color:#2796ec">
				<i style="width: 10px;display:inline-block"></i>throw
			</span>
			<span>
				(<span style="color: #a6a61f">"(╯°□°)╯︵ ┻━┻"</span>);
			</span>
				<span style="display:block">}</span>
				<span style="color: #777;font-style:italic;">
					// <a href="{c2r-path}/{c2r-lg}/">Go home!</a>
				</span>
			</span>
		</div>
	</body>
</html>
