<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<style>
			.big {
				font-size: 150px;
			}
		</style>
	</head>
	<body>
		<div class="home">
			<!-- here write the HTML code -->
			<div class="container">
				<div class="spacer all-150"></div>
				<div class="tacenter">
					<h1>Oops,</h1>
					<div class="spacer all-30"></div>
					<h2 class="big">403!</h2>
					<div class="spacer all-30"></div>
					<div class="text">
						<p>Clique <a href="{c2r-path}/{c2r-lg}/">aqui</a> para voltar</p>
					</div>
				</div>
			</div>
			<!-- Marianna Almeida - contato@agyrafa.com -->
		</div>
	</body>
</html>
