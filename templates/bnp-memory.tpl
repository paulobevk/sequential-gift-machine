<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-memory.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-memory.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-memory.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery.flip.min.js"></script>
	</head>
	<body>
		<main class="memory" style="background-image:url({c2r-app-bg})">
			{c2r-header}
			<div class="countDown"><div>3</div></div>
			<div class="container-fluid tacenter">
				<div class="row step-1">
					<div class="col">
						<div class="spacer xs-15 sm-150 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="boxShake">
							<button type="button" class="camera-shot-btn bnp-btn" id="startGame" style="background-image: url({c2r-step1-button})"></button>
						</div>
						<div class="spacer all-30"></div>
						<h1>Toca para jogar!</h1>
					</div>
				</div>
				<div class="container step-2">
					<div class="gameTable row"></div>
				</div>
				<div class="end-game step-3-1">
					<div class="flex-center">
						Fim do Jogo
					</div>
				</div>
				<div class="final-points step-3-2">
					<div class="flex-center">
						<div>
							Fizeste
							<div class="points-append">

							</div>
							pontos!
						</div>
					</div>
				</div>
				<div class="app-logo {c2r-app-logo-hidden}">
					<img src="{c2r-app-logo}" alt="">
				</div>
			</div>
			{c2r-footer}
		</main>
		{c2r-footer}
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',

			time = '{c2r-time}',
			size = '{c2r-size}',
			cardsList = {c2r-cards-list},
			cardBack = '{c2r-card-back}',
			time_convertion = {c2r-time-convertion},

			signup = '{c2r-signup}',

			js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
