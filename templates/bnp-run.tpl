<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
	</head>

	<body>
		<div class="bnp-run">
			<script type="text/javascript">
				if (
					navigator.userAgent.match(/Android/i)
					|| navigator.userAgent.match(/webOS/i)
					|| navigator.userAgent.match(/iPhone/i)
					|| navigator.userAgent.match(/iPad/i)
					|| navigator.userAgent.match(/iPod/i)
					|| navigator.userAgent.match(/BlackBerry/i)
				) {
					setTimeout(function() {
						window.location = path + '/' + lg + '/{c2r-run}/';
					}, {c2r-run-timeout});
				} else {
					setTimeout(function() {
						window.open(path + '/' + lg + '/{c2r-run}/');
					}, {c2r-run-timeout});
				}
			</script>
		</div>
	</body>
</html>
