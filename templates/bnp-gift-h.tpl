<!DOCTYPE html>
<html lang="{c2r-lg}">
<head>
	{c2r-head}
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-gift.css">
	<script type="text/javascript" src="{c2r-path}/site-assets/js/bnp-gift.js" charset="utf-8"></script>
	<script type="text/javascript" src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js" charset="utf-8"></script>
	<script type="text/javascript" src="{c2r-path}/site-assets/js/jquery.gift.js" charset="utf-8"></script>
</head>

<body>
	<main class="gift horizontal" style="background-image: url({c2r-app-bg});">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-8 offset-sm-2 machines_container tacenter">
					<div id="machine_1">{c2r-items}</div>
					<div id="machine_2">{c2r-items}</div>
					<div id="machine_3">{c2r-items}</div>
				</div>
			</div>
			<div class="spacer all-90"></div>
			<div class="panel-1 row sm-tacenter">
				<div class="col-sm-12">
					<h1>{c2r-prize-init}</h1>
				</div>
			</div>
			<div class="panel-2 row message sm-tacenter">
				<div class="col-sm-12">
					<h1></h1>
				</div>
			</div>
			<div class="panel-3 row no-stock sm-tacenter">
				<div class="col-sm-12">
					<h1>A repor stock...</h1>
				</div>
			</div>
			<div class="app-logo {c2r-app-logo-hidden} taright">
				<img src="{c2r-app-logo}" alt="">
			</div>
		</div>
	</main>

	<script type="text/javascript">
		var js_target = '{c2r-js-target}',
		id = '{c2r-id}',
		avatar = '{c2r-avatar}',
		signup = '{c2r-signup}',
		items_list = {c2r-items-list},

		winner_id = {c2r-winner-id},

		js_screensaver_status = {c2r-js-screensaver-status},
		js_screensaver_time = {c2r-js-screensaver-time};

		var machine_1, machine_2, machine_3;
	</script>
	<style media="screen">
		{c2r-settings-css}
	</style>
</body>
</html>
