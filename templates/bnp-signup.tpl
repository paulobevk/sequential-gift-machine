<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-signup.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-signup.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-signup.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>

		<!-- cKeyboard -->
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/cKeyboard.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/cKeyboard.mbl.css">
		<script src="{c2r-path}/site-assets/js/cKeyboard.js" charset="utf-8"></script>

		<script src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js"></script>
	</head>

	<body>
		<main class="signup" style="background-image:url({c2r-app-bg});">
			{c2r-header}
			<div class="spacer xs-15 sm-15 md-15 lg-90"></div>
			<!-- SLIDER -->
			<div class="carousel slide" id="signup-slider" data-ride="carousel" data-interval="false" data-wrap="false">
				<!-- PAGINATION -->
				<ol class="carousel-indicators d-none">
					{c2r-indicator-list}
				</ol>
				<!-- PAGINATION -->
				<!-- SLIDES -->
				<div class="carousel-inner" role="listbox">
					{c2r-list}
				</div>
				<!-- SLIDES -->
				<!-- ACTION BUTTONS -->
				<a href="#signup-slider" class="left carousel-control-prev d-none" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a href="#signup-slider" class="right carousel-control-next d-none" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				<!-- ACTION BUTTONS -->
			</div>
			<!-- SLIDER -->

			<div class="container-fluid">
				<div class="spacer xs-15 sm-15 md-15 lg-30"></div>
				<div class="spacer all-15"></div>
				<div class="row">
					<div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 offset-sm-1 offset-md-1 offset-lg-1 offset-xl-1 taright">
						<button type="button" name="button" class="accept-btn bnp-btn block spacer all-60" id="accept-btn" style="background-image:url({c2r-app-terms-bg}); opacity: .25;"></button>
						<input type="checkbox" name="accept-checkbox" class="d-none" id="accept-checkbox" value="1"/>
					</div>
					<div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
						<h4 class="terms">{c2r-terms-text}</h4>
					</div>
				</div>
				<div class="spacer xs-15 sm-15 md-30 lg-90"></div>
				<div class="row">
					<div class="col-12 {c2r-keyboard-theme}" id="keyboard"></div>
					<div class="col-12 {c2r-keyboard-theme}" id="keyboard_numeric" style="display: none"></div>
				</div>
			</div>

			<div class="app-logo {c2r-app-logo-hidden}">
				<img src="{c2r-app-logo}" alt="">
			</div>
			{c2r-footer}
		</main>

		<!-- Modal -->
		<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title text-center" id="termsModalLabel">Utilização de dados pessoais</h5>
					</div>
					<div class="modal-body">
						{c2r-terms-full-text}
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary go_ahead">Avançar mesmo assim!</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Voltar atrás</button>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			var js_api = '{c2r-path}/{c2r-lg}/bnp-signup-api/0/set',
			js_target = '{c2r-js-target}',
			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>
		<style type="text/css">{c2r-css}</style>
	</body>
</html>
