<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike-race.css">
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-bike.mbl.css">
		<script src="{c2r-path}/site-assets/js/bnp-bike-race.js" charset="utf-8"></script>

		<link rel="stylesheet" href="{c2r-path}/site-assets/libs/animate.css/animate.css">
		<script src="{c2r-path}/site-assets/libs/wow.js-1.3.0/wow.min.js" charset="utf-8"></script>
	</head>
	<body>
		<main class="bike" style="background-image:url({c2r-app-bg})">
			{c2r-header}
			<div class="countDown"><div>3</div></div>
			<div id="step-1" class="container-fluid">
				<div class="row">
					<div class="col tacenter">
						<div class="spacer xs-15 sm-150 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="spacer xs-15 sm-15 md-15px lg-150"></div>
						<div class="boxShake">
							<button type="button" class="bnp-start-game" id="startGame" style="background-image: url({c2r-step1-button})"></button>
						</div>
						<div class="spacer all-30"></div>
						<h1>Toca para jogar!</h1>
					</div>
				</div>
			</div>
			<div id="step-2" class="container-fluid">
				<div class="row">
					<div class="col animator" style="background-image: url('{c2r-path}/uploads/battery.png')">
						<div class="loader" style="background-image: url('{c2r-path}/uploads/animator-loader.svg')"></div>
					</div>
				</div>
				<div class="spacer sm-90"></div>
				<div class="row">
					<div class="col text-center message">
						<h1>PEDALA<br>sem parar!</h1>
					</div>
				</div>
				<div class="app-logo {c2r-app-logo-hidden}">
					<img src="{c2r-app-logo}" alt="">
				</div>
			</div>
			<div id="step-3" class="container-fluid">
				<div class="flex-center">
					Fim do Jogo
				</div>
			</div>
			<div id="step-4" class="container-fluid">
				<div class="flex-center">
					<div class="tacenter">
						FIZESTE
						<div class="points-append">

						</div>
						PONTOS!
					</div>
				</div>
			</div>
		</main>
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',

			time = '{c2r-time}',
			time_convertion = {c2r-time-convertion},

			signup = '{c2r-signup}',

			js_api = '{c2r-path}/{c2r-lg}/bnp-ranking-api/0/set',

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
		</script>
		<style media="screen">
			{c2r-settings-css}
		</style>
	</body>
</html>
