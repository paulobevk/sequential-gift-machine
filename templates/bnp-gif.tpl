<!DOCTYPE html>
<html lang="{c2r-lg}">
	<head>
		{c2r-head}
		<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-gif.css">
		<script src="{c2r-path}/site-assets/js/bnp-gif.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/jquery-ui.min.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/gif.min.js" charset="utf-8"></script>
		<script src="{c2r-path}/site-assets/js/caman.full.js"></script>
	</head>
	<body >
		<main class="gif" style="background-image:url({c2r-app-bg});">
			{c2r-header}
			<!-- LIVE CAMERA -->
			<div class="spacer all-90"></div>
			<div class="live-cam">
				<div class="sight tacenter" style="background-image: url('{c2r-sight}')"></div>
				<video id="video" width="1080" height="810" autoplay></video>
				<canvas id="canvas" class="d-none" width="1440" height="1080"></canvas>
				<img id="target-img" class="d-none" src="" data-camanwidth="1440" data-camanheight="1080">
			</div>
			<!-- LIVE CAMERA -->

			<div class="container-fluid tacenter">
				<div class="spacer all-90"></div>
				<!-- STEP 1 -->
				<div class="row step-1">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<h1>{c2r-step1-intro}</h1>
						<div class="spacer all-60"></div>
						<button type="button" class="camera-shot-btn bnp-btn" id="camera-shot-btn" style="background-image:url({c2r-button-bg});">
							<div class="spacer all-90"></div>
							<div class="spacer all-15"></div>
							<span>{c2r-step1-button-text}</span>
						</button>
					</div>
				</div>
				<!-- STEP 2 -->
				<div class="row step-2">
					<h1>{c2r-step2-intro}</h1>
					<div class="spacer all-60"></div>
					<img src="{c2r-step2-clock}" style="margin: auto;" class="wait-btn {c2r-step2-clock-display}">
				</div>
				<!-- STEP 3 -->
				<div class="row step-3">
					<h1>{c2r-step3-intro}</h1>
					<div class="spacer all-60"></div>
					<div class="container-fluid">
						<div class="row">
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 taright">
								<div class="spacer all-30"></div>
								<button type="button" class="repeat-btn bnp-btn" id="repeat-btn" style="background-image:url({c2r-step3-button-repeat});">
									<div class="spacer all-30"></div>
									<div class="spacer all-15"></div>
									<div><span>{c2r-step3-text-repeat}</span></div>
								</button>
							</div>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
								<button type="button" class="next-btn bnp-btn arrow" id="next-btn" style="background-image:url({c2r-step3-button-accept});">
									<div class="spacer all-90"></div>
									<div class="spacer all-15"></div>
									<span>{c2r-step3-text-accept}</span>
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- STEP 3.5 -->
				<div class="row step-3-5">
					<h1>{c2r-step3-5-intro}</h1>
					<h1 class="heading-progress"><span>0</span>%</h1>
				</div>
				<!-- STEP 4 -->
				<div class="row step-4">
					<div class="container-fluid">
					<div class="spacer all-30"></div>
						<div class="row d-flex justify-content-center align-items-center">
							{c2r-overlays-list}
						</div>
						<div class="spacer all-15"></div>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
								<h1>{c2r-step4-intro}</h1>
							</div>
						</div>
						<div class="spacer all-30"></div>
						<div class="row">
							<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xl-4 offset-sm-4 offset-md-4 offset-lg-4 offset-xl-4">
								<button type="button" class="next-btn bnp-btn arrow" id="next-btn" style="background-image:url({c2r-step4-button});">
									<div class="spacer all-90"></div>
									<div class="spacer all-15"></div>
									<span>{c2r-step4-text-accept}</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="app-logo {c2r-app-logo-hidden}">
				<img src="{c2r-app-logo}" alt="">
			</div>
			{c2r-footer}
		</main>
		<script type="text/javascript">
			var js_target = '{c2r-js-target}',
			id = '{c2r-id}',
			signup = '{c2r-signup}',
			overlay_tpl = '{c2r-overlay-tpl}',

			overlays = {c2r-overlays},
			snaps_width = {c2r-settings-snap-width},
			snaps_height = {c2r-settings-snap-height},
			snaps_limit = {c2r-settings-snap-limit},
			snaps_interval = {c2r-settings-snap-interval},
			gif_quality = {c2r-settings-gif-quality};
		</script>
		<style media="screen">
			{c2r-css}
		</style>
	</body>
</html>
