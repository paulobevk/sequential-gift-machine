<!DOCTYPE html>
<html lang="{c2r-lg}">

<head>
	{c2r-head}
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-prize.css">
	<link rel="stylesheet" href="{c2r-path}/site-assets/css/bnp-prize.mbl.css">
	<script src="{c2r-path}/site-assets/js/bnp-prize.js" charset="utf-8"></script>
	<script src="{c2r-path}/site-assets/js/bnp-screensaver-controller.js"></script>
</head>

<body>
	<main class="prize tacenter" style="background-image: url({c2r-app-bg});">
		{c2r-header}
		<!-- SLIDER -->
		<div class="spacer xs-15 sm-15 md-90 lg-150"></div>
		<div class="spacer xs-15 sm-15 md-15 lg-150"></div>
		<div class="step-container">
			<div class="step-1">
				<button type="button" class="prize-btn bnp-btn" id="prize-btn">
					<img src="{c2r-prize-btn}" class="prizeImg">
				</button>
			</div>
			<div class="step-2">
				{c2r-items}
			</div>
		</div>
		<div class="spacer xs-15 sm-15 md-60 lg-30"></div>
		<div class="step-1-1">
			<h1>{c2r-prize-init}</h1>
		</div>
		<div class="step-2-1">
			<h1>A sortear...</h1>
		</div>
		<div class="step-2-2">
			<h1 class="message">Ganhaste um<br>Marshmallow Plush</h1>
		</div>
		<div class="app-logo {c2r-app-logo-hidden}">
			<img src="{c2r-app-logo}" alt="">
		</div>
		{c2r-footer}
	</main>
	<script type="text/javascript">
		var js_target = '{c2r-js-target}',
			id = '{c2r-id}',
			avatar = '{c2r-avatar}',
			signup = '{c2r-signup}',

			rounds_min = {c2r-rounds-min},
			rounds_max = {c2r-rounds-max},
			winner_id = {c2r-winner-id},

			js_screensaver_status = {c2r-js-screensaver-status},
			js_screensaver_time = {c2r-js-screensaver-time};
	</script>
	<style media="screen">
		{c2r-settings-css}
	</style>
</body>

</html>
