-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 21-Jun-2019 às 15:55
-- Versão do servidor: 10.3.15-MariaDB
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bnp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_bike_settings`
--

CREATE TABLE `os_bnp_bike_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_config`
--

CREATE TABLE `os_bnp_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_gift_settings`
--

CREATE TABLE `os_bnp_gift_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_gif_settings`
--

CREATE TABLE `os_bnp_gif_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_memory_settings`
--

CREATE TABLE `os_bnp_memory_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_app`
--

CREATE TABLE `os_bnp_photo_app` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_modes`
--

CREATE TABLE `os_bnp_photo_modes` (
  `id` int(11) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `delay` text NOT NULL,
  `render_delay` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_photo_settings`
--

CREATE TABLE `os_bnp_photo_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_printer_settings`
--

CREATE TABLE `os_bnp_printer_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_answers`
--

CREATE TABLE `os_bnp_quiz_answers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `id_ass` int(11) NOT NULL,
  `code` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_question`
--

CREATE TABLE `os_bnp_quiz_question` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_quiz_settings`
--

CREATE TABLE `os_bnp_quiz_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking`
--

CREATE TABLE `os_bnp_ranking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking_schedule`
--

CREATE TABLE `os_bnp_ranking_schedule` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_ranking_settings`
--

CREATE TABLE `os_bnp_ranking_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_screensaver_settings`
--

CREATE TABLE `os_bnp_screensaver_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_item`
--

CREATE TABLE `os_bnp_stock_item` (
  `id` int(11) NOT NULL,
  `schedule_id` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_log`
--

CREATE TABLE `os_bnp_stock_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_schedule`
--

CREATE TABLE `os_bnp_stock_schedule` (
  `id` int(11) NOT NULL,
  `label` text NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `premium` tinyint(1) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_stock_settings`
--

CREATE TABLE `os_bnp_stock_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_tapgame_settings`
--

CREATE TABLE `os_bnp_tapgame_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_thanks_settings`
--

CREATE TABLE `os_bnp_thanks_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_thermo_printer_settings`
--

CREATE TABLE `os_bnp_thermo_printer_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users`
--

CREATE TABLE `os_bnp_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `comercial` tinyint(1) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL,
  `sync` tinyint(1) NOT NULL DEFAULT 0,
  `processed` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users_fields`
--

CREATE TABLE `os_bnp_users_fields` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `error_message` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `sort` int(11) DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_bnp_users_settings`
--

CREATE TABLE `os_bnp_users_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_files`
--

CREATE TABLE `os_files` (
  `id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `id_ass` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `sort` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_history`
--

CREATE TABLE `os_history` (
  `id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_modules`
--

CREATE TABLE `os_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_trash`
--

CREATE TABLE `os_trash` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_users`
--

CREATE TABLE `os_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rank` enum('owner','manager','member') DEFAULT 'member',
  `email` varchar(255) DEFAULT NULL,
  `code` text DEFAULT NULL,
  `custom_css` text NOT NULL,
  `user_key` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `os_users_fields`
--

CREATE TABLE `os_users_fields` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL,
  `type` text NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `os_bnp_bike_settings`
--
ALTER TABLE `os_bnp_bike_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_config`
--
ALTER TABLE `os_bnp_config`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_gift_settings`
--
ALTER TABLE `os_bnp_gift_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_gif_settings`
--
ALTER TABLE `os_bnp_gif_settings`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_memory_settings`
--
ALTER TABLE `os_bnp_memory_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_app`
--
ALTER TABLE `os_bnp_photo_app`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_modes`
--
ALTER TABLE `os_bnp_photo_modes`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_photo_settings`
--
ALTER TABLE `os_bnp_photo_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_printer_settings`
--
ALTER TABLE `os_bnp_printer_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_answers`
--
ALTER TABLE `os_bnp_quiz_answers`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_question`
--
ALTER TABLE `os_bnp_quiz_question`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_quiz_settings`
--
ALTER TABLE `os_bnp_quiz_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking`
--
ALTER TABLE `os_bnp_ranking`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking_schedule`
--
ALTER TABLE `os_bnp_ranking_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_ranking_settings`
--
ALTER TABLE `os_bnp_ranking_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_screensaver_settings`
--
ALTER TABLE `os_bnp_screensaver_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_item`
--
ALTER TABLE `os_bnp_stock_item`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_log`
--
ALTER TABLE `os_bnp_stock_log`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_bnp_stock_schedule`
--
ALTER TABLE `os_bnp_stock_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_stock_settings`
--
ALTER TABLE `os_bnp_stock_settings`
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_tapgame_settings`
--
ALTER TABLE `os_bnp_tapgame_settings`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_bnp_thanks_settings`
--
ALTER TABLE `os_bnp_thanks_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_thermo_printer_settings`
--
ALTER TABLE `os_bnp_thermo_printer_settings`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_bnp_users`
--
ALTER TABLE `os_bnp_users`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_users_fields`
--
ALTER TABLE `os_bnp_users_fields`
  ADD UNIQUE KEY `id` (`id`);

--
-- Índices para tabela `os_bnp_users_settings`
--
ALTER TABLE `os_bnp_users_settings`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Índices para tabela `os_files`
--
ALTER TABLE `os_files`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_history`
--
ALTER TABLE `os_history`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_modules`
--
ALTER TABLE `os_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `folder` (`folder`);

--
-- Índices para tabela `os_trash`
--
ALTER TABLE `os_trash`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `os_users`
--
ALTER TABLE `os_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_prefix_users_prefix_products1` (`id`),
  ADD KEY `fk_prefix_users_prefix_articles1` (`id`);

--
-- Índices para tabela `os_users_fields`
--
ALTER TABLE `os_users_fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `os_bnp_bike_settings`
--
ALTER TABLE `os_bnp_bike_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_config`
--
ALTER TABLE `os_bnp_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `os_bnp_gift_settings`
--
ALTER TABLE `os_bnp_gift_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_gif_settings`
--
ALTER TABLE `os_bnp_gif_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_memory_settings`
--
ALTER TABLE `os_bnp_memory_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_app`
--
ALTER TABLE `os_bnp_photo_app`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_modes`
--
ALTER TABLE `os_bnp_photo_modes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_photo_settings`
--
ALTER TABLE `os_bnp_photo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_printer_settings`
--
ALTER TABLE `os_bnp_printer_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_answers`
--
ALTER TABLE `os_bnp_quiz_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_question`
--
ALTER TABLE `os_bnp_quiz_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_quiz_settings`
--
ALTER TABLE `os_bnp_quiz_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking`
--
ALTER TABLE `os_bnp_ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking_schedule`
--
ALTER TABLE `os_bnp_ranking_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_ranking_settings`
--
ALTER TABLE `os_bnp_ranking_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_screensaver_settings`
--
ALTER TABLE `os_bnp_screensaver_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_item`
--
ALTER TABLE `os_bnp_stock_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_log`
--
ALTER TABLE `os_bnp_stock_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_schedule`
--
ALTER TABLE `os_bnp_stock_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_stock_settings`
--
ALTER TABLE `os_bnp_stock_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_tapgame_settings`
--
ALTER TABLE `os_bnp_tapgame_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_thanks_settings`
--
ALTER TABLE `os_bnp_thanks_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_thermo_printer_settings`
--
ALTER TABLE `os_bnp_thermo_printer_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_users`
--
ALTER TABLE `os_bnp_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_users_fields`
--
ALTER TABLE `os_bnp_users_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_bnp_users_settings`
--
ALTER TABLE `os_bnp_users_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_files`
--
ALTER TABLE `os_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_history`
--
ALTER TABLE `os_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_modules`
--
ALTER TABLE `os_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_trash`
--
ALTER TABLE `os_trash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `os_users`
--
ALTER TABLE `os_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
