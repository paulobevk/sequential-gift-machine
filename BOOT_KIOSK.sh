#!/bin/bash

# OPEN VPN
#openvpn --config nuc-4_at_evoke.ovpn

# OPEN CHROME
"/usr/bin/chromium-browser" http://127.0.0.1/pt/bnp-run/ --ignore-certificate-errors --use-fake-ui-for-media-stream --disable-popup-blocking --kiosk --incognito --disable-pinch --overscroll-history-navigation=0 --password-store=basic
# STOP FOR X SECONDS
#sleep 5
