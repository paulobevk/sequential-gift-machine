<?php

$cfg->db = new stdClass();

$cfg->db->connect = TRUE; // Database not connected by default. Change to TRUE to connect
$cfg->db->host = "127.0.0.1";
$cfg->db->user = "root";
$cfg->db->password = "";
$cfg->db->database = "bnp";
$cfg->db->prefix = "os";