<?php

if (is_dir("./backoffice") != FALSE) {
	$classes_folder = "./backoffice";
} else {
	$classes_folder = ".";
}

if (file_exists("{$classes_folder}/class/html2pdf/html2pdf.class.php")) {
	include "{$classes_folder}/class/html2pdf/html2pdf.class.php";
}
