<?php

class c0_project_sync {

	const PROTOCOL = "https";
	const DOMAIN = "www.brandnplay.com";
	const PATH = "/platform";
	const ARGS = "/en/api/0/";
	const TARGET = self::PROTOCOL."://".self::DOMAIN.self::PATH.self::ARGS;

	protected $id;
	protected $machine = [];
	protected $action = FALSE;
	protected $configs = [];
	protected $entries = [];
	protected $files = [];
	protected $object;
	protected $project;

	protected $data_to_send = FALSE;

	function __construct () {}

	function set_id ($i) {
		$this->id = $i;
	}

	function set_machine ($machine) {
		$this->machine = $machine;
	}

	function set_action ($a) {
		switch ($a) {
			case 'emailqueue':
				$this->action = $a;
				return TRUE;
				break;
			case 'participation':
				$this->action = $a;
				return TRUE;
				break;
			case 'users':
				$this->action = $a;
				return TRUE;
				break;
			case 'users_fields':
				$this->action = $a;
				return TRUE;
				break;
			case 'users_slides':
				$this->action = $a;
				return TRUE;
				break;
			case 'stock_item':
				$this->action = $a;
				return TRUE;
				break;
			case 'stock_schedule':
				$this->action = $a;
				return TRUE;
				break;
			case 'stock_log':
				$this->action = $a;
				return TRUE;
				break;
			case 'settings':
				$this->action = $a;
				return TRUE;
				break;
			case 'quiz':
				$this->action = $a;
				return TRUE;
				break;
			case 'ranking':
				$this->action = $a;
				return TRUE;
				break;
			case 'ranking_schedule':
				$this->action = $a;
				return TRUE;
				break;

			default:
				return FALSE;
				break;
		}
	}

	function set_object($o) {
		switch ($o) {
			case 'users':
				self::set_action("users");
				return $this->object = self::get_user();
				break;
			case 'users_fields':
				self::set_action("users_fields");
				return $this->object = self::get_users_fields();
				break;
			case 'users_slides':
				self::set_action("users_slides");
				return $this->object = self::get_users_slides();
				break;
			case 'stock_item':
				self::set_action("stock_item");
				return $this->object = self::get_item();
				break;
			case 'stock_schedule':
				self::set_action("stock_schedule");
				return $this->object = self::get_schedule();
				break;
			case 'stock_log':
				self::set_action("stock_log");
				return $this->object = self::get_logs();
				break;
			case 'settings':
				self::set_action("settings");
				return $this->object = self::get_settings();
				break;
			case 'quiz':
				self::set_action("quiz");
				return $this->object = self::get_quiz_options();
				break;
			case 'ranking':
				self::set_action("ranking");
				return $this->object = self::get_ranking();
				break;
			case 'ranking_schedule':
				self::set_action("ranking_schedule");
				return $this->object = self::get_ranking_schedule();
				break;
			default:
				return FALSE;
				break;
		}
	}

	function set_project() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_config WHERE name = '%s'",
			$cfg->db->prefix, "project-name"
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {

			$data = $source->fetch_object();
			$this->project = $data->value;

			return TRUE;
		}

		return FALSE;

	}

	function add_entry ($name = FALSE, $value = FALSE) {
		if ($name !== FALSE && $value !== FALSE) {
			array_push($this->entries, ["name" => $name, "value" => $value]);

			return TRUE;
		}
		return FALSE;
	}

	function add_config ($name = FALSE, $value = FALSE) {
		if ($name !== FALSE && $value !== FALSE) {
			$this->configs[$name] = $value;
			// array_push($this->configs, [$name => $value]);

			return TRUE;
		}
		return FALSE;
	}

	function add_file ($path) {
		array_push($this->files, $path);
	}

	public function get_schedule() {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_schedule WHERE true ORDER BY %s LIMIT %s",
			$cfg->db->prefix, "date_update DESC", 1
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			while ( $data = $source->fetch_object() ){
				$toReturn[$cfg->db->prefix."_bnp_stock_schedule"] = $data->date_update;
			}
		} else {
			$toReturn[$cfg->db->prefix."_bnp_stock_schedule"] = date("Y-m-d H:i:s", 0);
		}

		return json_encode($toReturn);
	}

	public function get_item() {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_item WHERE true ORDER BY %s LIMIT %s",
			$cfg->db->prefix, "date_update DESC", 1
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			while ( $data = $source->fetch_object() ){
				$toReturn[$cfg->db->prefix."_bnp_stock_item"] = $data->date_update;
			}
		} else {
			$toReturn[$cfg->db->prefix."_bnp_stock_item"] = date("Y-m-d H:i:s", 0);
		}

		return json_encode($toReturn);
	}

	public function get_logs() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_log WHERE sync = %s ORDER BY %s LIMIT %s",
			$cfg->db->prefix, 0, "date ASC", 10
		);

		$source = $db->query($query);

		$toReturn = [];

		if ($source->num_rows > 0) {
			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return json_encode($toReturn);
		}

		return FALSE;
	}

	function get_ranking() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_ranking WHERE sync = %s ORDER BY %s LIMIT %s",
			$cfg->db->prefix, 0, "date ASC", 10
		);

		$source = $db->query($query);

		$toReturn = [];

		if ($source->num_rows > 0) {
			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return json_encode($toReturn);
		}

		return FALSE;
	}

	function get_ranking_schedule() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_ranking_schedule WHERE true ORDER BY %s LIMIT %s",
			$cfg->db->prefix, "date_update DESC", 1
		);

		$source= $db->query($query);

		if ($source->num_rows > 0) {
			$data = $source->fetch_object();
			$toReturn["{$cfg->db->prefix}_bnp_ranking_schedule"] = $data->date_update;
		}

		return json_encode($toReturn);

	}

	public function get_user() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users WHERE status = %s AND date <= %s AND sync = %s ORDER BY %s LIMIT %s",
			$cfg->db->prefix, 1, "NOW() - INTERVAL 5 MINUTE", 0, 'date ASC', 1
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$data = $source->fetch_object();

			$query_f = sprintf(
				"SELECT * FROM %s_files WHERE module = '%s' AND user_id = %s",
				$cfg->db->prefix, 'bnp-users', $data->id
			);

			$source_f = $db->query($query_f);
			if ($source_f->num_rows > 0) {
				while ($file = $source_f->fetch_object()) {
					self::add_file($file->file);
				}
			}

			return json_encode($data);
		}

		return FALSE;
	}

	function get_users_fields() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users_fields WHERE true ORDER BY %s LIMIT %s",
			$cfg->db->prefix, "date_update DESC", 1
		);

		$source= $db->query($query);

		if ($source->num_rows > 0) {
			$data = $source->fetch_object();
			$toReturn["{$cfg->db->prefix}_bnp_users_fields"] = $data->date_update;
		}

		return json_encode($toReturn);
	}

	function get_users_slides() {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users_slides WHERE true ORDER BY %s LIMIT %s",
			$cfg->db->prefix, "date_update DESC", 1
		);

		$source= $db->query($query);

		if ($source->num_rows > 0) {
			$data = $source->fetch_object();
			$toReturn["{$cfg->db->prefix}_bnp_users_slides"] = $data->date_update;
		}

		return json_encode($toReturn);
	}

	public function get_settings () {
		global $cfg, $db;

		$toReturn = [];

		$query_s = sprintf(
			"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME LIKE '%s' OR TABLE_NAME LIKE '%s'",
			$cfg->db->database, "%settings%", "%bnp_config%"
		);

		$source_s = $db->query($query_s);

		if ($source_s->num_rows > 0) {
			$i = 0;
			while ($data_s = $source_s->fetch_object()) {

		 		$query[$i] = sprintf(
					"SELECT * FROM %s WHERE true ORDER BY %s LIMIT %s",
					$data_s->TABLE_NAME, "date_update DESC", 1
				);

				$source[$i] = $db->query($query[$i]);

				if ($source[$i]->num_rows > 0) {
					$data[$i] = $source[$i]->fetch_object();
					$toReturn[$data_s->TABLE_NAME] = $data[$i]->date_update;
				} else {
					$toReturn[$data_s->TABLE_NAME] = date("Y-m-d H:i:s", 0);
				}

				$i ++;
			}
		}

		return json_encode($toReturn);
	}

	function get_quiz_options() {
		global $cfg, $db;

		$toReturn = [];

		$query_s = sprintf(
			"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%s' AND TABLE_NAME LIKE '%s' OR TABLE_NAME LIKE '%s'",
			$cfg->db->database, "%quiz_answers%", "%quiz_question%"
		);

		$source_s = $db->query($query_s);

		if ($source_s->num_rows > 0) {
			$i = 0;
			while ($data_s = $source_s->fetch_object()) {

		 		$query[$i] = sprintf(
					"SELECT * FROM %s WHERE true ORDER BY %s LIMIT %s",
					$data_s->TABLE_NAME, "date_update DESC", 1
				);

				$source[$i] = $db->query($query[$i]);

				if ($source[$i]->num_rows > 0) {
					$data[$i] = $source[$i]->fetch_object();
					$toReturn[$data_s->TABLE_NAME] = $data[$i]->date_update;
				} else {
					$toReturn[$data_s->TABLE_NAME] = date('Y-m-d H:i:s', 0);
				}

				$i ++;
			}
		}

		return json_encode($toReturn);
	}

	function set_as_submited ($s) {
		switch ($s) {
			case 'users':
				return self::set_submited_users();
				break;
			case 'stock_log':
				return self::set_submited_logs();
				break;
			case 'ranking':
				return self::set_submited_ranking();
				break;
			default:
				return FALSE;
				break;
		}
	}

	function set_update($objs) {
		global $cfg, $db;
		$objs = get_object_vars($objs);

		switch ($this->action) {
			case 'settings':
				if (!empty($objs)) {
					foreach ($objs as $i => $obj) {
						foreach ($obj as $key => $value) {
							if (!isset($c)) {
								$c = 0;
							}

							if (self::check_setting($i, $value)) {
								$c++;
							}
						}
					}

					if ($c == count($obj)) {
						return TRUE;
					}
				}
				break;
				case 'stock_item':
					if (!empty($objs)) {
						foreach ($objs as $i => $obj) {
							foreach ($obj as $key => $value) {
								if (!isset($c)) {
									$c = 0;
								}

								if (self::check_stock_item($i, $value)) {
									$c++;
								}
							}
						}

						if ($c == count($obj)) {
							return TRUE;
						}
					}
					break;
				case 'stock_schedule':
					if (!empty($objs)) {
						foreach ($objs as $i => $obj) {
							foreach ($obj as $key => $value) {
								if (!isset($c)) {
									$c = 0;
								}

								if (self::check_stock_schedule($i, $value)) {
									$c++;
								}
							}
						}

						if ($c == count($obj)) {
							return TRUE;
						}
					}
					break;
			case 'quiz':
				if (!empty($objs)) {
					foreach ($objs as $i => $obj) {
						foreach ($obj as $key => $value) {
							if (!isset($c)) {
								$c = 0;
							}
							if (self::check_quiz($i, $value)) {
								$c++;
							}
						}
					}

					if ($c == count($obj)) {
						return TRUE;
					}
				}
				break;

			case 'ranking_schedule':
				if (!empty($objs)) {
					foreach ($objs as $i => $obj) {
						foreach ($obj as $key => $value) {
							if (!isset($c)) {
								$c = 0;
								}
							if (self::check_ranking($value)) {
								$c++;
							}
						}
					}

					if ($c == count($obj)) {
						return TRUE;
					}
				}
				break;

			case 'users_fields':
				if (!empty($objs)) {
					foreach ($objs as $i => $obj) {
						foreach ($obj as $key => $value) {
							if (!isset($c)) {
								$c = 0;
								}
							if (self::check_users_fields($value)) {
								$c++;
							}
						}
					}

					if ($c == count($obj)) {
						return TRUE;
					}
				}
				break;

			case 'users_slides':
				if (!empty($objs)) {
					foreach ($objs as $i => $obj) {
						foreach ($obj as $key => $value) {
							if (!isset($c)) {
								$c = 0;
								}
							if (self::check_users_slides($value)) {
								$c++;
							}
						}
					}

					if ($c == count($obj)) {
						return TRUE;
					}
				}
				break;
			default:
				return FALSE;
				break;
		}


	}

	/* SETTINGS FUNCTIONS */

	function check_setting($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {

			$query = sprintf(
				"SELECT * FROM %s WHERE name = '%s'",
				$table, $data->name
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_setting($table, $data);
			} else {
				return self::insert_setting($table, $data);
			}

		} else {
			return FALSE;
		}
	}

	function update_setting($table, $data) {
		global $cfg, $db;
		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s SET value = '%s', date_update = '%s' WHERE name = '%s'",
				$table, $data->value, $data->date_update, $data->name
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_setting($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"INSERT INTO %s (name, value, date, date_update) VALUES ('%s', '%s', '%s', '%s')",
				$table, $data->name, $data->value, $data->date, $data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* STOCK ITEM FUNCTIONS */

	function check_stock_item($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {

			$query = sprintf(
				"SELECT * FROM %s WHERE id = %s",
				$table, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_stock_item($table, $data);
			} else {
				return self::insert_stock_item($table, $data);
			}

		} else {
			return FALSE;
		}
	}

	function update_stock_item($table, $data) {
		global $cfg, $db;
		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s SET schedule_id = '%s', name = '%s', image = '%s', quantity = '%s', message = '%s', sort = '%s', date_update = '%s' WHERE id = '%s'",
				$table, $data->schedule_id, $data->name, $data->image, $data->quantity, $data->message, $data->sort, $data->date_update, $data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_stock_item($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
		print 	$query = sprintf(
				"INSERT INTO %s (id, schedule_id, name, image, quantity, message, sort, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
				$table, $data->id, $data->schedule_id, $data->name, $data->image, $data->quantity, $data->message, $data->sort, $data->date, $data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* STOCK SCHEDULE FUNCTIONS */

	function check_stock_schedule($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {

			$query = sprintf(
				"SELECT * FROM %s WHERE id = %s",
				$table, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_stock_schedule($table, $data);
			} else {
				return self::insert_stock_schedule($table, $data);
			}

		} else {
			return FALSE;
		}
	}

	function update_stock_schedule($table, $data) {
		global $cfg, $db;
		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s SET label = '%s', date_start = '%s', date_end = '%s', premium = '%s', date_update = '%s' WHERE id = '%s'",
				$table, $data->label, $data->date_start, $data->date_end, $data->premium, $data->date_update, $data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_stock_schedule($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"INSERT INTO %s (id, label, date_start, date_end, premium, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
				$table, $data->id, $data->label, $data->date_start, $data->date_end, $data->premium, $data->date, $data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* QUIZ FUNCTIONS */

	function check_quiz($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"SELECT * FROM %s WHERE id = %s",
				$table, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_quiz($table, $data);
			} else {
				return self::insert_quiz($table, $data);
			}

		} else {
			return FALSE;
		}
	}

	function update_quiz($table, $data) {
		global $cfg, $db;
		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s SET title = '%s', code = '%s', status = '%s', date = '%s', date_update = '%s' %s WHERE id = %s",
				$table, $db->real_escape_string($data->title), $data->code, $data->status, $data->date, $data->date_update, (isset($data->id_ass) ? ", id_ass = {$data->id_ass}" : ""), $data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_quiz($table, $data) {
		global $cfg, $db;

		if (isset($table) && !empty($table) && isset($data) && !empty($data)) {
			$query = sprintf(
				"INSERT INTO %s (id, title, code, status, date, date_update %s) VALUES ('%s', '%s', '%s', '%s', '%s', '%s' %s)",
				$table, (isset($data->id_ass) ? ", id_ass" : ""), $data->id, $db->real_escape_string($data->title), $data->code, $data->status, $data->date, $data->date_update, (isset($data->id_ass) ? ", {$data->id_ass}" : "")
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* RANKING FUNCTIONS */

	function check_ranking($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
			var_dump($data);
			$query = sprintf(
				"SELECT * FROM %s_bnp_ranking_schedule WHERE id = %s",
				$cfg->db->prefix, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_ranking($data);
			} else {
				return self::insert_ranking($data);
			}

		} else {
			return FALSE;
		}
	}

	function update_ranking($data) {
		global $cfg, $db;
		if (isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s_bnp_ranking_schedule SET label = '%s', date_start = '%s', date_end = '%s', date = '%s', date_update = '%s' WHERE id = %s",
				$cfg->db->prefix, $db->real_escape_string($data->label), $data->date_start, $data->date_end, $data->date, $data->date_update, $data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_ranking($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
			$query = sprintf(
				"INSERT INTO %s_bnp_ranking_schedule (id, label, date_start, date_end, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
				$cfg->db->prefix, $data->id, $db->real_escape_string($data->label), $data->date_start, $data->date_end, $data->date, $data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* USERS FIELDS FUNCTIONS */

	function check_users_fields($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
			var_dump($data);
			$query = sprintf(
				"SELECT * FROM %s_bnp_users_fields WHERE id = %s",
				$cfg->db->prefix, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_users_fields($data);
			} else {
				return self::insert_users_fields($data);
			}

		} else {
			return FALSE;
		}
	}

	function update_users_fields($data) {
		global $cfg, $db;
		if (isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s_bnp_users_fields SET label = '%s', name = '%s', value = '%s', type = '%s', error_message = '%s', required = %s, sort = %s, date = '%s', date_update = '%s' WHERE id = %s",
				$cfg->db->prefix,
				$db->real_escape_string($data->label),
				$db->real_escape_string($data->name),
				$db->real_escape_string($data->value),
				$data->type,
				$db->real_escape_string($data->error_message),
				$data->required,
				$data->sort,
				$data->date,
				$data->date_update,
				$data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_users_fields($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
		$query = sprintf(
				"INSERT INTO %s_bnp_users_fields (id, label, name, value, type, error_message, required, sort, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
				$cfg->db->prefix,
				$data->id,
				$db->real_escape_string($data->label),
 				$db->real_escape_string($data->name),
 				$db->real_escape_string($data->value),
 				$data->type,
				$db->real_escape_string($data->error_message),
 				$data->required,
 				$data->sort,
 				$data->date,
 				$data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* USERS SLIDES FUNCTIONS */

	function check_users_slides($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
			var_dump($data);
			$query = sprintf(
				"SELECT * FROM %s_bnp_users_slides WHERE id = %s",
				$cfg->db->prefix, $data->id
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				return self::update_users_slides($data);
			} else {
				return self::insert_users_slides($data);
			}

		} else {
			return FALSE;
		}
	}

	function update_users_slides($data) {
		global $cfg, $db;
		if (isset($data) && !empty($data)) {
			$query = sprintf(
				"UPDATE %s_bnp_users_slides SET name = '%s', published = %s, sort = %s, date = '%s', date_update = '%s' WHERE id = %s",
				$cfg->db->prefix,
				$db->real_escape_string($data->name),
				$data->published,
				$data->sort,
				$data->date,
				$data->date_update,
				$data->id
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	function insert_users_slides($data) {
		global $cfg, $db;

		if (isset($data) && !empty($data)) {
			$query = sprintf(
				"INSERT INTO %s_bnp_users_slides (id, name, published, sort, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
				$cfg->db->prefix,
				$data->id,
				$db->real_escape_string($data->name),
				$data->published,
				$data->sort,
				$data->date,
				$data->date_update
			);

			if ($db->query($query)) {
				return TRUE;
			} else {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	/* EOF FUNCTIONS */

	function set_submited_users() {
		global $cfg, $db;

		$user = json_decode($this->object);

		$query = sprintf(
			"UPDATE %s_bnp_users SET sync = '%s' WHERE id = %s",
			$cfg->db->prefix, (int)TRUE, $user->id
		);

		return (bool)$db->query($query);
	}

	function set_submited_logs() {
		global $cfg, $db;

		$toReturn = FALSE;

		$logs = json_decode($this->object);
		if (is_array($logs)) {
			foreach ($logs as $l => $log) {
				if (!isset($i)) {
					$i = 0;
				}
				$query = sprintf(
					"UPDATE %s_bnp_stock_log SET sync = %s WHERE id = %s",
					$cfg->db->prefix, (int)TRUE, $log->id
				);

				if ($db->query($query)) {
					$i++;
				}

			}

			if ($i == count($logs)) {
				$toReturn = TRUE;
			}

		}
		return $toReturn;
	}

	function set_submited_ranking() {
		global $cfg, $db;

		$toReturn = FALSE;

		$ranks = json_decode($this->object);

		if (is_array($ranks)) {
			foreach ($ranks as $r => $rank) {
				if (!isset($i)) {
					$i = 0;
				}
				$query = sprintf(
					"UPDATE %s_bnp_ranking SET sync = %s WHERE id = %s",
					$cfg->db->prefix, (int)TRUE, $rank->id
				);

				if ($db->query($query)) {
					$i++;
				}

			}

			if ($i == count($ranks)) {
				$toReturn = TRUE;
			}

		}
		return $toReturn;
	}

	/*
	function sync($machine) {
		global $cfg, $db;

		define('MULTIPART_BOUNDARY', '--------------------------'.microtime(true));

		$options = [
			"ssl" => [ "verify_peer" => FALSE, "verify_peer_name" => FALSE ],
			"http" => [
				"method" => "POST",
				"header" => "Content-Type: multipart/form-data; boundary=".MULTIPART_BOUNDARY,
				"content" => ""
			]
		];

		if (isset($this->files) && is_array($this->files))  {
			foreach ($this->files as $f => $file) {
				if (file_exists(BASE_PATH."/uploads/{$file}")) {
					$options["http"]["content"] .=
					"--".MULTIPART_BOUNDARY."\r\n".
					"Content-Disposition: form-data; name=\"".basename($file)."\"; filename=\"".basename($file)."\"\r\n".
					"Content-Type: application/octet-stream\r\n\r\n".
					file_get_contents(BASE_PATH."/uploads/{$file}")."\r\n";
				}
			}
		}
		// SEND MACHINE NAME
		$options["http"]["content"] .= "--".MULTIPART_BOUNDARY."\r\n"."Content-Disposition: form-data;
		name=\"machine-name\"\r\n\r\n"."{$this->machine->machine_name}\r\n";

		// SEND MACHINE API_KEY
		$options["http"]["content"] .= "--".MULTIPART_BOUNDARY."\r\n"."Content-Disposition: form-data;
		name=\"api-key\"\r\n\r\n"."{$this->machine->api_key}\r\n";

		// SEND PROJECT NAME
		$options["http"]["content"] .= "--".MULTIPART_BOUNDARY."\r\n"."Content-Disposition: form-data;
		name=\"project-name\"\r\n\r\n"."{$this->project}\r\n";

		// ADD FORM FIELDS
		$options["http"]["content"] .= "--".MULTIPART_BOUNDARY."\r\n"."Content-Disposition: form-data;
		name=\"object\"\r\n\r\n"."{$this->object}\r\n";

		// ADD FILES
		$options["http"]["content"] .= "--".MULTIPART_BOUNDARY."\r\n"."Content-Disposition: form-data;
		name=\"files\"\r\n\r\n"."{$this->object}\r\n";

		echo $response = file_get_contents( self::TARGET.$this->action, FALSE, stream_context_create($options));

		$response = json_decode($response);

		if (!is_null($response) && !isset($response->error) && isset($response->success)) {
			if (isset($response->object) && !empty($response->object)) {
				if ($this->set_update($response->object)) {
					print "<br> Update Done";
				} else {
					print "<br> Something Went Wrong";
				}
			} else {
				if ($this->set_as_submited($this->action)) {
					print "<br>Update Done";
				} else {
					print "<br>Update Not Done";
				}
			}
		}

		return FALSE;
	}
	*/

	function sync($machine) {
		global $cfg, $db;

		$headers = array("Content-Type:multipart/form-data");
		$postfields = [
			"machine-name"  => $this->machine->machine_name,
			"api-key"  		=> $this->machine->api_key,
			"project-name"  => $this->project,
			"object" 		=> $this->object
		];

		if (isset($this->files) && is_array($this->files))  {
			foreach ($this->files as $f => $file) {
				if (file_exists(BASE_PATH."/uploads/{$file}")) {

					$postfields["file_{$f}"] = curl_file_create(BASE_PATH."/uploads/{$file}");
				}
			}
		}

		$ch = curl_init();
		$options = [
			CURLOPT_URL => self::TARGET.$this->action,
			CURLOPT_HEADER => false,
			CURLOPT_POST => 1,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POSTFIELDS => $postfields,
			// CURLOPT_INFILESIZE => $filesize,
			CURLOPT_RETURNTRANSFER => true
		];

		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		$response = json_decode($response);

		if (!curl_errno($ch)) {
			$info = curl_getinfo($ch);
			if ($info['http_code'] == 200) {
				if (!is_null($response) && !isset($response->error) && isset($response->success)) {
					if (isset($response->object) && !empty($response->object)) {
						if ($this->set_update($response->object)) {
							print "<br> Update Done";
						} else {
							print "<br> Something Went Wrong";
						}
					} else {
						if ($this->set_as_submited($this->action)) {
							print "<br>Update Done";
						} else {
							print "<br>Update Not Done";
						}
					}
				}
			}
		} else {
			$errmsg = curl_error($ch);
		}

		curl_close($ch);

		return FALSE;
	}
}
