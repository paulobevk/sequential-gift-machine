<?php

class bnp_quiz {

	protected $id;
	protected $id_ass;
	protected $date;
	protected $date_update;

	public function __construct () {}

	public function setId($i) {
		$this->id = $i;
	}

	public function setIdAss($ia) {
		$this->id_ass = $ia;
	}
	public function setContent($t, $d) {
		$this->title = $t;
		$this->content = $d;
	}

	public function setTitle($t) {
		$this->title = $t;
	}

	public function setCode($c) {
		$this->code = $c;
	}

	public function setStatus($s) {
		$this->status = $s;
	}

	public function setDate($d = null) {
		$this->date = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public function setDateUpdate($d = null) {
		$this->date_update = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	// BNP QUIZ QUESTIONS

	public function insertQuestion() {
		global $cfg, $db;

		$query = sprintf("INSERT INTO %s_bnp_quiz_question (`title`, `code`, `status`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->code),
			$this->status,
			$this->date,
			$this->date_update
		);

		return $db->query($query);

	}

	public function updateQuestion() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_quiz_question SET title = '%s', code = '%s', status = %s, `date` = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->code),
			$this->status,
			$this->date,
			$this->id
		);

		return $db->query($query);
	}

	public function deleteQuestion() {
		global $cfg, $db, $authData;

		$gp = new bnp_quiz();
		$gp->setId($this->id);
		$gp = $gp->returnOneQuestion();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_quiz_question WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public function returnAllQuestions() {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_quiz_question",
			$cfg->db->prefix
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}

		return $toReturn;

	}

	public function returnOneQuestion() {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_quiz_question WHERE id = %s",
			$cfg->db->prefix, $this->id
		);

		$source = $db->query($query);

		return $source->fetch_object();

	}

	// EOF BNP QUIZ QUESTIONS

	// BNP QUIZ ANSWERS

	public function insertAnswer () {
		global $cfg, $db;

		$toReturn = [
			"status" => FALSE,
			"message" => "",
			"object" => []
		];

		$query = sprintf("INSERT INTO %s_bnp_quiz_answers (`title`, `id_ass`, `status`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$this->id_ass,
			$this->status,
			$this->date,
			$this->date_update
		);

		if ($db->query($query)){
			$toReturn["object"] = $db->insert_id;
			$toReturn["status"] = TRUE;
		}

		return json_encode($toReturn);
	}

	public function updateAnswer() {
		global $cfg, $db;

		$toReturn = [
			"status" => FALSE,
			"message" => "",
			"object" => []
		];

		$query = sprintf(
			"UPDATE %s_bnp_quiz_answers SET  title = '%s', status = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$this->status,
			$this->date_update,
			$this->id
		);

		if($db->query($query)) {
			$toReturn["status"] = TRUE;
		}

		return json_encode($toReturn);
	}

	public function deleteAnswer() {
		global $cfg, $db, $authData;

		$gp = new bnp_quiz();
		$gp->setId($this->id);
		$gp = $gp->returnOneAnswer();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_quiz_answers WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public function returnOneAnswer () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_quiz_answers WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public function returnAnswers() {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_quiz_answers WHERE id_ass = %s",
			$cfg->db->prefix, $this->id_ass
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}

		return $toReturn;
	}

	// EOF BNP QUIZ ANSWERS

	// BNP QUIZ SETTINGS

	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_quiz_settings (`name`, `value`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date,
			$this->date_update
		);
		if ($db->query($query)){
			return true;
		}

		return false;
	}

	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_quiz_settings SET  name = '%s', value = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date_update,
			$this->id
		);
		return $db->query($query);
	}

	public static function update_Setting($args = []) {
		global $cfg, $db;

		$toReturn = true;

		foreach ($args as $name => $value) {
			$query = sprintf(
				"UPDATE %s_bnp_quiz_settings SET value = '%s' WHERE name = '%s' LIMIT %s",
				$cfg->db->prefix, $value, $name, 1
			);

			if ($db->query($query) == false) {
				$toReturn = false;
			}
		}

		return $toReturn;
	}

	public function deleteSetting() {
		global $cfg, $db, $authData;

		$gp = new bnp_quiz();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_quiz_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public static function returnSettings () {
		global $cfg, $db;
		$query = sprintf("SELECT * FROM %s_bnp_quiz_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function getSettings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_quiz_settings WHERE true",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		foreach ($list as $index => $value) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$toReturn[$value->name] = $value->value;
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public function returnOneSetting () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_quiz_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	// EOF BNP QUIZ SETTINGS

	// BNP QUIZ FUNCTIONS

	public static function checkQuestion($id = null) {
		global $cfg, $db;

		$toReturn = FALSE;

		if(!is_null($id) && $id != 0) {
			$source = $db->query(
				sprintf(
					"SELECT * FROM %s_bnp_quiz_answers WHERE id_ass = '%s' AND status = '%s'",
					$cfg->db->prefix, $id, 1
				)
			);

			if($source->num_rows > 0) {
				$toReturn = TRUE;
			}
		}

		return $toReturn;

	}

	public static function getQuestions ($number = 1) {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_quiz_question WHERE status = %s ORDER BY RAND()",
			$cfg->db->prefix,
			1
		);

		$source = $db->query($query);

		$c = 1;

		while ($data = $source->fetch_object()) {
			if($c <= $number) {
				if(self::checkQuestion($data->id)) {
					array_push($toReturn, $data);
					$c++;
				}
			} else {
				break;
			}
		}

		return $toReturn;
	}

	public static function getAnswer ($id) {
		global $cfg, $db;
		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_quiz_answers WHERE id_ass = %s AND status = %s ORDER BY RAND() LIMIT %s",
			$cfg->db->prefix,
			$id,
			1,
			1
		);

		$source = $db->query($query);

		$toReturn = $source->fetch_object();

		return $toReturn;
	}

	public static function getWrongAnswer ($id, $total) {
		global $cfg, $db;
		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_quiz_answers WHERE id_ass = %s AND status = %s ORDER BY RAND() LIMIT %s",
			$cfg->db->prefix,
			$id,
			0,
			$total
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}
		return $toReturn;
	}

	// EOF BNP QUIZ FUNCTIONS
}
