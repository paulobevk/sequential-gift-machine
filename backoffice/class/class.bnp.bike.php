<?php

class bnp_bike {
	protected $id;
	protected $title;
	protected $content;

	/* BNP MEMORY SETTINGS */

	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_bike_settings (`name`, `value`) VALUES ('%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content)
		);

		if ($db->query($query)){
			return true;
		}

		return false;

	}

	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_bike_settings SET  name = '%s', value = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->id
		);
		return $db->query($query);
	}

	public function deleteSetting () {
		global $cfg, $db, $authData;

		$gp = new bnp_thanks();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_bnp_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public function returnOneSetting () {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_bike_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function returnAllSettings () {
		global $cfg, $db;
		$query = sprintf("SELECT * FROM %s_bnp_bike_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function return_settings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_bike_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		if (isset($list)) {
			foreach ($list as $index => $value) {
				if (!isset($toReturn)) {
					$toReturn = [];
				}

				$toReturn[$value->name] = $value->value;
			}
		}

		return isset($toReturn) ? $toReturn : false;
	}

	/* EOF BNP MEMORY SETTINGS */
}
