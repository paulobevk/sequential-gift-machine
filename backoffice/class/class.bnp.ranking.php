<?php

class bnp_ranking {

	protected $id;
	protected $label;
	protected $start;
	protected $end;
	protected $date;
	protected $date_update;

	public function __construct () {}

	public function setId($i) {
		$this->id = $i;
	}
	public function setContent($t, $d) {
		$this->title = $t;
		$this->content = $d;
	}

	public function setLabel($l) {
		$this->label = $l;
		return $this->label;
	}

	public function setStart($s) {
		$this->start = $s;
	}

	public function setEnd($e) {
		$this->end = $e;
	}

	public function setDate($d = null) {
		$this->date = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public function setDateUpdate($d = null) {
		$this->date_update = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	// EOF BNP RANKING SETTINGS

	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_ranking_settings (`name`, `value`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date,
			$this->date_update
		);
		if ($db->query($query)){
			return true;
		}

		return false;

	}

	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_ranking_settings SET  name = '%s', value = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date_update,
			$this->id
		);
		return $db->query($query);
	}

	public function deleteSetting () {
		global $cfg, $db, $authData;

		$gp = new bnp_ranking();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_ranking_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public static function return_settings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_ranking_settings WHERE true",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		foreach ($list as $index => $value) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$toReturn[$value->name] = $value->value;
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public function returnOneSetting () {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_ranking_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function returnAllSettings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_ranking_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	// EOF BNP RANKING SETTINGS

	// BNP RANKING FUNCTIONS

	public static function returnData ($qtd = false, $schedule = false) {
		global $cfg, $db;

		if ($schedule !== false && !empty($schedule))  {
			$query = sprintf(
				"SELECT ranking.*, users.name FROM %s_bnp_ranking AS ranking INNER JOIN %s_bnp_users AS users ON ranking.user_id = users.id WHERE users.date_update >= '%s' AND users.date_update <= '%s' ORDER BY ranking.points DESC %s",
				$cfg->db->prefix,
				$cfg->db->prefix,
				$schedule->date_start,
				$schedule->date_end,
				($qtd != false) ? "LIMIT {$qtd}" : ""
			);

			$source = $db->query($query);

			while ($data = $source->fetch_object()) {
				if (!isset($toReturn)) {
					$toReturn = [];
				}
				array_push($toReturn, $data);
			}
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function  returnResultById ($id = false, $schedule = false) {
		global $cfg, $db;

		if ($id !== false) {
			if ($schedule !== false && !empty($schedule))  {
			$query = sprintf(
					"SELECT ranking.*, users.name FROM %s_bnp_ranking AS ranking INNER JOIN %s_bnp_users AS users ON ranking.user_id = users.id WHERE users.date_update >= '%s' AND users.date_update <= '%s' ORDER BY ranking.points DESC",
					$cfg->db->prefix,
					$cfg->db->prefix,
					$schedule->date_start,
					$schedule->date_end
				);

				$source = $db->query($query);
				$i = 1;
				while ($data = $source->fetch_object()) {
					if ($data->user_id == $id) {
						$data->position = $i;
						return $data;
					}
					$i++;
				}
			}
		}
		return false;
	}

	public static function returnSchedules () {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf(
			"SELECT * FROM %s_bnp_ranking_schedule ORDER BY date_start ASC",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}

		return $toReturn;
	}

	public static function returnSchedulesID ($id) {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_ranking_schedule WHERE  id = %s ORDER BY date_start ASC",
			$cfg->db->prefix,
			$id
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$toReturn = [];

			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return $toReturn;
		}

		return false;
	}

	public function returnRankingExport($schedule) {
		global $cfg, $db;

		$toReturn = [];

		if ($schedule != "all") {
			$query = sprintf(
				"SELECT ranking.*, schedule.*, users.name FROM %s_bnp_ranking AS ranking INNER JOIN %s_bnp_ranking_schedule AS schedule ON schedule.id = %s INNER JOIN %s_bnp_users AS users ON ranking.user_id = users.id WHERE ranking.date >= schedule.date_start AND ranking.date <= schedule.date_end ORDER BY ranking.points DESC",
				$cfg->db->prefix,
				$cfg->db->prefix,
				$schedule,
				$cfg->db->prefix
			);
		} else {
			$query = sprintf("SELECT ranking.*, users.name FROM %s_bnp_ranking AS ranking INNER JOIN %s_bnp_users AS users ON ranking.user_id = users.id ORDER BY ranking.points DESC",
				$cfg->db->prefix,
				$cfg->db->prefix
			);
		}

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$toReturn = [];

			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return $toReturn;
		}

		return false;
	}

	// EOF BNP RANKING FUNCTIONS

	// BNP RANKING SCHEDULE

	public function insertSchedule() {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_ranking_schedule (label, date_start, date_end, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$this->label,
			$this->start,
			$this->end,
			date("Y-m-d H:i", time()),
			date("Y-m-d H:i", time())
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public function updateSchedule () {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_ranking_schedule SET label = '%s', date_start = '%s', date_end = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$this->label,
			$this->start,
			$this->end,
			date("Y-m-d H:i", time()),
			$this->id
		);

		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public function deleteSchedule($id) {
		global $cfg, $db;
		$query = sprintf("DELETE FROM %s_bnp_ranking_schedule WHERE id = %s",
			$cfg->db->prefix,
			$id
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public static function returnCurrentSchedule() {
		global $cfg, $db;

		$toReturn = FALSE;

		$query = sprintf(
				"SELECT * FROM %s_bnp_ranking_schedule WHERE date_start <= '%s' AND date_end >= '%s' ORDER BY %s LIMIT %s",
				$cfg->db->prefix, date('Y-m-d H:i:s', time()), date('Y-m-d H:i:s', time()), 'date_update ASC', 1
			);

		$source = $db->query($query);
		if($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return $toReturn;

	}

	// EOF BNP RANKING SCHEDULE
}
