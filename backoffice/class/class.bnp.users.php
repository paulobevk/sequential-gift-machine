<?php

class bnp_users {

	protected $id;
	protected $pi;
	protected $date;
	protected $date_update;

	function __construct () {}

	public function setId ($i) {
		$this->id = $i;
	}
	public function setParentId ($pi) {
		$this->pi = $pi;
	}

	public function setContent($t, $d) {
		$this->title = $t;
		$this->content = $d;
	}

	public function setDate($d = null) {
		$this->date = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public function setDateUpdate($d = null) {
		$this->date_update = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	/* BNP USERS QUERIES */

	public static function returnUsers () {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users WHERE true",
			$cfg->db->prefix
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}
			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function returnSearchUsers ($args = "") {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users WHERE name LIKE '%s' OR email LIKE '%s'",
			$cfg->db->prefix, "%".$db->real_escape_string($args)."%", "%".$db->real_escape_string($args)."%"
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}
			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	/* EOF BNP USERS QUERIES */

	/* BNP USERS SETTINGS */

	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_users_settings (`name`, `value`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date,
			$this->date_update
		);
		if ($db->query($query)){
			return true;
		}

		return false;
	}

	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_users_settings SET  name = '%s', value = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date_update,
			$this->id
		);

		return $db->query($query);
	}

	public function deleteSetting () {
		global $cfg, $db, $authData;

		$gp = new bnp_users();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_users_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}

	public function returnOneSetting () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_users_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function return_settings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_users_settings WHERE true",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		foreach ($list as $index => $value) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$toReturn[$value->name] = $value->value;
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function returnAllSettings () {
		global $cfg, $db;
		$query = sprintf("SELECT * FROM %s_bnp_users_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function addUser ($obj = []) {
		global $cfg, $db;

		if (count($obj) == 6) { // verify if all elements are inside, 6 total
			$query = sprintf(
				"INSERT INTO %s_bnp_users (name, email, code, date, date_update, status) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
				$cfg->db->prefix,
				$obj['name'], $obj['email'], $obj['code'], $obj['date'], $obj['date_update'], $obj['status']
			);

			return $db->query($query);
		}

		return false;
	}

	/* EOF BNP USERS SETTINGS */

	/* BNP USERS FIELDS */

	public function insertField ($value, $error, $sort, $required, $type) {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_users_fields (`slide_id`, `label`,`name`, `value`, `type`, `error_message`,`required`, `sort`) VALUES ('%s', '%s', '%s','%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$this->pi,
			$this->title,
			$this->content,
			$value,
			$type,
			$error,
			$required,
			$sort
		);
		bo3::dump($query);
		if ($db->query($query)){
			return true;
		}

		return false;
	}
	public function updateField ($value, $error, $sort, $required, $type) {
		global $cfg, $db;
		$query = sprintf("UPDATE %s_bnp_users_fields SET `label`= '%s',`name`= '%s', `value`= '%s', `type`= '%s', `error_message`= '%s',`required`= '%s', `sort`= '%s' WHERE id = %s",
			$cfg->db->prefix,
			$this->title,
			$this->content,
			$value,
			$type,
			$error,
			$required,
			$sort,
			$this->id
		);
		bo3::dump($query);
		if ($db->query($query)){
			return true;
		}

		return false;
	}

	public static function returnFields ($slide_id = false) {
		global $cfg, $db;


		$query = sprintf(
			"SELECT * FROM %s_bnp_users_fields WHERE status = %s ORDER BY sort ASC",
			$cfg->db->prefix, 1
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}
			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function return_field ($id) {
		global $cfg, $db;
		$query = sprintf(
			"SELECT * FROM %s_bnp_users_fields WHERE id = %s LIMIT 1",
			$cfg->db->prefix, $id
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function delete_field ($id = false) {
		global $cfg, $db;

		if ($id) {
			$query = sprintf(
				"DELETE FROM %s_bnp_users_fields WHERE id = %s",
				$cfg->db->prefix, $id
			);

			$db->query($query);

			return ($db->affected_rows > 0) ? true : false;
		}

		return false;
	}

	/* EOF BNP USERS FIELDS */

	/* BNP USERS SLIDES */

	public static function insert_slide ($name, $sort = 0, $published = false) {
		global $cfg, $db;

		print $query = sprintf(
			"INSERT INTO %s_bnp_users_slides (name, sort, published, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix, $name, (int)$sort, (bool)$published, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
		);

		$db->query($query);

		return ($db->affected_rows > 0) ? true : false;
	}

	public static function update_slide ($name, $sort = 0, $published = false, $id) {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_users_slides SET name = '%s', sort = '%s', published = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix, $name, (int)$sort, (bool)$published, date('Y-m-d H:i:s'), $id
		);

		$db->query($query);

		return ($db->affected_rows > 0) ? true : false;
	}

	public function updatePublished () {
		global $cfg, $db;

		$toReturn = FALSE;

		$query = sprintf(
			"UPDATE %s_bnp_users_slides SET published = !published WHERE id = %s",
			$cfg->db->prefix, $this->id
		);

		if($db->query($query)) {
			$toReturn = TRUE;
		}

		return $toReturn;
	}

	public static function return_slide ($id) {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users_slides WHERE id = %s LIMIT 1",
			$cfg->db->prefix, $id
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function returnSlides () {
		global $cfg, $db;

		$query = sprintf(
			"SELECT * FROM %s_bnp_users_slides WHERE true ORDER BY sort ASC",
			$cfg->db->prefix
		);

		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$fields = bnp_users::returnFields($data->id);
			$data->fields = ($fields !== false) ? count($fields) : 0;

			array_push($toReturn, $data);
		}

		return isset($toReturn) ? $toReturn : false;
	}

	public static function delete_slide ($id = false) {
		global $cfg, $db;

		if ($id !== false) {
			$query = sprintf(
				"DELETE FROM %s_bnp_users_slides WHERE id = %s",
				$cfg->db->prefix, $id
			);

			$db->query($query);

			return ($db->affected_rows > 0) ? true : false;
		}

		return false;
	}

	/* EOF BNP USERS SLIDES */

}
