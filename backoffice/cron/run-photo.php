<?php

$settings = bnp_photo::return_settings();
$mode = bnp_photo::return_mode_settings();

$query = sprintf(
	"SELECT * FROM %s_bnp_users WHERE status = %s AND processed = %s AND date <= '%s' ORDER BY %s LIMIT %s",
	$cfg->db->prefix, 1, 0, date("Y-m-d H:i:s", time() - 2),"RAND()", 1
);

$source = $db->query($query);

if($source->num_rows > 0) {
	$data = $source->fetch_object();

	$path ="uploads/photo-gif/{$data->id}/";
	$images = [];

	if (
		_global_::check_folder($path)
	) {
		if(file_exists("c:/xampp/htdocs/bnp/uploads/photo-gif/{$data->id}/0000.jpg")) {
			$im = imagecreatefromjpeg("c:/xampp/htdocs/bnp/uploads/photo-gif/{$data->id}/0000.jpg");

			$format = explode(":", $settings["format"]);
			$frame_width = $settings["image-width"];
			$frame_height = $settings["image-height"];
			$f_width = $settings["image-height"];
			$f_height = round($f_width * $format[1] / $format[0]);
			$margin = ($frame_width - $f_height) / 2;

			$shell_crop_cmd = sprintf(
				"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\0000.jpg -filter:v \"crop=%s:%s:%s:%s\" C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\0000-cropped.jpg",
				$data->id, $f_width, $f_height, 0, $margin, $data->id
			);

			shell_exec($shell_crop_cmd);

			if(
				file_exists("c:/xampp/htdocs/bnp/uploads/photo-gif/{$data->id}/0000-cropped.jpg")
			) {

				$shell_overlay_cmd = sprintf(
					"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\0000-cropped.jpg -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\overlay.png -filter_complex \"overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2\" C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\%s.jpg",
					$data->id, $data->id, $data->id, $data->id, $data->id
				);

				shell_exec($shell_overlay_cmd);

				if(
					file_exists(
						sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\%s.jpg", $data->id, $data->id)
					)
				) {

					$query_processed = sprintf(
						"UPDATE %s_bnp_users SET processed = %s WHERE id = %s",
						$cfg->db->prefix, 1, $data->id
					);

					if($db->query($query_processed)) {
						$query_file = sprintf(
							"INSERT INTO %s_files (file, type, module, id_ass, user_id, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
							$cfg->db->prefix, "photo-gif/{$data->id}/{$data->id}.jpg", "file", "bnp-users", $data->id, $data->id, date('Y-m-d H:i:s', time()), date('Y-m-d H:i:s', time())
						);

						if($db->query($query_file)) {
							unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\0000-cropped.jpg", $data->id));
							print "Photo created with Success!";
							return TRUE;
						}
					}
				}

			}
		}

	}

}

return FALSE;
