<?php

define("BASE_PATH", "/opt/lampp/htdocs/");

set_time_limit(0);

require_once(BASE_PATH."/backoffice/config/cfg.php");
require_once(BASE_PATH."/backoffice/config/database.php");
require_once(BASE_PATH."/backoffice/config/email.php");
require_once(BASE_PATH."/backoffice/config/languages.php");
require_once(BASE_PATH."/backoffice/config/system.php");

require_once(BASE_PATH."/backoffice/controller/database.php");

require_once(BASE_PATH."/backoffice/class/class.bo3.php");
require_once(BASE_PATH."/backoffice/class/class.bnp.photo.php");
require_once(BASE_PATH."/pages-e/_global_.php");

$settings = bnp_photo::return_settings();

switch ($settings["mode"]) {
	case 'photo':
		require_once(BASE_PATH."/backoffice/cron/run-photo.php");
		$tpl = "";
		break;
	case 'gif':
		require_once(BASE_PATH."/backoffice/cron/run-video.php");
		$tpl = "";
		break;
	case 'slowmotion':
		require_once(BASE_PATH."/backoffice/cron/run-video.php");
		$tpl = "";
		break;
	case 'timelapse':
		require_once(BASE_PATH."/backoffice/cron/run-video.php");
		$tpl = "";
		break;
	case 'cinemagraph':
		require_once(BASE_PATH."/backoffice/cron/run-cinemagraph.php");
		$tpl = "";
		break;
	case 'boomerang':
		require_once(BASE_PATH."/backoffice/cron/run-boomrang.php");
		$tpl = "";
		break;
	default:
		$tpl = "";
		break;
}
