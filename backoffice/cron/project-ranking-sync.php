<?php

define("JSON_PATH", "/home/evk/Documents");
define("BASE_PATH", "/opt/lampp/htdocs");

set_time_limit(0);

require_once(BASE_PATH."/backoffice/config/cfg.php");
require_once(BASE_PATH."/backoffice/config/database.php");
require_once(BASE_PATH."/backoffice/config/email.php");
require_once(BASE_PATH."/backoffice/config/languages.php");
require_once(BASE_PATH."/backoffice/config/system.php");

require_once(BASE_PATH."/backoffice/controller/database.php");

require_once(BASE_PATH."/backoffice/class/class.bo3.php");
require_once(BASE_PATH."/backoffice/class/class.0.project-sync.php");

// GET INFO ABOUT THIS MACHINE
$machine = json_decode(file_get_contents(JSON_PATH."/project-update-client.json"));

$sync = new c0_project_sync();
if($sync->set_object("ranking")) {
	$sync->set_project();
	$sync->set_machine($machine);
	$sync->sync($machine);
}
