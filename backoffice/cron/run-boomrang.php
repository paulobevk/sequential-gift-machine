<?php

$settings = bnp_photo::return_settings();
$mode = bnp_photo::return_mode_settings();

$query = sprintf(
	"SELECT * FROM %s_bnp_users WHERE status = %s AND processed = %s AND date <= '%s' ORDER BY %s LIMIT %s",
	$cfg->db->prefix, 1, 0, date("Y-m-d H:i:s", time() - 2),"RAND()", 1
);

$source = $db->query($query);

if($source->num_rows > 0) {
	$data = $source->fetch_object();

	$path ="uploads/photo-gif/{$data->id}/";
	$images = [];

	if (
		_global_::check_folder($path)
	) {
		if(file_exists("c:/xampp/htdocs/bnp/uploads/photo-gif/{$data->id}/files.txt")) {

			if(isset($mode->render_delay)) {
				$frames = explode("/", $mode->render_delay);
				$fps = $frames[1];
			} else {
				$fps = "30";
			}

			$format = explode(":", $settings["format"]);
			$frame_width = $settings["image-width"];
			$frame_height = $settings["image-height"];
			$video_width = $settings["image-height"];
			$video_height = round($video_width * $format[1] / $format[0]);
			$margin = ($frame_width - $video_height) / 2;

			$shell_cmd = sprintf(
				"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -f concat -safe 0 -r %s -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\files.txt -vcodec mpeg4 -vb 8000k  C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_%s.mp4",
				$fps, $data->id, $data->id, $data->id
			);

			shell_exec($shell_cmd);

			if(
				file_exists(
					sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_%s.mp4", $data->id, $data->id)
				)
			) {
				$shell_crop_cmd = sprintf(
					"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_%s.mp4 -filter:v \"crop=%s:%s:%s:%s\" C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_crop_%s.mp4",
					$data->id, $data->id, $video_width, $video_height, 0, $margin, $data->id, $data->id
				);

				shell_exec($shell_crop_cmd);

				if(
					file_exists(
						sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_crop_%s.mp4", $data->id, $data->id)
					)
				) {
					$shell_overlay_cmd = sprintf(
						"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_crop_%s.mp4 -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\overlay.png -filter_complex \"overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2\" -vcodec mpeg4 -vb 8000k -r 30 C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_final_%s.mp4",
						$data->id, $data->id, $data->id, $data->id, $data->id
					);

					shell_exec($shell_overlay_cmd);

					if(
						file_exists(
							sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_final_%s.mp4", $data->id, $data->id)
						)
					) {

						$shell_reverse_cmd = sprintf(
							"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_final_%s.mp4 -vf reverse C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_reverse_%s.mp4",
							$data->id, $data->id, $data->id, $data->id
						);

						shell_exec($shell_reverse_cmd);

						$shell_join_cmd = sprintf(
							"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_final_%s.mp4 -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_reverse_%s.mp4 -filter_complex \"[0:v:0][1:v:0]concat=n=2\" C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mp4",
							$data->id, $data->id, $data->id, $data->id, $data->id, $data->id, $data->id
						);

						shell_exec($shell_join_cmd);

						$shell_mkv_cmd = sprintf(
							"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mp4 -c copy C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mkv",
							$data->id, $data->id, $data->id, $data->id
						);

						shell_exec($shell_mkv_cmd);

						$shell_loop_cmd = sprintf(
							"C:\\ffmpeg\\bin\\ffmpeg.exe -threads 1 -y -stream_loop 2 -i C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mkv -vcodec mpeg4 -vb 8000k -r 30 C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\%s.mp4",
							$data->id, $data->id, $data->id, $data->id
						);

						shell_exec($shell_loop_cmd);

						$query_processed = sprintf(
							"UPDATE %s_bnp_users SET processed = %s WHERE id = %s",
							$cfg->db->prefix, 1, $data->id
						);

						if($db->query($query_processed)) {
							$query_file = sprintf(
								"INSERT INTO %s_files (file, type, module, id_ass, user_id, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')",
								$cfg->db->prefix, "photo-gif/{$data->id}/{$data->id}.mp4", "file", "bnp-users", $data->id, $data->id, date('Y-m-d H:i:s', time()), date('Y-m-d H:i:s', time())
							);

							if($db->query($query_file)) {
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_%s.mp4", $data->id, $data->id));
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_crop_%s.mp4", $data->id, $data->id));
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_final_%s.mp4", $data->id, $data->id));
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_reverse_%s.mp4", $data->id, $data->id));
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mp4", $data->id, $data->id));
								unlink(sprintf("C:\\xampp\\htdocs\\bnp\\uploads\\photo-gif\\%s\\video_join_%s.mkv", $data->id, $data->id));
								print "Video Created with Success!";
								return TRUE;
							}
						}
					}
				}
			}
		}
	}
}
print "Nothing to Process";
return FALSE;
