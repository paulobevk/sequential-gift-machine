<div class="spacer all-30"></div>
<div class="row">
	<div class="col">
		<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-edit/{c2r-id}" method="post">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="form-group">
						<label for="input-name">{c2r-title}</label>
						<input type="text" class="form-control" id="input-name" name="input-name" value="{c2r-post-name}" required>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="form-group">
						<label for="input-value">{c2r-value}</label>
						<textarea class="form-control code" rows="1" id="input-value" name="input-value">{c2r-post-value}</textarea>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-2 d-flex align-items-end justify-content-end taright">
					<button type="submit" class="btn btn-success" name="save"><i class="fas fa-save"></i><span class="block all-15"></span>Save</button>
				</div>
			</div>
			<div class="row">
			</div>
		</form>
	</div>
</div>


<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/style.css">
