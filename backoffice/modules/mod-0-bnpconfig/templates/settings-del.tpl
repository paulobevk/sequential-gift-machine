<div class="row">
	<div class="col-12 col-sm-12">
		<form class="sm-tacenter" action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-del/{c2r-id}" method="post">
			<p>{c2r-phrase}</p>
			<button type="submit" name="input-submit" class="btn btn-cancel">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
			<span class="xs-block15 sm-block15"></span>
			<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/" class="btn btn-edit" role="button">
				<i class="fa fa-edit" aria-hidden="true"></i>
			</a>
		</form>

	</div>
</div>
