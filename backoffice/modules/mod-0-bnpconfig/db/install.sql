INSERT INTO `{c2r-prefix}_modules` (`name`, `folder`, `sort`) VALUES ("{c2r-mod-name}", "{c2r-mod-folder}", 0);

CREATE TABLE `{c2r-prefix}_bnp_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{c2r-prefix}_bnp_config` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, `project-name`, ``, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(2, `app-type`, ``, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(3, `client-logo`, `, `, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(4, `app-bg`, ``, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`);
