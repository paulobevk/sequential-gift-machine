<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-10 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1" style="justify-content: center;">
				<p><strong>{c2r-id}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-5">
				<p><strong>{c2r-file}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-1" style="justify-content: center;">
				<p><strong>{c2r-module}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-1" style="justify-content: center;">
				<p><strong>{c2r-id-ass}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-1" style="justify-content: center;">
				<p><strong>{c2r-sort}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-3" style="justify-content: center;">
				<p><strong>{c2r-date-update}</strong></p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-2" style="justify-content: center;">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/edit/{c2r-id}" class="btn btn-success btn-edit" role="button">
			<i class="fas fa-edit" aria-hidden="true"></i>
		</a>
	</div>
</div>
