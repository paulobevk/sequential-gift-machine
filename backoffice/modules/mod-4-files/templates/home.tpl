{c2r-plg-files}
<div class="spacer all-15"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-10">
				<div class="row">
					<div class="block col-sm-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-5 tacenter">
						<strong>File</strong>
					</div>
					<div class="block col-sm-1 tacenter">
						<strong>Module</strong>
					</div>
					<div class="block col-sm-1 tacenter">
						<strong>ID Ass.</strong>
					</div>
					<div class="block col-sm-1 tacenter">
						<strong>Sort</strong>
					</div>
					<div class="block col-sm-3 tacenter">
						<strong>Last Update</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-2 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-list}
	</div>
</div>
