<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1">
				<p><strong>{c2r-id}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-4">
				<p><strong>{c2r-title}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-5">
				<p><strong>{c2r-value}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-2">
				<p title="date-update: {c2r-date-update}">{c2r-date}</p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
		<a class="btn btn-edit" href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-edit/{c2r-id}">
			<i class="fas fa-edit" aria-hidden="true"></i>
		</a>
		<a class="btn btn-del" href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-del/{c2r-id}">
			<i class="fas fa-trash" aria-hidden="true"></i>
		</a>
	</div>
</div>
