$(document).ready(function () {
	if ($("#tablet").prop('checked')) {
		 $("#layout").css("display","none");
	}
	if ($("#filterMain").prop('checked')) {
		 $("#filter").css("display","block");
	}
	if ($("#overlayMain").prop('checked')) {
		 $("#overlay").css("display","block");
	}
});
$("#tablet").click(function(){
	if($("#layout").is(":visible")) {
   	 	 $("#layout").css("display","none");
    }
});
$("#computer").click(function(){
 if( !$("#layout").is(":visible")) {
	 $("#layout").css("display","block");
 }
});
$('#filterMain').change(function() {
	  if ($(this).prop('checked')) {
		$("#filter").css("display","block");//checked
		$(".filter").prop("checked", true);
   } else {
		$("#filter").css("display","none"); //not checked
   }
  });
$('#overlayMain').change(function() {
   if ($(this).prop('checked')) {
		$("#overlay").css("display","block");//checked
		$(".overlay").prop("checked", true);
   }
   else {
		$("#overlay").css("display","none"); //not checked
	  }
});
