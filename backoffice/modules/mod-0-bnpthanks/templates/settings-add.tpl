<div class="spacer all-30"></div>
<div class="row">
	<div class="col">
		<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-add/" method="post">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="{c2r-title}" id="input-name" name="input-name" value="{c2r-post-name}" required>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-5">
					<div class="form-group">
						<textarea class="form-control code" placeholder="{c2r-value}" rows="1" id="input-value" name="input-value">{c2r-post-value}</textarea>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-2">
					<button type="submit" class="btn btn-success" name="save"><i class="fas fa-save"></i><span class="block all-15"></span>Save</button>
				</div>
			</div>
			<div class="row">
			</div>
		</form>
	</div>
</div>


<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/style.css">
