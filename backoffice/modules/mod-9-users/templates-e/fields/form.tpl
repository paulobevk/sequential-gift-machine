<form class="tacenter" action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/fields-del/{c2r-id}" method="post">
	<p>{c2r-phrase}</p>
	<button type="submit" name="submit" class="btn btn-danger btn-cancel">
		<i class="fas fa-trash-alt" aria-hidden="true"></i><span class="block all-15"></span>{c2r-del}
	</button>
	<span class="block all-15"></span>
	<a href="{c2r-path-bo}/{c2r-lg}/9-users/fields/" class="btn btn-warning btn-edit" role="button">
		<i class="fas fa-pencil-alt" aria-hidden="true"></i><span class="block all-15"></span>{c2r-cancel}
	</a>
</form>
