<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1">
				<p>{c2r-id}</p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-2">
				<p>{c2r-user}</p>
			</div>
			<div class="box col-sm-6 col-md-6 col-lg-5">
				<p><strong>{c2r-ip}</strong></p>
			</div>
			<div class="box date col-sm-6 col-md-6 col-lg-4">
				<p>{c2r-date}</p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/logs-view/{c2r-id}" class="btn btn-view" role="button">
			<i class="fas fa-eye" aria-hidden="true"></i>
			<div class="block all-15"></div>
			{c2r-but-view}
		</a>
	</div>
</div>
