<div class="spacer sm-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-4 col-md-4 col-lg-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-8 col-md-8 col-lg-2">
						<strong>{c2r-user}</strong>
					</div>
					<div class="block col-sm-6 col-md-6 col-lg-5">
						<strong>{c2r-ip}</strong>
					</div>
					<div class="block col-sm-12 col-md-12 col-lg-4 tacenter">
						<strong>{c2r-date}</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-list}
	</div>
</div>
