<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 taright">
		<div class="spacer all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/9-users/add/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-plus" aria-hidden="true"></i>
			<div class="block all-15"></div>{c2r-lg-add-btn}
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/9-users/fields/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-address-card" aria-hidden="true"></i>
			<div class="block all-15"></div>{c2r-lg-fields-btn}
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/9-users/logs/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-eye" aria-hidden="true"></i>
			<div class="block all-15"></div>{c2r-lg-logs-btn}
		</a>
		<div class="spacer all-15"></div>
	</div>
</div>
<div class="row">
	{c2r-home-list}
</div>
<div class="spacer all-30"></div>

<link rel="stylesheet" href="{c2r-module-path}site-assets/css/style.css">
