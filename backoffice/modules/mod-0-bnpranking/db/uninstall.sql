DELETE FROM `{c2r-prefix}_modules` WHERE `folder` = '{c2r-mod-folder}';

DROP TABLE IF EXISTS `{c2r-prefix}_bnp_ranking`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_ranking_schedule`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_ranking_settings`;
