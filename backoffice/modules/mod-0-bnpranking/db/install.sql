INSERT INTO `{c2r-prefix}_modules` (`name`, `folder`, `sort`) VALUES ("{c2r-mod-name}", "{c2r-mod-folder}", 0);

CREATE TABLE `{c2r-prefix}_bnp_ranking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_ranking_schedule` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_ranking_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `{c2r-prefix}_bnp_ranking_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '{c2r-path}/{c2r-lg}/bnp-thanks/', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
