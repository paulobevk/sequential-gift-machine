$(function () {
	$( ".scheduleOption" ).change(function() {
		if ($('.scheduleOption').find(":selected").val() == "all") {
			var start = "01/01/1970";
			var end = "01/01/2970";
			console.log(start);
		} else {
			var start = $('.scheduleOption').find(":selected").data("start");
			var end = $('.scheduleOption').find(":selected").data("end");
		}
		$('tbody  > tr').each(function() {
			var tr = this;
			this.style.display = "";
			var dateRow = tr.getElementsByClassName("date")[0].innerHTML;
			if(!dateCheck(start,end,dateRow)) {
				this.style.display = "none";
			}
		});
	});
});


function dateCheck(from,to,check) {
    var fDate,lDate,cDate;
    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);
    if((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}
