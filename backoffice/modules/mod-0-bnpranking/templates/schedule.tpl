<form method="post" action="">
	<div class="spacer all-15"></div>
	<div class="row">
		<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-5">
			<input type='text' placeholder="Label" class="form-control" id='label' name='label'/>
		</div>
		<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3">
			<div class="form-group">
				<div class='input-group date'>
					<input type='text' placeholder="Start" class="form-control" id='datetimepicker1' name='start'/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-3">
			<div class="form-group">
				<div class='input-group date'>
					<input type='text' placeholder="End" class="form-control" id='datetimepicker2' name='end'/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-1">
			<div class="tacenter">
				<button type="submit" class="btn btn-success" name="save"><i class="fas fa-save"></i><div class="block all-15"></div>Save</button>
			</div>
		</div>
	</div>
</form>

<div class="spacer all-30"></div>
<div class="row">
	<div class="spacer all-30"></div>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-4 tacenter">
						<strong>Label</strong>
					</div>
					<div class="block col-sm-4 tacenter">
						<strong>Start</strong>
					</div>
					<div class="block col-sm-4 tacenter">
						<strong>End</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-schedule}
	</div>
</div>

<script src="{c2r-module-path}/site-assets/js/jquery.datetimepicker.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/jquery.datetimepicker.css">
<script src="{c2r-module-path}/site-assets/js/scripts.js" charset="utf-8"></script>
