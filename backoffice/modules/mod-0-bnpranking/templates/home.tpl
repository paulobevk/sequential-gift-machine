<div class="row">
	<form method="post" id="export" action="" class="w-100">
		<div class="spacer all-15"></div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<div class="row float-right">
				<button type="submit" class="btn btn-success" name="save">
					<i class="fas fa-file-export" aria-hidden="true"></i>
					<div class="block all-15"></div>Export by filter
				</button>
				<div class="block all-15"></div>
				<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/schedule/" class="btn btn-success btn-add" role="button">
					<i class="fas fa-calendar-alt" aria-hidden="true"></i>
					<div class="block all-15"></div>Schedules
				</a>
				<div class="block all-15"></div>
				<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings/" class="btn btn-success btn-add" role="button">
					<i class="fas fa-history" aria-hidden="true"></i>
					<div class="block all-15"></div>Settings
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
				<select name="scheduleOption" class="scheduleOption form-control">
					<option value="all">All</option>
					{c2r-schedule}
				</select>
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2">
				<button class="btn btn-primary"><i class="fas fa-filter"></i><div class="block all-15"></div>Filter</button>
			</div>
		</div>
	</form>
</div>
<div class="spacer all-30"></div>
<div class="row">
	<div class="spacer all-30"></div>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-4">
						<strong>User</strong>
					</div>
					<div class="block col-sm-5 tacenter">
						<strong>Points</strong>
					</div>
					<div class="block col-sm-2 tacenter">
						<strong>Date</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Status</strong>
			</div>
		</div>
		{c2r-table-body}
	</div>
</div>

<script src="{c2r-module-path}/site-assets/js/home.js" charset="utf-8"></script>
