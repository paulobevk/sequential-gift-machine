<div class="row">
	<form method="post" action="">
		<div class='col-sm-4'>
				<label>Label</label>
					<input type='text' class="form-control" id='label' name='label' value="{c2r-label}"/>
		</div>
		<input type='hidden' class="form-control" value="{c2r-id}" name='id'/>
	<div class='col-sm-4'>
		<div class="form-group">
			<label>Start</label>
			<div class='input-group date'>
				<input type='text' class="form-control" id='datetimepicker1' value="{c2r-start}" name='start'/>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
	</div>
	<div class='col-sm-4'>
		<div class="form-group">
			<label>End</label>
			<div class='input-group date'>
				<input type='text' class="form-control" id='datetimepicker2' value="{c2r-end}" name='end'/>
				<span class="input-group-addon">
					<span class="glyphicon glyphicon-calendar"></span>
				</span>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-12">
		<div class="sm-taright">
			<button type="submit" class="btn btn-success" name="save">Save</button>
		</div>
	</div>
</form>
</div>
<script src="{c2r-module-path}/site-assets/js/jquery.datetimepicker.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/jquery.datetimepicker.css">
<script src="{c2r-module-path}/site-assets/js/scripts.js" charset="utf-8"></script>
