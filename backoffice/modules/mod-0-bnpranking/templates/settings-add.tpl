<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-add/" method="post">
	<div class="spacer all-15"></div>
	<div class="row">
		<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
			<div class="form-group">
				<input type="text" placeholder="{c2r-title}" class="form-control" name="name" value="{c2r-post-name}" required>
			</div>
		</div>
		<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
			<div class="form-group">
				<input type="text" placeholder="{c2r-value}" class="form-control" name="value" value="{c2r-post-value}" required>
			</div>
		</div>
		<div class="col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
			<button type="submit" class="btn btn-success" name="save"><i class="fas fa-save"></i><div class="block all-15"></div>Save</button>
		</div>
	</div>
</form>

<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/style.css">
