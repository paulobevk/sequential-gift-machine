<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1">
				<p><strong>{c2r-id}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-4">
				<p><strong>{c2r-name}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-5" style="justify-content: center;">
				<p><strong>{c2r-points}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-2" style="justify-content: center;">
				<p title="date-update: {c2r-date-update}">{c2r-date}</p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 tacenter" style="justify-content: center;">
		<i class="fas {c2r-status}" aria-hidden="true"></i>
	</div>
</div>
