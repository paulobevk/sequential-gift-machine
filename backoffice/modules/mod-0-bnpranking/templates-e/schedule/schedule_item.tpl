<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-label}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-start}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-end}</strong></p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 tacenter" style="justify-content: center;">
		<a href="{c2r-path-bo}/0/{c2r-lg}/{c2r-module-folder}/edit/{c2r-id}" class="btn btn-edit" role="button">
			<i class="fas fa-edit" aria-hidden="true"></i>
		</a>
		<a href="{c2r-path-bo}/0/{c2r-lg}/{c2r-module-folder}/delete/{c2r-id}" class="btn btn-del" role="button">
			<i class="fas fa-trash" aria-hidden="true"></i>
		</a>
	</div>
</div>
