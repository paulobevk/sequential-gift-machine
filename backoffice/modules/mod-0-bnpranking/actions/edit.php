<?php

if (!isset($_POST["save"])){
	if (isset($id) && !empty($id)) {
		$schedule = bnp_ranking::returnSchedulesID($id);
		foreach ($schedule as $schedule) {
			$mdl = bo3::c2r(
				[
					"label" => $schedule->label,
					"start" => $schedule->date_start,
					"end" => $schedule->date_end,
					"id" => $schedule->id
				],
				bo3::mdl_load("templates/edit.tpl")
			);
		}
	}
} else {
	$schedule = new bnp_ranking();
	$schedule->setId($_POST["id"]);
	$schedule->setStart($_POST["start"]);
	$schedule->setEnd($_POST["end"]);
	$schedule->setLabel($_POST["label"]);
	$result_schedule = $schedule->updateSchedule();
	$mdl = bo3::c2r(
		[
			"content" => ($result_schedule ? "Actualização realizada com sucesso!" : "Ocorreu um erro!")
		],
		bo3::mdl_load("templates/result.tpl")
	);
}

include "pages/module-core.php";
