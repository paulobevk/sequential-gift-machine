<?php

if (!isset($_POST["save"])){
	$line_tpl = bo3::mdl_load("templates-e/home/table-row.tpl");
	$ranking = bnp_ranking::returnData();
	$i = 1;
	foreach ($ranking as $ranks) {
		if (!isset($table_items)) {
			$table_items = "";
		}
		$table_items .= bo3::c2r(
			[
				"id" => $ranks->id,
				"position" => $i,
				"name" => $ranks->name,
				"points" => $ranks->points,
				"date" => $ranks->date,
				"status" => ($ranks->status) ? "fa-toggle-on" : "fa-toggle-off"
			],
			$line_tpl
		);
		$i++;
	}
	$option_schedule = bo3::mdl_load("templates-e/home/schedule_item.tpl");
	$schedule = bnp_ranking::returnSchedules();
	foreach ($schedule as $opt) {
		if (!isset($option_item)) {
			$option_item = "";
		}
		$option_item .= bo3::c2r(
			[
				"label" => $opt->label,
				"id" => $opt->id,
				"start" => $opt->date_start,
				"end" => $opt->date_end
			],
			$option_schedule
		);
	}
	$mdl = bo3::c2r(
		[
			"table-body" => (isset($table_items)) ? $table_items : "",
			"schedule" => (isset($option_item)) ? $option_item : ""
		],
		bo3::mdl_load("templates/home.tpl")
	);
	include "pages/module-core.php";
} else {
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=export.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	$results = bnp_ranking::returnRankingExport($_POST['scheduleOption']);
	foreach ($results as $i => $obj) {
		print "{$obj->name}, {$obj->points}, {$obj->date}\n";
	}
	$tpl = "";
}
