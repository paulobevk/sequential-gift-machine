<?php

if (!isset($_POST["save"])) {
	$option_schedule = bo3::mdl_load("templates-e/schedule/schedule_item.tpl");
	$schedule = bnp_ranking::returnSchedules();
	foreach ($schedule as $opt) {
		if (!isset($option_item)) {
			$option_item = "";
		}
		$option_item .= bo3::c2r(
			[
				"label" => $opt->label,
				"start" => $opt->date_start,
				"end" => $opt->date_end,
				"id" => $opt->id
			],
			$option_schedule
		);
	}
	$mdl = bo3::c2r(
		[
			"schedule" => (isset($option_item)) ? $option_item : ""
		],
		bo3::mdl_load("templates/schedule.tpl")
	);
} else {
	$schedule = new bnp_ranking();
	$schedule->setStart($_POST["start"]);
	$schedule->setEnd($_POST["end"]);
	$schedule->setLabel($_POST["label"]);
	$schedule->insertSchedule();
	$option_schedule = bo3::mdl_load("templates-e/schedule/schedule_item.tpl");
	$scheduleReturn = bnp_ranking::returnSchedules();
	foreach ($scheduleReturn as $opt) {
		if (!isset($option_item)) {
			$option_item = "";
		}
		$option_item .= bo3::c2r(
			[
				"label" => $opt->label,
				"start" => $opt->date_start,
				"end" => $opt->date_end,
				"id" => $opt->id
			],
			$option_schedule
		);
	}
	$mdl = bo3::c2r(
		[
			"schedule" => (isset($option_item)) ? $option_item : ""
		],
		bo3::mdl_load("templates/schedule.tpl")
	);
}
include "pages/module-core.php";
