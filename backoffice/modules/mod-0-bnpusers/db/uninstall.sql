DELETE FROM `{c2r-prefix}_modules` WHERE `folder` = '{c2r-mod-folder}';

DROP TABLE IF EXISTS `{c2r-prefix}_bnp_users`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_users_fields`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_users_settings`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_users_slides`;
