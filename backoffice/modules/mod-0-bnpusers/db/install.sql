INSERT INTO `{c2r-prefix}_modules` (`name`, `folder`, `sort`) VALUES ("{c2r-mod-name}", "{c2r-mod-folder}", 0);


CREATE TABLE `{c2r-prefix}_bnp_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `code` text NOT NULL,
  `comercial` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_users_fields` (
  `id` int(11) NOT NULL,
  `slide_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `error_message` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `sort` int(11) DEFAULT '0',
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_users_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `os_bnp_users_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'css', '', '2017-09-13 00:00:00', '2018-01-24 19:19:02'),
(2, 'target', '{c2r-path}/{c2r-lg}/bnp-photo/', '2017-09-13 00:00:00', '2017-09-13 00:00:00'),
(6, 'app-bg', '', '2018-01-24 16:35:54', '2018-01-24 19:14:55'),
(7, 'client-logo', '', '2018-01-24 16:39:18', '2018-01-24 16:40:52'),
(8, 'app-avatar', '', '2018-01-24 16:46:26', '2018-01-24 19:15:25'),
(9, 'app-terms-bg', '', '2018-01-24 16:53:12', '2018-01-24 19:15:34'),
(10, 'app-terms-check', '', '2018-01-24 16:53:49', '2018-01-24 19:15:42');

CREATE TABLE `{c2r-prefix}_bnp_users_slides` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
