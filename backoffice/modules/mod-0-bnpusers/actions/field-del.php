<?php

$returned_entry = bnp_users::return_field($id);

if (isset($_POST["input-submit"])) {
	if (bnp_users::delete_field($id)) {
		header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/slide-edit/{$returned_entry->slide_id}");
	}
}

$mdl = bo3::c2r([
	"lg-del" => $mdl_lang["entry-del"]["button-del"],
	"lg-cancel" => $mdl_lang["entry-del"]["button-cancel"],
	"id" => $id,
	"phrase" => $mdl_lang["entry-del"]["phrase"],
	"title" => $returned_entry->label
], bo3::mdl_load("templates/field-del.tpl"));

include "pages/module-core.php";
