<?php

if (isset($_POST["input-submit"])) {
	if (bnp_users::delete_slide($id)) {
		header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/home/");
	}
} else {
	$returned_entry = bnp_users::return_slide($id);
}

$mdl = bo3::c2r([
	"lg-del" => $mdl_lang["entry-del"]["button-del"],
	"lg-cancel" => $mdl_lang["entry-del"]["button-cancel"],

	"id" => $id,
	"phrase" => $mdl_lang["entry-del"]["phrase"],
	"title" => $returned_entry->name
], bo3::mdl_load("templates/slide-del.tpl"));

include "pages/module-core.php";
