<?php

$results = bnp_users::returnSlides();

if ($results !== false) {
	foreach ($results as $i => $obj) {
		if (!isset($list)) {
			$list = "";
			$item_tpl = bo3::mdl_load('templates-e/slides/line.tpl');
		}

		$list .= bo3::c2r([
			'id' => $obj->id,
			'name' => $obj->name,
			'fields' => $obj->fields,
			'sort' => $obj->sort,
			'published' => ($obj->published) ? "fa-toggle-on" : "fa-toggle-off",
			'date' => date('Y-m-d H:i', strtotime($obj->date))
		], $item_tpl);
	}
} else {
	$list = bo3::mdl_load('templates-e/slides/no-results.tpl');
}

$mdl = bo3::c2r([
	'list' => isset($list) ? $list : ''
], bo3::mdl_load("templates/home.tpl"));

include "pages/module-core.php";
