<?php

header('Content-Encoding: UTF-8');
header("Content-type: text/csv; charset=UTF-8");
header("Content-Disposition: attachment; filename=export.csv");
header("Pragma: no-cache");
header("Expires: 0");

print "\xEF\xBB\xBF"; // UTF-8 BOM

$results = bnp_users::returnUsers();

print "#; Name; Email; Code; Comerial; Date; Date Update; Status\n";

foreach ($results as $i => $obj) {
	print "{$obj->id}; {$obj->name}; {$obj->email}; {$obj->code}; {$obj->comercial}; {$obj->date}; {$obj->date_update}; {$obj->status}\n";
}

$tpl = "";
