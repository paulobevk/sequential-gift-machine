<?php

if(!isset($_POST["submit"])) {
	$results = bnp_users::returnUsers();

	if ($results != false) {
		foreach ($results as $i => $obj) {
			if (!isset($list)) {
				$list = "";
				$item_tpl = bo3::mdl_load('templates-e/users/item.tpl');
			}

			$list .= bo3::c2r(
				[
					'id' => $obj->id,
					'name' => $obj->name,
					'email' => $obj->email,
					//'code' => substr($obj->code, 0, 50).'...',
					'code' => $obj->code,
					'comercial' => ($obj->comercial) ? 'fa-check' : 'fa-times-circle',
					'date' => date('Y-m-d H:i', strtotime($obj->date)),
					'status' => ($obj->status) ? 'fa-check' : 'fa-times-circle',

				],
				$item_tpl
			);
		}
	} else {
		$list = bo3::mdl_load('templates-e/users/no-results.tpl');
	}
} else {
	$results = bnp_users::returnSearchUsers($_POST["search"]);

	$count = 0;

	if ($results != false) {
		$count = count($results);
		foreach ($results as $i => $obj) {
			if (!isset($list)) {
				$list = "";
				$item_tpl = bo3::mdl_load('templates-e/users/item.tpl');
			}

			$list .= bo3::c2r(
				[
					'id' => $obj->id,
					'name' => $obj->name,
					'email' => $obj->email,
					//'code' => substr($obj->code, 0, 50).'...',
					'code' => $obj->code,
					'comercial' => ($obj->comercial) ? 'fa-check' : 'fa-times-circle',
					'date' => date('Y-m-d H:i', strtotime($obj->date)),
					'status' => ($obj->status) ? 'fa-check' : 'fa-times-circle',
				],
				$item_tpl
			);
		}
	} else {
		$list = bo3::mdl_load('templates-e/users/no-results.tpl');
	}
}

$mdl = bo3::c2r(
	[
		'list' => $list,
		"search-value" => (isset($_POST["search"])) ? $_POST["search"] : "",
		"search-show" => (!isset($_POST["search"])) ? "d-none" : "",
		"number" => (isset($count)) ? $count : ""
	],
	bo3::mdl_load("templates/users.tpl")
);

include "pages/module-core.php";
