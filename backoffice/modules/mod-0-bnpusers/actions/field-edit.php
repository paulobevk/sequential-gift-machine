<?php


$returned_entry = bnp_users::return_field($id);

if (isset($_POST["save"])) {
	if (isset($_POST["labelField"]) && !empty($_POST["labelField"]) && isset($_POST["nameField"]) && !empty($_POST["nameField"])) {
    $field = new bnp_users();
	 	$field->setContent($_POST["labelField"], $_POST["nameField"]);
		$field->setId($id);
		$value = (isset($_POST["valueField"]) ? $_POST["valueField"] : "");
		$error = (isset($_POST["errorField"]) ? $_POST["errorField"] : "");
		$sort = (isset($_POST["sortField"]) ? $_POST["sortField"] : "0");
		$required = (isset($_POST["required"]) ? "1" : "0");
		if ($field->updateField ($value, $error, $sort, $required, $_POST["type"])) {
			header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/slide-edit/{$returned_entry->slide_id}");
		}
	}
}

$mdl = bo3::c2r([
	"label" => $returned_entry->label,
	"value" => $returned_entry->value,
	"name" => $returned_entry->name,
	"error" => $returned_entry->error_message,
	"opnumber" => ($returned_entry->type == "number") ? "selected" : "",
	"opemail" => ($returned_entry->type == "email") ? "selected" : "",
	"optext" => ($returned_entry->type == "text") ? "selected" : "",
	"opphone" => ($returned_entry->type == "phone") ? "selected" : "",
	"required" => ($returned_entry->required == 1) ? "checked" : "",
	"sort" => $returned_entry->sort,
	"id" => $returned_entry->id,

], bo3::mdl_load("templates/field-edit.tpl"));

include "pages/module-core.php";
