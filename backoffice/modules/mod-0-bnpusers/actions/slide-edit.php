<?php
if (isset($_POST["save"])) {
	if (isset($_POST["input-name"]) && !empty($_POST["input-name"])) {
		if (bnp_users::update_slide($_POST["input-name"], (int)$_POST["input-sort"], isset($_POST["published"]) ? true : false, $id)) {
			header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/");
		}
	}
} else {
	$result = bnp_users::return_slide($id);

	if ($result !== false) {
		$fields = bnp_users::returnFields($id);

		if ($fields !== false) {
			foreach ($fields as $i => $obj) {
				if (!isset($list_fields)) {
					$list_fields = '';
					$line_field_tpl = bo3::mdl_load('templates-e/fields/line.tpl');
				}

				$list_fields .= bo3::c2r([
					'id' => $obj->id,
					'label' => $obj->label,
					'type' => $obj->type,
					'sort' => $obj->sort,
					'required' => ($obj->required) ? 'on' : 'off'
				], $line_field_tpl);
			}
		} else {
			$list_fields = bo3::mdl_load('templates-e/fields/no-results.tpl');
		}

		$mdl = bo3::c2r([
			"lg-published" => $mdl_lang["label"]["published"],
			'name' => $result->name,
			'sort' => $result->sort,
			'id' => $id,
			"published" => (bool)$result->published ? "checked" : "",
			'list' => $list_fields
		], bo3::mdl_load("templates/slide-edit.tpl"));
	}

	include "pages/module-core.php";
}
