<?php

if (isset($_POST["save"])) {
	if (!empty($_POST["name"])) {
		$settings = new bnp_users();
		$settings->setContent($_POST["name"], $_POST["value"]);
		$settings->setDate();
		$settings->setDateUpdate();

		if ($settings->insertSetting()) {
			header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/settings/");
		}
	}
}

$mdl = bo3::c2r([
	"title" => $mdl_lang["settings-add"]["title"],
	"value" => $mdl_lang["settings-add"]["value"],
	"post-name" => isset($_POST["input-name"]) ? $_POST["input-name"] : "",
	"post-value" => isset($_POST["input-value"]) ? $_POST["input-value"] : ""
], bo3::mdl_load("templates/settings-add.tpl"));

$breadcrumb = [
	["name" => "Settings", "link" => "{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings/"],
	["name" => "Add", "link" => "{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-add/"]
];

include "pages/module-core.php";
