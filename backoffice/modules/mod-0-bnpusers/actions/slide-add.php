<?php

if (isset($_POST["input-save"])) {
	if (isset($_POST["input-name"]) && !empty($_POST["input-name"])) {
		if (bnp_users::insert_slide($_POST["input-name"], (int)$_POST["input-sort"], isset($_POST["input-published"]) ? true : false)) {
			header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpusers/home/");
		}
	}
}

$mdl = bo3::c2r([
	"lg-published" => $mdl_lang["label"]["published"],

	"name" => isset($_POST["input-name"]) ? $_POST["input-name"] : "",
	"sort" => isset($_POST["input-sort"]) ? (int)$_POST["input-sort"] : 0,
	"published" => isset($_POST["input-published"]) ? "checked" : ""
], bo3::mdl_load("templates/slide-add.tpl"));

include "pages/module-core.php";
