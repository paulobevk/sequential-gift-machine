<div class="line row">
	<div class="box id col-sm-4 col-md-4 col-lg-4 col-xl-1">
		<p><strong>{c2r-id}</strong></p>
	</div>
	<div class="box col-sm-8 col-md-8 col-lg-8 col-xl-2">
		<p><strong>{c2r-name}</strong></p>
	</div>
	<div class="box col-sm-12 col-md-12 col-lg-12 col-xl-2">
		<p>{c2r-email}</p>
	</div>
	<div class="box col-sm-12 col-md-12 col-lg-12 col-xl-3">
		<code>{c2r-code}</code>
	</div>
	<div class="box date col-sm-6 col-md-6 col-lg-6 col-xl-2">
		<p>{c2r-date}</p>
	</div>
	<div class="box published col-sm-3 col-md-3 col-lg-3 col-xl-1">
		<i class="fa fa-toggle-{c2r-comercial}" aria-hidden="true"></i>
	</div>
	<div class="box published col-sm-3 col-md-3 col-lg-3 col-xl-1">
		<i class="fa fa-toggle-{c2r-status}" aria-hidden="true"></i>
	</div>
</div>
