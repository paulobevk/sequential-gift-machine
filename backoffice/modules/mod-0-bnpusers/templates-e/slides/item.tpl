<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1">
				<p><strong>{c2r-id}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-3">
				<p><strong>{c2r-name}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-2 justify-content-center">
				<p>{c2r-fields}</p>
			</div>
			<div class="box justify-content-center col-sm-4 col-md-4 col-lg-1">
				<p>{c2r-sort}</p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-2 justify-content-center">
				<label class="switch">
					<input type="checkbox" {c2r-published}>
					<span class="slider round" data-id="{c2r-id}"></span>
				</label>
			</div>
			<div class="box date col-sm-12 col-md-12 col-lg-3">
				<p >{c2r-date}</p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-edit/{c2r-id}" class="btn btn-edit" role="button">
			<i class="fa fa-pencil" aria-hidden="true"></i>
			<span class="block all-15"></span>
			Edit
		</a>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-del/{c2r-id}" class="btn btn-del">
			<i class="fa fa-trash" aria-hidden="true"></i>
		</a>
	</div>
</div>
