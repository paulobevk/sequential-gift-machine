<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-1">
				<p><strong>{c2r-id}</strong></p>
			</div>
			<div class="box col-sm-8 col-md-8 col-lg-3">
				<p><strong>{c2r-label}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-3">
				<p>{c2r-type}</p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-1">
				<p>{c2r-sort}</p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-1 published">
				<i class="fa fa-toggle-{c2r-required}" aria-hidden="true"></i>
			</div>
			<div class="box date col-sm-4 col-md-4 col-lg-2">
				<p title="{c2r-date-updated-label} : {c2r-date-updated}">{c2r-date-created}</p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-edit/{c2r-id}" class="btn btn-edit" role="button">
			<i class="fas fa-edit" aria-hidden="true"></i>
		</a>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-del/{c2r-id}" class="btn btn-del">
			<i class="fas fa-trash" aria-hidden="true"></i>
		</a>
	</div>
</div>
