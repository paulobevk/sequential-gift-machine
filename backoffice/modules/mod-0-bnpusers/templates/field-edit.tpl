<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-edit/{c2r-id}" method="post">
<div class="col-sm-12">
  <label for="slide-name">Fields form</label>
  <hr>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="labelField">Label</label>
        <input type="text" name="labelField" class="form-control" value="{c2r-label}" id="labelField">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="nameField">Name</label>
        <input type="text" name="nameField" class="form-control" value="{c2r-name}" id="nameField">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <label for="valueField">Value</label>
        <input type="text" name="valueField" class="form-control" value="{c2r-value}" id="valueField">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="errorField">Error Message</label>
        <input type="text" name="errorField" class="form-control" value="{c2r-error}" id="errorField">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <label for="type">Type</label><br>
        <select name="type" class="form-control">
          <option value="text" {c2r-optext}>Text</option>
          <option value="email" {c2r-opemail}>Email</option>
          <option value="number" {c2r-opnumber}>Number</option>
          <option value="phone" {c2r-opphone}>Phone</option>
        </select>
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label for="slide-sort">Sort</label>
        <input type="text" class="form-control" id="slide-sort" name="input-sort" value="{c2r-sort}">
      </div>
    </div>
    <div class="col-sm-2">
      <div class="form-group">
        <label for="type">Required</label><br>
        <input type="checkbox" name="required" value="required" {c2r-required}>
      </div>
    </div>

    <div class="col-sm-4">
      <label for=""><br></label>
      <button type="submit" class="btn btn-add btn-block" name="save">Update Field</button>
    </div>
  </div>
</div>
</form>
