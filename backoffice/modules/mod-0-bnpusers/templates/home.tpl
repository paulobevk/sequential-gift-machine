<div class="spacer all-30"></div>
<div class="row">
	<div class="col-md-12 sm-taright">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/users/" class="btn btn-primary" role="button">
			<i class="fa fa-users" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Users
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings/" class="btn btn-primary" role="button">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Settings
		</a>
	</div>
</div>
<div class="spacer all-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-3">
						<strong>Slide label</strong>
					</div>
					<div class="block col-sm-2 tacenter">
						<strong>Fields</strong>
					</div>
					<div class="block col-sm-1 tacenter">
						<strong>Sort</strong>
					</div>
					<div class="block col-sm-2 tacenter">
						<strong>Published</strong>
					</div>
					<div class="block col-sm-3 tacenter">
						<strong>Date</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-list}
	</div>
</div>
