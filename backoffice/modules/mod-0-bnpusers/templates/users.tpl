<div class="spacer all-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 p-0 taleft">
		<form class="" action="" method="post">
			<div class="row no-gutters">
				<div class="col">
					<input class="form-control border-secondary border-right-0 rounded-0" type="search" value="{c2r-search-value}" name="search" placeholder="Search here" id="bnp-search">
				</div>
				<div class="col-auto">
					<button class="btn bnp-search-submit btn-outline-secondary border-left-0 rounded-0 rounded-right" type="submit" name="submit">
					<i class="fa fa-search"></i>
				</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 p-0 taright">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/users/" class="btn btn-primary" role="button">
			<i class="fa fa-eye" aria-hidden="true"></i>
			<div class="block all-15"></div>See All
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/export/" class="btn btn-primary" role="button">
			<i class="fa fa-cloud-download-alt" aria-hidden="true"></i>
			<div class="block all-15"></div>Export
		</a>
	</div>
</div>
<div class="spacer all-30"></div>
<div class="row {c2r-search-show}">
	<div class="col p-0 tacenter">
		<p><strong>{c2r-number} records found!</strong></p>
	</div>
</div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="block col-sm-1 tacenter">
				<strong>#</strong>
			</div>
			<div class="block col-sm-2">
				<strong>Name</strong>
			</div>
			<div class="block col-sm-2">
				<strong>Email</strong>
			</div>
			<div class="block col-sm-3">
				<strong>Code</strong>
			</div>
			<div class="block col-sm-2 tacenter">
				<strong>Date</strong>
			</div>
			<div class="block col-sm-1 tacenter">
				<strong>Comercial</strong>
			</div>
			<div class="block col-sm-1 tacenter">
				<strong>Status</strong>
			</div>
		</div>
		{c2r-list}
	</div>
</div>
