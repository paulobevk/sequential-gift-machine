<div class="row">
	<div class="col-md-12">
		<div class="row">
			<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/slide-add/" method="post">
			<div class="col-md-4 col-sm-offset-4">
				<div class="form-group">
					<label for="slide-name">Slide Name</label>
					<input type="text" class="form-control" id="slide-name" name="input-name" value="{c2r-name}">
				</div>
				<div class="form-group">
					<label for="slide-sort">Sort</label>
					<input type="text" class="form-control" id="slide-sort" name="input-sort" value="{c2r-sort}">
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="input-published" value="1"/> {c2r-lg-published}</label>
				</div>
				<button type="submit" class="btn btn-add btn-block" name="input-save">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="xs-spacer30 sm-spacer30"></div>

<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/style.css">
