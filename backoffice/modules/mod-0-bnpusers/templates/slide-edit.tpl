<div class="row">
	<div class="col-md-12">
		<div class="row">
			<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/slide-edit/{c2r-id}" method="post">
			<div class="col-md-4">
				<div class="form-group">
					<label for="slide-name">Slide Name</label>
					<input type="text" class="form-control" id="slide-name" value="{c2r-name}" name="input-name">
				</div>
				<div class="form-group">
					<label for="slide-sort">Sort</label>
					<input type="text" class="form-control" id="slide-sort" name="input-sort" value="{c2r-sort}">
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="published" value="1" {c2r-published} /> {c2r-lg-published}</label>
				</div>
				<button type="submit" class="btn btn-add btn-block" name="save">Save</button>
			</div>
			</form>
			<form action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/field-add/{c2r-id}" method="post">
			<div class="col-sm-8">
				<label for="slide-name">Fields form</label>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="labelField">Label</label>
							<input type="text" name="labelField" class="form-control" id="labelField">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="nameField">Name</label>
							<input type="text" name="nameField" class="form-control" id="nameField">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label for="valueField">Value</label>
							<input type="text" name="valueField" class="form-control" id="valueField">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="errorField">Error Message</label>
							<input type="text" name="errorField" class="form-control" id="errorField">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="type">Type</label><br>
							<select name="type" class="form-control">
								<option value="text">Text</option>
								<option value="email">Email</option>
								<option value="number">Number</option>
								<option value="phone">Phone</option>
							</select>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="form-group">
							<label for="sortField">Sort</label>
							<input type="text" class="form-control" id="sortField" name="sortField" value="0">
						</div>
					</div>
					<div class="col-sm-2 sm-tacenter">
						<div class="form-group">
							<label for="type">Required</label><br>
							<input type="checkbox" name="required" value="required">
						</div>
					</div>

					<div class="col-sm-4">
						<label for=""><br></label>
						<button type="submit" class="btn btn-add btn-block" name="save">Add Field</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="xs-spacer60 sm-spacer60"></div>
<div class="row">
	<div class="col-md-8 col-sm-offset-4">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Field Label</th>
					<th>Type</th>
					<th>Sort</th>
					<th class="sm-tacenter">Required</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{c2r-list}
			</tbody>
		</table>
	</div>
</div>
<div class="xs-spacer30 sm-spacer30"></div>
<script type="text/javascript">
	var layout = '{c2r-layout}';

	$(document).ready(function () {
		$('input[name=layoutSight]').each(function () {
			if ($(this).attr('value') === layout) {
				$(this).click();
			}
		});
	});
</script>
<link rel="stylesheet" href="{c2r-module-path}/site-assets/css/style.css">
