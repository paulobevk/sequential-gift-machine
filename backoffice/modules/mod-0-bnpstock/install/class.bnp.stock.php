<?php

class bnp_stock {

	protected $id;
	protected $label;
	protected $start;
	protected $end;
	protected $premium;
	protected $image;
	protected $quantity;
	protected $message;
	protected $item;
	protected $schedule;

	public function __construct () {}

	public function setId($i) {
		$this->id = $i;
	}

	public function setLabel($i) {
		$this->label = $i;
		return $this->label;
	}

	public function setStart($i) {
		$this->start = $i;
	}

	public function setEnd($i) {
		$this->end = $i;
	}

	public function setPremium($i) {
		$this->premium = $i;
	}
	public function setContent($t, $d) {
		$this->title = $t;
		$this->content = $d;
	}

	public function setImage($i) {
		$this->image = $i;
	}

	public function setQuantity($i) {
		$this->quantity = $i;
	}

	public function setItemID($i) {
		$this->item = $i;
	}

	public function setScheduleID($i) {
		$this->schedule = $i;
	}

	public function setMessage($i) {
		$this->message = $i;
	}
	public function setDate($d = null) {
		$this->date = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public function setDateUpdate($d = null) {
		$this->date_update = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public static function return_settings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_stock_settings WHERE true",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		foreach ($list as $index => $value) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$toReturn[$value->name] = $value->value;
		}

		return (isset($toReturn)) ? $toReturn : false;
	}

	public static function returnAllSettings () {
		global $cfg, $db;
		$query = sprintf("SELECT * FROM %s_bnp_stock_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}
	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_stock_settings (`name`, `value`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date,
			$this->date_update
		);
		if ($db->query($query)){
			return true;
		}

		return false;

	}
	public function insertSchedule() {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_stock_schedule (label, date_start, date_end, premium, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$this->label,
			$this->start,
			$this->end,
			$this->premium,
			date("Y-m-d H:i", time()),
			date("Y-m-d H:i", time())
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public function insertAssociente() {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_stock_ass (item_id, schedule_id, quantity, date, date_update) VALUES ('%s', '%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$this->item,
			$this->schedule,
			$this->quantity,
			date("Y-m-d H:i", time()),
			date("Y-m-d H:i", time())
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}
	public function deleteSetting () {
		global $cfg, $db, $authData;

		$gp = new bnp_stock();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_stock_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}
	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_stock_settings SET  name = '%s', value = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date_update,
			$this->id
		);
		return $db->query($query);
	}
	public function returnOneSetting () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}
	public function returnOneItem () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_item WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}
	public function returnOneSchedule () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_schedule WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}

	public static function returnData () {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_stock_item ORDER BY quantity DESC",
			$cfg->db->prefix
		);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}
		return $toReturn;
	}

	public static function returnConnects () {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT %s_bnp_stock_ass.*, %s_bnp_stock_item.name, %s_bnp_stock_schedule.label FROM %s_bnp_stock_ass INNER JOIN %s_bnp_stock_item ON %s_bnp_stock_ass.item_id = %s_bnp_stock_item.id INNER JOIN %s_bnp_stock_schedule ON %s_bnp_stock_schedule.id = %s_bnp_stock_ass.schedule_id ORDER BY %s_bnp_stock_ass.date_update DESC",
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix,
			$cfg->db->prefix
		);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}
		return $toReturn;
	}

	public static function returnSchedules () {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_stock_schedule",
			$cfg->db->prefix
		);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			array_push($toReturn, $data);
		}
		return $toReturn;
	}

	public function delete() {
		global $cfg, $db;
		$query = sprintf("DELETE FROM %s_bnp_stock_schedule WHERE id = %s",
			$cfg->db->prefix,
			$this->id
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public function deleteItem() {
		global $cfg, $db;
		$query = sprintf("DELETE FROM %s_bnp_stock_item WHERE id = %s",
			$cfg->db->prefix,
			$this->id
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public function deleteConnect() {
		global $cfg, $db;
		$query = sprintf("DELETE FROM %s_bnp_stock_ass WHERE id = %s",
			$cfg->db->prefix,
			$this->id
		);
		if ($db->query($query)){
			$toReturn = true;
		}
		return $toReturn;
	}

	public static function returnSchedulesID ($id) {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_stock_schedule WHERE  id = %s ORDER BY date_start ASC",
			$cfg->db->prefix,
			$id
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$toReturn = [];

			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return $toReturn;
		}

		return false;
	}

	public static function returnAssociateID ($id) {
		global $cfg, $db;

		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_stock_ass WHERE  id = %s",
			$cfg->db->prefix,
			$id
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$toReturn = [];

			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}

			return $toReturn;
		}

		return false;
	}

	public function updateSchedule () {
		global $cfg, $db;
		$query = sprintf("UPDATE %s_bnp_stock_schedule SET label = '%s', date_start = '%s', date_end = '%s', date_update = '%s', premium = '%s' WHERE id = '%s'",
			$cfg->db->prefix,
			$this->label,
			$this->start,
			$this->end,
			date("Y-m-d H:i", time()),
			$this->premium,
			$this->id
		);
		if ($db->query($query)){
			$toReturn = $this->id;
		}
		return $toReturn;
	}

	public function updateItem () {
		global $cfg, $db;

		if (empty($this->image)) {
			$query = sprintf("UPDATE %s_bnp_stock_item SET name = '%s', quantity = '%s', message = '%s' WHERE id = '%s'",
				$cfg->db->prefix,
				$this->label,
				$this->quantity,
				$this->message,
				$this->id
			);
		} else {
			$query = sprintf("UPDATE %s_bnp_stock_item SET name = '%s', image = '%s', quantity = '%s', message = '%s' WHERE id = '%s'",
				$cfg->db->prefix,
				$this->label,
				$this->image,
				$this->quantity,
				$this->message,
				$this->id
			);
		}

		if ($db->query($query)){
			$toReturn = $this->id;
		}
		return $toReturn;
	}

	public function updateAssociate () {
		global $cfg, $db;
		$query = sprintf("UPDATE %s_bnp_stock_ass SET item_id = '%s', schedule_id = '%s', quantity = '%s', date_update = '%s' WHERE id = '%s'",
			$cfg->db->prefix,
			$this->item,
			$this->schedule,
			$this->quantity,
			date("Y-m-d H:i", time()),
			$this->id
		);
		if ($db->query($query)){
			$toReturn = $this->id;
		}
		return $toReturn;
	}

	public function returnItemID ($id) {
		global $cfg, $db;
		$toReturn = [];
		$query = sprintf("SELECT * FROM %s_bnp_stock_item WHERE  id = %s",
			$cfg->db->prefix,
			$id
		);
		$source = $db->query($query);
		if ($source->num_rows > 0) {
			$toReturn = [];
			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}
			return $toReturn;
		}
		return false;
	}

	public function returnItems () {
		global $cfg, $db;

		$toReturn = [];

		$query = sprintf("SELECT * FROM %s_bnp_stock_item WHERE  true",
			$cfg->db->prefix
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {

			$toReturn = [];

			while ($data = $source->fetch_object()) {
				array_push($toReturn, $data);
			}
			return $toReturn;
		}

		return false;
	}

	public static function returnRafflePrize () {
		global $cfg, $db;

		$toReturn = [
			'status' => false,
			'message' => '',
			'object' => []
		];

		$schedule_list = [];

		// GET CURRENT TIME
		$currentDate = date('Y-m-d H:i:s', time());

		// GET PREMIUM SCHEDULE
		$query = sprintf(
			"SELECT * FROM %s_bnp_stock_schedule WHERE premium = 1 AND (date_start <= '%s' and date_end >= '%s') ORDER BY date_start ASC, date_end ASC LIMIT 1",
			$cfg->db->prefix,
			$currentDate,
			$currentDate
		);

		$source = $db->query($query);

		if ($source->num_rows > 0) {
			$source = $db->query($query);
			$data = $source->fetch_object();

			array_push($schedule_list, $data->id);
		} else {
			// GET NON PREMIUM SCHEDULE
			$query = sprintf(
				"SELECT * FROM %s_bnp_stock_schedule WHERE premium = 0 AND date_start <= '%s' ORDER BY date_start ASC, date_end ASC",
				$cfg->db->prefix,
				$currentDate,
				$currentDate
			);

			$source = $db->query($query);

			while ($data = $source->fetch_object()) {
				array_push($schedule_list, $data->id);
			}
		}

		if (count($schedule_list) > 0) {
			foreach ($schedule_list as $i => $schedule) {
				if (!isset($where)) { $where = ""; }

				$where .= sprintf("sa.schedule_id = %s OR ", $schedule);
			}

			$where = substr($where, 0, -4);

			$query = sprintf(
				"SELECT sa.*, si.name, si.message, si.image, ss.label AS schedule_label, ss.id AS schedule_id
				FROM %s_bnp_stock_ass AS sa INNER JOIN %s_bnp_stock_item AS si ON sa.item_id = si.id INNER JOIN %s_bnp_stock_schedule AS ss ON ss.id = sa.schedule_id
				WHERE %s
				ORDER BY sa.date_update DESC",
				$cfg->db->prefix,
				$cfg->db->prefix,
				$cfg->db->prefix,
				isset($where) ? $where : true
			);

			$source = $db->query($query);

			if ($source->num_rows > 0) {
				while ($data = $source->fetch_object()) {
					for ($i = 0; $i < $data->quantity; $i++) {
						if (!isset($items_list)) {
							$items_list = [];
						}

						array_push($items_list, $data);
					}
				}
				$choosen_one = array_rand($items_list);

				$toReturn['status'] = true;
				$toReturn['object'] = $items_list[$choosen_one];
			}
		}

		return json_encode($toReturn);
	}

	public static function decreaseStock ($user_id, $item_id, $ass_id) {
		global $cfg, $db;

		// DECREASE STOCK
		$query = sprintf(
			"UPDATE %s_bnp_stock_item SET quantity = quantity - 1 WHERE id = %s",
			$cfg->db->prefix, $item_id
		);

		$stock = $db->query($query);

		// DECREASE ASS STOCK
		$query = sprintf(
			"UPDATE %s_bnp_stock_ass SET quantity = quantity - 1 WHERE id = %s",
			$cfg->db->prefix, $ass_id
		);

		$stock = $db->query($query);

		// ADD LOG
		$query = sprintf(
			"INSERT INTO %s_bnp_stock_log (`user_id`, `item_id`, `date`) VALUES ('%s', '%s', '%s')",
			$cfg->db->prefix,
			$user_id !== false ? $user_id : 0,
			$item_id,
			date('Y-m-d H:i:s', time())
		);

		$log = $db->query($query);

		return ($stock && $log);
	}
}
