<?php

$query = sprintf(
	"SELECT * FROM %s_bnp_stock_item WHERE true",
	$cfg->db->prefix
);
$source = $db->query($query);

header("Content-type: text/csv, charset=UTF-8");
header("Content-Disposition: attachment; filename=export.csv");
header("Pragma: no-cache");
header("Expires: 0");

while ($data = $source->fetch_object()) {
	print "{$data->id}, {$data->name}, {$data->image}, {$data->quantity}, {$data->message}\n";
}

$tpl = "";
