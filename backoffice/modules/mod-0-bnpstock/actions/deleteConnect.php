<?php
$obj = new bnp_stock();
$obj->setId($id);
if (isset($_POST["input-submit"])) {
	if ($obj->deleteConnect()) {
		header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpstock/connect/");
	}
} 
$mdl = bo3::c2r([
	"lg-del" => $mdl_lang["entry-del"]["button-del"],
	"lg-cancel" => $mdl_lang["entry-del"]["button-cancel"],
	"id" => $id,
	"phrase" => $mdl_lang["entry-del"]["phrase"],
	"title" => "connect"
], bo3::mdl_load("templates/connect-del.tpl"));

include "pages/module-core.php";
