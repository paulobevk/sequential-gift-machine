<?php

$query = sprintf(
	"SELECT * FROM %s_bnp_stock_logs WHERE true",
	$cfg->db->prefix
);
$source = $mysqli->query($query);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=export.csv");
header("Pragma: no-cache");
header("Expires: 0");

while ($data = $source->fetch_object()) {
	print "{$data->id}, {$data->users_id}, {$data->item_id}, {$data->date}\n";
}

$tpl = "";
