<?php
if (isset($_POST["save"])){
	$schedule = new bnp_stock();
	$schedule->setQuantity($_POST["quantity"]);
	$schedule->setItemID($_POST["item"]);
	$schedule->setScheduleID($_POST["schedule"]);
	$result_schedule = $schedule->insertAssociente();
}
$line_tpl = bo3::mdl_load("templates-e/connect/table-row.tpl");
$connects = bnp_stock::returnConnects();
foreach ($connects as $itens) {
	if (!isset($table_items)) {
		$table_items = "";
	}
	$table_items .= bo3::c2r(
		[
			"name" => $itens->name,
			"schedule" => $itens->label,
			"quantity" => $itens->quantity,
			"id" => $itens->id
		],
		$line_tpl
	);
}
$line_tpl2 = bo3::mdl_load("templates-e/connect/option-item.tpl");
$itens = bnp_stock::returnData();
foreach ($itens as $item) {
	if (!isset($table_items2)) {
		$table_items2 = "";
	}
	$table_items2 .= bo3::c2r(
		[
			"name" => $item->name,
			"id" => $item->id,
			"quantity" => $item->quantity,
		],
		$line_tpl2
	);
}

$schedules_tpl = bo3::mdl_load("templates-e/connect/option-schedule.tpl");
$schedules = bnp_stock::returnSchedules();
foreach ($schedules as $sche) {
	if (!isset($table_schedules)) {
		$table_schedules = "";
	}
	$table_schedules .= bo3::c2r(
		[
			"label" => $sche->label,
			"id" => $sche->id,
			"start" => $sche->date_start,
			"end" => $sche->date_end,
		],
		$schedules_tpl
	);
}
$mdl = bo3::c2r(
	[
		"table-body" => (isset($table_items)) ? $table_items : "",
		"option-item" => (isset($table_items2)) ? $table_items2 : "",
		"option-schedule" => (isset($table_schedules)) ? $table_schedules : ""
	],
	bo3::mdl_load("templates/connect.tpl")
);
include "pages/module-core.php";
