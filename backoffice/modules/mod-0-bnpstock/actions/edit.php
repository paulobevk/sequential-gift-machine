<?php

if (!isset($_POST["save"])){
	if (isset($id) && !empty($id)) {
		$schedule = bnp_stock::returnItemID($id);
		foreach ($schedule as $schedule) {
			$mdl = bo3::c2r(
				[
					"name" => $schedule->name,
					"image" => $schedule->image,
					"quantity" => $schedule->quantity,
					"message" => $schedule->message,
					"id" => $schedule->id
				],
				bo3::mdl_load("templates/edit.tpl")
			);
		}
	}
} else {
	$schedule = new bnp_stock();
	$schedule->setId($id);
	$schedule->setImage($_POST["image"]);
	$schedule->setQuantity($_POST["quantity"]);
	$schedule->setLabel($_POST["label"]);
	$schedule->setMessage($_POST["message"]);
	$result_schedule = $schedule->updateItem();
	if($result_schedule) {
		header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpstock/");
	}
}

include "pages/module-core.php";
