<?php

$line_tpl = bo3::mdl_load("templates-e/home/table-row.tpl");
$stock = bnp_stock::returnData();
foreach ($stock as $itens) {
	if (!isset($table_items)) {
		$table_items = "";
	}
	$table_items .= bo3::c2r(
		[
			"name" => $itens->name,
			"image" => $itens->image,
			"quantity" => $itens->quantity,
			"id" => $itens->id,
			"message" => strip_tags($itens->message)
		],
		$line_tpl
	);
}
$mdl = bo3::c2r(
	[
		"table-body" => (isset($table_items)) ? $table_items : ""
	],
	bo3::mdl_load("templates/home.tpl")
);
include "pages/module-core.php";
