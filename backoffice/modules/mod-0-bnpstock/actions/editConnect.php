<?php
if (!isset($_POST["save"])){
	if (isset($id) && !empty($id)) {
		$line_tpl = bo3::mdl_load("templates-e/connect/option-item.tpl");
		$associate = bnp_stock::returnAssociateID($id);
		$itens = bnp_stock::returnData();
		foreach ($itens as $item) {
			if (!isset($table_items)) {
				$table_items = "";
			}
			$table_items .= bo3::c2r(
				[
					"name" => $item->name,
					"id" => $item->id,
					"check" => (($item->id == $associate[0]->item_id)) ? "selected" : ""
				],
				$line_tpl
			);
		}

		$schedules_tpl = bo3::mdl_load("templates-e/connect/option-schedule.tpl");
		$schedules = bnp_stock::returnSchedules();
		foreach ($schedules as $sche) {
			if (!isset($table_schedules)) {
				$table_schedules = "";
			}
			$table_schedules .= bo3::c2r(
				[
					"label" => $sche->label,
					"id" => $sche->id,
					"check" => (($sche->id == $associate[0]->schedule_id)) ? "selected" : "",
					"start" => $sche->date_start,
					"end" => $sche->date_end,
				],
				$schedules_tpl
			);
		}
		$mdl = bo3::c2r(
			[
				"option-item" => (isset($table_items)) ? $table_items : "",
				"option-schedule" => (isset($table_schedules)) ? $table_schedules : "",
				"quantity" => $associate[0]->quantity
			],
			bo3::mdl_load("templates/editConnect.tpl")
		);
	}
} else {
	// AQUI
	$schedule = new bnp_stock();
	$schedule->setId($id);
	$schedule->setItemID($_POST["item"]);
	$schedule->setScheduleID($_POST["schedule"]);
	$schedule->setQuantity($_POST["quantity"]);
	$result_schedule = $schedule->updateAssociate();
	if($result_schedule) {
		header("Location: {$cfg->system->path_bo}/{$lg_s}/0-bnpstock/connect/");
	}
}

include "pages/module-core.php";
