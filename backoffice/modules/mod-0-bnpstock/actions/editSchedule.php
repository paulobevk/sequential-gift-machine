<?php

if (!isset($_POST["save"])){
	if (isset($id) && !empty($id)) {
		$schedule = bnp_stock::returnSchedulesID($id);
		foreach ($schedule as $schedule) {
			$mdl = bo3::c2r(
				[
					"label" => $schedule->label,
					"start" => $schedule->date_start,
					"end" => $schedule->date_end,
					"premium-true" => (($schedule->premium == 1) ? "selected" : "" ),
					"premium-false" => (($schedule->premium == 0) ? "selected" : "" ),
					"id" => $schedule->id
				],
				bo3::mdl_load("templates/editSchedule.tpl")
			);
		}
	}
} else {
	$schedule = new bnp_stock();
	$schedule->setId($_POST["id"]);
	$schedule->setStart($_POST["datetimepicker1"]);
	$schedule->setEnd($_POST["datetimepicker2"]);
	$schedule->setLabel($_POST["label"]);
	$result_schedule = $schedule->updateSchedule();
	$mdl = bo3::c2r(
		[
			"content" => ($result_schedule ? "Actualização realizada com sucesso!" : "Ocorreu um erro!")
		],
		bo3::mdl_load("templates/result.tpl")
	);
}

include "pages/module-core.php";
