<?php

if (!isset($_POST["save"])) {
	$option_schedule = bo3::mdl_load("templates-e/schedule/table-row.tpl");
	$schedule = bnp_stock::returnSchedules();
	foreach ($schedule as $opt) {
		if (!isset($option_item)) {
			$option_item = "";
		}
		$option_item .= bo3::c2r(
			[
				"label" => $opt->label,
				"start" => $opt->date_start,
				"end" => $opt->date_end,
				"premium" => (($opt->premium == 1) ? "Sim" : "Não"),
				"id" => $opt->id
			],
			$option_schedule
		);
	}
	$mdl = bo3::c2r(
		[
			"schedule" => (isset($option_item)) ? $option_item : ""
		],
		bo3::mdl_load("templates/schedules.tpl")
	);
} else {
	$schedule = new bnp_stock();
	$schedule->setStart($_POST["datetimepicker1"]);
	$schedule->setEnd($_POST["datetimepicker2"]);
	$schedule->setLabel($_POST["label"]);
	$schedule->setPremium($_POST["premium"]);
	$schedule->insertSchedule();
	$option_schedule = bo3::mdl_load("templates-e/schedule/table-row.tpl");
	$scheduleReturn = bnp_stock::returnSchedules();
	foreach ($scheduleReturn as $opt) {
		if (!isset($option_item)) {
			$option_item = "";
		}
		$option_item .= bo3::c2r(
			[
				"label" => $opt->label,
				"start" => $opt->date_start,
				"end" => $opt->date_end,
				"premium" => (($opt->premium == 1) ? "Sim" : "Não"),
				"id" => $opt->id
			],
			$option_schedule
		);
	}
	$mdl = bo3::c2r(
		[
			"schedule" => (isset($option_item)) ? $option_item : ""
		],
		bo3::mdl_load("templates/schedules.tpl")
	);
}
include "pages/module-core.php";
