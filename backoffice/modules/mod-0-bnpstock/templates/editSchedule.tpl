<div class="row">
	<form method="post" action="">

		<div class='col-sm-3'>
			<div class="form-group">
				<label>Label</label>

					<input type="text" class="form-control" id="label" name="label" value="{c2r-label}">
<input type='hidden' class="form-control" value="{c2r-id}" name='id'/>
			</div>
		</div>
		<div class='col-sm-3'>
			<div class="form-group">
				<label>Start</label>
				<div class='input-group date'>
					<input type='text' class="form-control" id='datetimepicker1' name='datetimepicker1' value="{c2r-start}"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class='col-sm-3'>
			<div class="form-group">
				<label>End</label>
				<div class='input-group date'>
					<input type='text' class="form-control" id='datetimepicker2' name='datetimepicker2' value="{c2r-end}"/>
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
		</div>
		<div class='col-sm-3'>
			<label>Premium</label><br>
			<select name="premium">
				<option value="1" {c2r-premium-true}>True</option>
				<option value="0" {c2r-premium-false}>False</option>
			</select>
		</div>
		<div class="col-xs-12 col-sm-12">
			<div class="sm-taright">
				<button type="submit" class="btn btn-add" name="save">Save</button>
			</div>
		</div>
	</form>
</div>
