<div class="row">
	<div class="col-md-12 taright">
		<div class="spacer all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/connect" class="btn btn-add btn-success" role="button">
			<i class="fas fa-columns" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Connect Item | Schedule
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/exportLogs" class="btn btn-add btn-success" role="button">
			<i class="fas fa-download" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Export
		</a>
	</div>
</div>
<div class="spacer all-15"></div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped">
			<thead>
				<tr>
					<th class="tacenter">#</th>
					<th class="tacenter">User</th>
					<th class="tacenter">Item</th>
					<th class="tacenter">Date</th>
					<th class="tacenter">Actions</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="tacenter">1</td>
					<td class="tacenter">Toni</td>
					<td class="tacenter">Tshirt</td>
					<td class="tacenter">10/7/2017</td>
					<td class="tacenter">
						<a href="/backoffice/pt/categories/edit/6" class="btn btn-edit btn-success" role="button">
							<i class="fas fa-edit" aria-hidden="true"></i>
						</a>
						<div class="block all-15"></div>
						<a href="/backoffice/pt/categories/delete/6" class="btn btn-cancel btn-danger" role="button">
							<i class="fas fa-trash" aria-hidden="true"></i>
						</a>
					</td>
				</tr>
				<tr>
					<td class="tacenter">1</td>
					<td class="tacenter">Toni</td>
					<td class="tacenter">Tshirt</td>
					<td class="tacenter">10/7/2017</td>
					<td class="tacenter">
						<a href="/backoffice/pt/categories/edit/6" class="btn btn-edit btn-success" role="button">
							<i class="fas fa-edit" aria-hidden="true"></i>
						</a>
						<div class="block all-15"></div>
						<a href="/backoffice/pt/categories/delete/6" class="btn btn-cancel btn-danger" role="button">
							<i class="fas fa-trash" aria-hidden="true"></i>
						</a>
					</td>
				</tr>
				<tr>
					<td class="tacenter">1</td>
					<td class="tacenter">Toni</td>
					<td class="tacenter">Tshirt</td>
					<td class="tacenter">10/7/2017</td>
					<td class="tacenter">
						<a href="/backoffice/pt/categories/edit/6" class="btn btn-edit btn-success" role="button">
							<i class="fas fa-edit" aria-hidden="true"></i>
						</a>
						<div class="block all-15"></div>
						<a href="/backoffice/pt/categories/delete/6" class="btn btn-cancel btn-danger" role="button">
							<i class="fas fa-trash" aria-hidden="true"></i>
						</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
