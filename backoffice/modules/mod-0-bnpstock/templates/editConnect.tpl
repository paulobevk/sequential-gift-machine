<form method="post" action="">
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="form-group">
			<label for="name">Item</label>
			<select class="form-control" id="item"  name="item">
			  	{c2r-option-item}
			</select>
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="schedule">Schedule</label>
			<select class="form-control" id="schedule" name="schedule">
			  	{c2r-option-schedule}
			</select>
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-2">
		<div class="form-group">
			<label for="quantity">Quantity</label>
			<input type="number" class="form-control" id="quantity" name="quantity" value="{c2r-quantity}">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-12">
		<div class="sm-taright">
			<button type="submit" class="btn btn-add" name="save">Save</button>
		</div>
	</div>
</div>
</form>

<div class="xs-spacer30 sm-spacer30"></div>

<script src="{c2r-module-path}site-assets/js/scripts.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}site-assets/css/style.css">
