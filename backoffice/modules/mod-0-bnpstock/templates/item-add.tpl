<form method="post" action="">
<div class="row">
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="name">Nome</label>
			<input type="text" class="form-control" id="name" placeholder="Nome...">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="quantity">Quantidade</label>
			<input type="number" class="form-control" id="quantity">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="message">Mensagem</label>
			<input type="text" class="form-control" id="message" placeholder="Ganhou a Tshirt!...">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="form-group">
			<label for="image">Imagem</label>
			<input type="file" class="form-control" id="image">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-12">
		<div class="sm-taright">
			<button type="submit" class="btn btn-add">Save</button>
		</div>
	</div>
</div>
</form>

<div class="xs-spacer30 sm-spacer30"></div>

<script src="{c2r-module-path}site-assets/js/scripts.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}site-assets/css/style.css">
