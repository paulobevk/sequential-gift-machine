<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 taright">
		<div class="spacer all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/export/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-file-export" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Export
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-cog" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Settings
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/schedules/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-calendar-alt" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Schedules
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/connect/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-columns" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Connect Item | Schedule
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/logs/" class="btn btn-add btn-success" role="button">
			<i class="fas fa-history" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Logs
		</a>
	</div>
</div>
<div class="spacer all-15"></div>
<div class="row">
	<h1 class="separator">
		<span>Add new item</span>
	</h1>
</div>
<div class="spacer all-15"></div>
<form method="post" action="">
	<div class="row">
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="form-group">
				<label for="name">Nome</label>
				<input type="text" class="form-control" id="name" placeholder="Nome...">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="form-group">
				<label for="quantity">Quantidade</label>
				<input type="number" class="form-control" id="quantity">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="form-group">
				<label for="message">Mensagem</label>
				<input type="text" class="form-control" id="message" placeholder="Ganhou...">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="form-group">
				<label for="image">Imagem</label>
				<input type="file" class="form-control" id="image">
			</div>
			<div class="spacer all-30"></div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 tacenter">
			<button type="submit" class="btn btn-add btn-success"><i class="fas fa-save"></i><div class="block all-15"></div>Save</button>
		</div>
	</div>
</form>
<div class="spacer all-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-3 tacenter">
						<strong>Name</strong>
					</div>
					<div class="block col-sm-3 tacenter">
						<strong>Image</strong>
					</div>
					<div class="block col-sm-3 tacenter">
						<strong>Quantity</strong>
					</div>
					<div class="block col-sm-3 tacenter">
						<strong>Message</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-table-body}
	</div>
</div>

<style>
	h1.separator {
		width: 100%;
		height: 28px;
		border-bottom: 1px solid transparent;
		-moz-border-image: -moz-linear-gradient(right, #00ccff 0%, #37c871 50%);
		-webkit-border-image: -webkit-linear-gradient(right, #00ccff 0%, #37c871 50%);
		border-image: linear-gradient(to right, #00ccff 0%, #37c871 50%);
		border-image-slice: 1;
	}

	.separator > span {
		font-size: 20px;
		background-color: #fff;
		padding: 0 15px;
	}
</style>
