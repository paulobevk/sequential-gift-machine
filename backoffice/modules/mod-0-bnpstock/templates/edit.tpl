<form method="post" action="">
	<div class="row">
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="spacer all-15"></div>
			<div class="form-group">
				<label for="name">Nome</label>
				<input type="text" class="form-control" id="name" name="label" placeholder="Nome..." value="{c2r-name}">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="spacer all-15"></div>
			<div class="form-group">
				<label for="quantity">Quantidade</label>
				<input type="number" class="form-control" id="quantity" name="quantity" value="{c2r-quantity}">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="spacer all-15"></div>
			<div class="form-group">
				<label for="message">Mensagem</label>
				<input type="text" class="form-control" id="message" name="message" value="{c2r-message}" placeholder="Ganhou a Tshirt!...">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="spacer all-15"></div>
			<div class="form-group">
				<label for="image">Imagem</label>
				<input type="file" class="form-control" id="image" name="image" value="{c2r-image}">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 tacenter">
			<div class="spacer all-30"></div>
			<button type="submit" class="btn btn-add btn-success" name="save"><i class="fas fa-save"></i><div class="block all-15"></div>Save</button>
		</div>
	</div>
</form>
<div class="spacer all-30"></div>
<script src="{c2r-module-path}site-assets/js/scripts.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}site-assets/css/style.css">
