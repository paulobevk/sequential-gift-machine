<form method="post" action="">
	<div class="spacer all-15"></div>
	<div class="row">
		<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
			<div class="form-group">
				<!--<label for="name">Item</label>-->
				<select class="form-control" name="item">
					<option selected disabled>Item</option>
					{c2r-option-item}
				</select>
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
			<div class="form-group">
				<!--<label for="schedule">Schedule</label>-->
				<select class="form-control" name="schedule">
					<option selected disabled>Schedule</option>
					{c2r-option-schedule}
				</select>
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
			<div class="form-group">
				<input type="number" class="form-control" placeholder="Quantity" name="quantity" value="{c2r-quantity}">
			</div>
			<div class="spacer all-15"></div>
		</div>
		<div class="col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
			<button type="submit" class="btn btn-add btn-success" name="save"><i class="fas fa-save"></i><div class="block all-15"></div>Save</button>
		</div>
	</div>
</form>
<div class="spacer all-15"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-4 tacenter">
						<strong>Item</strong>
					</div>
					<div class="block col-sm-4 tacenter">
						<strong>Schedule</strong>
					</div>
					<div class="block col-sm-4 tacenter">
						<strong>Quantity</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-table-body}
	</div>
</div>
