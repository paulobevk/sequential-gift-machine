<form method="post" action="">
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="form-group">
			<label for="name">Item</label>
			<select class="form-control" id="item">
			  	<option>Tshirt</option>
			  	<option>Bone</option>
			</select>
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<div class="form-group">
			<label for="schedule">Schedule</label>
			<select class="form-control" id="schedule">
			  	<option>schedule 1</option>
			  	<option>schedule 2</option>
			</select>
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<div class="form-group">
			<label for="quantity">Quantity</label>
			<input type="number" class="form-control" id="quantity">
		</div>
		<div class="xs-spacer15 sm-spacer15"></div>
	</div>
	<div class="col-xs-12 col-sm-12">
		<div class="sm-taright">
			<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
</div>
</form>

<div class="xs-spacer30 sm-spacer30"></div>

<script src="{c2r-module-path}site-assets/js/scripts.js" charset="utf-8"></script>
<link rel="stylesheet" href="{c2r-module-path}site-assets/css/style.css">
