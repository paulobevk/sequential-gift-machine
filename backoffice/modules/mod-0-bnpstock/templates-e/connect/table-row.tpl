<div class="line row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 d-flex">
		<div class="row flex-grow-1">
			<div class="box id col-sm-4 col-md-4 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-name}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-schedule}</strong></p>
			</div>
			<div class="box col-sm-4 col-md-4 col-lg-4" style="justify-content: center;">
				<p><strong>{c2r-quantity}</strong></p>
			</div>
		</div>
	</div>
	<div class="action-list col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3" style="justify-content: center;">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/editConnect/{c2r-id}" class="btn btn-edit" role="button">
			<i class="fas fa-edit" aria-hidden="true"></i>
		</a>
		<div class="xs-block15 sm-block15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/deleteConnect/{c2r-id}" class="btn btn-del" role="button">
			<i class="fas fa-trash" aria-hidden="true"></i>
		</a>
	</div>
</div>
