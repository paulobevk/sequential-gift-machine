DELETE FROM `{c2r-prefix}_modules` WHERE `folder` = '{c2r-mod-folder}';

DROP TABLE IF EXISTS `{c2r-prefix}_bnp_stock_ass`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_stock_item`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_stock_log`;
DROP TABLE IF EXISTS `{c2r-prefix}_bnp_stock_schedule`;
