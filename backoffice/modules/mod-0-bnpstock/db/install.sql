INSERT INTO `{c2r-prefix}_modules` (`name`, `folder`, `sort`) VALUES ("{c2r-mod-name}", "{c2r-mod-folder}", 0);

CREATE TABLE `{c2r-prefix}_bnp_stock_ass` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_stock_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_stock_log` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_stock_schedule` (
  `id` int(11) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `premium` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `{c2r-prefix}_bnp_stock_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL,
  `date_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `os_bnp_stock_settings`
--

INSERT INTO `{c2r-prefix}_bnp_stock_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(1, 'target', '{c2r-path}/{c2r-lg}/bnp-thanks/', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
