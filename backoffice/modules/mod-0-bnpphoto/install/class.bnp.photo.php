<?php

class bnp_photo {
	public function setId ($i) {
		$this->id = $i;
	}
	public function setContent($t, $d) {
		$this->title = $t;
		$this->content = $d;
	}

	public function setDate($d = null) {
		$this->date = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}

	public function setDateUpdate($d = null) {
		$this->date_update = ($d !== null) ? $d : date("Y-m-d H:i:s", time());
	}
	public function insertSetting () {
		global $cfg, $db;
		$query = sprintf("INSERT INTO %s_bnp_photo_settings (`name`, `value`, `date`, `date_update`) VALUES ('%s', '%s', '%s', '%s')",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date,
			$this->date_update
		);
		if ($db->query($query)){
			return true;
		}

		return false;

	}
	public function updateSetting() {
		global $cfg, $db;

		$query = sprintf(
			"UPDATE %s_bnp_photo_settings SET  name = '%s', value = '%s', date_update = '%s' WHERE id = %s",
			$cfg->db->prefix,
			$db->real_escape_string($this->title),
			$db->real_escape_string($this->content),
			$this->date_update,
			$this->id
		);
		return $db->query($query);
	}
	public function deleteSetting () {
		global $cfg, $db, $authData;

		$gp = new bnp_photo();
		$gp->setId($this->id);
		$gp = $gp->returnOneSetting();

		$trash = new trash();
		$trash->setCode(json_encode($gp));
		$trash->setDate();
		$trash->setModule($cfg->mdl->folder);
		$trash->setUser($authData["id"]);
		$trash->insert();

		unset($gp);

		$query = sprintf("DELETE FROM %s_bnp_photo_settings WHERE id = %s", $cfg->db->prefix, $this->id);

		return $db->query($query);
	}
	public function returnOneSetting () {
		global $cfg, $db;
		$toReturn = [];

		$query = sprintf(
			"SELECT * FROM %s_bnp_photo_settings WHERE id = %s LIMIT %s",
			$cfg->db->prefix, $this->id, 1
		);
		$source = $db->query($query);

		if ($source->num_rows > 0) {
			return $source->fetch_object();
		}

		return false;
	}
	public static function returnAllSettings () {
		global $cfg, $db;
		$query = sprintf("SELECT * FROM %s_bnp_photo_settings WHERE true", $cfg->db->prefix);
		$source = $db->query($query);
		while ($data = $source->fetch_object()) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			array_push($toReturn, $data);
		}

		return (isset($toReturn)) ? $toReturn : false;
	}
	public static function return_settings () {
		global $cfg, $db;

		$query = sprintf("SELECT * FROM %s_bnp_photo_settings WHERE true",
			$cfg->db->prefix
		);
		$source = $db->query($query);

		while ($data = $source->fetch_object()) {
			if (!isset($list)) {
				$list = [];
			}

			array_push($list, $data);
		}

		foreach ($list as $index => $value) {
			if (!isset($toReturn)) {
				$toReturn = [];
			}

			$toReturn[$value->name] = $value->value;
		}

		return (isset($toReturn)) ? $toReturn : false;
	}
}
