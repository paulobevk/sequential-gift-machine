<div class="spacer all-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 p-0 taright">
		<a class="btn btn-primary" href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-add/">
			<i class="fa fa-plus" aria-hidden="true"></i>
			<span class="block all-15"></span>
			Add Settings
		</a>
	</div>
</div>
<div class="spacer all-30"></div>
<div class="row">
	<div class="spacer all-30"></div>
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-4">
						<strong>{c2r-lg-title}</strong>
					</div>
					<div class="block col-sm-5">
						<strong>{c2r-lg-value}</strong>
					</div>
					<div class="block col-sm-2 tacenter">
						<strong>{c2r-lg-date}</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-list}
	</div>
</div>
<div class="spacer all-30"></div>
