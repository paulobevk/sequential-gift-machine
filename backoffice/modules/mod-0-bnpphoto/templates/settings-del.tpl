<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<form class="sm-tacenter" action="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings-del/{c2r-id}" method="post">
			<p>{c2r-phrase}</p>
			<button type="submit" name="input-submit" class="btn btn-cancel btn-warning">
				<i class="fas fa-eraser" aria-hidden="true"></i>
				<span class="block all-15"></span>
				{c2r-lg-del}
			</button>
			<span class="block all-15"></span>
			<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/" class="btn btn-edit btn-success" role="button">
				<i class="fas fa-pencil" aria-hidden="true"></i>
				<span class="block all-15"></span>
				{c2r-lg-cancel}
			</a>
		</form>
	</div>
</div>
