INSERT INTO `{c2r-prefix}_modules` (`name`, `folder`, `sort`) VALUES ("{c2r-mod-name}", "{c2r-mod-folder}", 0);

CREATE TABLE `{c2r-prefix}_bnp_photo_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{c2r-prefix}_bnp_photo_settings` (`id`, `name`, `value`, `date`, `date_update`) VALUES
(2, `target`, `{c2r-path}/{c2r-lg}/bnp-thanks/`, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(5, `filters`, `sunrise, hazyDays, love, orangePeel`, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(8, `overlays`, `{c2r-path}/u-files/ov-1.png, {c2r-path}/u-files/ov-2.png, {c2r-path}/u-files/ov-3.png, {c2r-path}/u-files/ov-4.png`, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(10, `layoutSight`, `layout2.png`, `2017-07-21 00:00:00`, `2017-07-21 00:00:00`),
(13, `css`, ``, `2017-09-13 00:00:00`, `2017-09-13 00:00:00`),
(15, `app-logo`, `{c2r-path}/u-files/app-logo.svg`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(16, `sight`, ``, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(17, `app-bg`, `{c2r-path}/site-assets/images/bnp-photo/bg.png`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(18, `app-avatar`, ``, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(19, `client-logo`, ``, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(20, `image-width`, `1440`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(21, `image-height`, `1080`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(22, `button-bg`, `{c2r-path}/site-assets/images/bnp-photo/2_Botao_Foto_BG.png`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(23, `step1-button`, `{c2r-path}/site-assets/images/bnp-photo/3_Icon.png`, `0000-00-00 00:00:00`, `0000-00-00 00:00:00`),
(24, `step2-clock`, `{c2r-path}/site-assets/images/bnp-photo/2_clock_icon.png`, `2018-01-31 17:45:21`, `2018-01-31 17:45:21`),
(25, `step1-intro`, `Toca no botão<br>e tira a tua foto!`, `2018-01-31 17:52:54`, `2018-01-31 17:52:54`),
(26, `step1-button-text`, `Tirar Foto`, `2018-01-31 17:52:54`, `2018-01-31 17:52:54`),
(27, `step2-intro`, `Posiciona-te...<br>Aguarda...`, `2018-01-31 18:15:33`, `2018-01-31 18:15:33`),
(28, `step3-intro`, `Pronto...<br>Avança, ou repete a foto`, `2018-02-01 12:14:31`, `2018-02-01 12:14:31`),
(29, `step3-text-repeat`, `Repetir`, `2018-02-01 12:14:31`, `2018-02-01 12:14:31`),
(30, `step3-text-accept`, `Avançar`, `2018-02-01 12:16:41`, `2018-02-01 12:16:41`),
(31, `step3-button-repeat`, `{c2r-path}/site-assets/images/bnp-photo/3_repetir_icon.png`, `2018-02-01 12:16:41`, `2018-02-01 12:16:41`),
(32, `step3-button-accept`, `{c2r-path}/site-assets/images/bnp-photo/arrow-next.svg`, `2018-02-01 12:17:27`, `2018-02-01 12:17:27`),
(33, `step3-5-intro`, `A processar...`, `2018-02-01 12:34:01`, `2018-02-01 12:34:01`),
(34, `step4-check`, `{c2r-path}/site-assets/images/bnp-photo/1_visto.png`, `2018-02-01 12:34:01`, `2018-02-01 12:34:01`),
(35, `step4-intro`, `Escolhe um efeito<br>para a tua foto`, `2018-02-01 12:50:42`, `2018-02-01 12:50:42`),
(36, `step4-text-accept`, `Avançar`, `2018-02-01 12:52:13`, `2018-02-01 12:52:13`),
(37, `step4-button`, `{c2r-path}/site-assets/images/bnp-photo/arrow-next.svg`, `2018-02-01 12:52:13`, `2018-02-01 12:52:13`);
