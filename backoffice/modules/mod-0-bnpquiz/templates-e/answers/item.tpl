<div class="row answer-item">
	<div class="col-xs-6 col-sm-6">
		<input type="text" name="answer[]" class=" answer-title form-control">
	</div>
	<div class="col-xs-3 col-sm-3">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="status[]" class="answer-status"> Correct?
			</label>
		</div>
	</div>
	<div class="col-xs-3 col-sm-3 sm-taright">
		<button type="button" class="btn btn-add save-answer" style="margin-right: 5px;">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>
		</button>
		<button type="button" class="btn btn-cancel remove-answer" >
			<i class="fa fa-trash" aria-hidden="true"></i>
		</button>
	</div>
</div>
