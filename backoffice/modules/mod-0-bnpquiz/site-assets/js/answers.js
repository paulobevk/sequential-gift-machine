$(document).ready(function() {
	// get the anwsers list

	$('body').on('click', '.add-answer', function() {
		$('.answers-list').append(item_tpl);
	});

	$('body').on('click', '.save-answer', function() {
		var attr = $(this).attr("data-id");
		var btn = $(this);
		if(typeof attr !== typeof undefined) {
			var id_ass = attr;
			var name = $($($($(btn).parent()).parent()).find(".answer-title")).val();
			var status = $($($($(btn).parent()).parent()).find(".answer-status")).is(":checked");

			$.post(
				path + "/backoffice/" + lg + "/0-bnpquiz/api/?r=updateAnswer",
				{'id':attr, 'title':name, 'status': +status},
				function(data) {
					data = JSON.parse(data);
					console.log(data);
					if(data.status == true) {
						console.log("123");
					}
				}
			);
		} else {
			var name = $($($($(btn).parent()).parent()).find(".answer-title")).val();
			var status = $($($($(btn).parent()).parent()).find(".answer-status")).is(":checked");
			$.post(
				path + "/backoffice/" + lg + "/0-bnpquiz/api/?r=insertAnswer",
				{'id_ass':question_id, 'title':name, 'status': +status},
				function(data) {
					data = JSON.parse(data);
					if(data.status == true) {
						$(btn).attr("data-id", data.object);
						$($($($(btn).parent()).parent()).find(".remove-answer")).attr("data-id", data.object);
					}
				}
			);
		}
	});

	$('body').on('click', '.remove-answer', function() {
		var attr = $(this).attr("data-id");
		var btn = $(this);
		if(typeof attr !== typeof undefined) {
			$.post(
				path + "/backoffice/" + lg + "/0-bnpquiz/api/" + attr + "?r=deleteAnswer",
				function(data) {
					data = JSON.parse(data);
					if(data == true) {
						$($($(btn).parent()).parent()).empty();
					}
				}
			);
		} else {
			$($($(btn).parent()).parent()).empty();
		}
	});

	$.get(
		path + "/backoffice/" + lg + "/0-bnpquiz/api/" + question_id + "?r=getAnswers",
		function(data) {
			data = JSON.parse(data);
			$.each(data, function(i, obj) {
				$('.answers-list').append(item_tpl);
				$($($(".answer-item")[i]).find(".answer-title")).val(obj.title);
				if(obj.status == true) {
					$($($(".answer-item")[i]).find(".answer-status")).prop("checked", true);
				}
				$($($(".answer-item")[i]).find(".save-answer")).attr("data-id", obj.id);
				$($($(".answer-item")[i]).find(".remove-answer")).attr("data-id", obj.id);
			});
		}
	)
});
