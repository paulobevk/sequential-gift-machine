<div class="row">
	<div class="col-xs-12 col-sm-6">
		<form action="" name="edit-question" method="post">
			<div class="form-group">
				<label for="inputTitle">{c2r-title}</label>
				<input type="text" name="title" class="form-control" id="inputTitle" value="{c2r-title-value}">
			</div>
			<div class="form-group">
				<label for="inputDate">{c2r-date}</label>
				<input name="date" type="text" class="form-control" id="inputDate" placeholder="{c2r-date-placeholder}" value="{c2r-date-value}">
			</div>
			<div class="form-group">
				<label for="inputCode">{c2r-code}</label>
				<textarea name="code" id="inputCode" class="form-control" rows="3"  placeholder="{c2r-code-placeholder}" style="resize: vertical;">{c2r-code-value}</textarea>
			</div>
			<div class="checkbox">
			<label>
				<input type="checkbox" name="status" {c2r-checked}> Status
			</label>
			</div>
			<button type="submit" class="btn btn-add" name="save">{c2r-but-submit}</button>
		</form>
	</div>
	<div class="col-xs-12 col-sm-6">
		<div class="row">
			<div class="col-xs-10 col-sm-8">
				<h4><strong>Answers</strong></h4>
			</div>
			<div class="col-xs-2 col-sm-4 sm-taright">
				<button type="button" class="btn btn-add add-answer">
					<i class="fa fa-plus" aria-hidden="true"></i>
				</button>
			</div>
		</div>
		<div class="xs-spacer30 sm-spacer30"></div>
		<div class="answers-list">

		</div>
	</div>
</div>
<script type="text/javascript">
	var question_id = '{c2r-id}';
	var item_tpl = '{c2r-item}';
</script>
<script src="{c2r-module-path}/site-assets/js/answers.js" charset="utf-8"></script>
