<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<form action="" name="add-question" method="post">
			<div class="form-group">
				<div class="spacer all-15"></div>
				<label for="inputTitle">{c2r-title}</label>
				<input type="text" name="title" class="form-control" id="inputTitle">
			</div>
			<div class="form-group">
				<div class="spacer all-15"></div>
				<label for="inputDate">{c2r-date}</label>
				<input name="date" type="text" class="form-control" id="inputDate" placeholder="{c2r-date-placeholder}" value="{c2r-date-value}">
			</div>
			<div class="form-group">
				<div class="spacer all-15"></div>
				<label for="inputCode">{c2r-code}</label>
				<textarea name="code" id="inputCode" class="form-control" rows="3"  placeholder="{c2r-code-placeholder}" style="resize: vertical;"></textarea>
			</div>
			<div class="spacer all-15"></div>
			<div class="checkbox">
				<label><input type="checkbox" name="status"> Status</label>
			</div>
			<div class="spacer all-15"></div>
			<div class="tacenter">
				<button type="submit" class="btn btn-add btn-success" name="save"><i class="fas fa-save"></i><div class="block all-15"></div>{c2r-but-submit}</button>
			</div>
		</form>
	</div>
</div>
