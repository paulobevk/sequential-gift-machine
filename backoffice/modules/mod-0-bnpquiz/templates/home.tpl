<div class="spacer all-30"></div>
<div class="row">
	<div class="col taright">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/add/" class="btn btn-primary" role="button">
			<i class="fa fa-plus" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Add Question
		</a>
		<div class="block all-15"></div>
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/settings/" class="btn btn-primary" role="button">
			<i class="fa fa-history" aria-hidden="true"></i>
			<div class="block all-15"></div>
			Settings
		</a>
	</div>
</div>
<div class="spacer all-30"></div>
<div class="row">
	<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
		<div class="line-header d-none d-xl-flex row">
			<div class="col-sm-9">
				<div class="row">
					<div class="block col-sm-1 tacenter">
						<strong>#</strong>
					</div>
					<div class="block col-sm-4">
						<strong>{c2r-question}</strong>
					</div>
					<div class="block col-sm-5 tacenter">
						<strong>{c2r-status}</strong>
					</div>
					<div class="block col-sm-2 tacenter">
						<strong>{c2r-date}</strong>
					</div>
				</div>
			</div>
			<div class="block col-sm-3 tacenter">
				<strong>Actions</strong>
			</div>
		</div>
		{c2r-table-body}
	</div>
</div>

<script src="{c2r-module-path}/site-assets/js/script.js" charset="utf-8"></script>
