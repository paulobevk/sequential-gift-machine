<div class="row">
	<div class="col-xs-12 col-sm-12">
		<h5><strong>QUESTION</strong></h5>
		<h3>{c2r-question-title}</h3>
	</div>
</div>
<div class="xs-spacer30 sm-spacer30"></div>
<div class="row">
	<div class="col-xs-12 col-sm-4">
		<h5><strong>{c2r-date-label}</strong></h5>
		<p>{c2r-date}</p>
	</div>
	<div class="col-xs-12 col-sm-4">
		<h5><strong>{c2r-date-update-label}</strong></h5>
		<p>{c2r-date-update}</p>
	</div>
	<div class="col-xs-12 col-sm-4">
		<h5><strong>{c2r-status-label}</strong></h5>
		<p>{c2r-status}</p>
	</div>
</div>
<div class="xs-spacer30 sm-spacer30"></div>
<div class="xs-spacer30 sm-spacer30"></div>
<div class="row">
	<div class="col-sm-12">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>{c2r-answer-label}</th>
					<th>{c2r-status-label}</th>
					<th>{c2r-date-label}</th>
				</tr>
			</thead>
			<tbody>
				{c2r-table-body}
			</tbody>
		</table>
	</div>
</div>
<div class="xs-spacer30 sm-spacer30"></div>
<div class="row">
	<div class="col-xs-12 col-sm-12 sm-taright">
		<a href="{c2r-path-bo}/{c2r-lg}/{c2r-module-folder}/" class="btn btn-add">
			<i class="fa fa-history" aria-hidden="true"></i>
			<div class="xs-block15 sm-block15"></div>
			Go Back
		</a>
	</div>
</div>
<script src="{c2r-module-path}/site-assets/js/script.js" charset="utf-8"></script>
