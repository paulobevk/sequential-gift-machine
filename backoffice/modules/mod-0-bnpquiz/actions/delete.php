<?php

	if (isset($id) && !empty($id)) {
		$question = new bnp_quiz();
		$question->setId($id);
		$toReturn = "";

		if (isset($_POST["submit"])) {
			if ($question->deleteQuestion()) {
				$toReturn = $mdl_lang["delete"]["success"];
			} else {
				$toReturn =  $mdl_lang["delete"]["failure"];
			}
		} else {
			$question = $question->returnOneQuestion();

			$toReturn = bo3::c2r(
				[
					"id" => $id,

					"phrase" => $mdl_lang["delete"]["phrase"],
					"title" => $question->title,

					"del" => $mdl_lang["delete"]["button-del"],
					"cancel" => $mdl_lang["delete"]["button-cancel"]
				],
				bo3::mdl_load("templates/delete.tpl")
			);
		}

		$mdl = bo3::c2r(["content" => $toReturn], bo3::mdl_load("templates/result.tpl"));
	} else {
		// if doesn't exist an action response, system sent you to 404
		header("Location: {$cfg->system->path_bo}/0/{$lg_s}/404/");
	}

include "pages/module-core.php";
