<?php

function deleteAnswer($id) {
	global $cfg, $db;

	$delete = new bnp_quiz();
	$delete->setId($id);
	$delete = $delete->deleteAnswer();

	return json_encode($delete);
}

function insertAnswer() {
	global $cfg, $db;

	$insert = new bnp_quiz();
	$insert->setIdAss($_POST["id_ass"]);
	$insert->setStatus((int)$_POST["status"]);
	$insert->setTitle($_POST["title"]);
	$insert->setDate();
	$insert->setDateUpdate();
	$insert = $insert->insertAnswer();

	return json_encode($insert);
}

function updateAnswer() {
	global $cfg, $db;

	$update = new bnp_quiz();
	$update->setId($_POST["id"]);
	$update->setStatus((int)$_POST["status"]);
	$update->setTitle($_POST["title"]);
	$update->setDateUpdate();
	$update = $update->updateAnswer();

	return json_encode($update);
}

function getAnswers($id) {
	global $cfg, $db;

	$answers = new bnp_quiz();
	$answers->setIdAss($id);
	$answers = $answers->returnAnswers();

	return json_encode($answers);
}

switch ($_GET["r"]) {
	case 'getAnswers':
		$tpl = getAnswers($id);
		break;
	case 'deleteAnswer':
		$tpl = deleteAnswer($id);
		break;
	case 'insertAnswer':
		$tpl = insertAnswer($_POST);
		break;
	case 'updateAnswer':
		$tpl = updateAnswer($_POST);
		break;
	default:
		$tpl = '';
		break;
}
