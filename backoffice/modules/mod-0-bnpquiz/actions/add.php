<?php

if(!isset($_POST["save"])) {
	$mdl = bo3::c2r(
		[
			"title" => $mdl_lang["label"]["question"],
			"date" => $mdl_lang["label"]["date"],
			"date-placeholder" => $mdl_lang["label"]["date"],
			"date-value" => date("Y-m-d H:i:s", time()),
			"code" => $mdl_lang["label"]["code"],
			"code-placeholder" => $mdl_lang["label"]["code"],
			"but-submit" => $mdl_lang["label"]["but-submit"],
		],
		bo3::mdl_load("templates/add.tpl")
	);
} else {
	$insert = new bnp_quiz();
	$insert->setTitle($_POST["title"]);
	$insert->setCode($_POST["code"]);
	$insert->setStatus((isset($_POST["status"])) ? 1 : 0);
	$insert->setDate($_POST["date"]);
	$insert->setDateUpdate();
	$result_insert = $insert->insertQuestion();

	$mdl = bo3::c2r(
		[
			"content" => ($result_insert ? "Inserção realizada com sucesso!" : "Ocorreu um erro!")
		],
		bo3::mdl_load("templates/result.tpl")
	);
}


include "pages/module-core.php";
