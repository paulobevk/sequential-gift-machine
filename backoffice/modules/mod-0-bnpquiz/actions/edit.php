<?php

if(!isset($_POST["save"])) {
	$question = new bnp_quiz();
	$question->setId($id);
	$question = $question->returnOneQuestion();

	$mdl = bo3::c2r(
		[
			"title" => $mdl_lang["label"]["question"],
			"title-value" => $question->title,
			"date" => $mdl_lang["label"]["date"],
			"date-placeholder" => $mdl_lang["label"]["date"],
			"date-value" => $question->date,
			"code" => $mdl_lang["label"]["code"],
			"code-placeholder" => $mdl_lang["label"]["code"],
			"code-value" => $question->code,
			"checked" => ($question->status) ? "checked" : "",
			"but-submit" => $mdl_lang["label"]["but-submit"],
			"item" => bo3::mdl_load("templates-e/answers/item.tpl"),
			"id" => $id
		],
		bo3::mdl_load("templates/edit.tpl")
	);
} else {
	$question = new bnp_quiz();
	$question->setId($id);
	$question->setTitle($_POST["title"]);
	$question->setCode($_POST["code"]);
	$question->setDate($_POST["date"]);
	$question->setStatus((isset($_POST['status'])) ? 1 : 0);
	$result_question = $question->updateQuestion();

	$mdl = bo3::c2r(
		[
			"content" => ($result_question ? "Actualização realizada com sucesso!" : "Ocorreu um erro!")
		],
		bo3::mdl_load("templates/result.tpl")
	);
}


include "pages/module-core.php";
