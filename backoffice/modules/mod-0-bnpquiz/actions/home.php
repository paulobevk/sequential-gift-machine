<?php

$item_tpl = bo3::mdl_load("templates-e/home/table-row.tpl");

$questions = new bnp_quiz();
$questions = $questions->returnAllQuestions();

$list = "";

foreach ($questions as $q => $question) {
	$list .= bo3::c2r([
		"id" => $question->id,
		"title" => $question->title,
		"date-update-label" => $mdl_lang["label"]["date-update"],
		"date-updated" => $question->date_update,
		"date-created" => $question->date,
		"but-view" => $mdl_lang["label"]["but-view"],
		"but-edit" => $mdl_lang["label"]["but-edit"],
		"but-delete" => $mdl_lang["label"]["but-delete"],
		"active" => ($question->status) ? "fa-toggle-on" : "fa-toggle-off"
	], $item_tpl);
}

$mdl = bo3::c2r(
	[
		"question" => $mdl_lang["label"]["question"],
		"status" => $mdl_lang["label"]["status"],
		"date" => $mdl_lang["label"]["date"],
		"table-body" => $list
	],
	bo3::mdl_load("templates/home.tpl")
);

include "pages/module-core.php";
