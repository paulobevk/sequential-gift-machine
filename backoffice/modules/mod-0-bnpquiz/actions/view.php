<?php

$item_tpl = bo3::mdl_load("templates-e/view/item.tpl");

$question = new bnp_quiz();
$question->setId($id);
$question = $question->returnOneQuestion();

$answers = new bnp_quiz();
$answers->setIdAss($id);
$answers = $answers->returnAnswers();

$list = "";

foreach ($answers as $i => $item) {
	$list .= bo3::c2r([
		"id" => $item->id,
		"title" => $item->title,
		"code" => ($item->status) ? "fa-check" : "fa-times",
		"date-update-label" => $mdl_lang["label"]["date-update"],
		"date-update" => $item->date_update,
		"date-created" => $item->date

	], $item_tpl);
}

$mdl = bo3::c2r(
	[
		"question-title" => $question->title,
		"date" => $question->date,
		"date-update" => $question->date_update,
		"status" => $question->status,
		"date-label" => $mdl_lang["label"]["date"],
		"date-update-label" => $mdl_lang["label"]["date-update"],
		"status-label" => $mdl_lang["label"]["status"],
		"answer-label" => $mdl_lang["label"]["answer"],
		"table-body" => $list
	],
	bo3::mdl_load("templates/view.tpl")
);

include "pages/module-core.php";
